﻿using EnvioNotificacion.Clases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnvioNotificacion
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string titulo = Titulo.Text;
            string mensaje = Mensaje.Text;
            string token = Token.Text;
            string responsable = Responsable.Text;

            EnviarNotificacion not = new EnviarNotificacion(token, responsable, titulo, mensaje);
        }
    }
}