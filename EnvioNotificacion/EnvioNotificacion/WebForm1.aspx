﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="EnvioNotificacion.WebForm1" %>

<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="Titulo" runat="server" Placeholder="Titulo"></asp:TextBox>
        <asp:TextBox ID="Mensaje" runat="server" Placeholder="Mensaje"></asp:TextBox>
        <asp:TextBox ID="Responsable" runat="server" Placeholder="Responsable"></asp:TextBox>
        <asp:TextBox ID="Token" runat="server" Text="cxEvCviPdSo:APA91bE1EoKmLyK-XtPw4RN_X2TDaXvPao2QYiAWvn_gwSKlsGg6MStvR9tx0b0B-SF-VZMoTmhoxvqwHsBYG3JQh1iqoYDSsPPJfUlUseQ80Ybtbt0F1JfYoSc78QC5AIyD0aFr-EHD"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Enviar Notificacion" OnClick="Button1_Click"/>
    </div>
    </form>
</body>
</html>
