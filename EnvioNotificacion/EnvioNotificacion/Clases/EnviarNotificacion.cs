﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace EnvioNotificacion.Clases
{
    public class EnviarNotificacion
    {
        public EnviarNotificacion(string token, string responsable, string titulo, string mensaje){
            if (token != null)
            {
                try
                {
                    var applicationID = "AIzaSyDEVnVEcHtmZ-NCkfYZPcX371_rlyztDcQ";
                    var senderId = "355803624584";

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                    tRequest.Method = "post";
                    tRequest.ContentType = "application/json";

                    var data = new
                    {
                        data = new 
                        {
                            title = titulo,
                            body = mensaje,
                            responsable = responsable,
                            estado = "0",
                            fecha=DateTime.Now.ToString("dd/MM/yyyy"),
                            hora=DateTime.Now.ToString("HH:mm:ss tt"),
                            fecha_exp="30/04/2018",
                            hora_exp="12:00"
                        },                                   
                        to = token
                    };

                    //var notificacion = new
                    //{
                    //    notification = new
                    //    {
                    //        click_action = ".Home",
                    //        body = mensaje,
                    //        title = titulo,
                    //        sound = "Enabled"
                    //    },
                    //    to = token
                    //};

                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data);
                    
                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;
                    
                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    string str = sResponseFromServer;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
            }
        }
    }
}