﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnvioNotificacion.Modelos
{
    public class Usuario
    {
        int id {get;set;}
        string usuario { get; set; }
        string password { get; set; }
        string nombres { get; set; }
        string rut { get; set; }
        string email { get; set; }
        string telefono { get; set; }
        string direccion { get; set; }
    }
}