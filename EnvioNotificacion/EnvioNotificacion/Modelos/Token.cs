﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnvioNotificacion.Modelos
{
    public class Token
    {
        string usuario { get; set; }
        string token { get; set; }
    }
}