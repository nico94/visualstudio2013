﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_SGA_Lite.ModelosResponse;

namespace WS_SGA_Lite.Servicios
{
    [ServiceContract]
    public interface ICoordenadas
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
           UriTemplate = "/coordenadas",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped)]
        CoordenadaResponse ObtenerCoordenadas(int id_usuario);  
    }
}
