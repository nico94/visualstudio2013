﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using WS_JesusPonse.Modelos;
using WS_JesusPonse.Servicios;

namespace WS_JesusPonse.Clases
{
    public class EnviarNotificacion
    {
        private Model.Token token;
        private Tareamodelo tarea;

        public EnviarNotificacion(string token, Tareamodelo tarea)
        {
            if (token != null)
            {
                try
                {
                    var applicationID = "AIzaSyDEVnVEcHtmZ-NCkfYZPcX371_rlyztDcQ";
                    //var senderId = "355803624584";

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                    tRequest.Method = "post";
                    tRequest.ContentType = "application/json";

                    var data = new
                    {
                        data = new
                        {
                            id_accion = tarea.id_accion,
                            id_cierre_negocio = tarea.id_cierre_negocio,
                            cod_ficha_personal = tarea.cod_ficha_personal,
                            responsable = tarea.responsable,
                            estado = tarea.estado,
                            detalle = tarea.detalle,
                            marca = tarea.marca,
                            modelo = tarea.modelo,
                            fecha_crea = tarea.fecha_crea,
                            hora_crea = DateTime.Now.ToString("HH:mm:ss"),
                            fecha_exp = tarea.fecha_exp,
                            //hora_exp = tarea.hora_exp,
                            hora_exp = DateTime.Now.ToString("HH:mm:ss"),
                            fecha_ini = tarea.fecha_ini,
                            hora_ini = tarea.hora_ini,
                            fecha_fin = tarea.fecha_fin,
                            hora_fin = tarea.hora_fin
                        },
                        to = token
                    };

                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                    //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;

                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    string str = sResponseFromServer;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
            }
        }

        public EnviarNotificacion(Model.Token token, Tareamodelo tarea)
        {
            // TODO: Complete member initialization
            this.token = token;
            this.tarea = tarea;
        }
    }
}