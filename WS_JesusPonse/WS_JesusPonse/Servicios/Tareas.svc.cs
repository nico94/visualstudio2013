﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WS_JesusPonse.Clases;
using WS_JesusPonse.Model;
using WS_JesusPonse.Modelos;
using WS_JesusPonse.ModelosResponse;

namespace WS_JesusPonse.Servicios
{
    public class Tareas : ITareas
    {
        /////////////////////////////////
        // METODO PARA CREAR UNA TAREA //
        public TareaResponse CrearTarea(Tareamodelo tarea)
        {
            if (tarea != null)
            {
                using(var db = new GestionComercialEntities())
                {
                    string user = tarea.cod_ficha_personal.ToString();
                    var queryTokens = db.Token.Where(t => t.des_usuario_token == user).ToList();
                    foreach (var token in queryTokens)
                    {
                        EnviarNotificacion not = new EnviarNotificacion(token.des_token, tarea);
                    }               
                    return new TareaResponse()
                    {
                        id_tarea = tarea.id_accion,
                        id_cierre_negocio = tarea.id_cierre_negocio,
                        cod_ficha_personal = tarea.cod_ficha_personal,
                        responsable = tarea.responsable,
                        estado = tarea.estado,
                        detalle = tarea.detalle,
                        marca = tarea.marca,
                        modelo = tarea.modelo,
                        fecha_crea = tarea.fecha_crea,
                        hora_crea = tarea.hora_crea,
                        fecha_exp = tarea.fecha_exp,
                        hora_exp = tarea.hora_exp,
                        fecha_ini = tarea.fecha_ini,
                        hora_ini = tarea.hora_ini,
                        fecha_fin = tarea.fecha_fin,
                        hora_fin = tarea.fecha_fin,
                        est = true,
                        mensaje = "Tarea creada con éxito"
                    };
                }
            }
            return new TareaResponse() { est = false , mensaje = "Sin contenido" };
        }

        ///////////////////////////////////////
        // LISTAR TAREAS QUE ESTAN ASIGNADAS //
        public Listatareas ListarTareas(int cod_ficha_personal)
        {
            if (cod_ficha_personal > 0)
            {
                using (var db = new GestionComercialEntities())
                {                   
                    var actividades = db.snap_ActividadesUsuario.Where(t=> t.cod_fichapersonal_fichapersonal == cod_ficha_personal);
                    var ficha = db.FichasPersonales.FirstOrDefault(f => f.cod_fichapersonal_fichapersonal == cod_ficha_personal);

                    Listatareas lista = new Listatareas();                   
                    foreach (var item in actividades)
                    {
                        var tareaModelo = new Tareamodelo();
                        var labor = db.Labor_ManoObra.FirstOrDefault(l => l.cod_labormanoobra_labormanoobra == item.cod_labormanoobra_labormanoobra);
                        var cierreNegocio = db.CierreNegocioVehiculo.FirstOrDefault(c=> c.id_cierrenegocio_cierrenegocio == item.id_cierrenegocio_cierrenegocio);
                        var marca = db.Marca.FirstOrDefault(m => m.cod_marca_marca == cierreNegocio.cod_marca_marca);
                        var modelo = db.Modelo.FirstOrDefault(m => m.cod_modelo_modelo == cierreNegocio.cod_modelo_modelo);

                        tareaModelo.marca = marca.des_marca_marca;
                        tareaModelo.modelo = modelo.des_modelo_modelo;
                        tareaModelo.vin = cierreNegocio.cod_vin_vinstock;

                        tareaModelo.id_accion = Convert.ToInt32(item.cod_labormanoobra_labormanoobra);
                        tareaModelo.id_cierre_negocio = Convert.ToInt32(item.id_cierrenegocio_cierrenegocio);
                        tareaModelo.cod_ficha_personal = Convert.ToInt32(item.cod_fichapersonal_fichapersonal);
                        tareaModelo.detalle = labor.des_descripcion_labormanoobra;
                        tareaModelo.responsable = ficha.des_nombre_fichapersonal+" "+ficha.des_appaterno_fichapersonal+" "+ficha.des_apmaterno_fichapersonal;
                        if (item.bit_activo_snapactusuario == null && item.bit_terminada_snapactusuario == null)
                        {
                            tareaModelo.estado = 1;
                            tareaModelo.fecha_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.fecha_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("HH:mm:ss");

                        }
                        if (item.bit_activo_snapactusuario == true && item.bit_terminada_snapactusuario == false)
                        {
                            tareaModelo.fecha_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.fecha_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.estado = 2;

                        }
                        if (item.bit_activo_snapactusuario == false && item.bit_terminada_snapactusuario == true)
                        {
                            tareaModelo.fecha_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_crea = Convert.ToDateTime(item.dat_finicio_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.fecha_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_exp = Convert.ToDateTime(item.dat_fterm_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.fecha_fin = Convert.ToDateTime(item.dat_ftermAct_snapActUsuario).ToString("dd/MM/yyyy");
                            tareaModelo.hora_fin = Convert.ToDateTime(item.dat_ftermAct_snapActUsuario).ToString("HH:mm:ss");
                            tareaModelo.estado = 3;
                        }
                      
                        lista.ListadoTareas.Add(tareaModelo);
                    }
                    lista.mensaje = "Ok";
                    lista.estado = true;
                    return lista;
                }
            }
            return new Listatareas();
        }


        //////////////////////////////////
        // METODO PARA ESTADOS DE TAREA //
        public TareaResponse ModificarTarea(Tareamodelo tarea)
        {
            if (tarea != null)
            {
                int id_accion = Convert.ToInt32(tarea.id_accion);
                int cod_cierre = Convert.ToInt32(tarea.id_cierre_negocio);
                int cod_ficha_personal = Convert.ToInt32(tarea.cod_ficha_personal);

                if (id_accion > 0 && cod_cierre > 0 && cod_ficha_personal > 0)
                {
                    using (var db = new GestionComercialEntities())
                    {
                        //Esto deberia actualizar las tareas
                        //if (tarea.estado == 2 || tarea.estado == 3)
                        //{
                        //    string user = cod_ficha_personal.ToString();
                        //    var queryTokens = db.Token.Where(t => t.des_usuario_token == user).ToList();
                        //    foreach (var token in queryTokens)
                        //    {
                        //        EnviarNotificacion not = new EnviarNotificacion(token.des_token, tarea);
                        //    }         
                        //}
                        // fin

                        try
                        {
                            var tarea_upd = db.snap_ActividadesUsuario.FirstOrDefault(
                                            tar => tar.cod_labormanoobra_labormanoobra == id_accion &&
                                            tar.cod_fichapersonal_fichapersonal == cod_ficha_personal &&
                                            tar.id_cierrenegocio_cierrenegocio == cod_cierre);
                            int estado = Convert.ToInt32(tarea.estado); 
                           
                            //var fecha_fin = DateTime.ParseExact(tarea.fecha_fin + " " + tarea.hora_fin, "dd/MM/yyyy hh:mm:ss", null);                          
                          
                            if (estado == 2) // CAMBIA A INICIADA
                            {
                                tarea_upd.bit_activo_snapactusuario = true;
                                tarea_upd.bit_terminada_snapactusuario = false;
                            }
                            else if (estado == 3) // CAMBIA A TERMINADA
                            {
                                tarea_upd.dat_ftermAct_snapActUsuario = DateTime.Now;                                   
                                tarea_upd.bit_activo_snapactusuario = false;
                                tarea_upd.bit_terminada_snapactusuario = true;
                            }
                            else
                            {
                                tarea_upd.bit_activo_snapactusuario = null;
                                tarea_upd.bit_terminada_snapactusuario = null;
                            }
                            db.SaveChanges();
                            return new TareaResponse()
                            {
                                id_tarea = tarea.id_accion,
                                id_cierre_negocio = tarea.id_cierre_negocio,
                                cod_ficha_personal = tarea.cod_ficha_personal,
                                estado = tarea.estado,
                                detalle = tarea.detalle,
                                marca = tarea.marca,
                                modelo = tarea.modelo,
                                fecha_crea = tarea.fecha_crea,
                                hora_crea = tarea.hora_crea,
                                fecha_exp = tarea.fecha_exp,
                                hora_exp = tarea.hora_exp,
                                fecha_ini = tarea.fecha_ini,
                                hora_ini = tarea.hora_ini,
                                fecha_fin = tarea.fecha_fin,
                                hora_fin = tarea.hora_fin,
                                mensaje = "Tarea actualizada",
                                est = true
                            };
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            return new TareaResponse() { est = false, mensaje = "ERROR: No se pudo acualizar la tarea" };
                        }
                    }
                }
            }
            return new TareaResponse() { est = false, mensaje = "No se pudo acualizar la tarea" };
        }
    }
}