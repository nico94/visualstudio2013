﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WS_JesusPonse.Model;
using WS_JesusPonse.Modelos;
using WS_JesusPonse.ModelosResponse;

namespace WS_JesusPonse.Servicios
{
    public class Tokens : ITokens
    {
        public TokenResponse AgregaToken(Tokenmodelo token)
        {
            using (var db = new GestionComercialEntities())
            {
                if (token != null)
                {
                    Token tok = new Model.Token();
                    tok.des_dispositivo = token.des_dispositivo;
                    tok.des_usuario_token = token.des_usuario_token;
                    tok.des_token = token.des_token;

                    if (tok != null)
                    {
                        var token_registrado = db.Token.FirstOrDefault(tk => tk.des_token == tok.des_token);
                        if (token_registrado == null)
                        {
                            try
                            {    
                                db.Token.Add(tok);                          
                                db.SaveChanges();
                                return new TokenResponse() {
                                    id_usuario = tok.des_usuario_token,
                                    dispositivo = tok.des_dispositivo,
                                    mensaje = "AGEREGADO",
                                    estado = true 
                                };
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                return new TokenResponse() { mensaje = "NO AGREGADO", estado = false };
                            }
                        }
                        else
                        {
                            try
                            {
                                var tok_temp = db.Token.FirstOrDefault(tk => tk.des_token == token.des_token);
                                tok_temp.des_dispositivo = token.des_dispositivo;
                                tok_temp.des_usuario_token = token.des_usuario_token;
                                tok_temp.des_token = token.des_token;
                                db.SaveChanges();
                                return new TokenResponse()
                                {
                                    id_usuario = tok.des_usuario_token,
                                    dispositivo = tok.des_dispositivo,
                                    mensaje = "MODIFICADO",
                                    estado = true
                                };
                            }
                            catch(Exception e)
                            {
                                Console.WriteLine(e);
                                return new TokenResponse() {mensaje = "NO MODIFICADO", estado = false };
                            }
                        }
                    }
                }
            }
            return new TokenResponse() { mensaje = "VACIO", estado = false };
        }
    }
}