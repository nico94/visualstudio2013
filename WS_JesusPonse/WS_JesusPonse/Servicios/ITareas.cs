﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_JesusPonse.Modelos;
using WS_JesusPonse.ModelosResponse;

namespace WS_JesusPonse.Servicios
{
    [ServiceContract]
    public interface ITareas
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/listar_tareas",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        Listatareas ListarTareas(int cod_ficha_personal);
         
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/add_tarea",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        TareaResponse CrearTarea(Tareamodelo tarea);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/modificar_tarea",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        TareaResponse ModificarTarea(Tareamodelo tarea);

    }
}
