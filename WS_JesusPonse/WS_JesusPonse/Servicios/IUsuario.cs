﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_JesusPonse.Modelos;

namespace WS_JesusPonse.Servicios
{
    [ServiceContract]
    public interface IUsuario
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/login",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioResponse InciarSesion(Usuariomodelo usuario);
        
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/get_fichas",
            ResponseFormat = WebMessageFormat.Json)]
        FichasResponse ObtenerFichas();
    }
}
