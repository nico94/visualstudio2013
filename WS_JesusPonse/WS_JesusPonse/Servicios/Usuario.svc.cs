﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WS_JesusPonse.Model;
using WS_JesusPonse.Modelos;

namespace WS_JesusPonse.Servicios
{
    public class Usuario : IUsuario
    {
        /////////////////////
        // METODO DE LOGIN //
        public UsuarioResponse InciarSesion(Usuariomodelo usu)
        {
            if (usu != null)
            {
                using (var db = new GestionComercialEntities())
                {
                    var fichaquery = db.FichasPersonales.Include("Cargos").FirstOrDefault(f => f.des_usuario == usu.IdUsuario && f.des_paswor_usuario == usu.PswUsuario);
                    if (fichaquery != null)
                    {
                        return new UsuarioResponse()
                        {
                            codigo = Convert.ToInt32(fichaquery.cod_fichapersonal_fichapersonal),
                            cod_cargo = Convert.ToInt32(fichaquery.cod_cargo_cargo),
                            des_cargo = fichaquery.Cargos.des_descripcion_cargo,
                            rut = fichaquery.des_rut_fichapersonal,
                            nombres = fichaquery.des_nombre_fichapersonal,
                            ap_paterno = fichaquery.des_appaterno_fichapersonal,
                            ap_materno = fichaquery.des_apmaterno_fichapersonal,
                            fecha_nacimiento = Convert.ToString(fichaquery.dat_fechanac_fichapersonal),
                            email = fichaquery.des_mail_fichapersonal,
                            telefono = fichaquery.des_telefono_fichapersonal,
                            direccion = fichaquery.des_direccion_fichapersonal,
                            cod_comuna = Convert.ToInt32(fichaquery.cod_comuna_comuna),
                            estado = true
                        };
                    }
                }
            }
            return new UsuarioResponse() { estado = false };
        }
          
        /////////////////////////////////
        // METODO DE FICHAS PERSONALES //
        public FichasResponse ObtenerFichas()
        {
            using (var db = new GestionComercialEntities())
            {
                List<Ficha> listaFichas = new List<Ficha>();

                var fichasQuery = db.FichasPersonales.ToList();
                foreach (var item in fichasQuery)
                {
                    listaFichas.Add(new Ficha()
                    {
                        codUsuario = Convert.ToString(item.cod_fichapersonal_fichapersonal),
                        RutUsuario = item.des_rut_fichapersonal,
                        NombreUsuario = item.des_nombre_fichapersonal,
                        ApePUsuario = item.des_appaterno_fichapersonal,
                        ApeMUsuario = item.des_apmaterno_fichapersonal
                    });
                }
                return new FichasResponse() { listaFicha = listaFichas };
            }
        }

        // METODO
    }
}
