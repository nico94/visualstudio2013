﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_JesusPonse.Modelos;
using WS_JesusPonse.ModelosResponse;

namespace WS_JesusPonse.Servicios
{
    [ServiceContract]
    public interface ITokens
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/reg_token",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        TokenResponse AgregaToken(Tokenmodelo token);  
    }
}
