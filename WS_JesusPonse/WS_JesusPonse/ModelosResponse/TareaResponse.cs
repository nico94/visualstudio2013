﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_JesusPonse.ModelosResponse
{
    public class TareaResponse
    {
        public int id_tarea { get; set; }
        public int id_cierre_negocio { get; set; }
        public int cod_ficha_personal { get; set; }
        public string responsable { get; set; }
        public int estado { get; set; }
        public string detalle { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string vin { get; set; }
        public string fecha_crea { get; set; }
        public string hora_crea { get; set; }
        public string fecha_exp { get; set; }
        public string hora_exp { get; set; }
        public string fecha_ini { get; set; }
        public string hora_ini { get; set; }
        public string fecha_fin { get; set; }
        public string hora_fin { get; set; }
        public bool est { get; set; }
        public string mensaje { get; set; }
    }
}