﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WS_JesusPonse.Model;
using WS_JesusPonse.Modelos;

namespace WS_JesusPonse.ModelosResponse
{
    public class TokenResponse
    {
        public int id { get; set; }
        public string id_usuario { get; set; }
        public string dispositivo { get; set; }
        public string token { get; set; }
        public string mensaje { get; set; }
        public bool estado { get; set; }
    }
}