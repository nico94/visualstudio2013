﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_JesusPonse.Modelos
{
    public class UsuarioResponse
    {
        public int codigo { get; set; }
        public int cod_cargo { get; set; }
        public string des_cargo { get; set; }
        public string rut { get; set; }
        public string nombres { get; set; }
        public string ap_paterno { get; set; }
        public string ap_materno { get; set; }
        public string fecha_nacimiento { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public int cod_comuna { get; set; }

        public bool estado { get; set; }
        public string mensaje { get; set; }
    }
}