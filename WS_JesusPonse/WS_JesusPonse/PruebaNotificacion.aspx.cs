﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WS_JesusPonse.Clases;
using WS_JesusPonse.Model;
using WS_JesusPonse.Modelos;

namespace WS_JesusPonse
{
    public partial class PruebaNotificacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Enviar_Click(object sender, EventArgs e)
        {
            string tit= Titulo.Text.ToString();
            string det= Detalle.Text.ToString();
            string usr = Repsonsable.Text.ToString();
            ConsumirWS(tit, det, usr);
        }

        private string ConsumirWS(string titulo, string detalle, string usuario)
        {
            if (titulo != null && detalle != null && usuario != null)
            {
                try
                {
                    string url = "http://186.10.19.170/wsJesusPons/Servicios/Tareas.svc/rest/add_tarea";
                    WebRequest tRequest = WebRequest.Create(url);
                    tRequest.Method = "post";
                    tRequest.ContentType = "application/json";

                    var data = new
                    {
                        tarea = new
                        {
                            id_tarea = 1,
                            estado = 1,
                            id_usuario = usuario,
                            titulo = titulo,
                            detalle = detalle,
                            fecha = DateTime.Now.ToString("dd/MM/yyyy"),
                            hora = DateTime.Now.ToString("HH:mm:ss tt"),
                            fecha_exp = "26/04/2018",
                            hora_exp = "18:09:00"
                        }
                    };

                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    tRequest.ContentLength = byteArray.Length;
                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();

                                    var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                                    var jsonObject = ser.DeserializeObject(tReader.ReadToEnd());

                                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                                    Tareamodelo objCustomer = jsonSerializer.Deserialize<Tareamodelo>(sResponseFromServer);

                                    return sResponseFromServer;
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
            }
            return null;
        }
    }
}