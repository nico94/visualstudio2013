﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_JesusPonse.Modelos
{
    public class Ficha
    {
        public string codUsuario { get; set; }
        public string RutUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApePUsuario { get; set; }
        public string ApeMUsuario { get; set; }
    }
}