﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WS_JesusPonse.Modelos
{
    [DataContract]
    public class Tokenmodelo
    {
        [DataMember]
        public string id_token{ get; set; }
        [DataMember]
        public string des_usuario_token { get; set; }
        [DataMember]
        public string des_dispositivo { get; set; }
        [DataMember]
        public string des_token { get; set; }
    }
}