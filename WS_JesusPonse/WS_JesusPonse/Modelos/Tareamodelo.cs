﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WS_JesusPonse.Modelos
{
    [DataContract]
    public class Tareamodelo
    {
        [DataMember]
        public int id_accion { get; set; }
        [DataMember]
        public int id_cierre_negocio { get; set; }
        [DataMember]
        public int cod_ficha_personal { get; set; }
        [DataMember]
        public string responsable { get; set; }
        [DataMember]
        public int estado { get; set; }
        [DataMember]
        public string detalle { get; set; }
        [DataMember]
        public string marca { get; set; }
        [DataMember]
        public string modelo { get; set; }
        [DataMember]
        public string vin { get; set; }
        [DataMember]
        public string fecha_crea { get; set; }
        [DataMember]
        public string hora_crea { get; set; }
        [DataMember]
        public string fecha_exp { get; set; }
        [DataMember]
        public string hora_exp { get; set; }
        [DataMember]
        public string fecha_ini { get; set; }
        [DataMember]
        public string hora_ini { get; set; }
        [DataMember]
        public string fecha_fin { get; set; }
        [DataMember]
        public string hora_fin { get; set; }
    }
}