﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WS_JesusPonse.Modelos
{
    [DataContract]
    public class Usuariomodelo
    {
        [DataMember]
        public string IdUsuario{ get; set; }
        [DataMember]
        public string PswUsuario{ get; set; }
    }
}