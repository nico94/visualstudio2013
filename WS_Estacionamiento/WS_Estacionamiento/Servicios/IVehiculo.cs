﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_Estacionamiento.Modelos;

namespace WS_Estacionamiento.Servicios
{
    [ServiceContract]
    public interface IVehiculo
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
           UriTemplate = "/obtener_por_codigo",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped)]
        Transaccion ObtenerPorCodigo(string cod);

        [OperationContract]
        [WebInvoke(Method = "POST",
           UriTemplate = "/listar_registros",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped)]
        ListaTransacciones ListadoRegistros();
        
        
        [OperationContract]
        [WebInvoke(Method = "POST",
           UriTemplate = "/registro",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped)]
        TransaccionResponse IngresarRegistro(Transaccion trans);

        [OperationContract]
        [WebInvoke(Method = "POST",
           UriTemplate = "/sincronizar",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped)]
        TransaccionResponse Sincronizar(ListaTransaccionesRequest lista);

    }
}
