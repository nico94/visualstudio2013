﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WS_Estacionamiento.Model;
using WS_Estacionamiento.Modelos;

namespace WS_Estacionamiento.Servicios
{
    public class Tarifa : ITarifa
    {
        //////////////////////////////
        // OBTENER LISTA DE TARIFAS //
        /// //////////////////////////
        public ListaTarifas ListarTarifas()
        {
            ListaTarifas lista = new ListaTarifas();
            using (var db = new GestionComercialEntities())
            {
                var tarifas = db.Estacionamiento_tarifa.ToList();
                foreach (var item in tarifas)
                {
                    var tar = new TarifaModelo();
                    tar.id_tarifa = Convert.ToInt32(item.id_tarifa);
                    tar.descripcion = item.descripcion;
                    tar.precio = item.precio;
                    tar.estado = item.estado;
                    lista.ListadoTarifas.Add(tar);
                }
                lista.mensaje = "OK";
                lista.estado_res = true;

            }
            return lista;
        }

        ///////////////////////////
        // CALCULAR TOTAL TARIFA //
        ///////////////////////////
        public CargoResponse CalcularTarifa(Transaccion trans)
        {
            string tiempoTranscurrido = "";
            if (trans.id != null && trans.fechaIngreso != null && trans.horaIngreso != null && trans.horaIngreso != null && trans.estado == 0)
            {
                using (var db = new GestionComercialEntities())
                {
                    //var registro = db.Estacionamiento_reg.FirstOrDefault(r=> r.id== trans.id && r.patente == trans.patente);
                    //if(registro != null )
                    //{
                    DateTime f_actual = DateTime.Now;
                    //DateTime f_ingreso = DateTime.Parse(trans.fechaIngreso + " " + trans.horaIngreso); //ANTES
                    //DateTime f_ingreso = DateTime.Parse(trans.fechaIngreso + " " + trans.horaIngreso).ToString("dd/MM/yyyy");

                    DateTime f_ingreso;
                    var FechInsp = trans.fechaIngreso.Replace("-", "/") + ' ' + trans.horaIngreso;
                    //string result = FechInsp.ToString("dd/mm/yyyy", CultureInfo.InvariantCulture);


                    f_ingreso = Convert.ToDateTime(FechInsp);

                    TimeSpan diferencia = f_actual.Subtract(f_ingreso);
                    var dias = diferencia.Days;
                    var horas = diferencia.Hours;
                    var minutos = diferencia.Minutes;
                    var segundos = diferencia.Seconds;
                    var totalMinutos = diferencia.TotalMinutes;
                    var totalHoras = diferencia.TotalHours;
                    if (diferencia.Days > 0)
                    {
                        tiempoTranscurrido = Convert.ToString(dias) + " días, ";
                    }
                    if (diferencia.Hours > 0)
                    {
                        tiempoTranscurrido = tiempoTranscurrido + Convert.ToString(horas) + " horas, ";
                    }
                    if (diferencia.Minutes > 0)
                    {
                        tiempoTranscurrido = tiempoTranscurrido + Convert.ToString(minutos) + " minutos, ";
                    }
                    if (diferencia.Seconds > 0)
                    {
                        tiempoTranscurrido = tiempoTranscurrido + Convert.ToString(segundos) + " segundos.";
                    }

                    var tarifas = db.Estacionamiento_tarifa.ToList();
                    var precioAcumulado = 0.0;
                    // Algoritmo
                    var minReales = totalMinutos;
                    var _minReales = totalMinutos;
                    var tarifa = 0.0;
                    var tramo = 0;
                    var tolerancia = 0;
                    foreach (var item in tarifas)
                    {
                        tarifa = item.precio;
                        tramo = item.minutos;
                        tolerancia = item.tolerancia;
                        precioAcumulado += tarifador(_minReales, tramo, tarifa, tolerancia);
                        _minReales -= item.minutos;
                    }
                    if (_minReales > 0)
                    {
                        while (_minReales > 0)
                        {
                            precioAcumulado += tarifador(_minReales, tramo, tarifa, tolerancia);
                            _minReales -= tramo;
                        }
                    }
                    return new CargoResponse()
                    {
                        id_transaccion = trans.id,
                        estado = true,
                        fecha_entrada = Convert.ToDateTime(f_ingreso).ToString("dd/MM/yyyy"),
                        hora_entrada = Convert.ToDateTime(f_ingreso).ToString("HH:mm:ss"),
                        fecha_salida = Convert.ToDateTime(f_actual).ToString("dd/MM/yyyy"),
                        hora_salida = Convert.ToDateTime(f_actual).ToString("HH:mm:ss"),
                        tiempo_transcurrido = tiempoTranscurrido,
                        patente = trans.patente,
                        mensaje = "OK",
                        des_tarifa = "",
                        total_cobro = precioAcumulado,
                        imagen = trans.imagen
                    };
                    //}
                    //else
                    //{
                    //    return new CargoResponse()
                    //    {
                    //        estado = false,
                    //        mensaje = "No existe registro"
                    //    };
                    //}
                }
            }
            return new CargoResponse() { estado = false, mensaje = "Error" };
        }


        ////////////////////
        // AGREGAR TARIFA //
        ////////////////////
        public TarifaResponse AgregarTarifa(TarifaModelo tarif)
        {
            if (tarif != null)
            {
                using (var db = new GestionComercialEntities())
                {
                    var tar = db.Estacionamiento_tarifa.FirstOrDefault(t => t.id_tarifa == tarif.id_tarifa);
                    if (tar == null)
                    {
                        //Crea
                        var est_tarifa = new Estacionamiento_tarifa();
                        try
                        {
                            est_tarifa.id_tarifa = Convert.ToInt64(tarif.id_tarifa);
                            est_tarifa.estado = tarif.estado;
                            est_tarifa.descripcion = tarif.descripcion;
                            est_tarifa.minutos = tarif.minutos;
                            est_tarifa.tolerancia = tarif.tolerancia;
                            est_tarifa.precio = tarif.precio;
                            tar = est_tarifa;
                            db.Estacionamiento_tarifa.Add(tar);
                            db.SaveChanges();
                            return new TarifaResponse()
                            {
                                id_tarifa = Convert.ToInt32(tar.id_tarifa),
                                descripcion = tar.descripcion,
                                precio = Convert.ToDouble(tar.precio),
                                estado = tar.estado,
                                mensaje = "Creado",
                                estado_res = true
                            };
                        }
                        catch (Exception e)
                        {
                            return new TarifaResponse()
                            {
                                id_tarifa = Convert.ToInt32(tar.id_tarifa),
                                mensaje = "error",
                                estado_res = false
                            };
                        }
                    }
                    else
                    {
                        return new TarifaResponse()
                        {
                            id_tarifa = Convert.ToInt32(tar.id_tarifa),
                            mensaje = "Ya existe",
                            estado_res = true
                        };
                    }
                }
            }
            return new TarifaResponse() { estado_res = false, mensaje = "error" };
        }

        public double tarifador(double minutos, double tramo, double tarifa, int tolerancia)
        {
            var tiempo = 0.0;
            minutos -= tramo;
            if (minutos < 0)
            {
                tiempo = tramo + minutos;
                if (tiempo > tramo * tolerancia / 100)
                {
                    return tarifa;
                }
            }
            else
            {
                return tarifa;
            }
            return 0.0;
        }


        //////////////////////
        // MODIFICAR TARIFA //
        //////////////////////

        /////////////////////
        // ELIMINAR TARIFA //
        /////////////////////
    }
}
