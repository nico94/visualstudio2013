﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WS_Estacionamiento.Modelos;

namespace WS_Estacionamiento.Servicios
{
    [ServiceContract]
    public interface ITarifa
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/listar_tarifas",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        ListaTarifas ListarTarifas();

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/agregar_tarifa",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        TarifaResponse AgregarTarifa(TarifaModelo tarifa);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/calcular_tarifa",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        CargoResponse CalcularTarifa(Transaccion trans);
    }
}
