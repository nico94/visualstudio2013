﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WS_Estacionamiento.Model;
using WS_Estacionamiento.Modelos;

namespace WS_Estacionamiento.Servicios
{
    public class Vehiculo : IVehiculo
    {
        ////////////////////////
        // OBTENER POR CODIGO //
        ////////////////////////
        public Transaccion ObtenerPorCodigo(string cod)
        {
            if (cod.Length == 13)
            {
                var patente = cod.Substring(0, 6);
                var id = Convert.ToInt64(cod.Substring(6, 7).TrimStart(new Char[] { '0' }));

                using (var db = new GestionComercialEntities())
                {
                    var trans = db.Estacionamiento_reg.FirstOrDefault(e => e.id == id && e.patente == patente);
                    if (trans != null)
                    {
                        return new Transaccion()
                        {
                            id = trans.id.ToString(),
                            estado = trans.estado,
                            patente = trans.patente,
                            fechaIngreso = Convert.ToDateTime(trans.ingreso).ToString("dd/MM/yyyy"),
                            horaIngreso = Convert.ToDateTime(trans.ingreso).ToString("HH:mm:ss"),
                            fechaSalida = Convert.ToDateTime(trans.salida).ToString("dd/MM/yyyy"),
                            horaSalida = Convert.ToDateTime(trans.salida).ToString("HH:mm:ss"),
                            pago = Convert.ToInt32(trans.pago),
                            imagen = Convert.ToBase64String(trans.imagen)
                        };
                    }
                    else
                    {
                        return new Transaccion() { };
                    }
                }
            }
            return new Transaccion() { };
        }


        //////////////////////////////////////
        // METODO PARA LISTAR LOS REGISTROS //
        //////////////////////////////////////
        public ListaTransacciones ListadoRegistros()
        {
            ListaTransacciones lista = new ListaTransacciones();
            using (var db = new GestionComercialEntities())
            {
                var transacciones = db.Estacionamiento_reg.ToList();
                foreach (var item in transacciones)
                {
                    var transM = new Transaccion();
                    transM.id = item.id.ToString();
                    transM.estado = item.estado;
                    transM.patente = item.patente;
                    transM.fechaIngreso = Convert.ToDateTime(item.ingreso).ToString("dd/MM/yyyy");
                    transM.horaIngreso = Convert.ToDateTime(item.ingreso).ToString("HH:mm:ss");
                    transM.fechaSalida = Convert.ToDateTime(item.salida).ToString("dd/MM/yyyy");
                    transM.horaSalida = Convert.ToDateTime(item.salida).ToString("HH:mm:ss");
                    transM.pago = Convert.ToInt32(item.pago);
                    if (item.imagen!=null)
                    {
                        transM.imagen = Convert.ToBase64String(item.imagen);
                    }
                    else
                    {
                        transM.imagen = "";
                    }
                    lista.ListadoTransacciones.Add(transM);
                }
                lista.mensaje = "OK";
                lista.estado_res = true;
            }
            return lista;
        }

        ///////////////////////////////////
        // METODO PARA SINCRONIZAR LISTA //
        ///////////////////////////////////
        public TransaccionResponse Sincronizar(ListaTransaccionesRequest lista)
        {
            // ITERAR LA LISTA Y GUARDAR SI NO EXISTE 


            return new TransaccionResponse() { estado_res = false };
        }
        

        /////////////////////////////
        // METODO PARA TRANSACCION //
        public TransaccionResponse IngresarRegistro(Transaccion trans)
        {
            if (trans != null)
            {
                using(var db = new GestionComercialEntities())
                {
                    var tr = db.Estacionamiento_reg.FirstOrDefault(e =>e.id == Convert.ToInt32(trans.id) && e.patente == trans.patente);
                    if (tr == null)
                    {
                        var Est_Reg = new Estacionamiento_reg();
                        try
                        {
                            //Existe crear
                            Est_Reg.id = Convert.ToInt64(trans.id);
                            Est_Reg.estado = trans.estado;
                            Est_Reg.patente = trans.patente;
                            Est_Reg.ingreso = Convert.ToDateTime(trans.fechaIngreso + " " + trans.horaIngreso);
                            Est_Reg.salida = null;
                            Est_Reg.pago = Convert.ToDouble(trans.pago);
                            if (trans.imagen !=null)
                            {
                                byte[] imageBytes = Convert.FromBase64String(trans.imagen);
                                Est_Reg.imagen = imageBytes;
                            }
                            else
                            {
                                Est_Reg.imagen = null;
                            }
                            tr = Est_Reg;
                            db.Estacionamiento_reg.Add(tr);
                            db.SaveChanges();
                            return new TransaccionResponse()
                            {
                                id = tr.id.ToString(),
                                patente = tr.patente,
                                estado_res = true,
                                mensaje = "Creado"
                            };

                        }catch(Exception e){
                            return new TransaccionResponse()
                            {
                                id = tr.id.ToString(),
                                patente = tr.patente,
                                estado_res = false,
                                mensaje = "Error al guardar en BD"
                            };
                        }
                    }
                    else
                    {
                        //Modificar
                        tr.estado = trans.estado;
                        if (tr.estado == 1)
                        {
                            tr.salida = Convert.ToDateTime(trans.fechaSalida + " " + trans.horaSalida);
                        }
                        else
                        {
                            tr.salida = null;
                        }
                        if (trans.imagen != null)
                        {
                            byte[] imageBytes = Convert.FromBase64String(trans.imagen);
                            tr.imagen = imageBytes;
                        }
                        else
                        {
                            tr.imagen = null;
                        }
                       
                        tr.pago = Convert.ToDouble(trans.pago);
                        db.SaveChanges();
                        return new TransaccionResponse()
                        {
                            id = tr.id.ToString(),
                            patente = tr.patente,
                            estado_res = true,
                            mensaje = "Modificado"
                        };
                    }
                }
            }
            return new TransaccionResponse() { estado_res = false, mensaje = "error" };
        }
    }
}
