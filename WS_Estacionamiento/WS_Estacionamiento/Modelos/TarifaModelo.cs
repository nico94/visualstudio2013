﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    [DataContract]
    public class TarifaModelo
    {
        [DataMember]
        public int id_tarifa { get; set; }
        [DataMember]
        public int estado { get; set; }
        [DataMember]
        public string descripcion{ get; set; }
        [DataMember]
        public int minutos { get; set; }
        [DataMember]
        public int tolerancia { get; set; }
        [DataMember]
        public double precio { get; set; }
    }
}