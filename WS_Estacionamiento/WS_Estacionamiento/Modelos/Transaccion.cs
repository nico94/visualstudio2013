﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    [DataContract]
    public class Transaccion
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public int estado { get; set; }
        [DataMember]
        public string patente { get; set; }
        [DataMember]
        public string fechaIngreso { get; set; }
        [DataMember]
        public string horaIngreso { get; set; }
        [DataMember]
        public string fechaSalida { get; set; }
        [DataMember]
        public string horaSalida { get; set; }
        [DataMember]
        public decimal pago { get; set; }
        [DataMember]
        public string imagen { get; set; }
    }
}