﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class ListaTransacciones
    {
        public bool estado_res;
        public string mensaje;
        public List<Transaccion> ListadoTransacciones;
        public ListaTransacciones()
        {
            ListadoTransacciones = new List<Transaccion>();
        }
    }
}