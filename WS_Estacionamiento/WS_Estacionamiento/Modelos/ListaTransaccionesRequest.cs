﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class ListaTransaccionesRequest
    {
        public List<Transaccion> ListadoTransacciones;
        public ListaTransaccionesRequest()
        {
            ListadoTransacciones = new List<Transaccion>();
        }

    }
}