﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class TransaccionResponse
    {
        public string id{ get; set; }
        public int estado{ get; set; }
        public string patente{ get; set; }
        public string dat_ingreso { get; set; }
        public string dat_salida{ get; set; }
        public decimal pago { get; set; }
        public string imagen { get; set; }

        public bool estado_res { get; set; }
        public string mensaje { get; set; }
    }
}