﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class CargoResponse
    {
        public int id_tarifa { get; set; }
        public string id_transaccion { get; set; }
        public string patente { get; set; }
        public string fecha_entrada{ get; set; }
        public string hora_entrada{ get; set; }
        public string fecha_salida {get; set;}
        public string hora_salida {get; set;}
        public string tiempo_transcurrido { get; set; }
        public double valor_tarifa { get; set; }
        public string des_tarifa { get; set; }
        public double total_cobro { get; set; }
        public string imagen{ get; set; }
        //PARA LA RESPUESTA
        public bool estado { get; set; }
        public string mensaje { get; set; }
    }
}