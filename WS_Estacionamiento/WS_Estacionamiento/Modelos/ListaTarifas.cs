﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class ListaTarifas
    {
        public bool estado_res;
        public string mensaje;
        public List<TarifaModelo> ListadoTarifas;
        public ListaTarifas()
        {
            ListadoTarifas = new List<TarifaModelo>();
        }
    }
}