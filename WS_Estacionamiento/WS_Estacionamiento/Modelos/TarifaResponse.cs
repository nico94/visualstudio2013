﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_Estacionamiento.Modelos
{
    public class TarifaResponse
    {
        public int id_tarifa { get; set; }
        public int estado { get; set; }
        public string descripcion { get; set; }
        public int minutos { get; set; }
        public int tolerancia { get; set; }
        public double precio { get; set; }
        
        public bool estado_res { get; set; }
        public string mensaje { get; set; }
    }
}