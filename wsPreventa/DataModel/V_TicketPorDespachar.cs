//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_TicketPorDespachar
    {
        public long cod_ordencompracliente_ordencompracliente { get; set; }
        public string Vendedor { get; set; }
        public bool bit_despachada_documentoventa { get; set; }
        public Nullable<System.DateTime> dat_fecha_occliente { get; set; }
        public Nullable<System.DateTime> dat_fecha_Despacho { get; set; }
    }
}
