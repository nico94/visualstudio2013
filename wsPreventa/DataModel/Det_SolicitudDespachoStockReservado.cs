//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Det_SolicitudDespachoStockReservado
    {
        public long num_solicituddespacho_id { get; set; }
        public string cod_empresa_empresa { get; set; }
        public string cod_producto_materiaprima { get; set; }
        public long cod_bodega_bodegaAbastecimiento { get; set; }
        public decimal id_empresa_sucursal { get; set; }
        public string num_lote_controlingresoinsumo { get; set; }
        public decimal id_empresa_sucursal_adespachar { get; set; }
        public Nullable<double> num_cantidad_ControlIngresoInsumoReservada { get; set; }
        public int num_posicion_x { get; set; }
        public int num_posicion_y { get; set; }
        public int num_posicion_z { get; set; }
        public string TipoReserva { get; set; }
    
        public virtual ControlIngresoInsumo ControlIngresoInsumo { get; set; }
        public virtual ControlIngresoInsumoProductoTerminado ControlIngresoInsumoProductoTerminado { get; set; }
    }
}
