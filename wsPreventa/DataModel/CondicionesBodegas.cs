//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class CondicionesBodegas
    {
        public CondicionesBodegas()
        {
            this.ControlIngresoInsumo = new HashSet<ControlIngresoInsumo>();
        }
    
        public long cod_condicionbodega_condicionbodega { get; set; }
        public string des_descripcion_condicionbodega { get; set; }
    
        public virtual ICollection<ControlIngresoInsumo> ControlIngresoInsumo { get; set; }
    }
}
