//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Documentos_CompraGenerales
    {
        public Documentos_CompraGenerales()
        {
            this.Detalle_DocumentoCompraGeneral = new HashSet<Detalle_DocumentoCompraGeneral>();
            this.snap_DocCompraControlIngreso = new HashSet<snap_DocCompraControlIngreso>();
            this.snap_ControlIngresoInsumo = new HashSet<snap_ControlIngresoInsumo>();
        }
    
        public string cod_documentocomprageneral_documentocomprageneral { get; set; }
        public Nullable<bool> bit_estaloteado_documentocomprageneral { get; set; }
        public string des_patente_documentocomprageneral { get; set; }
        public string cod_proveedor_proveedor { get; set; }
        public long cod_tipodocumento_tipodocumentoventa { get; set; }
        public string cod_empresa_empresa { get; set; }
        public long cod_condicionpago_condicionpago { get; set; }
        public Nullable<long> cod_bodega_bodega { get; set; }
        public System.DateTime dat_fechafactura_documentocomprageneral { get; set; }
        public System.DateTime dat_fechacontable_documentocomprageneral { get; set; }
        public string des_glosa_documentocomprageneral { get; set; }
        public Nullable<double> num_neto_documentocomprageneral { get; set; }
        public Nullable<double> num_ivapagado_documentocomprageneral { get; set; }
        public Nullable<double> num_exento_documentocomprageneral { get; set; }
        public Nullable<System.DateTime> dat_fechaproximopago_documentocomprageneral { get; set; }
        public Nullable<double> num_impuestoespecifico_documentocomprageneral { get; set; }
        public Nullable<double> num_descuento_documentocomprageneral { get; set; }
        public Nullable<double> num_porciva_documentocomprageneral { get; set; }
        public Nullable<bool> bit_exentoiva_documentocomprageneral { get; set; }
        public Nullable<bool> bit_conguias_documentocomprageneral { get; set; }
        public Nullable<double> num_porotroimpuesto_documentogeneral { get; set; }
        public Nullable<double> num_otroimpuesto_documentogeneral { get; set; }
        public Nullable<long> cod_sucursal_sucursalproveedor { get; set; }
        public Nullable<long> cod_ordencompra_ordencompra { get; set; }
        public Nullable<long> cod_temporada_temporada { get; set; }
        public Nullable<long> cod_condiciontransporte_condiciontransporte { get; set; }
        public Nullable<decimal> id_empresa_sucursal { get; set; }
        public Nullable<long> cod_estado_estadodocvta { get; set; }
        public Nullable<double> Dolar { get; set; }
    
        public virtual Bodegas Bodegas { get; set; }
        public virtual Empresas_Holding Empresas_Holding { get; set; }
        public virtual Sucursal_empresa Sucursal_empresa { get; set; }
        public virtual Temporadas Temporadas { get; set; }
        public virtual Proveedores Proveedores { get; set; }
        public virtual ICollection<Detalle_DocumentoCompraGeneral> Detalle_DocumentoCompraGeneral { get; set; }
        public virtual ICollection<snap_DocCompraControlIngreso> snap_DocCompraControlIngreso { get; set; }
        public virtual ICollection<snap_ControlIngresoInsumo> snap_ControlIngresoInsumo { get; set; }
    }
}
