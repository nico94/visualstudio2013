//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Productos_Terminados
    {
        public Productos_Terminados()
        {
            this.Detalle_DocumentosVentas = new HashSet<Detalle_DocumentosVentas>();
            this.Detalle_OrdenCompraClientes = new HashSet<Detalle_OrdenCompraClientes>();
            this.Local_Lista_precio_vta = new HashSet<Local_Lista_precio_vta>();
            this.Local_Lista_precio_vta1 = new HashSet<Local_Lista_precio_vta>();
            this.Productos_Terminados1 = new HashSet<Productos_Terminados>();
            this.Oferta_ProductoTerminado = new HashSet<Oferta_ProductoTerminado>();
            this.Oferta_ProductoTerminado1 = new HashSet<Oferta_ProductoTerminado>();
            this.ControlIngresoInsumoProductoTerminado = new HashSet<ControlIngresoInsumoProductoTerminado>();
        }
    
        public string cod_productoterminado_productoterminado { get; set; }
        public string des_descripcion_productoterminado { get; set; }
        public long cod_subfamiliaprodterminado_subfamiliaproductoterminado { get; set; }
        public string cod_prodasociado_productoterminado { get; set; }
        public long cod_familiaproductosterminados_familiaproductosterminados { get; set; }
        public long cod_pesoproducto_pesoproducto { get; set; }
        public long cod_corteproducto_corteproducto { get; set; }
        public Nullable<double> num_stockminimo_productoterminado { get; set; }
        public Nullable<double> num_stockmaximo_productoterminado { get; set; }
        public Nullable<double> num_stockcritico_productoterminado { get; set; }
        public Nullable<double> num_stock_productoterminado { get; set; }
        public Nullable<double> num_valorcosto_productoterminado { get; set; }
        public Nullable<double> num_valorventa_productoterminado { get; set; }
        public Nullable<long> cod_marca_marca { get; set; }
        public Nullable<bool> bit_activo_productoterminado { get; set; }
        public Nullable<System.DateTime> dat_fechareceta_productoterminado { get; set; }
        public Nullable<bool> bit_materiaprima_productoterminado { get; set; }
        public Nullable<bool> bit_favorito_productoterminado { get; set; }
        public Nullable<bool> bit_muestra_productoterminado { get; set; }
        public Nullable<short> orden { get; set; }
        public bool bit_ticket { get; set; }
        public string cod_productoterminado_productoterminado_2 { get; set; }
        public string cod_productoterminado_productoterminado_3 { get; set; }
        public Nullable<long> cod_modelo_modelo { get; set; }
        public Nullable<long> cod_talla_talla { get; set; }
        public Nullable<long> cod_color_color { get; set; }
        public Nullable<bool> bit_fechaven { get; set; }
        public Nullable<double> num_comision_productoterminado { get; set; }
        public Nullable<bool> bit_venta_web { get; set; }
        public Nullable<bool> bit_controlstock_productoterminado { get; set; }
    
        public virtual Cortes_Productos Cortes_Productos { get; set; }
        public virtual ICollection<Detalle_DocumentosVentas> Detalle_DocumentosVentas { get; set; }
        public virtual ICollection<Detalle_OrdenCompraClientes> Detalle_OrdenCompraClientes { get; set; }
        public virtual Familia_Productos_Terminados Familia_Productos_Terminados { get; set; }
        public virtual ICollection<Local_Lista_precio_vta> Local_Lista_precio_vta { get; set; }
        public virtual ICollection<Local_Lista_precio_vta> Local_Lista_precio_vta1 { get; set; }
        public virtual Marca Marca { get; set; }
        public virtual Modelo Modelo { get; set; }
        public virtual Pesos_Productos Pesos_Productos { get; set; }
        public virtual Talla Talla { get; set; }
        public virtual ICollection<Productos_Terminados> Productos_Terminados1 { get; set; }
        public virtual Productos_Terminados Productos_Terminados2 { get; set; }
        public virtual Sub_Familia_Productos_Terminados Sub_Familia_Productos_Terminados { get; set; }
        public virtual Color_Producto Color_Producto { get; set; }
        public virtual ICollection<Oferta_ProductoTerminado> Oferta_ProductoTerminado { get; set; }
        public virtual ICollection<Oferta_ProductoTerminado> Oferta_ProductoTerminado1 { get; set; }
        public virtual ICollection<ControlIngresoInsumoProductoTerminado> ControlIngresoInsumoProductoTerminado { get; set; }
    }
}
