﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Caja;
using System.Runtime.Serialization;
using Model.Request;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface ICajaService
    {

        [OperationContract]
        ResultadoCaja GetEstadoCaja(GetEstadoCajaRequest getEstadoCajaRequest);
    }

    public class GetEstadoCajaRequest :BaseRequest
    {
        
    }
}
