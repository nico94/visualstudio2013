﻿using Model.Etiqueta_ZPL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IImpresionService
    {
        [OperationContract]
        ResultadoImpresion ImprimirZPL(ImprimirZPLRequest imprimirzplrequest);
    }

    public class ImprimirZPLRequest
    {
        public string zpl;
        public string ip;
        public int puerto;
    }

    public class ResultadoImpresion
    {
        public bool realizado;
        public string mensaje;
    }
}
