﻿using Model.Cotizacion;
using Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
     [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface ICotizacionService
    {
         [OperationContract]
         ResultadoCotizacion AddCotizacion(AddCotizacionRequest addcotizacionrequest);

         [OperationContract]
         ResultadoCotizacion GetCotizacion(GetCotizacionRequest getcotizacionrequest);
    }

    public class AddCotizacionRequest:BaseRequest
    {
        public Cotizacion cotizacion;
    }

    public class GetCotizacionRequest: BaseRequest
    {
        public long cod_cotizacion;
    }

    public class ResultadoCotizacion
    {
        public bool valido;
        public Cotizacion cotizacion;
        public string mensaje;
    }
}
