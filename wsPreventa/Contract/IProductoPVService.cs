﻿using Model.Etiqueta_ZPL;
using Model.ProductoPV;
using Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IProductoPVService
    {

         [OperationContract]
         ResultadoProductoPV GetProductoPorCodigo(GetProductoPorCodigoRequest getproductoporcodigorequest);

         [OperationContract]
         ResultadoProductos GetProductos(GetProductosRequest getproductosrequest);

         [OperationContract]
         ResultadoStock GetStockBodega(GetStockBodegaRequest getstockbodegarequest);

         [OperationContract]
         ResultadoStockCritico GetStockCritico(GetStockCriticoRequest getstockcriticorequest);

         [OperationContract]
         ResultadoEtiqueta GetEtiqueta(GetEtiquetaRequest getetiquetarequest);

         [OperationContract]
         ResultadoCodigos GetCodigos(GetCodigosRequest getcodigosrequest);

         [OperationContract]
         ResultadoImpresora GetImpresora(GetImpresoraRequest getimpresorarequest);

         [OperationContract]
         ResultadoOferta GetOfertasProducto(GetOfertasRequest getofertasrequest);

         [OperationContract]
         ResultadoProductos GetOfertas(GetOfertasLocalRequest getofertaslocalrequest);

         [OperationContract]
         ResultadoProductos GetStockProductosPorbodega(GetStockProductoPorBodegaRequest getstockproductoporbodega);

         [OperationContract]
         ResultadoStockCritico GetStockCriticoPorBodega(GetStockCriticoPorBodegaRequest getstockcriticoporbodega);

         //[OperationContract]
         //ResultadoOferta GetOfertaSeleccionada(GetOfertasRequest getofertasrequest);

    }

    public class GetProductoPorCodigoRequest : BaseRequest
    {
        public string codigo;
    }

    public class GetProductosRequest:BaseRequest
    {
        public string codigo;
    }
    public class GetStockCriticoRequest : BaseRequest
    {
    }
    public class GetStockBodegaRequest:BaseRequest
    {
        public string codigo;
    }

    public class GetOfertasRequest
    {
        public string codigo;
    }

    public class GetOfertasLocalRequest : BaseRequest
    {

    }

    public class ResultadoProductoPV
    {
        public string mensaje;
        public ProductoPV producto;
        public bool valido;
    }

    public class ResultadoProductos
    {
        public string mensaje;
        public List<ProductoPV> ListaProductos;
        public bool valido;

        public ResultadoProductos()
        {
            ListaProductos = new List<ProductoPV>();
        }
    }

    public class ResultadoStock
    {
        public bool valido;
        public List<StockBodega> listaStock;
        public ResultadoStock()
        {
            listaStock = new List<StockBodega>();
        }
    }

    public class ResultadoStockCritico
    {
        public bool valido;
        public string mensaje;
        public List<StockCritico> listaStockCritico;
        public ResultadoStockCritico()
        {
            listaStockCritico = new List<StockCritico>();
        }
    }



    public class GetEtiquetaRequest
    {
        public long codigo;
    }

    public class ResultadoEtiqueta
    {
        public bool valido;
        public List<Etiqueta> listaEtiqueta;
        public ResultadoEtiqueta()
        {
            listaEtiqueta = new List<Etiqueta>();
        }
    }

    public class GetCodigosRequest
    {
        public string codigo;
    }

    public class ResultadoCodigos
    {
        public bool valido;
        public List<Codigos> listaCodigo;
        public ResultadoCodigos()
        {
            listaCodigo = new List<Codigos>();
        }
    }


    public class GetImpresoraRequest
    {
        public string nombre;
    }

    public class ResultadoImpresora
    {
        public bool valido;
        public List<Impresora> listaImpresora;
        public ResultadoImpresora()
        {
            listaImpresora = new List<Impresora>();
        }
    }

    public class ResultadoOferta
    {
        public bool valido;
        public List<Oferta> listaOfertas;
        public ProductoPV producto;
        public ResultadoOferta()
        {
            listaOfertas = new List<Oferta>();
        }
    }

    public class GetStockProductoPorBodegaRequest: RequestHeader
    {
        public long cod_bodega;
    }

    public class GetStockCriticoPorBodegaRequest
    {
        public long cod_bodega;
        public long sucursal;
    }
}
