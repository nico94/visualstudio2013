﻿using Model.Despacho;
using Model.Request;
using Model.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;



namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IDespachoService
    { 

        //atributo
        [OperationContract]
        //metodo
        ValidaUsuario ValidarUsuario(ValidarUsuarioRequest validarusuariorequest);

        [OperationContract]
        ResultadoTiket listadoTicket(FiltraTickettRequest filtraticketrequest);

        [OperationContract]
        ResultadoDetalleTiket listadoDetalleTicket(FiltraDetalleTicketRequest filtradetalleticketrequest);

        [OperationContract]
        ResultadoUpdateTicket UpdateTicket(ResultadoUpdateTicketRequest resultadoUpdateticketrequest);

     }
    
    public class ValidarUsuarioRequest :BaseRequest
    {
        public string usuario;
        public string pass;
        public string modulo;

    }

    #region ticket

            public class ResultadoTiket
            {
                public bool valido;

                public List<Ticket> listaTiket;
                public ResultadoTiket()
                {
                    listaTiket = new List<Ticket>();
                }
            }

            public class FiltraTickettRequest
            {
                public bool bit_despachada_documentoventa;

            }

            public class FiltraDetalleTicketRequest 
            {
                public long cod_ordencompracliente_ordencompracliente;
                
            }

            public class ResultadoDetalleTiket 
            {
             
                public bool valido;

                public List<DetalleTicket> listaDetalleTiket;
                public ResultadoDetalleTiket()
                {
                    listaDetalleTiket = new List<DetalleTicket>();
                }
            }
            public class ResultadoUpdateTicket 
            {
              public bool bit_despachada_documentoventa;
              public DateTime dat_fecha_Despacho;
            }

            public class ResultadoUpdateTicketRequest
            {
                public long cod_ordencompracliente_ordencompracliente;
            }

    #endregion
}
