﻿using Model.Request;
using Model.Vendedora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IVendedoraService
    {
        [OperationContract]
        ResultadoVendedora GetVendedoras(GetVendedorasRequest getvendedorasrequest);
    }

    public class ResultadoVendedora
    {
        public bool valido;
        public List<Vendedora> listaVendedoras;
        public string mensaje;

        public ResultadoVendedora()
        {
            listaVendedoras = new List<Vendedora>();
        }
    }

    public class GetVendedorasRequest : BaseRequest
    {

    }
}
