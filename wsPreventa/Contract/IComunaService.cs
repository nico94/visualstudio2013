﻿using Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IComunaService
    {
        [OperationContract]
        ResultadoComuna GetComunas(GetComunasRequest getcomunasrequest);
    }

    public class ResultadoComuna
    {
        public bool valido;
        public List<Comuna> ListadoComunas;
        public ResultadoComuna()
        {
            ListadoComunas = new List<Comuna>();
        }
    }

    public class GetComunasRequest
    {
        public long cod_ciudad;
    }
}
