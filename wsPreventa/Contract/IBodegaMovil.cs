﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Runtime.Serialization;
using Model.Request;
using Model.BodegaMovil;
using Model.ControlIngresoInsumo;
using Model.Usuario;
using System.ServiceModel.Web;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IBodegaMovil
    {
         [OperationContract]
         ResultadoFolio GetFolio(GetFolioRequest getfoliorequest);        
         [OperationContract]
         ResultadoDetalleFolio GetDetalleFolio(GetDetalleFolioRequest getdetallefoliorequest);
         [OperationContract]
         string GetBodegaporCodigo(GetBodegaporCodigoRequest getbodegaporcodigorequest);
         [OperationContract]
         ResultadoSeccionporCodigo GetSeccionporCodigo(GetSeccionporCodigoRequest getseccionporcodigorequest);
         [OperationContract]
         ResultadoCabeceraIngresoInsumo AddCabeceraIngresoInsumo(AddCabeceraIngresoInsumoRequest addcabeceraingresoinsumorequest);        
         [OperationContract]
         ResultadoCondicionesBodegas GetCondicionesBodegas();        
         [OperationContract]
         ResultadoCondicionesProductos GetCondicionesProductos();
         [OperationContract]
         ResultadoLoteporFolio GetLoteporFolio(GetLoteporFolioRequest getloteporfoliorequest);
         [OperationContract]
         ResultadoPosciciones GetPosciciones(GetPoscicionesRequest getposcicionesrequest);
         [OperationContract]
         ResultadoLotes GetListaLotes();
         [OperationContract]
         ResultadoUpdateCabeceraIngresoInsumo UpdateCabeceraIngresoInsumo(UpdateCabeceraIngresoInsumoRequest updatecabeceraingresoinsumorequest);
         [OperationContract]
         ResultadoDetalleLote GetDetalleLote(GetDetalleLoteRequest getdetalleloterequest);
         [OperationContract]
         ResultadoBodegaTienda GetBodegaTienda(GetBodegaTiendaRequest getbodegatiendarequest);
         [OperationContract]
         ResultadoBodega GetBodegaPorSucursal(GetBodegaPorSucursalRequest getbodegaporsucursalrequest);
         [OperationContract]
         ResultadoBodega GetBodegaPorSucursalString(GetBodegaPorSucursalStringRequest getbodegaporsucursalrequest);
         [OperationContract]
         ResultadoSeccion GetSeccion(GetSeccionRequest getseccccionrequest);
         [OperationContract]
         ResultadoMoverLote MoverLote(GetMoverLoteRequest getmoverloterequest);
         [OperationContract]
         ResultadoLoteporProducto GetLoteporProducto(GetLoteporProductoRequest getloteporproductorequest);   
         [OperationContract]
         ResultadoLotesImprimir LotesImprimir(LotesImprimirRequest lotesimprimirrequest);
         [OperationContract]
         ResultadoCrealoteProductoTerminado CrealoteProductoTerminado();
         [OperationContract]
         ResultadoDetalleLoteUbicacion GetDetalleLoteUbicacion(GetDetalleLoteUbicacionRequest getdetalleloteubicacionrequest);
         [OperationContract]
         ResultadoEmpresa GetEmpresa();
         [OperationContract]
         ResultadoSucursalEmpresa GetSucursalEmpresa(GetSucursalPorEmpresaRequest GetSucursalPorEmpresarequest);
         [OperationContract]
         ResultadoCrearInventario CrearInventario(CrearInventarioRequest CrearInventariorequest);
         [OperationContract]
         ResultadoExisteInventarioActivo ExisteInventarioActivo(ExisteInventarioActivoRequest ExisteInventarioActivorequest);
         [OperationContract]
         ResultadoSumaCantidadInventarioActivo SumaCantidadInventarioActivo(SumaCantidadInventarioActivoRequest SumaCantidadInventarioActivorequest);
         [OperationContract]
         ResultadoRestaCantidadInventarioActivo RestaCantidadInventarioActivo(RestaCantidadInventarioActivoRequest RestaCantidadInventarioActivorequest);
         [OperationContract]
         ResultadoTerminarInventarioActivo TerminarInventarioActivo(TerminarInventarioActivoRequest TerminarInventarioActivorequest);
         [OperationContract]
         ResultadoListaSolicitudDespachoAprobada ListaSolicitudDespachoAprobada(GetListaSolicitudDespachoAprobadaRequest GetListaSolicitudDespachoAprobadarequest);
         [OperationContract]
         ResultadoListaSolicitudDespachoAprobadaDespachar ListaSolicitudDespachoAprobadaDespachar(GetListaSolicitudDespachoAprobadaDespacharRequest GetListaSolicitudDespachoAprobadaDespacharrequest);
         [OperationContract]
         ResultadoLoteReservadoSolicitud LoteReservadoSolicitud(GetLoteReservadoSolicitudRequest GetLoteReservadoSolicitudrequest);
         [OperationContract]
         ResultadoMotivoODPorCodigo MotivoODPorCodigo(GetMotivoODPorCodigoRequest GetMotivoODPorCodigorequest);
         [OperationContract]
         ResultadoLotePorProductoSucursal LotePorProductoSucursal(GetLotePorProductoSucursalRequest GetLotePorProductoSucursalrequest);
         [OperationContract]
         ResultadoCantidadLoteReservadoProductoSolicitud CantidadLoteReservadoProductoSolicitud(GetCantidadLoteReservadoProductoSolicitudRequest GetCantidadLoteReservadoProductoSolicitudrequest);
         [OperationContract]
         ResultadoCrearOrdenDespacho CrearOrdenDespacho(CrearOrdenDespachoRequest CrearOrdenDespachorequest);
         [OperationContract]
         ResultadoCrearOrdenDespachoFinal CrearOrdenDespachoFinal(CrearOrdenDespachoFinalRequest CrearOrdenDespachofinalrequest);
         [OperationContract]
         ResultadoExisteCantidadSinCumplir ExisteCantidadSinCumplir(ExisteExisteCantidadSinCumplirRequest ExisteExisteCantidadSinCumplirrequest);
         [OperationContract]
         ResultadoExisteNumeroOrden ExisteNumeroOrden(ExisteNumeroOrdenRequest ExisteNumeroOrdenrequest);
         [OperationContract]
         ResultadoListaDetalleNumeroOrden ListaDetalleNumeroOrden(GetListaDetalleNumeroOrdenRequest GetListaDetalleNumeroOrdenrequest);
         [OperationContract]
         ResultadoExisteProductoenOrden ExisteProductoenOrden(ExisteProductoenOrdenRequest ExisteProductoenOrdenrequest);
         [OperationContract]
         ResultadoSumaCantidadOrden SumaCantidadOrden(SumaCantidadOrdenRequest SumaCantidadOrdenrequest);
         [OperationContract]
         ResultadoConfirmarOrden ConfirmarOrden(ConfirmarOrdenRequest ConfirmarOrdenrequest);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
          UriTemplate = "/validarUsario",
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Wrapped)]
        ValidaUsuario ValidarUsuario(ValidarUsuarioBRequest validarusuariobrequest);

         [OperationContract]
         ResultadoGetDescuentaStockFifoMateriaPrima GetDescuentaStockFifoMateriaPrima(GetDescuentaStockFifoMateriaPrimaRequest GetDescuentaStockFifoMateriaprimaRequest);
    }

    public class ConfirmarOrdenRequest
    {
        public string num_ordendespacho;   
    }
    public class ResultadoConfirmarOrden
    {
        public bool Realizado;
        public string Mensaje;
    }

    public class SumaCantidadOrdenRequest
    {
        public string num_ordendespacho;
        public string cod_producto_materiaprima;
        public string Cantidad;
    }
    public class ResultadoSumaCantidadOrden
    {        
        public bool Realizado;
        public string Mensaje;
    }
    public class ExisteProductoenOrdenRequest
    {
        public string cod_producto_materiaprima;
        public string num_ordendespacho;    
    }
    public class ResultadoExisteProductoenOrden
    {
        public bool Existe;
    }
    public class GetListaDetalleNumeroOrdenRequest
    {
        public string num_ordendespacho;    
    }
    public class ResultadoListaDetalleNumeroOrden
    {
        public bool valido;
        public List<ListaDetalleOrden> lista;
        public ResultadoListaDetalleNumeroOrden()
        {
            lista = new List<ListaDetalleOrden>();
        }
    }
    public class ExisteNumeroOrdenRequest
    {
        public string num_ordendespacho;
    }
    public class ResultadoExisteNumeroOrden
    {
        public bool Existe;
    }
    public class ExisteExisteCantidadSinCumplirRequest
    {
        public string num_solicituddespacho_id;
        public string cod_empresa_empresasolicituddespacho;
        public string id_empresa_sucursalsolicituddespacho;
    }
    public class ResultadoExisteCantidadSinCumplir
    {
        public bool Existe;
    }
    public class CrearOrdenDespachoFinalRequest
    {      
        public string num_solicituddespacho_id;
        public string cod_empresa_empresasolicituddespacho;
        public string id_empresa_sucursalsolicituddespacho;       
       
    }
    public class ResultadoCrearOrdenDespachoFinal
    {
        public bool Realizado;
        public string Mensaje;
        public long num_ordendespacho;
    }
    public class CrearOrdenDespachoRequest
    {
        public string num_ordendespacho;
        public string cod_empresa_empresaordendespacho;
        public string id_empresa_sucursalordendespacho;       
        public string dat_ordendespacho;        
        public string num_solicituddespacho_id;
        public string cod_empresa_empresasolicituddespacho;
        public string id_empresa_sucursalsolicituddespacho;
        public string id_empresa_sucursalaDespachar;
        public string num_solicituddespacho_cantsolicitada;         
        public List<Det_OrdenDespacho> ListaDetalle;
        public CrearOrdenDespachoRequest()
        {
            ListaDetalle = new List<Det_OrdenDespacho>();
        }
    }
    public class ResultadoCrearOrdenDespacho
    {
        public bool Realizado;
        public string Mensaje;
        public long num_ordendespacho;
    }
    public class GetCantidadLoteReservadoProductoSolicitudRequest
    {
        public string num_solicituddespacho_id;
        public string id_empresa_sucursal;
        public string id_empresa_sucursalAbastecimiento;
        public string cod_producto_materiaprima;
        public string num_lote;
    }
    public class ResultadoCantidadLoteReservadoProductoSolicitud
    {   
        public double CantidadReservada;        
    }

    public class GetLotePorProductoSucursalRequest
    {
        public string cod_producto_materiaprima;    
        public string id_empresa_sucursalAbastecimiento;
    }

    public class ResultadoLotePorProductoSucursal
    {
        public bool valido;
        public List<Lote> listaLote;
        public ResultadoLotePorProductoSucursal()
        {
            listaLote = new List<Lote>();
        }
    }

    public class GetMotivoODPorCodigoRequest
    {
        public string cod_motivoOD;
    }

    public class ResultadoMotivoODPorCodigo
    {
        public bool valido;
        public List<Motivo_OD> Lista;
        public ResultadoMotivoODPorCodigo()
        {
            Lista = new List<Motivo_OD>();
        }
    }


    public class GetLoteReservadoSolicitudRequest
    {
        public string num_solicituddespacho_id;
        public string id_empresa_sucursal;
        public string id_empresa_sucursalAbastecimiento;
        public string cod_producto_materiaprima;
    }

    public class ResultadoLoteReservadoSolicitud
    {
        public bool valido;
        public List<Lote> listaLote;
        public ResultadoLoteReservadoSolicitud()
        {
            listaLote = new List<Lote>();
        }
    }

    public class GetListaSolicitudDespachoAprobadaDespacharRequest
    {
        public string num_solicituddespacho_id;
        public string id_empresa_sucursalSolicita;
        public string id_empresa_sucursalAbastecimiento;
        public string Estado;
    }

    public class ResultadoListaSolicitudDespachoAprobadaDespachar
    {
        public bool valido;
        public List<ListaSolicitudDespachoAprobadaDespachar> lista;

        public ResultadoListaSolicitudDespachoAprobadaDespachar()
        {
            lista = new List<ListaSolicitudDespachoAprobadaDespachar>();
        }
    }

    public class GetListaSolicitudDespachoAprobadaRequest
    {
        public string id_empresa_sucursal;
        public string Estado;
        public string FechaDesde;
        public string FechaHasta;
    }

    public class ResultadoListaSolicitudDespachoAprobada
    {
        public bool valido;
        public List<ListaSolicitudDespachoAprobada> lista;

        public ResultadoListaSolicitudDespachoAprobada()
        {
            lista = new List<ListaSolicitudDespachoAprobada>();
        }
    }


    public class GetDetalleLoteUbicacionRequest
    {
        public string num_lote;       
        public string TipoLote;
    }

    public class ResultadoDetalleLoteUbicacion
    {
        public bool valido;
        public List<Lote> listadodetallelote;

        public ResultadoDetalleLoteUbicacion()
        {
            listadodetallelote = new List<Lote>();
        }
    }
    
    public class ResultadoCrealoteProductoTerminado
    {
        public string Lote;
        public bool resultado;
    }
    

    public class LotesImprimirRequest
    {
        public string cod_documentocomprageneral_documentocomprageneral;
        public string cod_proveedor;
        public string fecha;
        public string cod_producto;
        public string Impresora;
        public string CodEtiqueta;
        public string num_lote;
        public string TipoImpresion;
    }

    public class ResultadoLotesImprimir
    {
        public bool valido;
        public List<Lote> listaLotesImprimir;
        public ResultadoLotesImprimir()
        {
            listaLotesImprimir = new List<Lote>();
        }
    }
   
       
    public class ResultadoLoteporRecepcionarenTienda
    {
        public bool valido;
        public List<Lote> listadoloterecepcionartienda;

        public ResultadoLoteporRecepcionarenTienda()
        {
            listadoloterecepcionartienda = new List<Lote>();
        }
    }

    public class GetLoteporProductoRequest
    {
        public string CodProducto;
    }

    public class ResultadoLoteporProducto
    {
        public bool valido;
        public List<Lote> listadoloteproducto;

        public ResultadoLoteporProducto()
        {
            listadoloteproducto = new List<Lote>();
        }
    }

    public class GetMoverLoteRequest
    {
        public string num_lote;
        public string Tipo_Lote;
        public string cod_producto;

        public string Cantidad_Nueva;        
        public string Nueva_Bodega;
        public string Nueva_Seccion;
        public string Nueva_X;
        public string Nueva_Y;
        public string Nueva_Z;

        public string Cantidad_Original;
        public string Original_Bodega;
        public string Original_Seccion;
        public string Original_X;
        public string Original_Y;
        public string Original_Z;
        public string Totienda;
    }

    public class ResultadoMoverLote
    {
        public string mensaje;
        public bool ok;
        public string num_lote;
    }

    public class GetBodegaTiendaRequest
    {
        public string esTienda;
    }
      
    public class ResultadoBodegaTienda
    {
        public List<Bodegas> listadobodegasT;
        public ResultadoBodegaTienda()
        {
            listadobodegasT = new List<Bodegas>();
        }
    }
    
    public class GetDetalleLoteRequest
    {
        public string num_lote;
        public string Pos_X;
        public string Pos_Y;
        public string Pos_Z;
        public string TipoLote;
    }
    
    public class ResultadoDetalleLote
    {
        public bool valido;
        public List<Lote> listadodetallelote;

        public ResultadoDetalleLote()
        {
            listadodetallelote = new List<Lote>();
        }
    }
    
    public class ResultadoLotes
    {
        public List<Lote> ListadoLotes;
        public ResultadoLotes()
        {
            ListadoLotes = new List<Lote>();
        }
    }

    public class GetPoscicionesRequest
    {
        public long cod_bodega;
        public long cod_seccion;
    }

    public class ResultadoPosciciones
    {
        public bool valido;
        public SnapSeccionBodegaUbicacion poscicion;
        public List<SnapSeccionBodegaUbicacion> listadoposciciones;

        public ResultadoPosciciones()
        {
            listadoposciciones = new List<SnapSeccionBodegaUbicacion>();
        }
    }

    public class GetSeccionRequest
    {
        public long cod_bodega;
        

    }

    public class ResultadoSeccion
    {
        public List<Seccion> listadoSeccion;

        public ResultadoSeccion()
        {
            listadoSeccion = new List<Seccion>();
        }
    }

    public class UpdateCabeceraIngresoInsumoRequest
    {
        public string num_lote;
        public string dat_fechavencimiento_ControlIngresoInsumo;
        public string num_cantidad_ControlIngresoInsumo;
        public string num_cantidadbultos_controlingresoinsumo;
        public string cod_producto_materiaprima;
        public string cod_condicionproducto_condicionproducto;
        public string cod_condicionbodega_condicionbodega;

        public string cod_documentocomprageneral_documentocomprageneral;
        public string cod_proveedor;
        public string fecha;
        public string Cantidad_Antes;

        public string Nueva_Bodega;
        public string Nueva_Seccion;
        public string Nueva_X;
        public string Nueva_Y;
        public string Nueva_Z;

    }
    
    public class ResultadoUpdateCabeceraIngresoInsumo
    {
        public string mensaje;
        public bool actualizado;

    }
       
    
    public class ResultadoCabeceraIngresoInsumo
    {
        public string mensaje;
        public string num_lote;
    }

    public class AddCabeceraIngresoInsumoRequest 
    {
        public string cod_producto_materiaprima;
        public snap_ControlIngresoInsumo Cabecera;
        public ControlIngresoInsumo DetalleInsumo;
        public ControlIngresoInsumoProductoTerminado DetalleProductoTerminado;
        
        public AddCabeceraIngresoInsumoRequest()
        {
            Cabecera = new snap_ControlIngresoInsumo();
            DetalleInsumo = new ControlIngresoInsumo();
            DetalleProductoTerminado = new ControlIngresoInsumoProductoTerminado();
        }
                        
    }
    
    public class GetFolioRequest
    {
        public bool Loteada;

    }

    public class ResultadoFolio

    {
        public bool valido;
        public List<Folio> listaFolio;
        public ResultadoFolio()
        {
            listaFolio = new List<Folio>();
        }
    }


    public class GetLoteporFolioRequest
    {
        public string cod_documentocomprageneral_documentocomprageneral;
        public string cod_producto;
       
    }

    public class ResultadoLoteporFolio
    {
        public bool valido;
        public List<Lote> listaLote;
        public ResultadoLoteporFolio()
        {
            listaLote = new List<Lote>();
        }
    }

    public class GetDetalleFolioRequest
    {
        public string cod_documentocomprageneral_documentocomprageneral;
        public string cod_proveedor;
        public string fecha;
        public string SoloSinLotear;
    }

    public class ResultadoDetalleFolio
    {
        public bool valido;
        public List<DetalleFolio> listaDetalleFolio;
        public ResultadoDetalleFolio()
        {
            listaDetalleFolio = new List<DetalleFolio>();
        }
    }


    public class GetBodegaporCodigoRequest
    {
        public string cod_bodega_bodega;
    }

    public class GetSeccionporCodigoRequest
    {
        public string cod_seccionbodega_seccionbodega;
        public string cod_bodega;
        
    }

    public class ResultadoBodega
    {
        public List<Bodegas> listadobodegas;
        public ResultadoBodega()
        {
            listadobodegas = new List<Bodegas>();
        }
    }

    public class GetBodegaPorSucursalRequest
    {
        public long sucursal;
    }

    public class GetBodegaPorSucursalStringRequest
    {
        public string sucursal;
    }

    public class GetSucursalPorEmpresaRequest
    {
        public string cod_empresa_empresa;
    }

    public class ResultadoSucursalEmpresa
    {
        public bool valido;
        public List<Sucursal_empresa> lista;
        public ResultadoSucursalEmpresa()
        {
            lista = new List<Sucursal_empresa>();
        }
    }

    public class ResultadoSeccionporCodigo
    {
        public bool valido;
        public List<SeccionBod> listaDetalleSeccion;
        public ResultadoSeccionporCodigo()
        {
            listaDetalleSeccion = new List<SeccionBod>();
        }
    }

    public class ResultadoCondicionesBodegas
    {
        public bool valido;
        public List<Condiciones_Bodegas> listaCondicionBodega;
        public ResultadoCondicionesBodegas()
        {
            listaCondicionBodega = new List<Condiciones_Bodegas>();
        }
    }

    public class ResultadoCondicionesProductos
    {
        public bool valido;
        public List<Condiciones_Productos> listaCondicionProducto;
        public ResultadoCondicionesProductos()
        {
            listaCondicionProducto = new List<Condiciones_Productos>();
        }
    }

    public class ResultadoEmpresa
    {
        public bool valido;
        public List<Empresas_Holding> lista;
        public ResultadoEmpresa()
        {
            lista = new List<Empresas_Holding>();
        }
    }

    public class CrearInventarioRequest
    {
        public string cod_empresa_empresa;
        public string id_empresa_sucursal;
        public string cod_bodega_bodega;
        public string cod_fichapersonal_fichapersonal;
        public string Ajusta_soloCapturado;
    }

    public class ResultadoCrearInventario
    {
        public bool Realizado;
        public string Mensaje;
        public long Cod_tomainventario;
    }

    public class ExisteInventarioActivoRequest
    {
        public string cod_empresa_empresa;
        public string id_empresa_sucursal;
        public string cod_bodega_bodega;           
    }

    public class ResultadoExisteInventarioActivo
    {     
        public long Cod_tomainventario;
    }

    public class SumaCantidadInventarioActivoRequest
    {     
        public string Cod_tomainventario;
        public string cod_producto_materiaprima;
    }

    public class ResultadoSumaCantidadInventarioActivo
    {
        public long Cantidad;
        public string NombreProductoLeido;
        public bool Realizado;
        public string Mensaje;
    }

    public class RestaCantidadInventarioActivoRequest
    {
        public string Cod_tomainventario;
        public string cod_producto_materiaprima;
    }

    public class ResultadoRestaCantidadInventarioActivo
    {
        public long Cantidad;
        public string NombreProductoLeido;
        public bool Realizado;
        public string Mensaje;
    }

    public class TerminarInventarioActivoRequest
    {
        public string Cod_tomainventario;       
    }

    public class ResultadoTerminarInventarioActivo
    {    
        public bool Realizado;
        public string Mensaje;
    }

    public class ValidarUsuarioBRequest 
    {
        public string usuario;
        public string pass;
        public string modulo;

    }

    public class GetDescuentaStockFifoMateriaPrimaRequest
    {
        public long cod_bodega_bodega;
        public string cod_producto_materiaprima;
        public string Tipo;
        public double CantidadRequerida;
        public long Numero;
        public string cod_empresa_empresa;
        public decimal id_empresa_sucursal;
        public decimal id_empresa_sucursal_adespachar;
    }
    public class ResultadoGetDescuentaStockFifoMateriaPrima
    {
        public bool realizado;
        public string Mensaje;
    }


  }       

