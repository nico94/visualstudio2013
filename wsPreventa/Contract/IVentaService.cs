﻿
using Pall.GestionComercial.Model;
using Pall.GestionComercial.Model.Venta;
//using Model.Venta;
//using Pall.GestionComercial.Model;
//using Pall.GestionComercial.Model.Venta;

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IVentaService
    {
        [OperationContract]
        ResultadoVenta AddVenta(AddVentaRequest addVentaRequest);

        [OperationContract]
        ResultadoVenta AddPedido(AddPedidoRequest addPedidoRequest);

        [OperationContract]
        ResultadoVenta AnularVenta(AnularRequest anularVentaRequest);

        [OperationContract]
        ResultadoPedido GetPedido(GetPedidoRequest getPedidoRequest);

        [OperationContract]
        List<Venta> GetPedidoPorRut(GetPedidoPorRutRequest getPedidoPorRutRequest);

        [OperationContract]
        Venta GetVenta(GetVentaRequest getVentaRequest);

        [OperationContract]
        long GetNumeroDocumento(GetNumeroDocumentoRequest getNumeroDocumentoRequest);

        [OperationContract]
        bool VentaImpresa(VentaImpresaRequest ventaImpresaRequest);

        [OperationContract]
        List<MedioPago> GetMediosDePago(GetMediosDePagoRequest getMediosDePagoRequest);

        [OperationContract]
        OpcionesConfiguracion GetOpcionesConfiguracion(GetOpcionesConfiguracionRequest getOpcionesConfiguracionRequest);

        [OperationContract]
        ResultadoCobranza AddCobranza(AddCobranzaRequest addcobranzarequest);

        [OperationContract]
        ResultadoAnticipo AddAnticipo(AddAnticipoRequest addanticiporequest);

    }

    public class AddVentaRequest : BaseRequest
    {
        public Venta venta { set; get; }
        public bool ImprimeComanda { set; get; }
        public long numnota;
    }

    public class AddPedidoRequest : BaseRequest
    {
        public Venta venta { set; get; }
    }

    public class AnularRequest : BaseRequest
    {
        public Venta venta { set; get; }
    }

    public class GetVentaRequest : BaseRequest
    {
        public long IdVenta { set; get; }
    }

    public class VentaImpresaRequest : BaseRequest
    {
        public long IdVenta { set; get; }
        public long NumeroDocumento { set; get; }
    }

    public class GetPedidoRequest : BaseRequest
    {
        public long IdPedido { set; get; }
    }

    public class GetPedidoPorRutRequest : BaseRequest
    {
        public string RutCliente { set; get; }
    }

    public class GetNumeroDocumentoRequest : BaseRequest
    {
        public string CodEmpresa { set; get; }
        public long IdTipoDocumento { set; get; }
    }

    public class GetMediosDePagoRequest : BaseRequest
    {

    }

    public class GetOpcionesConfiguracionRequest : BaseRequest
    {

    }

    public class AddCobranzaRequest
    {
        public Cobranza cobranza;
        public int pie;
    }

    public class ResultadoCobranza
    {
        public bool valido;
    }

    public class AddAnticipoRequest
    {
        public AnticipoCliente anticipo;
    }

    public class ResultadoAnticipo
    {
        public bool valido;
    }

}
