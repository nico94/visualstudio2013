﻿using Model.Cliente;
using Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IClienteService
    {
        [OperationContract]
        ResultadoCliente GetClientePorRut(GetClientePorRutRequest getclienteporrutrequest);

        [OperationContract]
        ResultadoCliente AddCliente(AddClienteRequest addclienterequest);

        [OperationContract]
        ResultadoCiudad GetCiudades(GetCiudadesRequest getciudadesrequest);

        [OperationContract]
        ResultadoRegion GetRegiones(GetRegionesRequest getregionesrequest);

    }

    public class ResultadoCliente
    {
        public string mensaje;
        public Cliente cliente;
        public bool valido;
    }

    public class GetClientePorRutRequest
    {
        public string rut;
    }

    public class AddClienteRequest : BaseRequest
    {
        public Cliente cliente;
    }

    public class ResultadoCiudad
    {
        public bool valido;
        public List<Ciudad> listaCiudades;
        public ResultadoCiudad()
        {
            listaCiudades = new List<Ciudad>();
        }
    }

    public class ResultadoRegion
    {
        public bool valido;
        public List<Region> listaRegiones;
        public ResultadoRegion()
        {
            listaRegiones = new List<Region>();
        }
    }

    public class GetCiudadesRequest
    {
        public string cod_region;
    }

    public class GetRegionesRequest
    {

    }
}
