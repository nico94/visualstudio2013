﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Model.Preventa;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IPreventaService
    {
        [OperationContract]
        ResultadoPreventa AddPreventa(AddPreventaRequest addpreventarequest);

        [OperationContract]
        ResultadoPulsera AddDescuento(AddDescuentoRequest adddescuentorequest);

        [OperationContract]
        ResultadoPreventa GetPreventa(GetPreventaRequest getpreventarequest);

        [OperationContract]
        ResultadoActualizacion UdpPreventa(UdpPreventaRequest udppreventarequest);
    }

    public class ResultadoPreventa
    {
        public string mensaje;
        public OrdenCompra ordenCompra;
    }

    public class ResultadoPulsera
    {
       public bool valido;
    }

    public class AddPreventaRequest
    {
        public OrdenCompra occ;
    }

    public class AddDescuentoRequest
    {
       public string codigo;
    }

    public class UdpPreventaRequest
    {
        public long codigo;
        public List<DetalleOrdenCompra> listaDetalleOrdenCompra;
    }

    public class ResultadoActualizacion
    {
        public string mensaje;
    }

    public class GetPreventaRequest
    {
        public long codigo;
    }
}
