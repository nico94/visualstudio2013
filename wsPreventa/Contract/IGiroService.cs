﻿using Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IGiroService
    {
        [OperationContract]
        ResultadoGiro GetGiros(GetGirosRequest getgirosrequest);
    }

    public class ResultadoGiro
    {
        public  List<Giro> listaGiros;
        public long codigoSG;
        public ResultadoGiro()
        {
            listaGiros= new List<Giro>();
        }
    }
    public class GetGirosRequest
    {

    }
}
