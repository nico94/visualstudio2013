﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Model.Envio_Correo;


namespace Contract
{
    [ServiceContract(Namespace = "http://PallTechnollogy.com/GestionComercial")]
    public interface IEnvioCorreo
    {
        [OperationContract]
        ResultadoEnvioCorreo GetEnvioCorreo(GetEnvioCorreoRequest getenviocorreorequest);
    }

    public class GetEnvioCorreoRequest
    {
        public string usuarioenvio;
        public string correoto;
        public string correofrom;
        public string asunto;
        
        public List<DetalleCorreo> listaDetalleCorreo;
        public GetEnvioCorreoRequest()
        {
            listaDetalleCorreo = new List<DetalleCorreo>();
        }
    }
    public class ResultadoEnvioCorreo
    {
        public bool enviado;
    }

    public class ResultadoCorreoComprador
    {
        public bool valido;

        public List<correoComprador> listaCorreo;
        public ResultadoCorreoComprador()
        {
            listaCorreo = new List<correoComprador>();
        }
    }
}
