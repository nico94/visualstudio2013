﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class ImpresionClient : ClientBase<IImpresionService>, IImpresionService
    {
        public ImpresionClient(string endpointConfigurationName)
            :base (endpointConfigurationName) { }

        public ResultadoImpresion ImprimirZPL(ImprimirZPLRequest imprimirzplrequest)
        {
            return Channel.ImprimirZPL(imprimirzplrequest);
        }
    }
}
