﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contract;
using Model;
using System.ServiceModel;
using Model.Caja;
using DataModel;

namespace ClientProxy
{
    public  class CajaClient : ClientBase<ICajaService>, ICajaService
    {

        public CajaClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }    

        public ResultadoCaja GetEstadoCaja(GetEstadoCajaRequest getEstadoCajaRequest)
        {
            return Channel.GetEstadoCaja(getEstadoCajaRequest);
        }
    }

}
