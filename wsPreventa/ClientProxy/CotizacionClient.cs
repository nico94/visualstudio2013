﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class CotizacionClient : ClientBase<ICotizacionService>, ICotizacionService
    {
        public CotizacionClient (string endpointconfigurationName)
            : base(endpointconfigurationName) { }

        public ResultadoCotizacion AddCotizacion(AddCotizacionRequest addcotizacionrequest)
        {
            return Channel.AddCotizacion(addcotizacionrequest);
        }

        public ResultadoCotizacion GetCotizacion(GetCotizacionRequest getcotizacionrequest)
        {
            return Channel.GetCotizacion(getcotizacionrequest);
        }
    }
}