﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class PreventaClient : ClientBase<IPreventaService>, IPreventaService
    {
        public PreventaClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }    

        public ResultadoPreventa AddPreventa(AddPreventaRequest addpreventaRequest)
        {
            return Channel.AddPreventa(addpreventaRequest);
        }

        public ResultadoPulsera AddDescuento(AddDescuentoRequest adddescuentorequest)
        {
            return Channel.AddDescuento(adddescuentorequest);
        }

        public ResultadoPreventa GetPreventa(GetPreventaRequest getpreventarequest)
        {
            return Channel.GetPreventa(getpreventarequest);
        }

        public ResultadoActualizacion UdpPreventa(UdpPreventaRequest udppreventarequest)
        {
            return Channel.UdpPreventa(udppreventarequest);
        }
    }
}
