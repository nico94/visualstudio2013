﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Contract;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Model.Venta;
using Pall.GestionComercial.Model.Venta;
using Pall.GestionComercial.Model;
using Pall.GestionComercial.Contract;


namespace Pall.GestionComercial.ClientProxy

{
    public class VentaClient : ClientBase<IVentaService>, IVentaService
    {
        private ICacheManager _objCacheManager = CacheFactory.GetCacheManager();

        public VentaClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public VentaClient()
        {
        }

        #region IVentaService Members

        public ResultadoVenta AddVenta(AddVentaRequest addVentaRequest)
        {
            return Channel.AddVenta(addVentaRequest);
        }

        public ResultadoVenta AnularVenta(AnularRequest anularVentaRequest)
        {
            return Channel.AnularVenta(anularVentaRequest);
        }

        public Venta GetVenta(GetVentaRequest getVentaRequest)
        {
            return Channel.GetVenta(getVentaRequest);
        }

        public long GetNumeroDocumento(GetNumeroDocumentoRequest getNumeroDocumentoRequest)
        {
            return Channel.GetNumeroDocumento(getNumeroDocumentoRequest);
        }

        public bool VentaImpresa(VentaImpresaRequest ventaImpresaRequest)
        {
            return Channel.VentaImpresa(ventaImpresaRequest);
        }

        public List<MedioPago> GetMediosDePago(GetMediosDePagoRequest getMediosDePagoRequest)
        {
            return Channel.GetMediosDePago(getMediosDePagoRequest);
        }

        public OpcionesConfiguracion GetOpcionesConfiguracion(GetOpcionesConfiguracionRequest getOpcionesConfiguracionRequest)
        {
            return Channel.GetOpcionesConfiguracion(getOpcionesConfiguracionRequest);
        }

        public ResultadoVenta AddPedido(AddPedidoRequest addPedidoRequest)
        {
            return Channel.AddPedido(addPedidoRequest);
        }


        public ResultadoPedido GetPedido(GetPedidoRequest getPedidoRequest)
        {
            return Channel.GetPedido(getPedidoRequest);
        }

        public List<Venta> GetPedidoPorRut(GetPedidoPorRutRequest getPedidoPorRutRequest)
        {
            return Channel.GetPedidoPorRut(getPedidoPorRutRequest);
        }

        public ResultadoCobranza AddCobranza(AddCobranzaRequest addcobranzarequest)
        {
            return Channel.AddCobranza(addcobranzarequest);
        }

        public ResultadoAnticipo AddAnticipo(AddAnticipoRequest addanticiporequest)
        {
            return Channel.AddAnticipo(addanticiporequest);
        }
        #endregion
    }
}
