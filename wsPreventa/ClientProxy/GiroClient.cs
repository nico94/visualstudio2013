﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class GiroClient : ClientBase<IGiroService>,IGiroService
    {
        public GiroClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }    

        public ResultadoGiro GetGiros(GetGirosRequest getgirosRequest)
        {
            return Channel.GetGiros(getgirosRequest);
        }
    }
}
