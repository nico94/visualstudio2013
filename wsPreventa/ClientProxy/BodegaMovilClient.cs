﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.BodegaMovil;
using DataModel;

namespace ClientProxy
{
    public class BodegaMovilClient : ClientBase<IBodegaMovil>, IBodegaMovil
    {

        public BodegaMovilClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public ResultadoFolio GetFolio(GetFolioRequest getfoliorequest)
                                  
        {
            return Channel.GetFolio(getfoliorequest);
        }


        public ResultadoDetalleFolio GetDetalleFolio(GetDetalleFolioRequest getdetallefoliorequest)
        {
            return Channel.GetDetalleFolio(getdetallefoliorequest);
        }

        public string GetBodegaporCodigo(GetBodegaporCodigoRequest getbodegaporcodigorequest)
        {
            return Channel.GetBodegaporCodigo(getbodegaporcodigorequest);
        }

        public ResultadoSeccionporCodigo GetSeccionporCodigo(GetSeccionporCodigoRequest getseccionporcodigorequest)
        {
            return Channel.GetSeccionporCodigo(getseccionporcodigorequest);
        }

        public ResultadoCabeceraIngresoInsumo AddCabeceraIngresoInsumo(AddCabeceraIngresoInsumoRequest addcabeceraingresoinsumorequest)
        {
            return Channel.AddCabeceraIngresoInsumo(addcabeceraingresoinsumorequest);
        }

        public ResultadoCondicionesBodegas GetCondicionesBodegas()
        {
            return Channel.GetCondicionesBodegas();
        }

        public ResultadoCondicionesProductos GetCondicionesProductos()

        {
            return Channel.GetCondicionesProductos();
        }

        public ResultadoLoteporFolio GetLoteporFolio(GetLoteporFolioRequest getloteporfoliorequest)
        {
            return Channel.GetLoteporFolio(getloteporfoliorequest);
        }

        public ResultadoPosciciones GetPosciciones(GetPoscicionesRequest getposcicionesrequest)
        {
            return Channel.GetPosciciones(getposcicionesrequest);
        }

        public ResultadoLotes GetListaLotes()
        {
            return Channel.GetListaLotes();
        }

        public ResultadoUpdateCabeceraIngresoInsumo UpdateCabeceraIngresoInsumo(UpdateCabeceraIngresoInsumoRequest updatecabeceraingresoinsumorequest)
        {
            return Channel.UpdateCabeceraIngresoInsumo(updatecabeceraingresoinsumorequest);
        }

        public ResultadoDetalleLote GetDetalleLote(GetDetalleLoteRequest getdetalleloterequest)
        {
            return Channel.GetDetalleLote(getdetalleloterequest);
        }

        public ResultadoDetalleLoteUbicacion GetDetalleLoteUbicacion(GetDetalleLoteUbicacionRequest getdetalleloteubicacionrequest)
        {
            return Channel.GetDetalleLoteUbicacion(getdetalleloteubicacionrequest);

        }
        
        public ResultadoBodegaTienda GetBodegaTienda(GetBodegaTiendaRequest getbodegatiendarequest)
        {
            return Channel.GetBodegaTienda(getbodegatiendarequest);

        }

        public ResultadoBodega GetBodegaPorSucursal(GetBodegaPorSucursalRequest getboegaporsucursalrequest)
        {
            return Channel.GetBodegaPorSucursal(getboegaporsucursalrequest);
        }

        public ResultadoSeccion GetSeccion(GetSeccionRequest getseccionrequest)
        {
            return Channel.GetSeccion(getseccionrequest);
        }

        public ResultadoMoverLote MoverLote(GetMoverLoteRequest getmoverloterequest)
        {
            return Channel.MoverLote(getmoverloterequest);
        }

        public ResultadoLoteporProducto GetLoteporProducto(GetLoteporProductoRequest getloteporproductorequest)
        {
            return Channel.GetLoteporProducto(getloteporproductorequest);
        }
        
        //public ResultadoLoteporRecepcionarenTienda GetLoteporRecepcionar()
        //{
        //    return Channel.GetLoteporRecepcionar();
        //}

        //public ResultadoUpdateRecepcionTienda UpdateRecepcionTienda(UpdateRecepcionTiendaRequest updaterecepciontiendarequest)
        //{
        //    return Channel.UpdateRecepcionTienda(updaterecepciontiendarequest);
        //}

        public ResultadoLotesImprimir LotesImprimir(LotesImprimirRequest lotesimprimirrequest)
        {
            return Channel.LotesImprimir(lotesimprimirrequest);
        }

        public ResultadoCrealoteProductoTerminado CrealoteProductoTerminado()
        {
            return Channel.CrealoteProductoTerminado();
        }

        public ResultadoEmpresa GetEmpresa()
        {
            return Channel.GetEmpresa();
        }

        public ResultadoSucursalEmpresa GetSucursalEmpresa(GetSucursalPorEmpresaRequest GetSucursalPorEmpresarequest)
        {
            return Channel.GetSucursalEmpresa(GetSucursalPorEmpresarequest);
        }


        public ResultadoBodega GetBodegaPorSucursalString(GetBodegaPorSucursalStringRequest getbodegaporsucursalrequest)
        {
            return Channel.GetBodegaPorSucursalString(getbodegaporsucursalrequest);
        }


        public ResultadoCrearInventario CrearInventario(CrearInventarioRequest CrearInventariorequest)
        {
            return Channel.CrearInventario(CrearInventariorequest);
        }

        public ResultadoExisteInventarioActivo ExisteInventarioActivo(ExisteInventarioActivoRequest ExisteInventarioActivorequest)
        {
            return Channel.ExisteInventarioActivo(ExisteInventarioActivorequest);
        }

        public ResultadoSumaCantidadInventarioActivo SumaCantidadInventarioActivo(SumaCantidadInventarioActivoRequest SumaCantidadInventarioActivorequest)
        {
            return Channel.SumaCantidadInventarioActivo(SumaCantidadInventarioActivorequest);
        }

        public ResultadoRestaCantidadInventarioActivo RestaCantidadInventarioActivo(RestaCantidadInventarioActivoRequest RestaCantidadInventarioActivorequest)
        {
            return Channel.RestaCantidadInventarioActivo(RestaCantidadInventarioActivorequest);
        }

        public ResultadoTerminarInventarioActivo TerminarInventarioActivo(TerminarInventarioActivoRequest TerminarInventarioActivorequest)
        {
            return Channel.TerminarInventarioActivo(TerminarInventarioActivorequest);
        }

        public ResultadoCrearOrdenDespacho CrearOrdenDespacho(CrearOrdenDespachoRequest CrearOrdenDespachorequest)
        {
            return Channel.CrearOrdenDespacho(CrearOrdenDespachorequest);
        }
        public ResultadoCantidadLoteReservadoProductoSolicitud CantidadLoteReservadoProductoSolicitud(GetCantidadLoteReservadoProductoSolicitudRequest GetCantidadLoteReservadoProductoSolicitudrequest)
        {
            return Channel.CantidadLoteReservadoProductoSolicitud(GetCantidadLoteReservadoProductoSolicitudrequest);
        }
        public ResultadoLotePorProductoSucursal LotePorProductoSucursal(GetLotePorProductoSucursalRequest GetLotePorProductoSucursalrequest)
        {
            return Channel.LotePorProductoSucursal(GetLotePorProductoSucursalrequest);
        }
        public ResultadoMotivoODPorCodigo MotivoODPorCodigo(GetMotivoODPorCodigoRequest GetMotivoODPorCodigorequest)
        {
            return Channel.MotivoODPorCodigo(GetMotivoODPorCodigorequest);
        }
        public ResultadoLoteReservadoSolicitud LoteReservadoSolicitud(GetLoteReservadoSolicitudRequest GetLoteReservadoSolicitudrequest)
        {
            return Channel.LoteReservadoSolicitud(GetLoteReservadoSolicitudrequest);
        }
        public ResultadoListaSolicitudDespachoAprobada ListaSolicitudDespachoAprobada(GetListaSolicitudDespachoAprobadaRequest GetListaSolicitudDespachoAprobadarequest)
        {
            return Channel.ListaSolicitudDespachoAprobada(GetListaSolicitudDespachoAprobadarequest);
        }
        public ResultadoListaSolicitudDespachoAprobadaDespachar ListaSolicitudDespachoAprobadaDespachar(GetListaSolicitudDespachoAprobadaDespacharRequest GetListaSolicitudDespachoAprobadaDespacharrequest)
        {
            return Channel.ListaSolicitudDespachoAprobadaDespachar(GetListaSolicitudDespachoAprobadaDespacharrequest);
        }




        public ResultadoCrearOrdenDespachoFinal CrearOrdenDespachoFinal(CrearOrdenDespachoFinalRequest CrearOrdenDespachofinalrequest)
        {
            return Channel.CrearOrdenDespachoFinal(CrearOrdenDespachofinalrequest);
        }

        public ResultadoExisteCantidadSinCumplir ExisteCantidadSinCumplir(ExisteExisteCantidadSinCumplirRequest ExisteExisteCantidadSinCumplirrequest)
        {
            return Channel.ExisteCantidadSinCumplir(ExisteExisteCantidadSinCumplirrequest);
        }

        public ResultadoExisteNumeroOrden ExisteNumeroOrden(ExisteNumeroOrdenRequest ExisteNumeroOrdenrequest)
        {
            return Channel.ExisteNumeroOrden(ExisteNumeroOrdenrequest);
        }

        public ResultadoListaDetalleNumeroOrden ListaDetalleNumeroOrden(GetListaDetalleNumeroOrdenRequest GetListaDetalleNumeroOrdenrequest)
        {
            return Channel.ListaDetalleNumeroOrden(GetListaDetalleNumeroOrdenrequest);
        }

        public ResultadoExisteProductoenOrden ExisteProductoenOrden(ExisteProductoenOrdenRequest ExisteProductoenOrdenrequest)
        {
            return Channel.ExisteProductoenOrden(ExisteProductoenOrdenrequest);
        }

        public ResultadoSumaCantidadOrden SumaCantidadOrden(SumaCantidadOrdenRequest SumaCantidadOrdenrequest)
        {
            return Channel.SumaCantidadOrden(SumaCantidadOrdenrequest);
        }

        public ResultadoConfirmarOrden ConfirmarOrden(ConfirmarOrdenRequest ConfirmarOrdenrequest)
        {
            return Channel.ConfirmarOrden(ConfirmarOrdenrequest);
        }

        public Model.Usuario.ValidaUsuario ValidarUsuario(ValidarUsuarioBRequest validarusuariobrequest)
        {
            return Channel.ValidarUsuario(validarusuariobrequest);
        }

        public ResultadoGetDescuentaStockFifoMateriaPrima GetDescuentaStockFifoMateriaPrima(GetDescuentaStockFifoMateriaPrimaRequest GetDescuentaStockFifoMateriaprimaRequest)
        {
            return Channel.GetDescuentaStockFifoMateriaPrima(GetDescuentaStockFifoMateriaprimaRequest);
        }
    }
}
