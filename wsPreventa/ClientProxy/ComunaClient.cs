﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class ComunaClient :ClientBase<IComunaService>, IComunaService
    {
        public ComunaClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }    

        public ResultadoComuna GetComunas(GetComunasRequest getcomunasRequest)
        {
            return Channel.GetComunas(getcomunasRequest);
        }
    }
}
