﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class ProductoPVClient : ClientBase<IProductoPVService> , IProductoPVService
    {
        public ProductoPVClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public ResultadoProductoPV GetProductoPorCodigo(GetProductoPorCodigoRequest getproductoporcodigorequest)
        {
            return Channel.GetProductoPorCodigo(getproductoporcodigorequest);
        }

        public ResultadoProductos GetProductos(GetProductosRequest getproductosrequest)
        {
            return Channel.GetProductos(getproductosrequest);
        }

        public ResultadoStock GetStockBodega(GetStockBodegaRequest getstockbodegarequest)
        {
            return Channel.GetStockBodega(getstockbodegarequest);
        }

        public ResultadoStockCritico GetStockCritico(GetStockCriticoRequest getstockcriticorequest)
        {
            return Channel.GetStockCritico(getstockcriticorequest);
        }
        

        public ResultadoEtiqueta GetEtiqueta(GetEtiquetaRequest getetiquetarequest)
        {


            return Channel.GetEtiqueta(getetiquetarequest);

        }

        public ResultadoCodigos GetCodigos(GetCodigosRequest getcodigosrequest)
        {

            return Channel.GetCodigos(getcodigosrequest);
        }

        public ResultadoImpresora GetImpresora(GetImpresoraRequest getimpresorarequest)
        {
            return Channel.GetImpresora(getimpresorarequest);
        }

        public ResultadoOferta GetOfertasProducto(GetOfertasRequest getofertasrequest)
        {
            return Channel.GetOfertasProducto(getofertasrequest);
        }

        //public ResultadoOferta GetOfertaSeleccionada(GetOfertasRequest getofertasrequest)
        //{
        //    return Channel.GetOfertaSeleccionada(getofertasrequest);
        //}

        public ResultadoProductos GetOfertas(GetOfertasLocalRequest getofertaslocalrequest)
        {
            return Channel.GetOfertas(getofertaslocalrequest);
        }

        public ResultadoProductos GetStockProductosPorbodega(GetStockProductoPorBodegaRequest getstockproductoporbodegarequest)
        {
            return Channel.GetStockProductosPorbodega(getstockproductoporbodegarequest);
        }

        public ResultadoStockCritico GetStockCriticoPorBodega(GetStockCriticoPorBodegaRequest getstockcriticoporbodegarequest)
        {
            return Channel.GetStockCriticoPorBodega(getstockcriticoporbodegarequest);
        }
    }
}
