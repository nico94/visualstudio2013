﻿using Contract;
using Model.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class DespachoClient : ClientBase<IDespachoService>,IDespachoService
    {

        public DespachoClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public ValidaUsuario ValidarUsuario(ValidarUsuarioRequest validarusuariorequest)
        {
            return Channel.ValidarUsuario(validarusuariorequest);
        }

        public ResultadoTiket listadoTicket(FiltraTickettRequest filtraticketrequest)
        {
            return Channel.listadoTicket(filtraticketrequest);
        }


       public ResultadoDetalleTiket listadoDetalleTicket(FiltraDetalleTicketRequest filtradetalleticketrequest)
       {
           return Channel.listadoDetalleTicket(filtradetalleticketrequest);
       }

      
       public  ResultadoUpdateTicket UpdateTicket(ResultadoUpdateTicketRequest resultadoUpdateticketrequest)
       {
           return Channel.UpdateTicket(resultadoUpdateticketrequest);
       }

    }
}
