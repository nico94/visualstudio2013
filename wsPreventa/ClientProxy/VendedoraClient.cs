﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ClientProxy
{
    public class VendedoraClient :ClientBase<IVendedoraService>, IVendedoraService
    {
        public VendedoraClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }    

        public ResultadoVendedora GetVendedoras(GetVendedorasRequest getvendedorasRequest)
        {
            return Channel.GetVendedoras(getvendedorasRequest);
        }
    }
}
