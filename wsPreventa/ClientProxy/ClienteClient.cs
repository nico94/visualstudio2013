﻿using Contract;
using Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using DataModel;


namespace ClientProxy
{
    public class ClienteClient : ClientBase<Contract.IClienteService>, IClienteService
    {
        public ClienteClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public ResultadoCliente GetClientePorRut (GetClientePorRutRequest getclienteporrutrequest)
        {
            return Channel.GetClientePorRut(getclienteporrutrequest);
        }

        public ResultadoCliente AddCliente(AddClienteRequest addclienterequest)
        {
            return Channel.AddCliente(addclienterequest);
        }

        public ResultadoCiudad GetCiudades(GetCiudadesRequest getciudadesrequest)
        {
            return Channel.GetCiudades(getciudadesrequest);
        }
        public ResultadoRegion GetRegiones(GetRegionesRequest getregionesrequest)
        {
            return Channel.GetRegiones(getregionesrequest);
        }
    }
}
