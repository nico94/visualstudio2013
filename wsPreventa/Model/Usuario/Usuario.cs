﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Usuario
{
 
        [DataContract]
        public class Usuario : ICloneable
        {
            [DataMember] 
            public string Apellido { get; set; }

            [DataMember]
            public long CodCargo { get; set; }

            [DataMember]
            public string Email { get; set; }

            [DataMember]
            public string Id { get; set; }

            [DataMember]
            public string Nombre { get; set; }

            [DataMember]
            public string Rut { get; set; }

            [DataMember]
            public string Telefono { get; set; }

            [DataMember]
            public long IdPersona { get; set; }

            [DataMember]
            public decimal IdPerfil { get; set; }

            [DataMember]
            public string NombreCompleto { get { return this.Nombre + " " + this.Apellido; } set { } }

            [DataMember]
            public List<long> CargosFuncionalidad { get; set; }

            #region ICloneable Members

            object ICloneable.Clone()
            {
                return CloneImpl();
            }
            public Usuario Clone()
            {
                return CloneImpl();
            }
            private Usuario CloneImpl()
            {
                var copy = (Usuario)MemberwiseClone();

                // Deep-copy children
                //if (FormasDePago != null)
                //    copy.FormasDePago = FormasDePago.Select(c => c.Clone()).ToList();

                return copy;
            }

            #endregion
        }

}
