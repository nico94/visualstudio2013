﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Usuario
{
    [DataContract]
    public class Funcionalidades
    {
        [DataMember]
        public long Id { set; get; }

        [DataMember]
        public string Nombre { set; get; }

        [DataMember]
        public string Codigo { set; get; }

        [DataMember]
        public List<Accion> Acciones { set; get; }
    }
}
