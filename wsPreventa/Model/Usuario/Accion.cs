﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Usuario
{
    [DataContract]
    public class Accion
    {
        [DataMember]
        public long Id { set; get; }

        [DataMember]
        public string Nombre { set; get; }

        [DataMember]
        public string Codigo { set; get; }
    }
}
