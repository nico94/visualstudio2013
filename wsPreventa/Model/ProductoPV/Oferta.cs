﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.ProductoPV
{
    [DataContract]
    public class Oferta
    {
        [DataMember]
        public string oferta { get; set; }

        [DataMember]
        public double precio { get; set; }

        [DataMember]
        public string codigo { get; set; }
    }
}
