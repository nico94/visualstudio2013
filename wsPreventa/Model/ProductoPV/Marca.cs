﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.producto
{
    [DataContract]
    public class Marca: ICloneable
    {
        [DataMember]
        public long cod_marca { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Marca Clone()
        {
            return CloneImpl();
        }

        private Marca CloneImpl()
        {
            var copy = (Marca)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
