﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.ProductoPV
{
    
    [DataContract]
    public class StockCritico
    {
        [DataMember]
        public string Producto { get; set; }

        [DataMember]
        public double Stock { get; set; }

        [DataMember]
        public double Stock_critico { get; set; }

    }

}
