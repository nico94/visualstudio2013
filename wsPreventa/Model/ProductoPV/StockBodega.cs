﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.ProductoPV
{
    [DataContract]
    public class StockBodega
    {
        [DataMember]
        public string Bodega { get; set; }

        [DataMember]
        public int Cantidad { get; set; }

        [DataMember]
        public string Producto { get; set; }

        [DataMember]
        public string cod_producto{get;set;}
    }

   
}
