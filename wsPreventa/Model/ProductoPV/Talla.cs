﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.producto
{
    [DataContract]
    public class Talla:ICloneable
    {
        [DataMember]
        public long cod_talla { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Talla Clone()
        {
            return CloneImpl();
        }

        private Talla CloneImpl()
        {
            var copy = (Talla)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
