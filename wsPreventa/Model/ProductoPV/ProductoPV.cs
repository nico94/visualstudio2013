﻿using Model.producto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.ProductoPV
{   
    [DataContract]
    public class ProductoPV : ICloneable
    {
        [DataMember]
        public string Cod_producto { get; set; }

        [DataMember]
        public string Cod_producto1 { get; set; }

        [DataMember]
        public string Cod_producto2 { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public decimal cantidad { get; set; }

        [DataMember]
        public decimal precio { get; set; }

        [DataMember]
        public decimal totalProducto
        {
            get
            {
                var monto = precio * cantidad;
                return monto;
            }
            set{}
        }

        [DataMember]
        public string familia { get; set; }

        [DataMember]
        public string subfamilia { get; set; }

        [DataMember]
        public string  marca { get; set; }

        [DataMember]
        public string modelo { get; set; }
        
        [DataMember]
        public string color { get; set; }

        [DataMember]
        public string  talla { get; set; }

        [DataMember]
        public double stock { get; set; }

        [DataMember]
        public double stock_critico { get; set; }

        [DataMember]
        public bool Seleccionar { get; set; }

        [DataMember]
        public double stock_tienda { get; set; }

        [DataMember]
        public List<StockBodega> stockProducto { get; set; }

        #region
        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public ProductoPV Clone()
        {
            return CloneImpl();
        }

        private ProductoPV CloneImpl()
        {
            var copy = (ProductoPV)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
