﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.producto
{
    [DataContract]
    public class Color: ICloneable
    {
        [DataMember]
        public long cod_color { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Color Clone()
        {
            return CloneImpl();
        }

        private Color CloneImpl()
        {
            var copy = (Color)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
