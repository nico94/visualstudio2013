﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.producto
{
    [DataContract]
    public class Familia: ICloneable
    {
        [DataMember]
        public long cod_familia { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Familia Clone()
        {
            return CloneImpl();
        }

        private Familia CloneImpl()
        {
            var copy = (Familia)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
