﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.producto
{
    [DataContract]
    public class SubFamilia: ICloneable
    {
        [DataMember]
        public long cod_SubFamilia { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public SubFamilia Clone()
        {
            return CloneImpl();
        }

        private SubFamilia CloneImpl()
        {
            var copy = (SubFamilia)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
