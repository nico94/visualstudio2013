﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class Lote
    {
        [DataMember]
        public long numero { get; set; }

        [DataMember]
        public string num_lote { get; set; }

        [DataMember]
        public string tipo { get; set; }

        [DataMember]
        public string producto { get; set; }

        [DataMember]
        public string bodega { get; set; }

        [DataMember]
        public string seccion { get; set; }

        [DataMember]
        public double? cantidad { get; set; }

        [DataMember]
        public double? cantidad_origen { get; set; }
        
        [DataMember]
        public int? x { get; set; }

        [DataMember]
        public int? y { get; set; }

        [DataMember]
        public int? z { get; set; }

        [DataMember]
        public DateTime fechaVencimiento { get; set; }

        [DataMember]
        public double valor { get; set; }

        [DataMember]
        public string condicionProducto { get; set; }

        [DataMember]
        public Int64? cod_condicionproducto_condicionproducto { get; set; }

        [DataMember]
        public Int64? cod_condicionbodega_condicionbodega { get; set; }

        [DataMember]
        public string cod_bodega { get; set; }

        [DataMember]
        public string cod_seccion { get; set; }

        [DataMember]
        public string cod_producto { get; set; }

        [DataMember]
        public DateTime fechaCreacion { get; set; }

        [DataMember]
        public string num_loteSinLote { get; set; }

    }
}
