﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class Bodegas
    {
        [DataMember]
        public long cod_bodega { get; set; }

        [DataMember]
        public string nombre { get; set; }
    }
}
