﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class Det_OrdenDespacho
    {
        [DataMember]
        public string num_ordendespacho { get; set; }

        [DataMember]
        public string cod_bodega_bodega { get; set; }

        [DataMember]
        public string cod_producto_materiaprima { get; set; }

        [DataMember]
        public string num_lote_controlingresoinsumo { get; set; }

        [DataMember]
        public string num_cantidad_ordendespacho { get; set; }

        [DataMember]
        public string num_posicion_x { get; set; }

        [DataMember]
        public string num_posicion_y { get; set; }

        [DataMember]
        public string num_posicion_z { get; set; }

        [DataMember]
        public string Es_PickingAsignado { get; set; }

        [DataMember]
        public string num_costo { get; set; }

        [DataMember]
        public string cod_motivoOD { get; set; }

    }
}
