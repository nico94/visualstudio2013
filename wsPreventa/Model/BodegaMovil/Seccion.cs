﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class Seccion
    {
        [DataMember]
        public decimal cod_seccion { get; set; }

        [DataMember]
        public string nombre { get; set; }
    }
}
