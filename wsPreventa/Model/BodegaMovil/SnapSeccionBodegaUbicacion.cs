﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class SnapSeccionBodegaUbicacion
    {
        [DataMember]
        public int pos_x;

        [DataMember]
        public int pos_y;

        [DataMember]
        public int pos_z;

        [DataMember]
        public bool SeIncluye;
    }
}
