﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    public class Folio
    {

        [DataMember]
        public string cod_documentocomprageneral_documentocomprageneral { get; set; }

        [DataMember]
        public string des_nombre_proveedor { get; set; }

        [DataMember]
        public DateTime? dat_fechafactura_documentocomprageneral { get; set; }


        [DataMember]
        public string cod_proveedor_proveedor { get; set; }

        [DataMember]
        public Int64? cod_tipodocumento_tipodocumentoventa { get; set; }

        [DataMember]
        public string cod_empresa { get; set; }

        [DataMember]
        public Int64? cod_temporada_temporada { get; set; }
                
    }


    public class DetalleFolio
    {

        [DataMember]
        public string cod_producto_materiaprima { get; set; }

        [DataMember]
        public string des_nombre_materiaprima { get; set; }

        [DataMember]
        public double? Cantidad_Comprada { get; set; }

        [DataMember]
        public double? Cantidad_Falta { get; set; }

        [DataMember]
        public double? Cantidad_Lote { get; set; }
        

    }
        

    public class SeccionBod
    {
        
        [DataMember]
        public string des_nombre_seccionbodega { get; set; }
              


    }

    public class Condiciones_Bodegas
    {

        [DataMember]
        public Int64 cod_condicionbodega_condicionbodega { get; set; }

        [DataMember]
        public string des_descripcion_condicionbodega { get; set; }

    }


    public class Condiciones_Productos
    {

        [DataMember]
        public Int64 cod_condicionproducto_condicionproducto { get; set; }

        [DataMember]
        public string des_descripcion_condicionproducto { get; set; }

        [DataMember]
        public bool? bit_estadoproducto_CondicionesProductos { get; set; }

    }

    public class Empresas_Holding
    {

        [DataMember]
        public string cod_empresa_empresa { get; set; }

        [DataMember]
        public string des_nombre_empresa { get; set; }    

    }

    public class Sucursal_empresa
    {

        [DataMember]
        public decimal id_empresa_sucursal { get; set; }

        [DataMember]
        public string des_sucursal { get; set; }

    }

    //public class Enc_TomaInventario
    //{
    //    [DataMember]
    //    public long Cod_tomainventario { get; set; }

    //    [DataMember]
    //    public long Num_doc_tomainventario { get; set; }

    //    [DataMember]
    //    public DateTime Dat_fecha_tomainventario { get; set; }

    //    [DataMember]
    //    public long cod_fichapersonal_fichapersonal { get; set; }

    //    [DataMember]
    //    public string cod_empresa_empresa { get; set; }

    //    [DataMember]
    //    public long id_empresa_sucursal { get; set; }

    //    [DataMember]
    //    public long cod_bodega_bodega { get; set; }

    //    [DataMember]
    //    public bool Ajusta_soloCapturado { get; set; }        
    //}
}
