﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    [DataContract]
    public class Motivo_OD
    {
        [DataMember]
        public long cod_motivoOD { get; set; }

        [DataMember]
        public string des_motivoOD { get; set; }

    }
}
