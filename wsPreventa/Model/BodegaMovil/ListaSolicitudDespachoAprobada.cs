﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.BodegaMovil
{
    public class ListaSolicitudDespachoAprobada
    {
        [DataMember]
        public long id_empresa_sucursal { get; set; }
        [DataMember]
        public string des_sucursal { get; set; }
        [DataMember]
        public long num_solicituddespacho_id { get; set; }
        [DataMember]
        public int prioridad { get; set; }
        [DataMember]
        public int dias_espera { get; set; }
        [DataMember]
        public string cod_empresa_empresa { get; set; }
    }

    public class ListaSolicitudDespachoAprobadaDespachar
    {
        [DataMember]
        public string cod_producto_materiaprima { get; set; }
        [DataMember]
        public string des_producto_materiaprima { get; set; }
        [DataMember]
        public long id_empresa_sucursalSolicita { get; set; }
        [DataMember]
        public long id_empresa_sucursalaDespachar { get; set; }
        [DataMember]
        public string des_sucursalaDespachar { get; set; }
        [DataMember]
        public long num_solicituddespacho_id { get; set; }
        [DataMember]
        public long num_solicituddespacho_cantaprobada { get; set; }
        [DataMember]
        public long num_cantidadordendespachar { get; set; }
        [DataMember]
        public double num_valor { get; set; }     

    }

    public class ListaDetalleOrden
    {
        [DataMember]
        public string cod_producto_materiaprima { get; set; }
        [DataMember]
        public string des_producto_materiaprima { get; set; }
        [DataMember]
        public long num_ordendespacho { get; set; }
        [DataMember]
        public long num_cantidad_ordendespacho { get; set; }
        [DataMember]
        public long? num_cantidad_ordendespachoConfirmada { get; set; }

    }
}
