﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class DetalleAgranda : ICloneable
    {
        [DataMember]
        public Producto AProducto { get; set; }

        [DataMember]
        public string DesdeProducto { get; set; }

        [DataMember]
        public decimal Precio { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public DetalleAgranda Clone()
        {
            return CloneImpl();
        }

        private DetalleAgranda CloneImpl()
        {
            var copy = (DetalleAgranda)MemberwiseClone();

            // Deep-copy children
            if (AProducto != null)
                copy.AProducto = AProducto.Clone();

            return copy;
        }

        #endregion
    }
}
