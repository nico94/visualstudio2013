﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class DetalleVenta : ICloneable
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public decimal Cantidad { get; set; }

        [DataMember]
        public Producto Producto { get; set; }

        [DataMember]
        public decimal ValorNeto { get; set; }

        [DataMember]
        public decimal ValorDescuento { get; set; }

        [DataMember]
        public bool bit_tiene_ticketcambio { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public DetalleVenta Clone()
        {
            return CloneImpl();
        }

        private DetalleVenta CloneImpl()
        {
            var copy = (DetalleVenta)MemberwiseClone();

            // Deep-copy children
            if (Producto != null)
                copy.Producto = Producto.Clone();

            return copy;
        }


        #endregion
    }
}
