﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class MedioPago
    {
        [DataMember]
        public string CodCliente { get; set; }

        [DataMember]
        public long CodTipoDocumento { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }
    }
}
