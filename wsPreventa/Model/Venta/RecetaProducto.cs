﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class RecetaProducto : ICloneable
    {
        [DataMember]
        public decimal Cantidad { get; set; }

        [DataMember]
        public decimal Id { get; set; }

        [DataMember]
        public decimal IdSucursal { get; set; }

        [DataMember]
        public MateriaPrima MateriaPrima { get; set; }

        [DataMember]
        public bool PuedeCambiar { get; set; }

        [DataMember]
        public bool PuedeRemover { get; set; }

        [DataMember]
        public bool Removido { get; set; }

        [DataMember]
        public ReemplazoMateriaPrima ReemplazadaPor { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public RecetaProducto Clone()
        {
            return CloneImpl();
        }

        private RecetaProducto CloneImpl()
        {
            var copy = (RecetaProducto)MemberwiseClone();

            // Deep-copy children
            if (MateriaPrima != null)
                copy.MateriaPrima = MateriaPrima.Clone();
            if (ReemplazadaPor != null)
                copy.ReemplazadaPor = ReemplazadaPor.Clone();

            return copy;
        }
        #endregion
    }
}
