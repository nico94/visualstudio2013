﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model.Venta
{
    [DataContract]
    public class AnticipoCliente
    {
        [DataMember]
        public long monto { get; set; }

        [DataMember]
        public string cliente { get; set; }

        [DataMember]
        public long Sucursal { get; set; }
    }
}
