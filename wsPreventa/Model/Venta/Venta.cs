﻿using Model.Cliente;
//using Model.Venta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Pall.GestionComercial.Model.Venta

{
    [DataContract]
    public class Venta : ICloneable
    {
        [DataMember]
        public bool Anulada { get; set; }

        [DataMember]
        public Cliente Cliente { get; set; }
        
        [DataMember]
        public List<DetalleVenta> Detalle { get; set; }

        [DataMember]
        public DateTime FechaEntrega { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public long IdOC { get; set; }

        [DataMember]
        public long IdCajaPOS { get; set; }

        [DataMember]
        public List<FormaDePago> FormasDePago { get; set; }

        [DataMember]
        public string Mesa { get; set; }

        [DataMember]
        public string NombreVendedor { get; set; }

        [DataMember]
        public long NumeroDocumento { get; set; }

        [DataMember]
        public string Observaciones { get; set; }

        [DataMember]
        public long TipoDocumento { get; set; }

        [DataMember]
        public decimal TotalDescuentos { get; set; }

        [DataMember]
        public decimal TotalIVA { get; set; }

        [DataMember]
        public decimal TotalBruto { get; set; }

        [DataMember]
        public decimal TotalPagado { get; set; }

        [DataMember]
        public string Vendedor { get; set; }

        [DataMember]
        public decimal Vuelto { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }
        public Venta Clone()
        {
            return CloneImpl();
        }

        private Venta CloneImpl()
        {
            var copy = (Venta)MemberwiseClone();

            // Deep-copy children
            if (FormasDePago != null)
                copy.FormasDePago = FormasDePago.Select(c => c.Clone()).ToList();
            if (Detalle != null)
                copy.Detalle = Detalle.Select(c => c.Clone()).ToList();
            if (Cliente != null)
                copy.Cliente = Cliente.Clone();

            return copy;
        }

        #endregion
    }
}
