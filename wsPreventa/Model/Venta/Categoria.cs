﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    public class Categoria
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
}
