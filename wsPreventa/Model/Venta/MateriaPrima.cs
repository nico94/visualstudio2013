﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class MateriaPrima : ICloneable
    {
        [DataMember]
        public string Id { set; get; }

        [DataMember]
        public string Nombre { set; get; }

        [DataMember]
        public List<ReemplazoMateriaPrima> Reemplazos { set; get; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public MateriaPrima Clone()
        {
            return CloneImpl();
        }

        private MateriaPrima CloneImpl()
        {
            var copy = (MateriaPrima)MemberwiseClone();

            if (Reemplazos != null)
                copy.Reemplazos = this.Reemplazos.Select(c => c.Clone()).ToList();

            return copy;
        }
        #endregion
    }
}
