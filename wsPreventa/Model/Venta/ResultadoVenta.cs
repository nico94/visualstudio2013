﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class ResultadoVenta
    {
        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long NumeroDocumento { get; set; }

        [DataMember]
        public string Observaciones { get; set; }

        [DataMember]
        public Sucursal Sucursal { get; set; }

        [DataMember]
        public long TipoDocumento { get; set; }
    }
}
