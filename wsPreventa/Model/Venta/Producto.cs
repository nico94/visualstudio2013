﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class Producto : ICloneable
    {
        [DataMember]
        public string _id { get; set; }

        [DataMember]
        public string _idPadreOferta { get; set; }

        [DataMember]
        public bool Activo { get; set; }

        [DataMember]
        public OpcionAgranda AgrandaSeleccionado { get; set; }

        [DataMember]
        public decimal Cantidad { get; set; }

        [DataMember]
        public List<Caracteristica> Caracteristicas { get; set; }

        [DataMember]
        public bool EsCombo { get; set; }

        [DataMember]
        public bool EsFavorito { get; set; }

        [DataMember]
        public bool EsProductoEspecial { get; set; }

        [DataMember]
        public Familia Familia { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public Categoria Marca { get; set; }

        [DataMember]
        public bool MuestraProducto { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public bool ParaLlevar { get; set; }

        [DataMember]
        public decimal Precio { get; set; }

        [DataMember]
        public List<Producto> OfertasProducto { get; set; }

        [DataMember]
        public List<Producto> OfertaSeleccionada { get; set; }

        [DataMember]
        public List<OpcionAgranda> OpcionesAgranda { get; set; }

        [DataMember]
        public List<Producto> ProductosEspeciales { get; set; }

        [DataMember]
        public List<Producto> ProductosRelacionados { get; set; }

        [DataMember]
        public List<RecetaProducto> Receta { get; set; }

        [DataMember]
        public bool Seleccionado { get; set; }

        [DataMember]
        public Categoria SubFamilia { get; set; }

        [DataMember]
        public bool TieneAgranda { get; set; }

        [DataMember]
        public decimal TotalProducto
        {
            get
            {
                decimal monto = this.Cantidad * this.Precio;
                if (this.AgrandaSeleccionado != null)
                {
                    monto += this.AgrandaSeleccionado.Precio * this.Cantidad;
                }
                if (this.OfertaSeleccionada != null && this.OfertaSeleccionada.Count > 0)
                {
                    monto += this.OfertaSeleccionada[0].Precio * this.Cantidad;
                }
                return monto;
            }
            set { }
        }

        [DataMember]
        public int Orden { get; set; }

        [DataMember]
        public bool ImprimeTicket { get; set; }

        [DataMember]
        public bool TicketCambio { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Producto Clone()
        {
            return CloneImpl();
        }

        private Producto CloneImpl()
        {
            var copy = (Producto)MemberwiseClone();

            // Deep-copy children
            if (Caracteristicas != null)
                copy.Caracteristicas = Caracteristicas.Select(c => c.Clone()).ToList();
            if (ProductosEspeciales != null)
                copy.ProductosEspeciales = ProductosEspeciales.Select(c => c.Clone()).ToList();
            if (ProductosRelacionados != null)
                copy.ProductosRelacionados = ProductosRelacionados.Select(c => c.Clone()).ToList();
            if (Receta != null)
                copy.Receta = Receta.Select(c => c.Clone()).ToList();
            if (OpcionesAgranda != null)
                copy.OpcionesAgranda = OpcionesAgranda.Select(c => c.Clone()).ToList();
            if (OfertasProducto != null)
                copy.OfertasProducto = OfertasProducto.Select(c => c.Clone()).ToList();
            if (OfertaSeleccionada != null)
                copy.OfertaSeleccionada = OfertaSeleccionada.Select(c => c.Clone()).ToList();

            return copy;
        }

        #endregion
    }
}
