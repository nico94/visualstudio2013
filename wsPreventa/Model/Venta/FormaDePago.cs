﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class FormaDePago : ICloneable
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public decimal Monto { get; set; }

        [DataMember]
        public decimal MontoOriginal { get; set; }

        [DataMember]
        public string NombreFormaPago { get; set; }

        [DataMember]
        public long TipoFormaPago { get; set; }

        [DataMember]
        public bool Mostrar { get; set; }

        [DataMember]
        public long ReferenciaExterna { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public FormaDePago Clone()
        {
            return CloneImpl();
        }

        private FormaDePago CloneImpl()
        {
            var copy = (FormaDePago)MemberwiseClone();

           
            return copy;
        }

        #endregion
    }
}
