﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class OpcionAgranda : ICloneable
    {
        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public List<DetalleAgranda> DetallesAgranda { get; set; }

        [DataMember]
        public decimal Precio { get; set; }

        [DataMember]
        public string CodProductoAgranda { get; set; }


        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public OpcionAgranda Clone()
        {
            return CloneImpl();
        }

        private OpcionAgranda CloneImpl()
        {
            var copy = (OpcionAgranda)MemberwiseClone();

            // Deep-copy children
            if (DetallesAgranda != null)
                copy.DetallesAgranda = DetallesAgranda.Select(c => c.Clone()).ToList();

            return copy;
        }

        #endregion
    }
}
