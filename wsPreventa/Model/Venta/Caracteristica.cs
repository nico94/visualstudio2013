﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class Caracteristica : ICloneable
    {
        [DataMember]
        public string Id { set; get; }

        [DataMember]
        public string Nombre { set; get; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Caracteristica Clone()
        {
            return CloneImpl();
        }

        private Caracteristica CloneImpl()
        {
            var copy = (Caracteristica)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
