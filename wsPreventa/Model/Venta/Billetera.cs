﻿using Pall.GestionComercial.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Venta
{
    public class Billetera
    {
        private List<FormaDePago> pagos;
        private int cantPagos;
        private decimal total;

        public Billetera()
        {
            limpiar();
        }

        //Metodos
        public void limpiar()
        {
            total = 0;
            cantPagos = 0;
            pagos = new List<FormaDePago>();

            //canasta vacia
            if (BilleteraVaciaEvent != null)
                BilleteraVaciaEvent();

            //actualizar total
            if (ListaActualizadaEvent != null)
                ListaActualizadaEvent(total, cantPagos);
        }

        public string ingresarPago(FormaDePago fdpParam, bool actualizado)
        {
            //agregarlo a la lista
            var prod = from p in pagos
                       where p.Id == fdpParam.Id
                       select p;

            string _id = "";

            if (prod.Count() == 0)
            {
                pagos.Add(fdpParam);

                total += fdpParam.Monto;
                cantPagos++;
            }
            else
            {
                bool agregado = false;
                for (int i = 0; i < pagos.Count; i++)
                {
                    if (pagos[i].TipoFormaPago == fdpParam.TipoFormaPago)
                    {
                        total -= pagos[i].Monto;
                        pagos[i].Monto += fdpParam.Monto;
                        total += pagos[i].Monto;

                        agregado = true;
                    }
                }
                if (!agregado)
                {
                    pagos.Add(fdpParam);

                    total += fdpParam.Monto;
                    cantPagos++;
                }
            }

            //actualizar total
            if (ListaActualizadaEvent != null)
                ListaActualizadaEvent(total, cantPagos);

            return _id;
        }

        public void borrarPago(FormaDePago pago)
        {
            //var prod = from p in pagos
            //           where p.Id == pago.Id
            //           select p;


            var p = pagos.FirstOrDefault(pa => pa.NombreFormaPago == pago.NombreFormaPago);
            if (p == null)
                return;


            //FormaDePago p = prod.First();
            //string idProd = p._id;

            //borrarlo de la lista
            pagos.Remove(p);

            //actualizar variables
            total -= pago.Monto;
            cantPagos--;

            //actualizar total
            if (ListaActualizadaEvent != null)
                ListaActualizadaEvent(total, cantPagos);

            //canasta vacia
            if (BilleteraVaciaEvent != null && cantPagos == 0)
                BilleteraVaciaEvent();
        }

        public List<FormaDePago> listaPagos()
        {
            List<FormaDePago> r = new List<FormaDePago>(pagos.ToList());

            return r;
        }

        public bool validar()
        {
            bool resp = false;

            if (cantPagos > 0 && total > 0)
                resp = true;

            return resp;
        }

        public decimal TotalPagado { get { return total; } }

        //  Eventos  //
        //lista actualizada
        public delegate void ListaActualizada(Decimal total, int cantidadPagos);
        public event ListaActualizada ListaActualizadaEvent;

        //canasta vacia
        public delegate void BilleteraVacia();
        public event BilleteraVacia BilleteraVaciaEvent;
    }
}
