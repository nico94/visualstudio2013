﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class ReemplazoMateriaPrima : ICloneable
    {
        [DataMember]
        public string IdMateriaPrima { set; get; }

        [DataMember]
        public decimal Cantidad { set; get; }

        [DataMember]
        public decimal Monto { set; get; }

        [DataMember]
        public string Nombre { set; get; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public ReemplazoMateriaPrima Clone()
        {
            return CloneImpl();
        }

        private ReemplazoMateriaPrima CloneImpl()
        {
            var copy = (ReemplazoMateriaPrima)MemberwiseClone();

            return copy;
        }

        #endregion
    }
}
