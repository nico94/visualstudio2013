﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class ResultadoPedido
    {
        [DataMember]
        public bool Valida { get; set; }

        [DataMember]
        public string ValidaMensaje { get; set; }

        [DataMember]
        public Pall.GestionComercial.Model.Venta.Venta Pedido { get; set; }

    }
}
