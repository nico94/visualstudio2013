﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model.Venta
{
    [DataContract]
    public class Cobranza : ICloneable
    {
        [DataMember]
        public double SucursalEmpresa { get; set; }

        [DataMember]
        public long NumeroCuota { get; set; }

        [DataMember]
        public long Cuotas { get; set; }

        [DataMember]
        public DateTime? FechaAbono { get; set; }

        [DataMember]
        public long saldo { get; set; }

        [DataMember]
        public long MontoAbonado { get; set; }

        [DataMember]
        public long Documento { get; set; }

        [DataMember]
        public long ValorCuota { get; set; }

        [DataMember]
        public long MedioPago { get; set; }

        [DataMember]
        public string Cliente { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }
        public Cobranza Clone()
        {
            return CloneImpl();
        }

        private Cobranza CloneImpl()
        {
            var copy = (Cobranza)MemberwiseClone();
            return copy;
        }

        #endregion
    }
}
