﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class OpcionesConfiguracion
    {
        [DataMember]
        public long IdCondicionEfectivo { get; set; }

        [DataMember]
        public long IdBoleta { get; set; }

        [DataMember]
        public long IdFactura { get; set; }

        [DataMember]
        public long IdGuia { get; set; }

        [DataMember]
        public long IdIVA { get; set; }

        [DataMember]
        public decimal PorcentajeIVA { get; set; }

        [DataMember]
        public long IdEstadoOCAprobada { get; set; }

        [DataMember]
        public long IdEstadoOPendiente { get; set; }
    }
}
