﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Empresa
{
    [DataContract]
    public class Sucursal : ICloneable
    {
        [DataMember]
        public Empresa Empresa { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public decimal Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Telefono { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Sucursal Clone()
        {
            return CloneImpl();
        }

        private Sucursal CloneImpl()
        {
            var copy = (Sucursal)MemberwiseClone();

            // Deep-copy children
            if (Empresa != null)
                copy.Empresa = Empresa.Clone();

            return copy;
        }
        #endregion
    }
}

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class Sucursal : ICloneable
    {
        [DataMember]
        public Empresa Empresa { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public decimal Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Telefono { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Sucursal Clone()
        {
            return CloneImpl();
        }

        private Sucursal CloneImpl()
        {
            var copy = (Sucursal)MemberwiseClone();

            // Deep-copy children
            if (Empresa != null)
                copy.Empresa = Empresa.Clone();

            return copy;
        }

        #endregion
    }
}
