﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Empresa
{
    [DataContract]
    public class Empresa : ICloneable
    {
        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Giro { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Empresa Clone()
        {
            return CloneImpl();
        }

        private Empresa CloneImpl()
        {
            var copy = (Empresa)MemberwiseClone();

            // Deep-copy children

            return copy;
        }
        #endregion
    }
}

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class Empresa : ICloneable
    {
        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Giro { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Empresa Clone()
        {
            return CloneImpl();
        }

        private Empresa CloneImpl()
        {
            var copy = (Empresa)MemberwiseClone();

            // Deep-copy children

            return copy;
        }

        #endregion
    }
}
