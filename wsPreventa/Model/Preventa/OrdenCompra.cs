﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Model.Cliente;

namespace Model.Preventa
{
    [DataContract]
    public class OrdenCompra : ICloneable
    {
        [DataMember]
        public long cod_ordencompra { get; set; }

        [DataMember]
        public bool? bit_tienestock_occliente { get; set; }

        [DataMember]
        public double? num_numoc_occliente { get; set; }

        [DataMember]
        public bool? bit_procesada_occliente { get; set; }

        [DataMember]
        public DateTime dat_fecha_occliente { get; set; }

        [DataMember]
        public DateTime dat_fechaentrega_occliente { get; set; }

        [DataMember]
        public double? num_ivapagado_occliente { get; set; }

        [DataMember]
        public double? num_otroimpuesto_occliente { get; set; }

        [DataMember]
        public double? num_neto_occliente { get; set; }

        [DataMember]
        public double? num_porotroimpuesto_occliente { get; set; }

        [DataMember]
        public double? num_impustoespecifico_occliente { get; set; }

        [DataMember]
        public double? num_descuento_occliente { get; set; }

        [DataMember]
        public string des_observacion_occliente { get; set; }

        [DataMember]
        public decimal? cod_sucursal_clientesucursal { get; set; }

        [DataMember]
        public Model.Cliente.Cliente cliente { get; set; }

        [DataMember]
        public string giro { get; set; }

        [DataMember]
        public long? cod_condicionpago_condicionpago { get; set; }

        [DataMember]
        public long? cod_estado_estadoordencompra { get; set; }

        [DataMember]
        public long cod_temporada_temporada { get; set; }

        [DataMember]
        public decimal id_empresa_sucursal { get; set; }

        [DataMember]
        public Model.Empresa.Empresa empresa { get; set; }

        [DataMember]
        public long cod_mediopago_mediopago { get; set; }

        [DataMember]
        public long? cod_fichapersonal_fichapersonal { get; set; }

        [DataMember]
        public string des_fichapersonal { get; set; }

        [DataMember]
        public long cod_id_pos { get; set; }

        [DataMember]
        public bool? bit_espreventa { get; set; }

        [DataMember]
        public bool? bit_generafactura { get; set; }

        [DataMember]
        public List<DetalleOrdenCompra> ListaDetalleOrdenCompra;

        public OrdenCompra()
        {
            ListaDetalleOrdenCompra = new List<DetalleOrdenCompra>();
        }

        #region Miembros de ICloneable

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public OrdenCompra Clone()
        {
            return CloneImpl();
        }

        private OrdenCompra CloneImpl()
        {
            var copy = (OrdenCompra)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
