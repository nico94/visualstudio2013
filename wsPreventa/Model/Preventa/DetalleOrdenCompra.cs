﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Model.ProductoPV;

namespace Model.Preventa
{
    [DataContract]
    public class DetalleOrdenCompra : ICloneable
    {
        [DataMember]
        public long cod_ordencompracliente_ordencompracliente { get; set; }

        [DataMember]
        public Model.ProductoPV.ProductoPV producto { get; set; }

        [DataMember]
        public double? num_cantidad_detalleocclientes { get; set; }

        [DataMember]
        public double? num_precio_detalleocclientes { get; set; }

        [DataMember]
        public double? num_descuento_detalleocclientes { get; set; }

        [DataMember]
        public long temporada { get; set; }

        [DataMember]
        public double? num_pordescuento_detalleocclientes { get; set; }

        [DataMember]
        public double? num_preciosindesc_detalleocclientes { get; set; }

        [DataMember]
        public long cod_detalleocclientes_detalleocclientes { get; set; }

        [DataMember]
        public Model.Empresa.Sucursal empresa_sucursal { get; set; }


        #region Miembros de ICloneable

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public DetalleOrdenCompra Clone()
        {
            return CloneImpl();
        }

        private DetalleOrdenCompra CloneImpl()
        {
            var copy = (DetalleOrdenCompra)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
