﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Caja
{
    [DataContract]
    public class Caja: ICloneable
    {
        [DataMember]
        public bool Activa { get; set; }

        //[DataMember]
        //public Cajero Cajero { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Caja Clone()
        {
            return CloneImpl();
        }

        private Caja CloneImpl()
        {
            var copy = (Caja)MemberwiseClone();

            // Deep-copy children
            //if (Cajero != null)
            //    copy.Cajero = this.Cajero.Clone();

            return copy;
        }


    }
}
