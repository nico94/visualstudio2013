﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Caja
{
    [DataContract]
    public class ResultadoCajaAbierta : ICloneable
    {
        [DataMember]
        public bool Abierta { get; set; }

        //[DataMember]
        //public Cajero Cajero { get; set; }

        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long IdPOS { get; set; }

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public ResultadoCajaAbierta Clone()
        {
            return CloneImpl();
        }

        private ResultadoCajaAbierta CloneImpl()
        {
            var copy = (ResultadoCajaAbierta)MemberwiseClone();

            // Deep-copy children
            //if (Cajero != null)
            //    copy.Cajero = this.Cajero.Clone();

            return copy;
        }
    }
}
