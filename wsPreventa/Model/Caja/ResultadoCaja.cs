﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Caja
{
    [DataContract]
    public class ResultadoCaja: ICloneable
    {
        [DataMember]
        public bool Abierta { get; set; }

        [DataMember]
        public string ValidaMensaje { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public ResultadoCaja Clone()
        {
            return CloneImpl();
        }

        private ResultadoCaja CloneImpl()
        {
            var copy = (ResultadoCaja)MemberwiseClone();
            return copy;
        }

        #endregion
    }
}
