﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Despacho
{
    public class Ticket
    {

        #region ticket 

                    [DataMember]
                    public long? cod_ordencompracliente_ordencompracliente { get; set; }

                    [DataMember]
                    public DateTime? dat_fecha_occliente { get; set; }

                    [DataMember]
                    public string Vendedor { get; set; }

                    [DataMember]
                    public bool bit_despachada_documentoventa { get; set; }

                    [DataMember]
                    public DateTime? dat_fecha_Despacho { get; set; }

        #endregion


    }
}
