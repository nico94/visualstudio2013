﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Despacho
{
    public class DetalleTicket
    {
              
        #region detalle ticket por despachar

                [DataMember]
                public string des_descripcion_productoterminado { get; set; }

                [DataMember]
                public double? num_cantidad_detdocumentoventa { get; set; }

                [DataMember]
                public long num_cantidad_detdocumentoventadespachada { get; set; }

                [DataMember]
                public string cod_productoterminado_productoterminado { get; set; }

                [DataMember]
                public string cod_productoterminado_productoterminado_2 { get; set; }

                [DataMember]
                public string cod_productoterminado_productoterminado_3 { get; set; }

                [DataMember]
                public long? cod_ordencompracliente_ordencompracliente { get; set; }
        
                [DataMember]
                public bool valido { get; set; }

                [DataMember]
                public string mensaje;



          
        #endregion
    }
}
