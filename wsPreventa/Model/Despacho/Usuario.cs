﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Despacho
{
    [DataContract]
    public class Usuario: ICloneable
    {
        [DataMember]
        public string Id_Usuario { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string apellido { get; set; }

        [DataMember]
        public long Id_FichaPersonal { get; set; }

        [DataMember]
        public string rut { get; set; }

        [DataMember]
        public long Cod_cargo { get; set; }


        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }
        public Usuario Clone()
        {
            return CloneImpl();
        }
        private Usuario CloneImpl()
        {
            var copy = (Usuario)MemberwiseClone();
            return copy;
        }

        #endregion

    }
}
