﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Model.ProductoPV;

namespace Model.Cotizacion
{
    [DataContract]
    public class DetalleCotizacion
    {
        [DataMember]
        public long cod_detalle { get; set; }

        [DataMember]
        public long cod_cotizacion { get; set; }

        [DataMember]
        public ProductoPV.ProductoPV producto { get; set; }

        [DataMember]
        public double cantidad { get; set; }

        [DataMember]
        public decimal precio { get; set; }

        [DataMember]
        public long temporada { get; set; }

        [DataMember]
        public long EmpresaSucursal { get; set; }
    }
}
