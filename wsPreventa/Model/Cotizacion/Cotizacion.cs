﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cotizacion
{
    [DataContract]
    public class Cotizacion
    {
        [DataMember]
        public long cod_cotizacion { get; set; }

        [DataMember]
        public DateTime fechaCotizacion { get; set; }

        [DataMember]
        public DateTime fechavencimiento { get; set; }

        [DataMember]
        public decimal total { get; set; }

        [DataMember]
        public string cliente { get; set; }

        [DataMember]
        public double SucursalCliente { get; set; }
        
        [DataMember]
        public string empresa { get; set; }

        [DataMember]
        public decimal SucursalEmpresa { get; set; }

        [DataMember]
        public long vendedora { get; set; }

        [DataMember]
        public List<DetalleCotizacion> listaDetalle { get; set;}

        public Cotizacion()
        {
            listaDetalle= new List<DetalleCotizacion>();
        }
    }
}
