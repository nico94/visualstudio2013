﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Envio_Correo
{
    public class DatosCorreo
    {
        [DataMember]
        public string correoTo { get; set; }

        [DataMember]
        public string correoFrom { get; set; }

        [DataMember]
        public string asunto { get; set; }

        [DataMember]
        public string usuarioEnvio { get; set; }
                           
    }

    public class DetalleCorreo
    {
       
        [DataMember]
        public string producto { get; set; }

        [DataMember]
        public double cantidad { get; set; }

    }

    public class correoComprador
    {

        [DataMember]
        public string correo { get; set; }
        

    }
}
 