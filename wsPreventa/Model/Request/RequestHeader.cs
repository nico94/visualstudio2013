﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Request
{
    [DataContract]
    public class RequestHeader
    {
        [DataMember]
        public long? IdPersona { set; get; }

        [DataMember(IsRequired = true)]
        public long IdDispositivo { set; get; }

        [DataMember(IsRequired = true)]
        public long Establecimiento { set; get; }
    }
}

//Para crear caja
namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class RequestHeader
    {
        [DataMember]
        public long IdPersona { set; get; }

        [DataMember]
        public string Usuario { set; get; }

        [DataMember(IsRequired = true)]
        public long IdDispositivo { set; get; }

        [DataMember(IsRequired = true)]
        public long Establecimiento { set; get; }
    }
}
