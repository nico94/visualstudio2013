﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Vendedora
{
    [DataContract]
    public class Vendedora : ICloneable
    {
        [DataMember]
        public long cod_vendedora { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Apellido { get; set; }

        [DataMember]
        public byte[] foto { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Vendedora Clone()
        {
            return CloneImpl();
        }

        private Vendedora CloneImpl()
        {
            var copy = (Vendedora)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
