﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Model.ControlIngresoInsumo
{
    public class ControlIngresoInsumo:ICloneable
    {
        [DataMember]
        public string num_lote_controlingresoinsumo { get; set; } //decimal

        [DataMember]
        public string cod_bodega_bodega { get; set; }//Int64

        [DataMember]
        public Int64 cod_snapcinsumo_snapcimsumo { get; set; }

        [DataMember]
        public string cod_producto_materiaprima { get; set; }

        [DataMember]
        public string cod_temporada_temporada { get; set; }//Int64

        [DataMember]
        public string cod_condicionproducto_condicionproducto { get; set; }//Int64?

        [DataMember]
        public string cod_condicionbodega_condicionbodega { get; set; }//Int64?

        [DataMember]
        public string dat_fechavencimiento_ControlIngresoInsumo { get; set; }//DateTime?

        [DataMember]
        public string num_cantidad_ControlIngresoInsumo { get; set; }//double

        [DataMember]
        public string num_valor_controlingresoinsumo { get; set; }//double

        [DataMember]
        public string num_horas_controlingresoinsumo { get; set; }//double?

        [DataMember]
        public string num_cantidadbultos_controlingresoinsumo { get; set; }//double?

        [DataMember]
        public string cod_productogenerico_materiaprima { get; set; }

        [DataMember]
        public string bit_analisisharina_controlingresoinsumo { get; set; }//bool

        [DataMember]
        public string cod_seccionbodega_seccionbodega { get; set; }//decimal?

        [DataMember]
        public string num_posicion_x { get; set; }//Int32

        [DataMember]
        public string num_posicion_y { get; set; }//Int32

        [DataMember]
        public string num_posicion_z { get; set; }//Int32



        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public ControlIngresoInsumo Clone()
        {
            return CloneImpl();
        }

        private ControlIngresoInsumo CloneImpl()
        {
            var copy = (ControlIngresoInsumo)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
