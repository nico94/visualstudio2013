﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Model.ControlIngresoInsumo
{
    [DataContract]
    public class snap_ControlIngresoInsumo 
    {

        //Nota: por alguna razón al trabajar desde la aplicación 

        [DataMember]
        public string cod_snapcinsumo_snapcimsumo { get; set; } // Int64

        [DataMember]
        public string cod_documentocomprageneral_documentocomprageneral { get; set; }

        [DataMember]
        public string cod_proveedor_proveedor { get; set; }

        [DataMember]
        public string cod_tipodocumento_tipodocumentoventa { get; set; } //Int64?

        [DataMember]
        public string cod_ordenmolienda_ordenmolienda { get; set; } //Int64?

        [DataMember]
        public string cod_opmpelaboradas_opmpelaboradas { get; set; }//Int64?
        
        [DataMember]
        public string cod_empresa_empresa { get; set; }

        [DataMember]
        public string cod_temporada_temporada { get; set; }//Int64
        
        [DataMember]
        public string cod_trigomolienda_trigomolienda { get; set; }//Int64?

        [DataMember]
        public string cod_correccion_correccionmatprima { get; set; }//Int64?

        [DataMember]
        public string dat_fecha_controlingresoinsumo { get; set; }//DateTime?

        [DataMember]
        public string dat_fecha_documentocompra { get; set; }//DateTime?


        //#region ICloneable Members

        //object ICloneable.Clone()
        //{
        //    return CloneImpl();
        //}

        //public snap_ControlIngresoInsumo Clone()
        //{
        //    return CloneImpl();
        //}

        //private snap_ControlIngresoInsumo CloneImpl()
        //{
        //    var copy = (snap_ControlIngresoInsumo)MemberwiseClone();

        //    return copy;
        //}
        //#endregion

    }
}
