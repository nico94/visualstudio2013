﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Model.ControlIngresoInsumo
{
    public class ControlIngresoInsumoProductoTerminado
    {
        [DataMember]
        public string num_lote { get; set; }//decimal

        [DataMember]
        public string cod_bodega_bodega { get; set; }//Int64

        [DataMember]
        public string cod_seccionbodega_seccionbodega { get; set; }//decimal?

        [DataMember]
        public string num_posicion_x { get; set; }//Int32?

        [DataMember]
        public string num_posicion_y { get; set; }//Int32? 

        [DataMember]
        public string num_posicion_z { get; set; }//Int32?

        [DataMember]
        public string cod_snapcinsumo_snapcimsumo { get; set; }//Int64?

        [DataMember]
        public string cod_productoterminado_productoterminado { get; set; }

        [DataMember]
        public string cod_temporada_temporada { get; set; }//Int64?

        [DataMember]
        public string cod_condicionproducto_condicionproducto { get; set; }//Int64?

        [DataMember]
        public string cod_condicionbodega_condicionbodega { get; set; }//Int64?

        [DataMember]
        public string cod_ordenproduccion_ordenproduccion { get; set; }//Int64?

        [DataMember]
        public string cod_recetaproductoterminado_recetaproductoterminado { get; set; }//Int64?
        
        [DataMember]
        public string dat_fechavencimiento { get; set; }//DateTime?

        [DataMember]
        public string num_cantidad { get; set; }//double?

        [DataMember]
        public string num_valor { get; set; }//double?

        [DataMember]
        public string num_horas { get; set; }//double?

        [DataMember]
        public string num_cantidadbultos { get; set; }//double?


      

      

    }
}
