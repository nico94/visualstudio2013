﻿using Model.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cliente
{
    [DataContract]
    public class Cliente : ICloneable
    {
        [DataMember]
        public string rut { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string telefono { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public Giro giro { get; set; }

        [DataMember]
        public Ciudad ciudad  { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public Comuna comuna { get; set; }

        [DataMember]
        public Sucursal sucursal { get; set; }

        [DataMember]
        public Region region { get; set; }
        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Cliente Clone()
        {
            return CloneImpl();
        }

        private Cliente CloneImpl()
        {
            var copy = (Cliente)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}

namespace Pall.GestionComercial.Model
{
    [DataContract]
    public class Cliente : ICloneable
    {
        [DataMember]
        public string Rut { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Telefono { get; set; }

        [DataMember]
        public string Correo { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public decimal MontoAprobadoCredito { get; set; }

        [DataMember]
        public long Giro { get; set; }

        [DataMember]
        public long IdSucursal { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Cliente Clone()
        {
            return CloneImpl();
        }

        private Cliente CloneImpl()
        {
            var copy = (Cliente)MemberwiseClone();

            // Deep-copy children
            //if (Caracteristicas != null)
            //    copy.Caracteristicas = Caracteristicas.Select(c => c.Clone()).ToList();

            return copy;
        }

        #endregion
    }
}


