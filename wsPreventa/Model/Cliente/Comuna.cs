﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cliente
{
    public class Comuna : ICloneable
    {
        [DataMember]
        public long Cod_comuna { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Comuna Clone()
        {
            return CloneImpl();
        }

        private Comuna CloneImpl()
        {
            var copy = (Comuna)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
