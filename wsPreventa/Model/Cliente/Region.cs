﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cliente
{
    [DataContract]
    public class Region
    {
        [DataMember]
        public string cod_region { get; set; }

        [DataMember]
        public string nombre { get; set; }
    }
}
