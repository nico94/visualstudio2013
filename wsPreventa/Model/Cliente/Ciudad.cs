﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cliente
{
    public class Ciudad :ICloneable
    {
        [DataMember]
        public long? cod_ciudad { get; set; }

        [DataMember]
        public string nombre { get; set; }
        
        #region ICloneable Members

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Ciudad Clone()
        {
            return CloneImpl();
        }

        private Ciudad CloneImpl()
        {
            var copy = (Ciudad)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
