﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Cliente
{
    public class Giro : ICloneable 
    {
        [DataMember]
        public long? cod_giro { get; set; }

        [DataMember]
        public string nombre { get; set; }

        #region Icloneable Menbers

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        public Giro Clone()
        {
            return CloneImpl();
        }

        private Giro CloneImpl()
        {
            var copy = (Giro)MemberwiseClone();

            return copy;
        }
        #endregion
    }
}
