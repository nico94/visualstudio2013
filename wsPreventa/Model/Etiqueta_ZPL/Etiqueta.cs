﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model.Etiqueta_ZPL
{
    public class Etiqueta
    {

        [DataMember]
        public long id_etiquetaZpl { get; set; }

        [DataMember]
        public string des_zpl { get; set; }

        [DataMember]
        public string glo_zpl { get; set; }

       
    }

    public class Codigos
    {


        [DataMember]
        public string codigo { get; set; }


    }

    public class Impresora
    {


        [DataMember]
        public int puerto { get; set; }

        [DataMember]
        public string ip { get; set; }



    }

}
