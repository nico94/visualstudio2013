﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel;
using Contract;
using Model;
using Model.Caja;
using System.Data.Entity;

namespace Service
{
    class CajaService : ICajaService
    {
       private DataModel.GestionComercialEntities gcEntities;
       


       public CajaService()
       {
           gcEntities = new GestionComercialEntities();
       }

       public ResultadoCaja GetEstadoCaja(GetEstadoCajaRequest getEstadoCajaRequest)
       {
            decimal suc = Convert.ToDecimal(getEstadoCajaRequest.requestHeader.Establecimiento);
            long idPOS = getEstadoCajaRequest.requestHeader.IdDispositivo;

            var hoy = DateTime.Today;

            var cajaL = gcEntities.Local_Caja.Where(m =>
                        m.Sucursal_empresa.id_empresa_sucursal == suc &&
                        m.POS.cod_id_pos == idPOS &&
                        m.bit_activa_localcaja == true);

            var res = new ResultadoCaja();
            
            res.Abierta = false;
            if (cajaL.Count() > 0)
            {
                var caja = cajaL.First();
                res.Abierta = true;
                res.ValidaMensaje = "caja abierta";
            }
            else
            {
                res.Abierta = false;
                res.ValidaMensaje = "la caja debe estar abierta para poder abrir este modulo";
            }
            return res;
        }
    }
}
