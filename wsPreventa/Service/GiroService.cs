﻿using Contract;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Cliente;
namespace Service
{
    public class GiroService: IGiroService
    {
        public ResultadoGiro GetGiros(GetGirosRequest getgirosrequest)
        {
            ResultadoGiro res = new ResultadoGiro();
            using (var db = new GestionComercialEntities())
            {
                var giros = db.Giros.ToList();
                foreach (var item in giros)
                {
                    var giro = new Giro();
                    giro.cod_giro = item.cod_giro_giro;
                    giro.nombre = item.des_giro_giro;
                    res.listaGiros.Add(giro);
                }
            }
            return res;
        }
    }
}
