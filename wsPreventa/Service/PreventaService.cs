﻿using Contract;
using DataModel;
using Model.Cliente;
using Model.Empresa;
using Model.Preventa;
using Model.ProductoPV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class PreventaService : IPreventaService
    {
        public ResultadoPreventa AddPreventa(AddPreventaRequest addpreventarequest)
        {
            using (var db = new GestionComercialEntities())
            {
                var res = new ResultadoPreventa();
                var objOrdencompra = new Ordenes_Compras_Clientes();

                objOrdencompra.bit_espreventa = false;
                objOrdencompra.bit_generafactura = addpreventarequest.occ.bit_generafactura;
                objOrdencompra.bit_tienestock_occliente = true;
                var suc = db.Clientes_Sucursales.First(s => s.cod_cliente_cliente == addpreventarequest.occ.cliente.rut);
                objOrdencompra.cod_sucursal_clientesucursal = suc.cod_sucursal_clientesucursal;
                objOrdencompra.cod_cliente_cliente = addpreventarequest.occ.cliente.rut;
                objOrdencompra.cod_condicionpago_condicionpago = 1;
                objOrdencompra.cod_estado_estadoordencompra = addpreventarequest.occ.cod_estado_estadoordencompra;
                objOrdencompra.cod_fichapersonal_fichapersonal = addpreventarequest.occ.cod_fichapersonal_fichapersonal;
                objOrdencompra.cod_id_pos = addpreventarequest.occ.cod_id_pos;
                objOrdencompra.cod_mediopago_mediopago = addpreventarequest.occ.cod_mediopago_mediopago;
                var temporada = db.Temporadas.First(t => t.bit_activa_temporada == true);
                objOrdencompra.cod_temporada_temporada = temporada.cod_temporada_temporada;
                objOrdencompra.dat_fecha_occliente = addpreventarequest.occ.dat_fecha_occliente;
                objOrdencompra.dat_fechaentrega_occliente = addpreventarequest.occ.dat_fechaentrega_occliente;
                objOrdencompra.des_observacion_occliente = addpreventarequest.occ.des_observacion_occliente;
                objOrdencompra.id_empresa_sucursal = addpreventarequest.occ.id_empresa_sucursal;
                objOrdencompra.num_descuento_occliente = addpreventarequest.occ.num_descuento_occliente;
                objOrdencompra.num_impustoespecifico_occliente = 0;
                objOrdencompra.num_ivapagado_occliente = addpreventarequest.occ.num_ivapagado_occliente;
                objOrdencompra.num_neto_occliente = addpreventarequest.occ.num_neto_occliente;
                objOrdencompra.num_numoc_occliente = addpreventarequest.occ.num_numoc_occliente;
                objOrdencompra.num_otroimpuesto_occliente = 0;
                objOrdencompra.num_porotroimpuesto_occliente = 0;
                var empresa = db.Sucursal_empresa.First(e => e.id_empresa_sucursal==addpreventarequest.occ.id_empresa_sucursal);
                objOrdencompra.cod_empresa_empresa = empresa.cod_empresa_empresa;

                db.Ordenes_Compras_Clientes.Add(objOrdencompra);
                db.SaveChanges();

                var codigo = objOrdencompra.cod_ordencompracliente_ordencompracliente;

                foreach (var item in addpreventarequest.occ.ListaDetalleOrdenCompra)
                {
                    var objDetalle = new Detalle_OrdenCompraClientes();
                    objDetalle.cod_ordencompracliente_ordencompracliente = codigo;
                    objDetalle.cod_productoterminado_productoterminado = item.producto.Cod_producto;
                    objDetalle.cod_temporada_temporada = item.temporada;
                    objDetalle.id_empresa_sucursal = item.empresa_sucursal.Id;
                    objDetalle.num_cantidad_detalleocclientes = item.num_cantidad_detalleocclientes;
                    objDetalle.num_descuento_detalleocclientes = item.num_descuento_detalleocclientes;
                    objDetalle.num_pordescuento_detalleocclientes = item.num_pordescuento_detalleocclientes;
                    objDetalle.num_precio_detalleocclientes = item.num_precio_detalleocclientes;
                    objDetalle.num_preciosindesc_detalleocclientes = item.num_preciosindesc_detalleocclientes;
                    objDetalle.id_empresa_sucursal = item.empresa_sucursal.Id;
                    db.Detalle_OrdenCompraClientes.Add(objDetalle);
                    db.SaveChanges();

                }
                addpreventarequest.occ.cod_ordencompra = codigo;
                res.ordenCompra = addpreventarequest.occ;
                res.mensaje = "Preventa realizada";
                return res;
            }
        }

        public ResultadoPulsera AddDescuento(AddDescuentoRequest adddescuentorequest)
        {
            ResultadoPulsera res = new ResultadoPulsera();
            using (var db = new GestionComercialEntities())
            {
                var cantidad = (from ficha in db.FichasPersonales
                                join opcion in db.Opciones on ficha.cod_cargo_cargo equals opcion.cod_jefelocal_opciones
                                where ficha.cod_pulsera_fichapersonal == adddescuentorequest.codigo
                                select ficha).Count();
                if (cantidad > 0)
                    res.valido = true;
                else
                    res.valido = false;
            }
            return res;
        }

        public ResultadoPreventa GetPreventa(GetPreventaRequest getPreVentaRequest)
        {
            ResultadoPreventa respuesta = new ResultadoPreventa();
            using (var db = new GestionComercialEntities())
            {
                var ordenCompra = db.Ordenes_Compras_Clientes.Include("Clientes_Sucursales").Include("Temporadas").Include("Medio_Pagos").Include("FichasPersonales").Include("Empresas_Holding").Include("POS").Include("Detalle_OrdenCompraClientes.Productos_Terminados").FirstOrDefault(
                    o => o.cod_ordencompracliente_ordencompracliente == getPreVentaRequest.codigo);
                if (ordenCompra == null)
                {

                    respuesta.ordenCompra = null;
                    respuesta.mensaje = "El id no existe";
                    return respuesta;
                }

                var documentoVenta = db.Documentos_Ventas.FirstOrDefault(d => d.cod_ordencompracliente_ordencompracliente
                    == getPreVentaRequest.codigo);

                if (documentoVenta != null)
                {
                    respuesta.ordenCompra = null;
                    respuesta.mensaje = "La preventa ya fue asociada a un documento de venta";
                    return respuesta;
                }

                if (ordenCompra.dat_fecha_occliente.Value.ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
                {

                    respuesta.ordenCompra = null;
                    respuesta.mensaje = "La preventa expiro su tiempo valido";
                    return respuesta;
                }


                //Armar obj orden compra cliente
                OrdenCompra objOrden = new OrdenCompra();

                objOrden.cod_ordencompra = ordenCompra.cod_ordencompracliente_ordencompracliente;
                objOrden.bit_tienestock_occliente = ordenCompra.bit_tienestock_occliente;
                objOrden.num_numoc_occliente = ordenCompra.num_numoc_occliente;
                objOrden.bit_procesada_occliente = ordenCompra.bit_procesada_occliente;
                objOrden.dat_fecha_occliente = (DateTime)ordenCompra.dat_fecha_occliente;
                objOrden.dat_fechaentrega_occliente = (DateTime)ordenCompra.dat_fechaentrega_occliente;
                objOrden.num_ivapagado_occliente = ordenCompra.num_ivapagado_occliente;
                objOrden.num_otroimpuesto_occliente = ordenCompra.num_otroimpuesto_occliente;
                objOrden.num_neto_occliente = ordenCompra.num_neto_occliente;
                objOrden.num_porotroimpuesto_occliente = ordenCompra.num_porotroimpuesto_occliente;
                objOrden.num_impustoespecifico_occliente = ordenCompra.num_impustoespecifico_occliente;
                objOrden.num_descuento_occliente = ordenCompra.num_descuento_occliente;
                objOrden.des_observacion_occliente = ordenCompra.des_observacion_occliente;
                objOrden.cod_sucursal_clientesucursal = ordenCompra.Clientes_Sucursales.cod_sucursal_clientesucursal;
                var cliente = db.Clientes.FirstOrDefault(c => c.cod_cliente_cliente == ordenCompra.Clientes_Sucursales.cod_cliente_cliente);
                Cliente cli = new Cliente();
                cli.rut = cliente.cod_cliente_cliente;
                cli.nombre = cliente.des_nombre_cliente;
                var giro = db.Giros.First(g => g.cod_giro_giro == cliente.cod_giro_giro);
                var gir = new Giro();
                gir.cod_giro = giro.cod_giro_giro;
                gir.nombre = giro.des_giro_giro;
                cli.giro = gir;
                objOrden.cliente = cli;
                objOrden.giro = giro.des_giro_giro;
                objOrden.cod_condicionpago_condicionpago = ordenCompra.cod_condicionpago_condicionpago;
                objOrden.cod_estado_estadoordencompra = ordenCompra.cod_estado_estadoordencompra;
                objOrden.cod_temporada_temporada = ordenCompra.Temporadas.cod_temporada_temporada;
                objOrden.id_empresa_sucursal = ordenCompra.id_empresa_sucursal;
                Empresa e = new Empresa();
                e.Id = ordenCompra.Empresas_Holding.cod_empresa_empresa;
                e.Nombre = ordenCompra.Empresas_Holding.des_nombre_empresa;
                objOrden.empresa = e;
                objOrden.cod_mediopago_mediopago = ordenCompra.Medio_Pagos.cod_mediopago_mediopago;
                if (ordenCompra.FichasPersonales != null)
                {
                    objOrden.cod_fichapersonal_fichapersonal = ordenCompra.FichasPersonales.cod_fichapersonal_fichapersonal;

                    objOrden.des_fichapersonal = ordenCompra.FichasPersonales.des_nombre_fichapersonal;
                }
                if (ordenCompra.POS != null)
                    objOrden.cod_id_pos = ordenCompra.POS.cod_id_pos;
                objOrden.bit_espreventa = ordenCompra.bit_espreventa;
                objOrden.bit_generafactura = ordenCompra.bit_generafactura;



                foreach (var item in ordenCompra.Detalle_OrdenCompraClientes)
                {
                    DetalleOrdenCompra objDetalle = new DetalleOrdenCompra();

                    objDetalle.cod_ordencompracliente_ordencompracliente = objOrden.cod_ordencompra;
                    ProductoPV p = new ProductoPV(); ;
                    p.Cod_producto = item.Productos_Terminados.cod_productoterminado_productoterminado;
                    p.nombre = item.Productos_Terminados.des_descripcion_productoterminado;
                    p.cantidad = (decimal)item.num_cantidad_detalleocclientes;
                    p.precio = (decimal)item.num_precio_detalleocclientes;
                    objDetalle.producto = p;
                    objDetalle.num_cantidad_detalleocclientes = item.num_cantidad_detalleocclientes;
                    objDetalle.num_precio_detalleocclientes = item.num_precio_detalleocclientes;
                    objDetalle.num_descuento_detalleocclientes = item.num_descuento_detalleocclientes;
                    objDetalle.temporada = item.Temporadas.cod_temporada_temporada;
                    objDetalle.num_pordescuento_detalleocclientes = item.num_pordescuento_detalleocclientes;
                    objDetalle.num_preciosindesc_detalleocclientes = item.num_preciosindesc_detalleocclientes;
                    objDetalle.cod_detalleocclientes_detalleocclientes = item.cod_detalleocclientes_detalleocclientes;
                    Sucursal s = new Sucursal();
                    s.Id = item.id_empresa_sucursal;
                    objDetalle.empresa_sucursal = (Sucursal)s;
                    objOrden.ListaDetalleOrdenCompra.Add(objDetalle);
                }
                respuesta.ordenCompra = objOrden;

                return respuesta;


            }
        }

        public ResultadoActualizacion UdpPreventa(UdpPreventaRequest udppreventarequest)
        {
            var res = new ResultadoActualizacion();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var detalles = db.Detalle_OrdenCompraClientes.Where(doc => doc.cod_ordencompracliente_ordencompracliente == udppreventarequest.codigo);

                    foreach (var item in detalles)
                    {
                        db.Detalle_OrdenCompraClientes.Remove(item);
                    }
                    db.SaveChanges();
                    foreach (var item in udppreventarequest.listaDetalleOrdenCompra)
                    {
                        var objDetalle = new Detalle_OrdenCompraClientes();
                        objDetalle.cod_ordencompracliente_ordencompracliente = udppreventarequest.codigo;
                        objDetalle.cod_productoterminado_productoterminado = item.producto.Cod_producto;
                        var temp = db.Temporadas.First(t => t.bit_activa_temporada == true);
                        objDetalle.cod_temporada_temporada = temp.cod_temporada_temporada;
                        objDetalle.id_empresa_sucursal = item.empresa_sucursal.Id;
                        objDetalle.num_cantidad_detalleocclientes = item.num_cantidad_detalleocclientes;
                        objDetalle.num_descuento_detalleocclientes = item.num_descuento_detalleocclientes;
                        objDetalle.num_pordescuento_detalleocclientes = item.num_pordescuento_detalleocclientes;
                        objDetalle.num_precio_detalleocclientes = item.num_precio_detalleocclientes;
                        objDetalle.num_preciosindesc_detalleocclientes = item.num_preciosindesc_detalleocclientes;
                        objDetalle.id_empresa_sucursal = item.empresa_sucursal.Id;
                        db.Detalle_OrdenCompraClientes.Add(objDetalle);
                        db.SaveChanges();

                    }
                    res.mensaje = "Preventa actualizada con exito";
                }
                catch (Exception ex)
                {
                    res.mensaje = "no se pudo actualizar la preventa";
                }
            }
            return res;
        }
    }
}
