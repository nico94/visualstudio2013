﻿using Contract;
using DataModel;
using Model.Cotizacion;
using Model.ProductoPV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class CotizacionService :ICotizacionService
    {
        public ResultadoCotizacion AddCotizacion(AddCotizacionRequest addCotizacionrequest)
        {
            var res = new ResultadoCotizacion();
            using(var db= new GestionComercialEntities())
            {
                try
                {
                    var cotizacion = new DataModel.Cotizacion();
                    cotizacion.cod_cliente_cliente = addCotizacionrequest.cotizacion.cliente;
                    var sucursal = db.Sucursal_empresa.First(s => s.id_empresa_sucursal == addCotizacionrequest.requestHeader.Establecimiento);
                    cotizacion.cod_empresa_empresa = sucursal.cod_empresa_empresa;
                    cotizacion.id_empresasucursal = sucursal.id_empresa_sucursal;
                    cotizacion.cod_fichapersonal_fichapersonal = addCotizacionrequest.cotizacion.vendedora;
                    var cliente=addCotizacionrequest.cotizacion.cliente.ToString();
                    var clientesuc= db.Clientes_Sucursales.First(c=>c.cod_cliente_cliente==cliente);
                    cotizacion.cod_sucursalcliente = clientesuc.cod_sucursal_clientesucursal;
                    cotizacion.dat_fechacotizacion_cotizacion = DateTime.Now;
                    cotizacion.dat_fechavencimiento_cotizacion = DateTime.Now.AddDays(30);
                    cotizacion.num_total_cotizacion = (double)addCotizacionrequest.cotizacion.total;
                    db.Cotizacion.Add(cotizacion);
                    db.SaveChanges();

                    var codigo = cotizacion.cod_cotizacion_cotizacion;

                    foreach (var item in addCotizacionrequest.cotizacion.listaDetalle)
                    {
                        var detalle = new Detalle_Cotizacion();
                        detalle.cod_cotizacion_cotizacion = codigo;
                        detalle.cod_poducto_productoterminado = item.producto.Cod_producto;
                        detalle.id_empresasucursal = addCotizacionrequest.requestHeader.Establecimiento;
                        detalle.num_cantidad_detallecotizacion = item.cantidad;
                        detalle.num_precio_detallecotizacion = (double)item.precio;
                        var temp = db.Temporadas.First(t => t.bit_activa_temporada == true);
                        detalle.cod_temporada_temporada = temp.cod_temporada_temporada;
                        detalle.Cotizacion = cotizacion;
                        db.Detalle_Cotizacion.Add(detalle);
                        db.SaveChanges();
                    }
                    res.valido = true;
                    addCotizacionrequest.cotizacion.cod_cotizacion = codigo;
                    res.cotizacion = addCotizacionrequest.cotizacion;
                }
                catch(Exception)
                {
                    res.cotizacion = null;
                    res.valido = false;
                }
            }
            return res;
        }

        public ResultadoCotizacion GetCotizacion(GetCotizacionRequest getcotizacionrequest)
        {
            var res = new ResultadoCotizacion();
            using (var db = new GestionComercialEntities())
            {
                var cot = new Model.Cotizacion.Cotizacion();
                var cotizacion = db.Cotizacion.FirstOrDefault(c => c.cod_cotizacion_cotizacion == getcotizacionrequest.cod_cotizacion);
                if (cotizacion == null)
                {
                    res.cotizacion = null;
                    res.valido = false;
                    res.mensaje = "Id no valido";
                }
                else
                {
                    if (cotizacion.dat_fechavencimiento_cotizacion < DateTime.Now)
                    {
                        res.mensaje = "Cotizacion expirada";
                        res.valido = false;
                        res.cotizacion = null;
                    }
                    else
                    {
                        int cont = 0;
                        var detalles = db.Detalle_Cotizacion.Where(d => d.cod_cotizacion_cotizacion == cotizacion.cod_cotizacion_cotizacion);
                        foreach (var item in detalles)
                        {

                            var detalle = new DetalleCotizacion();
                            detalle.cod_cotizacion = item.cod_cotizacion_cotizacion;
                            var producto = db.Productos_Terminados.First(p => p.cod_productoterminado_productoterminado == item.cod_poducto_productoterminado);
                            var prod = new ProductoPV();
                            prod.Cod_producto = producto.cod_productoterminado_productoterminado;
                            prod.cantidad = (decimal)item.num_cantidad_detallecotizacion;
                            prod.precio = (decimal)item.num_precio_detallecotizacion;
                            prod.nombre = producto.des_descripcion_productoterminado;
                            detalle.producto = prod;
                            detalle.EmpresaSucursal = item.id_empresasucursal;
                            if (item.num_cantidad_detallecotizacion != null)
                                detalle.cantidad = (double)item.num_cantidad_detallecotizacion;
                            detalle.precio = (decimal)item.num_precio_detallecotizacion;
                            detalle.temporada = (long)item.cod_temporada_temporada;
                            cot.listaDetalle.Add(detalle);
                        }
                        cot.cliente = cotizacion.cod_cliente_cliente;
                        cot.cod_cotizacion = cotizacion.cod_cotizacion_cotizacion;
                        cot.empresa = cotizacion.cod_empresa_empresa;
                        cot.fechaCotizacion = (DateTime)cotizacion.dat_fechacotizacion_cotizacion;
                        cot.fechavencimiento = (DateTime)cotizacion.dat_fechavencimiento_cotizacion;
                        cot.SucursalCliente = (double)cotizacion.cod_sucursalcliente;
                        cot.SucursalEmpresa = (decimal)cotizacion.id_empresasucursal;
                        cot.total = (decimal)cotizacion.num_total_cotizacion;
                        cot.vendedora = (long)cotizacion.cod_fichapersonal_fichapersonal;
                        res.valido = true;
                        res.mensaje = "Cotizacion encontrada";
                        res.cotizacion = cot;
                    }
                }
            }
            return res;
        }
    }
}
