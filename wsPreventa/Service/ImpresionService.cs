﻿using Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ImpresionService :IImpresionService
    {
        public ResultadoImpresion ImprimirZPL(ImprimirZPLRequest imprimirzplrequest)
        {
            var Ip=imprimirzplrequest.ip;
            var Puerto=imprimirzplrequest.puerto;
            var res = new ResultadoImpresion();
            TcpClient Zebraclient = new TcpClient();
            try
            {
                
                Zebraclient.SendTimeout = 2500;
                Zebraclient.ReceiveTimeout = 2500;
                Zebraclient.Connect(Ip, Puerto);

                if (Zebraclient.Connected == true)
                {

                    NetworkStream mynetworkstream;
                    StreamReader mystreamreader;
                    StreamWriter mystreamwriter;
                    mynetworkstream = Zebraclient.GetStream();
                    mystreamreader = new StreamReader(mynetworkstream);
                    mystreamwriter = new StreamWriter(mynetworkstream);
                    
                      
                        mystreamwriter.WriteLine(imprimirzplrequest.zpl);
                        mystreamwriter.Flush();


                    Zebraclient.Close();
                    res.realizado = true;
                    res.mensaje = "Impresion realizada con exito";
                }

            }
            catch
            {
                res.realizado = false;
                res.mensaje = "No se pudo realizar la impresion";
            }
            return res;
        }
    }
}
