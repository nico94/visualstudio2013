﻿using Contract;
using DataModel;
using Model.Etiqueta_ZPL;
using Model.producto;
using Model.ProductoPV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ProductoPVService :IProductoPVService 
    {

        //V_R_StockProductoTerminadoBodega bod = new V_R_StockProductoTerminadoBodega();

        public ProductoPVService()
        {
        }
        public ResultadoProductoPV GetProductoPorCodigo(GetProductoPorCodigoRequest getproductoporcodigorequest)
        {
            using (var db = new GestionComercialEntities())
            {
                var prod = (from producto in db.Productos_Terminados
                            join lista in db.Local_Lista_precio_vta on producto.cod_productoterminado_productoterminado equals lista.cod_productoterminado_productoterminado
                            join lspl in db.Local_snap_precio_lista on lista.id_local_lista_precio equals lspl.id_local_lista_precio
                            where lista.id_empresa_sucursal == getproductoporcodigorequest.requestHeader.Establecimiento && lspl.dat_fec_inicio < DateTime.Now && lspl.dat_fec_final > DateTime.Now && (producto.cod_productoterminado_productoterminado == getproductoporcodigorequest.codigo || producto.cod_productoterminado_productoterminado_2 == getproductoporcodigorequest.codigo || producto.cod_productoterminado_productoterminado_3 == getproductoporcodigorequest.codigo)
                            select new ProductoPV
                            {
                                Cod_producto = producto.cod_productoterminado_productoterminado,
                                Cod_producto1 = producto.cod_productoterminado_productoterminado_2,
                                Cod_producto2 = producto.cod_productoterminado_productoterminado_3,
                                nombre = producto.des_descripcion_productoterminado,
                                stock = (Double)((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0),
                                stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),
                                Seleccionar = (bool)(((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)),
                                precio = (decimal)lspl.Precio,
                                familia = producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados,
                                subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados,
                                marca = producto.Marca.des_marca_marca,
                                color = producto.Color_Producto.des_color_color,
                                talla = producto.Talla.des_talla_talla,
                            });


                var res = new ResultadoProductoPV();
                var Product = new ProductoPV();
                if (prod == null || prod.Count() == 0)
                {
                    var pp = db.Productos_Terminados.FirstOrDefault(p => p.cod_productoterminado_productoterminado == getproductoporcodigorequest.codigo);
                    if (pp != null)
                    {
                        res.valido = false;
                        res.producto = null;
                        res.mensaje = "El producto no tiene asociado un precio en esta sucursal";
                    }
                    else
                    {
                        res.valido = false;
                        res.producto = null;
                        res.mensaje = "El Producto no se encontro";
                    }
                }
                else
                {
                    foreach (var item in prod)
                    {
                        var bodega = db.Bodegas.FirstOrDefault(b => b.id_empresa_sucursal == getproductoporcodigorequest.requestHeader.Establecimiento && b.bit_estienda_bodega == true);
                        if (bodega != null)
                        {
                            var tienda = db.V_StockProductoTerminado.FirstOrDefault(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.cod_bodega_bodega == bodega.cod_bodega_bodega);
                            if (tienda.cantidad != null)
                                item.stock_tienda = (double)tienda.cantidad;
                        }
                    }
                    foreach (var item in prod)
                    {
                        Product.Cod_producto = item.Cod_producto;
                        Product.nombre = item.nombre;
                        Product.stock = item.stock;
                        Product.precio = item.precio;
                        Product.familia = item.familia;
                        Product.subfamilia = item.subfamilia;
                        Product.marca = item.marca;
                        Product.color = item.color;
                        Product.talla = item.talla;
                        Product.stock_tienda = item.stock_tienda;
                    }
                    res.valido = true;
                    res.producto = Product;
                    res.mensaje = "Producto Encontrado";
                }
                return res;
            }
        }

        public ResultadoProductos GetProductos(GetProductosRequest getproductosrequest)
        {
            ResultadoProductos res = new ResultadoProductos();
            //Carga todos los productos 
            if (getproductosrequest.codigo == "")
            {
                using (var db = new GestionComercialEntities())
                {
                                        
                    var x  = (from producto in db.Productos_Terminados
                                        join stock in db.V_R_StockProductoTerminadoBodega on producto.cod_productoterminado_productoterminado equals stock.cod_productoterminado_productoterminado
                                        where stock.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento 
                                        select new ProductoPV() {
                                            Cod_producto = producto.cod_productoterminado_productoterminado,
                                            Cod_producto1 = producto.cod_productoterminado_productoterminado_2,
                                            Cod_producto2 = producto.cod_productoterminado_productoterminado_3, 
                                            nombre = producto.des_descripcion_productoterminado,
                                            stock = (Double)((stock.num_cantidad != null) ? stock.num_cantidad : 0),
                                            stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),
                                            Seleccionar = (bool)(((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)),
                                            precio = (decimal)stock.Precio,
                                            familia=producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados, 
                                            subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados, 
                                            marca= producto.Marca.des_marca_marca, 
                                            color= producto.Color_Producto.des_color_color, 
                                            talla= producto.Talla.des_talla_talla,
                                            modelo=producto.Modelo.des_modelo_modelo}
                                            );

                                            res.ListaProductos = x.ToList().OrderByDescending(c => c.Seleccionar).ToList();
                                            foreach (var item in res.ListaProductos)
                                            {
                                                var bodega = db.Bodegas.FirstOrDefault(b => b.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento && b.bit_estienda_bodega == true);
                                                if (bodega != null)
                                                {
                                                    var tienda = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.cod_bodega_bodega==bodega.cod_bodega_bodega);
                                                    if (tienda != null)
                                                    {
                                                        if(tienda.num_cantidad!=null)
                                                        item.stock_tienda = (double)tienda.num_cantidad;
                                                    }
                                                }

                                                //var stockproducto = db.V_R_StockProductoTerminadoBodega.Where(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento);

                                                var stockproducto = (from producto in db.V_R_StockProductoTerminadoBodega
                                                                     where (producto.cod_productoterminado_productoterminado == item.Cod_producto || producto.cod_productoterminado_productoterminado == item.Cod_producto1 || producto.cod_productoterminado_productoterminado == item.Cod_producto2) && producto.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento
                                                                     select new stockProd()
                                                                     {
                                                                         des_nombre_bodega = producto.des_nombre_bodega,
                                                                         num_cantidad = producto.num_cantidad.Value,
                                                                         cod_productoterminado_productoterminado = producto.cod_productoterminado_productoterminado
                                                                     }


                                                ).ToList();
                                                
                                                var listabodega = new List<StockBodega>();
                                                if (stockproducto.Count() > 0)
                                                {
                                                    
                                                    foreach (var item2 in stockproducto)
                                                    {                                                        
                                                        var stockp = new StockBodega();
                                                        stockp.Bodega = item2.des_nombre_bodega;
                                                        if (item2.num_cantidad != null)
                                                            stockp.Cantidad = (int)item2.num_cantidad;
                                                        stockp.Producto = item2.cod_productoterminado_productoterminado;
                                                        stockp.cod_producto = item2.cod_productoterminado_productoterminado;
                                                        listabodega.Add(stockp);
                                                        item.stockProducto = listabodega;
                                                    }                                                    
                                                }
                                                else
                                                {
                                                    var stockbodega = new StockBodega();
                                                    stockbodega = null;
                                                    listabodega.Add(stockbodega);
                                                    item.stockProducto = listabodega;
                                                }
                                            }
                    
                    res.valido = true;
                    res.mensaje = "Productos Encontrados";
                }
            }
            else
            {
                using (var db = new GestionComercialEntities())
                {
                    var x = (from producto in db.Productos_Terminados
                                        join stock in db.V_R_StockProductoTerminadoBodega on producto.cod_productoterminado_productoterminado equals stock.cod_productoterminado_productoterminado
                                        where stock.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento && producto.cod_productoterminado_productoterminado.Contains(getproductosrequest.codigo)
                             select new ProductoPV()
                             {
                                 Cod_producto = producto.cod_productoterminado_productoterminado,
                                 Cod_producto1 = producto.cod_productoterminado_productoterminado_2,
                                 Cod_producto2 = producto.cod_productoterminado_productoterminado_3, 
                                 nombre = producto.des_descripcion_productoterminado,
                                 stock = (Double)((stock.num_cantidad != null) ? stock.num_cantidad : 0),
                                 stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),
                                 Seleccionar = (bool)(((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)),
                                 precio = (decimal)stock.Precio,
                                 familia = producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados,
                                 subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados,
                                 marca = producto.Marca.des_marca_marca,
                                 color = producto.Color_Producto.des_color_color,
                                 talla = producto.Talla.des_talla_talla,
                                 modelo = producto.Modelo.des_modelo_modelo
                             });

                    res.ListaProductos = x.ToList().OrderByDescending(c => c.Seleccionar).ToList();
                    
                    foreach (var item in res.ListaProductos)
                    {
                        var bodega = db.Bodegas.FirstOrDefault(b => b.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento && b.bit_estienda_bodega == true);
                        if (bodega != null)
                        {
                            var tienda = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.cod_bodega_bodega == bodega.cod_bodega_bodega);
                            if (tienda.num_cantidad != null)
                                item.stock_tienda = (double)tienda.num_cantidad;
                        }
                        var stockproducto = db.V_R_StockProductoTerminadoBodega.Where(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.id_empresa_sucursal == getproductosrequest.requestHeader.Establecimiento);
                        var listabodega = new List<StockBodega>();
                        if (stockproducto != null)
                        {
                            
                            foreach (var item2 in stockproducto)
                            {
                                var stockp = new StockBodega();
                                stockp.Bodega = item2.des_nombre_bodega;
                                if (item2.num_cantidad != null)
                                    stockp.Cantidad = (int)item2.num_cantidad;
                                stockp.Producto = item2.cod_productoterminado_productoterminado;
                                stockp.cod_producto = item2.cod_productoterminado_productoterminado;
                                listabodega.Add(stockp);
                            }
                            item.stockProducto = listabodega;
                        }
                        else
                        {
                            var stockbodega = new StockBodega();
                            stockbodega = null;
                            listabodega.Add(stockbodega);
                            item.stockProducto = listabodega;
                        }
                    }

                    res.ListaProductos = x.ToList(); res.valido = true;
                    res.mensaje = "Productos Encontrados";
                }
            }
            return res;
        }

        public ResultadoStock GetStockBodega(GetStockBodegaRequest getstockbodegarequest)
        {
            var res = new ResultadoStock();
            using(var db= new GestionComercialEntities())
            {
                var stock = db.V_StockProductoTerminado.Where(s=>s.cod_productoterminado_productoterminado==getstockbodegarequest.codigo && s.id_empresa_sucursal==getstockbodegarequest.requestHeader.Establecimiento);

                if (stock.Count() == 0)
                {
                    res.valido = false;
                }
                else
                {
                    foreach (var item in stock)
                    {
                        var stockbodega = new StockBodega();
                        stockbodega.Bodega = item.des_nombre_bodega;
                        stockbodega.Cantidad = int.Parse(item.cantidad.ToString());
                        res.listaStock.Add(stockbodega);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoStockCritico GetStockCritico(GetStockCriticoRequest getstockcriticorequest)
        {

            ResultadoStockCritico res = new ResultadoStockCritico();
            
            {
                using (var db = new GestionComercialEntities())
                {

                    var x = (from producto in db.Productos_Terminados
                                        join stock in db.V_R_StockProductoTerminadoBodega on producto.cod_productoterminado_productoterminado equals stock.cod_productoterminado_productoterminado
                             where stock.id_empresa_sucursal == getstockcriticorequest.requestHeader.Establecimiento && (Double)((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)
                                                          
                             select new StockCritico()
                             {
                                 Producto = producto.des_descripcion_productoterminado,
                                 Stock = (Double)((stock.num_cantidad != null) ? stock.num_cantidad : 0),
                                 Stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),                                 
                             }
                           );

                    res.listaStockCritico = x.ToList();

                    res.valido = true;
                    res.mensaje = "Productos Encontrados";
                    return res;
                }
            }
        }

        public ResultadoEtiqueta GetEtiqueta(GetEtiquetaRequest getetiquetarequest)
        {

            ResultadoEtiqueta res = new ResultadoEtiqueta();

            {
                using (var db = new GestionComercialEntities())

                if (getetiquetarequest.codigo>0)
 
                {

                    var x = (from etiqueta in db.Etiquetas_ZPL where etiqueta.id_etiquetaZPL == getetiquetarequest.codigo
                                                       
                             select new Etiqueta()
                             {
                                 id_etiquetaZpl = etiqueta.id_etiquetaZPL,
                                 des_zpl = etiqueta.des_zpl,
                                 glo_zpl = etiqueta.glo_zpl

                             }
                           );


                    res.listaEtiqueta = x.ToList();

                    res.valido = true;
                    return res;
                }
                else
                {
                    var x = (from etiqueta in db.Etiquetas_ZPL
                            select new Etiqueta()
                             {
                                 id_etiquetaZpl = etiqueta.id_etiquetaZPL,
                                 des_zpl = etiqueta.des_zpl,
                                 glo_zpl = etiqueta.glo_zpl

                             }
                           );

                    res.listaEtiqueta = x.ToList();

                    res.valido = true;
                    return res;

                }
            }
        }

        public ResultadoCodigos GetCodigos(GetCodigosRequest getcodigosrequest)
        {

            ResultadoCodigos res = new ResultadoCodigos();

            {
                using (var db = new GestionComercialEntities())
                                    
                    {

                        var x = (from codigo in db.V_CodigosProducto
                                 where codigo.cod_productoterminado_productoterminado == getcodigosrequest.codigo && codigo.cod_productoterminado_productoterminado_1 != null
                                                              
                                 select new Codigos()
                                 {
                                     codigo = codigo.cod_productoterminado_productoterminado_1
                                  
                                 }
                                                                  
                               );


                        res.listaCodigo = x.ToList();

                        res.valido = true;
                        return res;
                    }
                   
                    
            }
        }

        public ResultadoImpresora GetImpresora(GetImpresoraRequest getimpresorarequest)
        {
            ResultadoImpresora res = new ResultadoImpresora();

            {
                using (var db = new GestionComercialEntities())
                {

                    var x = (from imp in db.Impresoras
                             where imp.NombreImpresora == getimpresorarequest.nombre 

                             select new Impresora()
                             {
                                 ip = imp.Ip,
                                 puerto = imp.Puerto

                             }

                           );

                    res.listaImpresora = x.ToList();

                    res.valido = true;
                    return res;
                }


            }
        }

        public ResultadoOferta GetOfertasProducto(GetOfertasRequest getofertasrequest)
        {
            var res = new ResultadoOferta();
            using (var db = new GestionComercialEntities())
            {
                var oferta = db.Oferta_ProductoTerminado.Where(o => o.cod_productoterminado_productoterminado == getofertasrequest.codigo && o.bol_activa_oferta == true);
                if (oferta.Count() == 0)
                {
                    res.valido = false;
                    res.listaOfertas = null;
                }
                else
                {
                    foreach (var item in oferta)
                    {
                        var promo = new Oferta();
                        var prod = db.Productos_Terminados.First(p => p.cod_productoterminado_productoterminado == item.cod_productoterminado_oferta);
                        promo.oferta = prod.des_descripcion_productoterminado;
                        promo.precio = int.Parse(item.num_precio_prodoferta.ToString());
                        promo.codigo = prod.cod_productoterminado_productoterminado;
                        res.listaOfertas.Add(promo);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoProductos GetOfertas(GetOfertasLocalRequest getofertaslocalrequest)
        {
            var res = new ResultadoProductos();
            using (var db = new GestionComercialEntities())
            {
                var prod = (from producto in db.Productos_Terminados
                            join opcion in db.Opciones on producto.Familia_Productos_Terminados.cod_familiaproductosterminados_familiaproductosterminados equals opcion.cod_familiaoferta_opciones
                            join lista in db.Local_Lista_precio_vta on producto.cod_productoterminado_productoterminado equals lista.cod_productoterminado_productoterminado
                            join lspl in db.Local_snap_precio_lista on lista.id_local_lista_precio equals lspl.id_local_lista_precio
                            where lista.id_empresa_sucursal == getofertaslocalrequest.requestHeader.Establecimiento && lspl.dat_fec_inicio < DateTime.Now && lspl.dat_fec_final > DateTime.Now
                            select new ProductoPV
                            {
                                Cod_producto = producto.cod_productoterminado_productoterminado,
                                nombre = producto.des_descripcion_productoterminado,
                                stock = (Double)((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0),
                                precio = (decimal)lspl.Precio,
                                familia = producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados,
                                subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados,
                                marca = producto.Marca.des_marca_marca,
                                color = producto.Cortes_Productos.des_descripcion_corteproducto,
                                talla = producto.Pesos_Productos.des_descripcion_pesoproducto
                            });
                res.ListaProductos = prod.ToList();
                res.valido = true;
                res.mensaje = "Ofertas encontradas";
            }
            return res;
        }

        public ResultadoProductos GetStockProductosPorbodega(GetStockProductoPorBodegaRequest getstockproductoporbodegarequest)
        {
            var res = new ResultadoProductos();
            using (var db = new GestionComercialEntities())
            {


                //////////var stockproducto = db.V_R_StockProductoTerminadoBodega.Where(s => s.cod_productoterminado_productoterminado == item.Cod_producto && s.id_empresa_sucursal == getstockproductoporbodegarequest.Establecimiento && s.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega);
                //////////        var listabodega = new List<StockBodega>();
                //////////        if (stockproducto.Count() > 0)
                //////////        {

                //////////            foreach (var item2 in stockproducto)
                //////////            {

                //////////                var stockp = new StockBodega();
                //////////                stockp.Bodega = item2.des_nombre_bodega;
                //////////                if (item2.num_cantidad != null)
                //////////                    stockp.Cantidad = (int)item2.num_cantidad;
                //////////                stockp.Producto = item2.des_nombre_bodega;
                //////////                stockp.cod_producto = item2.cod_productoterminado_productoterminado;
                //////////                listabodega.Add(stockp);


                //////////            }
                //////////            item.stockProducto = listabodega;



                var x = (from producto in db.Productos_Terminados
                         join stock in db.V_R_StockProductoTerminadoBodega on producto.cod_productoterminado_productoterminado equals stock.cod_productoterminado_productoterminado
                         where stock.id_empresa_sucursal == getstockproductoporbodegarequest.Establecimiento && stock.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega

                         select new ProductoPV()
                         {
                             Cod_producto = producto.cod_productoterminado_productoterminado,
                             Cod_producto1 = producto.cod_productoterminado_productoterminado_2,
                             Cod_producto2 = producto.cod_productoterminado_productoterminado_3,
                             nombre = producto.des_descripcion_productoterminado,
                             stock = (Double)((stock.num_cantidad != null) ? stock.num_cantidad : 0),
                             stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),
                             Seleccionar = (bool)(((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)),
                             precio = (decimal)stock.Precio,
                             familia = producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados,
                             subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados,
                             marca = producto.Marca.des_marca_marca,
                             color = producto.Color_Producto.des_color_color,
                             talla = producto.Talla.des_talla_talla,
                             modelo = producto.Modelo.des_modelo_modelo
                         }
                        );

                res.ListaProductos = x.ToList().OrderByDescending(c => c.Seleccionar).ToList();

                

                foreach (var item in res.ListaProductos)
                {
                    //var bod = new V_R_StockProductoTerminadoBodega();
                    var bodega = db.Bodegas.FirstOrDefault(b => b.id_empresa_sucursal == getstockproductoporbodegarequest.Establecimiento && b.bit_estienda_bodega == true);
                    if (bodega != null)
                    {
                        var tienda = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => s.cod_productoterminado_productoterminado == item.Cod_producto && s.cod_bodega_bodega == bodega.cod_bodega_bodega);
                        if (tienda != null)
                        {
                            if (tienda.num_cantidad != null)
                                item.stock_tienda = (double)tienda.num_cantidad;
                        }
                    }


                    var bod = (from producto in db.V_R_StockProductoTerminadoBodega
                             where producto.cod_productoterminado_productoterminado == item.Cod_producto && producto.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega select producto.num_cantidad).FirstOrDefault();


                    //V_R_StockProductoTerminadoBodega bod = new V_R_StockProductoTerminadoBodega();
                    
                    //bod = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => s.cod_productoterminado_productoterminado == item.Cod_producto && s.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega);
                    

                    if (bod != null)
                    {
                        if (bod != null)
                        {
                            item.stock = (double)bod;
                            if (item.stock < item.stock_critico)
                            {
                                item.Seleccionar = true;
                            }
                        }
                        else
                        {
                            item.stock = 0;
                            if (item.stock < item.stock_critico)
                            {
                                item.Seleccionar = true;
                            }
                        }
                    }
                    else
                    {
                        item.stock = 0;
                        if (item.stock < item.stock_critico)
                        {
                            item.Seleccionar = true;
                        }
                    }


                    var stockproducto = (from producto in db.V_R_StockProductoTerminadoBodega
                                         where producto.cod_productoterminado_productoterminado == item.Cod_producto && producto.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega
                                         select new stockProd()
                                         {
                                             des_nombre_bodega = producto.des_nombre_bodega,
                                             num_cantidad = producto.num_cantidad.Value,
                                             cod_productoterminado_productoterminado = producto.cod_productoterminado_productoterminado
                                         }


                               ).ToList();


                  //  var stockproducto = db.V_R_StockProductoTerminadoBodega.Where(s => s.cod_productoterminado_productoterminado == item.Cod_producto && s.id_empresa_sucursal == getstockproductoporbodegarequest.Establecimiento && s.cod_bodega_bodega == getstockproductoporbodegarequest.cod_bodega);
                    
                    
                    var listabodega = new List<StockBodega>();
                    if (stockproducto.Count() > 0)
                    {

                        foreach (var item2 in stockproducto)
                        {

                            var stockp = new StockBodega();
                            stockp.Bodega = item2.des_nombre_bodega;
                            if (item2.num_cantidad != null)
                                stockp.Cantidad = (int)item2.num_cantidad;
                            stockp.Producto = item2.des_nombre_bodega;
                            stockp.cod_producto = item2.cod_productoterminado_productoterminado;
                            listabodega.Add(stockp);


                        }
                        item.stockProducto = listabodega;
                    }
                    else
                    {
                        var stockbodega = new StockBodega();
                        stockbodega = null;
                        listabodega.Add(stockbodega);
                        item.stockProducto = listabodega;
                    }
                }
                res.ListaProductos = res.ListaProductos.OrderByDescending(p => p.Seleccionar).ToList();
                res.valido = true;
                res.mensaje = "Productos Encontrados";
            }
            return res;
        }

        //private void  Dev(string prod, string bod)
        //{ 
        //    using (var db = new GestionComercialEntities())
        //    {
        //        bod = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => s.cod_productoterminado_productoterminado == prod && s.cod_bodega_bodega == Convert.ToInt64(bod.ToString()));
        //    }
        //}


        public ResultadoStockCritico GetStockCriticoPorBodega(GetStockCriticoPorBodegaRequest getstockcriticoporbodegarequest)
        {
            ResultadoStockCritico res = new ResultadoStockCritico();
            
            
                using (var db = new GestionComercialEntities())
                {

                    var x = (from producto in db.Productos_Terminados
                             join stock in db.V_R_StockProductoTerminadoBodega on producto.cod_productoterminado_productoterminado equals stock.cod_productoterminado_productoterminado
                             where stock.id_empresa_sucursal == getstockcriticoporbodegarequest.sucursal
                             select new ProductoPV()
                             {
                                 Cod_producto = producto.cod_productoterminado_productoterminado,
                                 Cod_producto1 = producto.cod_productoterminado_productoterminado_2,
                                 Cod_producto2 = producto.cod_productoterminado_productoterminado_3,
                                 nombre = producto.des_descripcion_productoterminado,
                                 stock = (Double)((stock.num_cantidad != null) ? stock.num_cantidad : 0),
                                 stock_critico = (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0),
                                 Seleccionar = (bool)(((producto.num_stock_productoterminado != null) ? producto.num_stock_productoterminado : 0) <= (Double)((producto.num_stockcritico_productoterminado != null) ? producto.num_stockcritico_productoterminado : 0)),
                                 precio = (decimal)stock.Precio,
                                 familia = producto.Familia_Productos_Terminados.des_descripcion_familiaproductosterminados,
                                 subfamilia = producto.Sub_Familia_Productos_Terminados.des_descripcion_subfamiliaproductosterminados,
                                 marca = producto.Marca.des_marca_marca,
                                 color = producto.Color_Producto.des_color_color,
                                 talla = producto.Talla.des_talla_talla,
                                 modelo = producto.Modelo.des_modelo_modelo
                             }
                             );


                    var listaproductos = new List<ProductoPV>();
                    listaproductos = x.ToList();

                    foreach (var item in listaproductos)
                    {
                        var bod = db.V_R_StockProductoTerminadoBodega.FirstOrDefault(s => (s.cod_productoterminado_productoterminado == item.Cod_producto || s.cod_productoterminado_productoterminado == item.Cod_producto1 || s.cod_productoterminado_productoterminado == item.Cod_producto2) && s.cod_bodega_bodega == getstockcriticoporbodegarequest.cod_bodega);
                        if (bod != null)
                        {
                            if (bod.num_cantidad != null)
                            {
                                item.stock = (double)bod.num_cantidad;
                                if (item.stock < item.stock_critico)
                                {
                                    item.Seleccionar = true;
                                }
                            }
                            else
                            {
                                item.stock = 0;
                                if (item.stock < item.stock_critico)
                                {
                                    item.Seleccionar = true;
                                }
                            }
                        }
                        else
                        {
                            item.stock = 0;
                            if (item.stock < item.stock_critico)
                            {
                                item.Seleccionar = true;
                            }
                        }
                    }

                    foreach (var item2 in listaproductos)
                    {
                        if (item2.Seleccionar)
                        {
                            var stockCritico = new StockCritico();
                            stockCritico.Producto = item2.nombre;
                            stockCritico.Stock = item2.stock;
                            stockCritico.Stock_critico = item2.stock_critico;
                            res.listaStockCritico.Add(stockCritico);
                        }
                    }
                    res.valido = true;
                    res.mensaje = "Productos Encontrados";
                    return res;
                }
        }
    }
}
