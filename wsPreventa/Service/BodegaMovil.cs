﻿using Contract;
using DataModel;
using Model.BodegaMovil;
using Model.Usuario;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace Service
{
    public class BodegaMovil : IBodegaMovil
    {
        private DataModel.GestionComercialEntities gcEntities;

        static Det_SolicitudDespachoStockReservado xDetSolicitudDespachoStockReservado = new Det_SolicitudDespachoStockReservado();

        public BodegaMovil()
        {
            gcEntities = new GestionComercialEntities();
        }
        public ResultadoFolio GetFolio(GetFolioRequest getfoliorequest)
        {

            var res = new ResultadoFolio();
            using (var db = new GestionComercialEntities())
            {
                var fol = db.Documentos_CompraGenerales.Where(s => s.bit_estaloteado_documentocomprageneral == getfoliorequest.Loteada).ToList();

                if (fol.Count() > 0)
                {
                    foreach (var item in fol)
                    {
                        var folios = new Folio();
                        folios.cod_documentocomprageneral_documentocomprageneral = item.cod_documentocomprageneral_documentocomprageneral;
                        folios.des_nombre_proveedor = item.Proveedores.des_nombre_proveedor;
                        folios.dat_fechafactura_documentocomprageneral = item.dat_fechafactura_documentocomprageneral;
                        folios.cod_proveedor_proveedor = item.cod_proveedor_proveedor;
                        folios.cod_tipodocumento_tipodocumentoventa = item.cod_tipodocumento_tipodocumentoventa;
                        folios.cod_empresa = item.cod_empresa_empresa;
                        folios.cod_temporada_temporada = item.cod_temporada_temporada;

                        res.listaFolio.Add(folios);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoDetalleFolio GetDetalleFolio(GetDetalleFolioRequest getdetallefoliorequest)
        {
            var res = new ResultadoDetalleFolio();
            using (var db = new GestionComercialEntities())
            {
                var detfol = new List<V_ListadoFolioCompra>();
                if (getdetallefoliorequest.SoloSinLotear == "true")
                {
                    detfol = db.V_ListadoFolioCompra.Where(s => s.cod_documentocomprageneral_documentocomprageneral == getdetallefoliorequest.cod_documentocomprageneral_documentocomprageneral
                        && s.Falta > 0).ToList();
                }
                else
                {
                    detfol = db.V_ListadoFolioCompra.Where(s => s.cod_documentocomprageneral_documentocomprageneral == getdetallefoliorequest.cod_documentocomprageneral_documentocomprageneral).ToList();
                }

                if (detfol.Count() > 0)
                {
                    foreach (var item in detfol)
                    {
                        var detallefolios = new DetalleFolio();
                        detallefolios.Cantidad_Comprada = item.Cantidad_Comprada;
                        detallefolios.cod_producto_materiaprima = item.cod_producto_materiaprima;
                        detallefolios.des_nombre_materiaprima = item.des_nombre_materiaprima;
                        detallefolios.Cantidad_Falta = item.Falta;
                        detallefolios.Cantidad_Lote = item.Cantidad_Lotes;
                        res.listaDetalleFolio.Add(detallefolios);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public string GetBodegaporCodigo(GetBodegaporCodigoRequest getbodegaporcodigorequest)
        {

            string res = "";

            using (var db = new GestionComercialEntities())
            {

                Int64 codigo = Int64.Parse(getbodegaporcodigorequest.cod_bodega_bodega);

                var detfol = db.Bodegas.Where(s => s.cod_bodega_bodega == codigo);

                if (detfol.Count() > 0)
                {
                    foreach (var item in detfol)
                    {
                        res = item.des_nombre_bodega;
                    }

                }
            }
            return res;

        }

        public ResultadoSeccionporCodigo GetSeccionporCodigo(GetSeccionporCodigoRequest getseccionporcodigorequest)
        {

            var res = new ResultadoSeccionporCodigo();
            using (var db = new GestionComercialEntities())
            {
                Int64 Codigo = Int64.Parse(getseccionporcodigorequest.cod_seccionbodega_seccionbodega);
                Int64 CodigoBod = Int64.Parse(getseccionporcodigorequest.cod_bodega);

                var detfol = db.V_SeccionBodega.Where(s => s.cod_seccionbodega_seccionbodega == Codigo && s.cod_bodega_bodega == CodigoBod);

                if (detfol.Count() > 0)
                {
                    foreach (var item in detfol)
                    {
                        var detalleseccion = new SeccionBod();
                        detalleseccion.des_nombre_seccionbodega = item.des_nombre_seccionbodega;

                        res.listaDetalleSeccion.Add(detalleseccion);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoCrealoteProductoTerminado CrealoteProductoTerminado()
        {
            ResultadoCrealoteProductoTerminado res = new ResultadoCrealoteProductoTerminado();
            using (var db = new GestionComercialEntities())
            {
                var Lot = db.Id_LoteConfiguracion.OrderByDescending(i => i.id_lote).FirstOrDefault();
                if (Lot != null)
                {
                    Lot.id_lote += 1;
                    //Rellenar con ceros a la izquierda (largo 8)
                    res.Lote = Lot.id_lote.ToString().PadLeft(Lot.id_lote.ToString().Length + (8 - Lot.id_lote.ToString().Length), '0');
                    res.resultado = true;
                }
                else
                {
                    res.Lote = "";
                    res.resultado = false;
                }
                db.SaveChanges();
            }
            return res;
        }

        public ResultadoCabeceraIngresoInsumo AddCabeceraIngresoInsumo(AddCabeceraIngresoInsumoRequest addcabeceraingresoinsumorequest)
        {

            ResultadoCabeceraIngresoInsumo res = new ResultadoCabeceraIngresoInsumo();
            Int64 xcod_snapcinsumo_snapcimsumo = 0;
            Int64 xbodega = 0;
            string xLote = "";
            bool EsProductoTerminado = false;
            bool Esanalisisarina = false;

            var xCabecera = new snap_ControlIngresoInsumo();
            string fechastring = addcabeceraingresoinsumorequest.Cabecera.dat_fecha_documentocompra;
            DateTime Fecha = Convert.ToDateTime(fechastring);
            string ProdGenerico = "";



            using (var db = new GestionComercialEntities())
            {
                var Lote = CrealoteProductoTerminado();
                if (Lote.resultado)
                {
                    xLote = Lote.Lote;
                }
                else
                {

                    res.num_lote = "";
                    return res;
                }
                var SucursalDoc = db.Documentos_CompraGenerales.FirstOrDefault(f => f.cod_documentocomprageneral_documentocomprageneral == addcabeceraingresoinsumorequest.Cabecera.cod_documentocomprageneral_documentocomprageneral 
                    && f.cod_proveedor_proveedor == addcabeceraingresoinsumorequest.Cabecera.cod_proveedor_proveedor                     
                    && f.cod_empresa_empresa == addcabeceraingresoinsumorequest.Cabecera.cod_empresa_empresa);


                string xLole = SucursalDoc.cod_temporada_temporada.ToString().PadLeft(SucursalDoc.cod_temporada_temporada.ToString().Length + (2 - SucursalDoc.cod_temporada_temporada.ToString().Length), '0') + SucursalDoc.id_empresa_sucursal.ToString().PadLeft(SucursalDoc.id_empresa_sucursal.ToString().Length + (3 - SucursalDoc.id_empresa_sucursal.ToString().Length), '0') + xLote;


                var query = db.snap_ControlIngresoInsumo.FirstOrDefault(f => f.cod_documentocomprageneral_documentocomprageneral
                == addcabeceraingresoinsumorequest.Cabecera.cod_documentocomprageneral_documentocomprageneral
                && ((DateTime)(f.dat_fecha_documentocompra)).Day == Fecha.Day && ((DateTime)(f.dat_fecha_documentocompra)).Month == Fecha.Month && ((DateTime)(f.dat_fecha_documentocompra)).Year == Fecha.Year
                && f.cod_proveedor_proveedor == addcabeceraingresoinsumorequest.Cabecera.cod_proveedor_proveedor);

                //Si existe la tupla
                if (query != null)
                {
                    xcod_snapcinsumo_snapcimsumo = query.cod_snapcinsumo_snapcimsumo;
                }

                if (db.Materias_Primas.Any(MP => MP.cod_producto_materiaprima == addcabeceraingresoinsumorequest.cod_producto_materiaprima && MP.bit_esproductoterminado == true))
                {
                    EsProductoTerminado = true;
                }

                if (addcabeceraingresoinsumorequest.DetalleInsumo.bit_analisisharina_controlingresoinsumo == "1")
                {
                    Esanalisisarina = true;
                }

                //Buscar el atributo Producto generico
                var query1 = db.Materias_Primas.FirstOrDefault(MP => MP.cod_producto_materiaprima == addcabeceraingresoinsumorequest.cod_producto_materiaprima);
                {

                    if (query1 != null)
                    {
                        ProdGenerico = query1.cod_productogenerico_materiaprima;
                    }
                }

                if (Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_bodega_bodega) == 0)
                {
                    var query2 = db.Opciones.FirstOrDefault(MP => MP.cod_bodegaprincipal_opciones == MP.cod_bodegaprincipal_opciones);
                    {

                        if (query2 != null)
                        {
                            xbodega = query2.cod_bodegaprincipal_opciones.Value;
                        }
                    }
                }

                using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
                {

                    try
                    {
                        if (xcod_snapcinsumo_snapcimsumo == 0)
                        {

                            //Crea Encabezado
                            xCabecera.cod_documentocomprageneral_documentocomprageneral = addcabeceraingresoinsumorequest.Cabecera.cod_documentocomprageneral_documentocomprageneral;
                            xCabecera.cod_proveedor_proveedor = addcabeceraingresoinsumorequest.Cabecera.cod_proveedor_proveedor;
                            xCabecera.cod_tipodocumento_tipodocumentoventa = Int64.Parse(addcabeceraingresoinsumorequest.Cabecera.cod_tipodocumento_tipodocumentoventa);
                            xCabecera.cod_ordenmolienda_ordenmolienda = Int64.Parse(addcabeceraingresoinsumorequest.Cabecera.cod_ordenmolienda_ordenmolienda);
                            xCabecera.cod_opmpelaboradas_opmpelaboradas = null;
                            xCabecera.cod_empresa_empresa = addcabeceraingresoinsumorequest.Cabecera.cod_empresa_empresa;
                            xCabecera.cod_temporada_temporada = long.Parse(addcabeceraingresoinsumorequest.Cabecera.cod_temporada_temporada);
                            xCabecera.cod_trigomolienda_trigomolienda = Int64.Parse(addcabeceraingresoinsumorequest.Cabecera.cod_trigomolienda_trigomolienda);
                            xCabecera.cod_correccion_correccionmatprima = null;
                            xCabecera.dat_fecha_controlingresoinsumo = DateTime.Now;
                            xCabecera.dat_fecha_documentocompra = Fecha;
                            db.snap_ControlIngresoInsumo.Add(xCabecera);
                            db.SaveChanges();

                            xcod_snapcinsumo_snapcimsumo = xCabecera.cod_snapcinsumo_snapcimsumo;
                        }
             

                        //Crea Detalle
                        if (EsProductoTerminado)
                        {

                            //Si es producto terminado
                            var xDetalleInsumoProductoTerminado = new ControlIngresoInsumoProductoTerminado();

                            if (Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_bodega_bodega) == 0)
                            {
                                xDetalleInsumoProductoTerminado.cod_bodega_bodega = xbodega;
                            }
                            else
                            {
                                xDetalleInsumoProductoTerminado.cod_bodega_bodega = Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_bodega_bodega);
                            }

                            if (decimal.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_seccionbodega_seccionbodega) == 0)
                            {
                                xDetalleInsumoProductoTerminado.cod_seccionbodega_seccionbodega = null;
                            }
                            else
                            {
                                xDetalleInsumoProductoTerminado.cod_seccionbodega_seccionbodega = decimal.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_seccionbodega_seccionbodega);
                            }

                            xDetalleInsumoProductoTerminado.num_posicion_x = Int32.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_posicion_x);
                            xDetalleInsumoProductoTerminado.num_posicion_y = Int32.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_posicion_y);
                            xDetalleInsumoProductoTerminado.num_posicion_z = Int32.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_posicion_z);
                            xDetalleInsumoProductoTerminado.cod_snapcinsumo_snapcimsumo = xcod_snapcinsumo_snapcimsumo;
                            xDetalleInsumoProductoTerminado.cod_productoterminado_productoterminado = addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_productoterminado_productoterminado;
                            xDetalleInsumoProductoTerminado.cod_temporada_temporada = Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_temporada_temporada);
                            xDetalleInsumoProductoTerminado.cod_condicionproducto_condicionproducto = Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_condicionproducto_condicionproducto);
                            xDetalleInsumoProductoTerminado.cod_condicionbodega_condicionbodega = Int64.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_condicionbodega_condicionbodega);
                            xDetalleInsumoProductoTerminado.cod_ordenproduccion_ordenproduccion = null;
                            xDetalleInsumoProductoTerminado.cod_recetaproductoterminado_recetaproductoterminado = null;
                            string fechastringd = addcabeceraingresoinsumorequest.DetalleProductoTerminado.dat_fechavencimiento;
                            DateTime Fechad = Convert.ToDateTime(fechastringd);
                            xDetalleInsumoProductoTerminado.dat_fechavencimiento = Fechad;
                            xDetalleInsumoProductoTerminado.num_cantidad = double.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_cantidad);

                            var ValorProducto = db.Detalle_DocumentoCompraGeneral.FirstOrDefault(f => f.cod_documentocomprageneral_documentocomprageneral == addcabeceraingresoinsumorequest.Cabecera.cod_documentocomprageneral_documentocomprageneral
                              && f.cod_proveedor_proveedor == addcabeceraingresoinsumorequest.Cabecera.cod_proveedor_proveedor
                              && f.cod_empresa_empresa == addcabeceraingresoinsumorequest.Cabecera.cod_empresa_empresa
                              && f.cod_producto_materiaprima == addcabeceraingresoinsumorequest.DetalleProductoTerminado.cod_productoterminado_productoterminado);
                            xDetalleInsumoProductoTerminado.num_valor = double.Parse(ValorProducto.num_valor_detalledocumentocomprageneral.ToString());

                            xDetalleInsumoProductoTerminado.num_horas = double.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_horas);
                            xDetalleInsumoProductoTerminado.num_cantidadbultos = double.Parse(addcabeceraingresoinsumorequest.DetalleProductoTerminado.num_cantidadbultos);
                            xDetalleInsumoProductoTerminado.num_lote = "PT" + xLole;
                            db.ControlIngresoInsumoProductoTerminado.Add(xDetalleInsumoProductoTerminado);
                            db.SaveChanges();

                            res.num_lote = xDetalleInsumoProductoTerminado.num_lote;
                        }
                        else
                        {
                            //Si es insumo
                            var xDetalleInsumo = new ControlIngresoInsumo();

                            if (Int64.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_bodega_bodega) == 0)
                            {
                                xDetalleInsumo.cod_bodega_bodega = xbodega;
                            }
                            else
                            {
                                xDetalleInsumo.cod_bodega_bodega = Int64.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_bodega_bodega);
                            }

                            if (decimal.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_seccionbodega_seccionbodega) == 0)
                            {
                                xDetalleInsumo.cod_seccionbodega_seccionbodega = null;
                            }
                            else
                            {
                                xDetalleInsumo.cod_seccionbodega_seccionbodega = decimal.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_seccionbodega_seccionbodega);
                            }

                            xDetalleInsumo.cod_snapcinsumo_snapcimsumo = xcod_snapcinsumo_snapcimsumo;
                            xDetalleInsumo.cod_producto_materiaprima = addcabeceraingresoinsumorequest.cod_producto_materiaprima;
                            xDetalleInsumo.cod_temporada_temporada = Int64.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_temporada_temporada);
                            xDetalleInsumo.cod_condicionproducto_condicionproducto = Int64.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_condicionproducto_condicionproducto);
                            xDetalleInsumo.cod_condicionbodega_condicionbodega = Int64.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.cod_condicionbodega_condicionbodega);
                            string fechastringd = addcabeceraingresoinsumorequest.DetalleInsumo.dat_fechavencimiento_ControlIngresoInsumo;
                            DateTime Fechad = Convert.ToDateTime(fechastringd);
                            xDetalleInsumo.dat_fechavencimiento_ControlIngresoInsumo = Fechad;
                            xDetalleInsumo.num_cantidad_ControlIngresoInsumo = double.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_cantidad_ControlIngresoInsumo);

                            var ValorProducto = db.Detalle_DocumentoCompraGeneral.FirstOrDefault(f => f.cod_documentocomprageneral_documentocomprageneral == addcabeceraingresoinsumorequest.Cabecera.cod_documentocomprageneral_documentocomprageneral
                              && f.cod_proveedor_proveedor == addcabeceraingresoinsumorequest.Cabecera.cod_proveedor_proveedor
                              && f.cod_empresa_empresa == addcabeceraingresoinsumorequest.Cabecera.cod_empresa_empresa
                              && f.cod_producto_materiaprima == addcabeceraingresoinsumorequest.DetalleInsumo.cod_producto_materiaprima);                    
                            xDetalleInsumo.num_valor_controlingresoinsumo = double.Parse(ValorProducto.num_valor_detalledocumentocomprageneral.ToString());

                            xDetalleInsumo.num_horas_controlingresoinsumo = double.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_horas_controlingresoinsumo);
                            xDetalleInsumo.num_cantidadbultos_controlingresoinsumo = double.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_cantidadbultos_controlingresoinsumo);
                            xDetalleInsumo.cod_productogenerico_materiaprima = (string.IsNullOrEmpty(ProdGenerico)) ? null : ProdGenerico;
                            xDetalleInsumo.bit_analisisharina_controlingresoinsumo = Esanalisisarina;
                            xDetalleInsumo.num_posicion_x = Int32.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_posicion_x);
                            xDetalleInsumo.num_posicion_y = Int32.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_posicion_y);
                            xDetalleInsumo.num_posicion_z = Int32.Parse(addcabeceraingresoinsumorequest.DetalleInsumo.num_posicion_z);
                            xDetalleInsumo.num_lote_controlingresoinsumo = "MP" + xLole;
                            db.ControlIngresoInsumo.Add(xDetalleInsumo);
                            db.SaveChanges();

                            res.num_lote = xDetalleInsumo.num_lote_controlingresoinsumo;
                        }
                        dbTran.Commit();
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();

                    }
                }

            }
            return res;
        }


        public ResultadoCondicionesBodegas GetCondicionesBodegas()
        {
            var res = new ResultadoCondicionesBodegas();
            using (var db = new GestionComercialEntities())
            {
                var detcond = db.CondicionesBodegas.Where(s => s.cod_condicionbodega_condicionbodega == s.cod_condicionbodega_condicionbodega);

                if (detcond.Count() > 0)
                {
                    foreach (var item in detcond)
                    {
                        var Condic = new Condiciones_Bodegas();
                        Condic.cod_condicionbodega_condicionbodega = item.cod_condicionbodega_condicionbodega;
                        Condic.des_descripcion_condicionbodega = item.des_descripcion_condicionbodega;
                        res.listaCondicionBodega.Add(Condic);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoCondicionesProductos GetCondicionesProductos()
        {
            var res = new ResultadoCondicionesProductos();
            using (var db = new GestionComercialEntities())
            {
                var detcond = db.CondicionesProductos.Where(s => s.cod_condicionproducto_condicionproducto == s.cod_condicionproducto_condicionproducto);

                if (detcond.Count() > 0)
                {
                    foreach (var item in detcond)
                    {
                        var Condic = new Condiciones_Productos();
                        Condic.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                        Condic.des_descripcion_condicionproducto = item.des_descripcion_condicionproducto;
                        Condic.bit_estadoproducto_CondicionesProductos = item.bit_estadoproducto_CondicionesProductos;
                        res.listaCondicionProducto.Add(Condic);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoLoteporFolio GetLoteporFolio(GetLoteporFolioRequest getloteporfoliorequest)
        {
            var res = new ResultadoLoteporFolio();
            using (var db = new GestionComercialEntities())
            {
                var lot = db.V_LotesControlIngresoInsumo.Where(s => s.cod_documentocomprageneral_documentocomprageneral == getloteporfoliorequest.cod_documentocomprageneral_documentocomprageneral && s.cod_producto_materiaprima == getloteporfoliorequest.cod_producto).ToList();

                if (lot.Count() > 0)
                {
                    foreach (var item in lot)
                    {
                        var lotes = new Lote();

                        lotes.num_lote = item.num_lote;
                        lotes.tipo = item.tipo;
                        res.listaLote.Add(lotes);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoPosciciones GetPosciciones(GetPoscicionesRequest getposcicionesrequest)
        {
            var res = new ResultadoPosciciones();
            var snap = gcEntities.snap_SeccionBodegaUbicacion.FirstOrDefault(s => s.cod_bodega_bodega == getposcicionesrequest.cod_bodega && s.cod_seccionbodega_seccionbodega == getposcicionesrequest.cod_seccion);
            if (snap == null)
            {
                res.valido = false;
                res.poscicion = null;
                res.listadoposciciones = null;
            }
            else
            {
                var poscicion = new SnapSeccionBodegaUbicacion();
                poscicion.pos_x = snap.num_posicion_x;
                poscicion.pos_y = snap.max_posicion_y;
                poscicion.pos_z = snap.max_posicion_z;
                res.poscicion = poscicion;
                for (int i = 0; i < snap.num_posicion_x; i++)
                {
                    for (int j = 0; j < snap.max_posicion_y; j++)
                    {
                        for (int k = 0; k < snap.max_posicion_z; k++)
                        {
                            var pos = new SnapSeccionBodegaUbicacion();
                            pos.pos_x = i + 1;
                            pos.pos_y = j + 1;
                            pos.pos_z = k + 1;
                            pos.SeIncluye = true;
                            res.listadoposciciones.Add(pos);
                        }
                    }
                }
                res.valido = true;
            }
            return res;
        }

        public ResultadoLotes GetListaLotes()
        {
            var res = new ResultadoLotes();
            var lotes = gcEntities.V_ListadoLotesEnumerados.ToList();
            foreach (var item in lotes)
            {
                var lote = new Lote();
                lote.bodega = item.Bodega;
                if (item.Cantidad != null)
                    lote.cantidad = (double)item.Cantidad;
                lote.condicionProducto = item.condicion;
                if (item.fechaVencimiento != null)
                    lote.fechaVencimiento = (DateTime)item.fechaVencimiento;
                lote.num_lote = item.num_lote;
                lote.numero = item.Numero;
                lote.producto = item.producto;
                lote.seccion = item.Seccion;
                lote.tipo = item.tipo;
                if (item.valor != null)
                    lote.valor = (double)item.valor;
                if (item.X != null)
                    lote.x = (int)item.X;
                if (item.Y != null)
                    lote.y = (int)item.Y;
                if (item.Z != null)
                    lote.z = (int)item.Z;
                res.ListadoLotes.Add(lote);
            }
            return res;
        }

        public ResultadoUpdateCabeceraIngresoInsumo UpdateCabeceraIngresoInsumo(UpdateCabeceraIngresoInsumoRequest updatecabeceraingresoinsumorequest)
        {
            ResultadoUpdateCabeceraIngresoInsumo res = new ResultadoUpdateCabeceraIngresoInsumo();

            bool EsProductoTerminado = false;
            string Lote = updatecabeceraingresoinsumorequest.num_lote;
            string fechastring = updatecabeceraingresoinsumorequest.dat_fechavencimiento_ControlIngresoInsumo;
            DateTime Fecha = Convert.ToDateTime(fechastring);

            using (var db = new GestionComercialEntities())
            {

                if ((bool)db.Materias_Primas.FirstOrDefault(MP => MP.cod_producto_materiaprima == updatecabeceraingresoinsumorequest.cod_producto_materiaprima).bit_materiaprima_productoterminado)
                {
                    EsProductoTerminado = true;
                }

                using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {

                        //Actualiza Detalle
                        if (EsProductoTerminado)
                        {
                            //Si es producto terminado
                            var xDetalleInsumoProductoTerminado = db.ControlIngresoInsumoProductoTerminado.FirstOrDefault(s => s.num_lote == Lote);
                            xDetalleInsumoProductoTerminado.num_cantidad = double.Parse(updatecabeceraingresoinsumorequest.num_cantidad_ControlIngresoInsumo);
                            xDetalleInsumoProductoTerminado.num_cantidadbultos = double.Parse(updatecabeceraingresoinsumorequest.num_cantidadbultos_controlingresoinsumo);
                            xDetalleInsumoProductoTerminado.dat_fechavencimiento = Fecha;
                            xDetalleInsumoProductoTerminado.cod_condicionbodega_condicionbodega = Int64.Parse(updatecabeceraingresoinsumorequest.cod_condicionbodega_condicionbodega);
                            xDetalleInsumoProductoTerminado.cod_condicionproducto_condicionproducto = Int64.Parse(updatecabeceraingresoinsumorequest.cod_condicionproducto_condicionproducto);
                            db.SaveChanges();
                            res.actualizado = true;
                        }
                        else
                        {

                            var xDetalleInsumo = db.ControlIngresoInsumo.FirstOrDefault(s => s.num_lote_controlingresoinsumo == Lote);
                            xDetalleInsumo.num_cantidad_ControlIngresoInsumo = double.Parse(updatecabeceraingresoinsumorequest.num_cantidad_ControlIngresoInsumo);
                            xDetalleInsumo.num_cantidadbultos_controlingresoinsumo = double.Parse(updatecabeceraingresoinsumorequest.num_cantidadbultos_controlingresoinsumo);
                            xDetalleInsumo.dat_fechavencimiento_ControlIngresoInsumo = Fecha;
                            xDetalleInsumo.cod_condicionbodega_condicionbodega = Int64.Parse(updatecabeceraingresoinsumorequest.cod_condicionbodega_condicionbodega);
                            xDetalleInsumo.cod_condicionproducto_condicionproducto = Int64.Parse(updatecabeceraingresoinsumorequest.cod_condicionproducto_condicionproducto);
                            db.SaveChanges();
                            res.actualizado = true;
                        }
                        dbTran.Commit();
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.actualizado = false;

                    }
                }

            }
            return res;
        }


        public ResultadoDetalleLote GetDetalleLote(GetDetalleLoteRequest getdetalleloterequest)
        {
            string Xlote = getdetalleloterequest.num_lote;
            bool EsProductoTerminado = false;
            long PosX = long.Parse(getdetalleloterequest.Pos_X);
            long PosY = long.Parse(getdetalleloterequest.Pos_Y);
            long PosZ = long.Parse(getdetalleloterequest.Pos_Z);
            var res = new ResultadoDetalleLote();

            using (var db = new GestionComercialEntities())
            {

                if (getdetalleloterequest.TipoLote == "PT")
                {
                    EsProductoTerminado = true;
                }

                if (EsProductoTerminado)
                {
                    var fol = db.ControlIngresoInsumoProductoTerminado.Include("Productos_Terminados").Where(s => s.num_lote == Xlote
                        && s.num_posicion_x == PosX
                        && s.num_posicion_y == PosY
                        && s.num_posicion_z == PosZ).ToList();

                    if (fol.Count() > 0)
                    {
                        foreach (var item in fol)
                        {
                            var lotes = new Lote();
                            lotes.cantidad = item.num_cantidad.Value;
                            lotes.fechaVencimiento = (DateTime)item.dat_fechavencimiento;
                            lotes.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                            lotes.cod_condicionbodega_condicionbodega = item.cod_condicionbodega_condicionbodega;
                            lotes.x = item.num_posicion_x;
                            lotes.y = item.num_posicion_y;
                            lotes.z = item.num_posicion_z;
                            lotes.cod_bodega = item.cod_bodega_bodega.ToString();
                            lotes.bodega = item.Bodegas.des_nombre_bodega;
                            lotes.cod_seccion = item.cod_seccionbodega_seccionbodega.ToString();
                            lotes.seccion = (string.IsNullOrEmpty(lotes.cod_seccion)) ? null : item.SeccionBodega.des_nombre_seccionbodega;
                            lotes.cod_producto = item.cod_productoterminado_productoterminado;
                            lotes.producto = item.Productos_Terminados.des_descripcion_productoterminado;
                            res.listadodetallelote.Add(lotes);
                        }
                        res.valido = true;
                    }
                }
                else
                {
                    var fol = db.ControlIngresoInsumo.Include("Materias_Primas").Where(s => s.num_lote_controlingresoinsumo == Xlote
                        && s.num_posicion_x == PosX
                        && s.num_posicion_y == PosY
                        && s.num_posicion_z == PosZ).ToList();

                    if (fol.Count() > 0)
                    {
                        foreach (var item in fol)
                        {
                            var lotes = new Lote();
                            lotes.cantidad = item.num_cantidad_ControlIngresoInsumo;
                            lotes.fechaVencimiento = (DateTime)item.dat_fechavencimiento_ControlIngresoInsumo;
                            lotes.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                            lotes.cod_condicionbodega_condicionbodega = item.cod_condicionbodega_condicionbodega;
                            lotes.x = item.num_posicion_x;
                            lotes.y = item.num_posicion_y;
                            lotes.z = item.num_posicion_z;
                            lotes.cod_bodega = item.cod_bodega_bodega.ToString();
                            lotes.bodega = item.Bodegas.des_nombre_bodega;
                            lotes.cod_seccion = item.cod_seccionbodega_seccionbodega.ToString();
                            lotes.seccion = (string.IsNullOrEmpty(lotes.cod_seccion)) ? null : item.SeccionBodega.des_nombre_seccionbodega;
                            lotes.cod_producto = item.cod_producto_materiaprima;
                            lotes.producto = item.Materias_Primas.des_nombre_materiaprima;
                            res.listadodetallelote.Add(lotes);
                        }
                        res.valido = true;
                    }
                }

            }

            return res;

        }

        public ResultadoBodegaTienda GetBodegaTienda(GetBodegaTiendaRequest GetBodegatiendarequest)
        {
            var res = new ResultadoBodegaTienda();

            bool EsT = bool.Parse(GetBodegatiendarequest.esTienda);

            using (var db = new GestionComercialEntities())
            {
                var detcond = db.Bodegas.Where(s => s.bit_estienda_bodega == EsT).ToList();

                if (detcond.Count() > 0)
                {
                    foreach (var item in detcond)
                    {
                        var Bod = new Model.BodegaMovil.Bodegas();
                        Bod.cod_bodega = item.cod_bodega_bodega;
                        Bod.nombre = item.des_nombre_bodega;
                        res.listadobodegasT.Add(Bod);
                    }
                }
            }
            return res;
        }

        public ResultadoBodega GetBodegaPorSucursal(GetBodegaPorSucursalRequest getbodegaporsucursalrequest)
        {
            var res = new ResultadoBodega();
            var bodegas = (from bod in gcEntities.Bodegas
                           where bod.id_empresa_sucursal == getbodegaporsucursalrequest.sucursal
                           select new Model.BodegaMovil.Bodegas()
                           {
                               cod_bodega = bod.cod_bodega_bodega,
                               nombre = bod.des_nombre_bodega
                           })
                          .Union(from bod in gcEntities.Bodegas
                                 select new Model.BodegaMovil.Bodegas()
                                 {
                                     cod_bodega = 0,
                                     nombre = "Seleccionar"
                                 }
                          );


            res.listadobodegas = bodegas.ToList();
            return res;
        }

        public ResultadoSeccion GetSeccion(GetSeccionRequest getseccionrequest)
        {
            var res = new ResultadoSeccion();

            var secciones = gcEntities.SeccionBodega.Where(sb => sb.cod_bodega_bodega == getseccionrequest.cod_bodega);
            if (secciones.Count() == 0)
            {
                res.listadoSeccion = null;
            }
            else
            {
                foreach (var item in secciones)
                {
                    var seccion = new Seccion();
                    seccion.cod_seccion = item.cod_seccionbodega_seccionbodega;
                    seccion.nombre = item.des_nombre_seccionbodega;
                    res.listadoSeccion.Add(seccion);
                }
            }
            return res;
        }

        public ResultadoMoverLote MoverLote(GetMoverLoteRequest getmoverloterequest)
        {
            ResultadoMoverLote res = new ResultadoMoverLote();
            bool EsProductoTerminado = false;
            string CodProducto = "";
            long NumeroSnap = 0;
            long CodTemporada = 0;
            long NumDoc = 0;
            string NumLote = getmoverloterequest.num_lote.ToString();
            double NuevaCantidad = double.Parse(getmoverloterequest.Cantidad_Nueva);
            long NuevaBodega = long.Parse(getmoverloterequest.Nueva_Bodega);


            long NuevaPosX = long.Parse(getmoverloterequest.Nueva_X);
            long NuevaPosY = long.Parse(getmoverloterequest.Nueva_Y);
            long NuevaPosZ = long.Parse(getmoverloterequest.Nueva_Z);

            long OriginalPosX = long.Parse(getmoverloterequest.Original_X);
            long OriginalPosY = long.Parse(getmoverloterequest.Original_Y);
            long OriginalPosZ = long.Parse(getmoverloterequest.Original_Z);


            if (getmoverloterequest.Tipo_Lote == "PT")
            {
                EsProductoTerminado = true;
            }
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (getmoverloterequest.Totienda == "true")//es a tienda
                    {
                        var xopciones = db.Opciones.FirstOrDefault(s => s.cod_documentocompra_guiainterna == s.cod_documentocompra_guiainterna);
                        //Descontar del lote origen                         
                        if (EsProductoTerminado)
                        {
                            //Si es producto terminado
                            var xDetalleInsumoProductoTerminado = db.ControlIngresoInsumoProductoTerminado.FirstOrDefault(s => s.num_lote == NumLote
                                && s.num_posicion_x == OriginalPosX
                                && s.num_posicion_y == OriginalPosY
                                && s.num_posicion_z == OriginalPosZ);
                            xDetalleInsumoProductoTerminado.num_cantidad -= NuevaCantidad;
                            if (xDetalleInsumoProductoTerminado.cod_snapcinsumo_snapcimsumo != null)
                            {
                                NumeroSnap = long.Parse(xDetalleInsumoProductoTerminado.cod_snapcinsumo_snapcimsumo.ToString());
                            }
                            CodProducto = xDetalleInsumoProductoTerminado.cod_productoterminado_productoterminado;
                            CodTemporada = long.Parse(xDetalleInsumoProductoTerminado.cod_temporada_temporada.ToString());
                            db.SaveChanges();
                        }
                        else
                        {
                            var xDetalleInsumo = db.ControlIngresoInsumo.FirstOrDefault(s => s.num_lote_controlingresoinsumo == NumLote
                                && s.num_posicion_x == OriginalPosX
                                && s.num_posicion_y == OriginalPosY
                                && s.num_posicion_z == OriginalPosZ);
                            if (xDetalleInsumo.cod_snapcinsumo_snapcimsumo != null)
                            {
                                NumeroSnap = xDetalleInsumo.cod_snapcinsumo_snapcimsumo;
                            }
                            xDetalleInsumo.num_cantidad_ControlIngresoInsumo -= NuevaCantidad;
                            CodProducto = xDetalleInsumo.cod_producto_materiaprima;
                            CodTemporada = long.Parse(xDetalleInsumo.cod_temporada_temporada.ToString());
                            db.SaveChanges();
                        }
                        //Obtener Documento de snap_control del lote  
                        var xsnap_ControlIngresoInsumo = db.snap_ControlIngresoInsumo.FirstOrDefault(s => s.cod_snapcinsumo_snapcimsumo == NumeroSnap);
                        //Obtener Bodega  
                        var xBodega = db.Bodegas.FirstOrDefault(s => s.cod_bodega_bodega == NuevaBodega);
                        //Crear nuevo documento de documento interno para la otra sucursal
                        Documentos_CompraGenerales DocNuevo = new Documentos_CompraGenerales();
                        Documentos_CompraGenerales docx = db.Documentos_CompraGenerales.FirstOrDefault(x => x.cod_documentocomprageneral_documentocomprageneral == xsnap_ControlIngresoInsumo.cod_documentocomprageneral_documentocomprageneral);

                        //Rango_Correlativos_DocVta obtener 
                        var xRango_Correlativos_DocVta = db.Rango_Correlativos_DocVta.FirstOrDefault(s => s.cod_empresa_empresa == docx.cod_empresa_empresa
                            && s.id_empresa_sucursal == docx.id_empresa_sucursal && s.cod_tipodocumento_tipodocumentoventa == xopciones.cod_documentocompra_guiainterna
                            && s.bit_RCorre_Activo == true);
                        NumDoc = long.Parse(xRango_Correlativos_DocVta.num_RCorre_Actual.ToString());
                        xRango_Correlativos_DocVta.num_RCorre_Actual += 1;
                        db.SaveChanges();

                        DocNuevo.cod_documentocomprageneral_documentocomprageneral = NumDoc.ToString();
                        DocNuevo.bit_estaloteado_documentocomprageneral = false;
                        DocNuevo.des_patente_documentocomprageneral = docx.des_patente_documentocomprageneral;
                        DocNuevo.cod_proveedor_proveedor = docx.cod_proveedor_proveedor;
                        DocNuevo.cod_tipodocumento_tipodocumentoventa = (long)((xopciones.cod_documentocompra_guiainterna != null) ? xopciones.cod_documentocompra_guiainterna : 0);
                        DocNuevo.cod_empresa_empresa = xBodega.Sucursal_empresa.cod_empresa_empresa;
                        DocNuevo.cod_condicionpago_condicionpago = docx.cod_condicionpago_condicionpago;
                        DocNuevo.cod_bodega_bodega = xBodega.cod_bodega_bodega;
                        var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                        DocNuevo.dat_fechafactura_documentocomprageneral = Convert.ToDateTime(Fecha);
                        DocNuevo.dat_fechacontable_documentocomprageneral = Convert.ToDateTime(Fecha);
                        DocNuevo.des_glosa_documentocomprageneral = docx.des_glosa_documentocomprageneral;
                        DocNuevo.num_neto_documentocomprageneral = docx.num_neto_documentocomprageneral;
                        DocNuevo.num_ivapagado_documentocomprageneral = docx.num_ivapagado_documentocomprageneral;
                        DocNuevo.num_exento_documentocomprageneral = docx.num_exento_documentocomprageneral;
                        DocNuevo.dat_fechaproximopago_documentocomprageneral = Convert.ToDateTime(Fecha);
                        DocNuevo.num_impuestoespecifico_documentocomprageneral = docx.num_impuestoespecifico_documentocomprageneral;
                        DocNuevo.num_descuento_documentocomprageneral = docx.num_descuento_documentocomprageneral;
                        DocNuevo.num_porciva_documentocomprageneral = docx.num_porciva_documentocomprageneral;
                        DocNuevo.bit_exentoiva_documentocomprageneral = docx.bit_exentoiva_documentocomprageneral;
                        DocNuevo.bit_conguias_documentocomprageneral = docx.bit_conguias_documentocomprageneral;
                        DocNuevo.num_porotroimpuesto_documentogeneral = docx.num_porotroimpuesto_documentogeneral;
                        DocNuevo.num_otroimpuesto_documentogeneral = docx.num_otroimpuesto_documentogeneral;
                        DocNuevo.cod_sucursal_sucursalproveedor = docx.cod_sucursal_sucursalproveedor;
                        DocNuevo.cod_ordencompra_ordencompra = docx.cod_ordencompra_ordencompra;
                        DocNuevo.cod_condiciontransporte_condiciontransporte = docx.cod_condiciontransporte_condiciontransporte;
                        DocNuevo.id_empresa_sucursal = xBodega.id_empresa_sucursal;
                        DocNuevo.cod_temporada_temporada = CodTemporada;
                        db.Documentos_CompraGenerales.Add(DocNuevo);
                        db.SaveChanges();

                        //Crear detalle de Documento
                        Detalle_DocumentoCompraGeneral DetalleDocNuevo = new Detalle_DocumentoCompraGeneral();
                        DetalleDocNuevo.cod_documentocomprageneral_documentocomprageneral = NumDoc.ToString();
                        DetalleDocNuevo.cod_proveedor_proveedor = docx.cod_proveedor_proveedor;
                        DetalleDocNuevo.cod_tipodocumento_tipodocumentoventa = (long)((xopciones.cod_documentocompra_guiainterna != null) ? xopciones.cod_documentocompra_guiainterna : 0);
                        DetalleDocNuevo.num_valor_detalledocumentocomprageneral = double.Parse(docx.num_neto_documentocomprageneral.ToString());
                        DetalleDocNuevo.num_cantidad_detalledocumentocomprageneral = NuevaCantidad;
                        DetalleDocNuevo.num_descuento_detalledocumentocomprageneral = double.Parse(docx.num_descuento_documentocomprageneral.ToString());
                        DetalleDocNuevo.cod_producto_materiaprima = CodProducto;
                        DetalleDocNuevo.cod_empresa_empresa = xBodega.Sucursal_empresa.cod_empresa_empresa;
                        DetalleDocNuevo.cod_temporada_temporada = CodTemporada;
                        db.Detalle_DocumentoCompraGeneral.Add(DetalleDocNuevo);
                        db.SaveChanges();

                        //Crear snap para lotear en destino conservando el lote
                        snap_DocCompraControlIngreso SnapDocCompra = new snap_DocCompraControlIngreso();
                        SnapDocCompra.num_lote = getmoverloterequest.num_lote;
                        SnapDocCompra.cod_tipodocumento_tipodocumentoventa = (long)((xopciones.cod_documentocompra_guiainterna != null) ? xopciones.cod_documentocompra_guiainterna : 0);
                        SnapDocCompra.cod_documentocomprageneral_documentocomprageneral = NumDoc.ToString();
                        SnapDocCompra.cod_proveedor_proveedor = docx.cod_proveedor_proveedor;
                        SnapDocCompra.cod_empresa_empresa = xBodega.Sucursal_empresa.cod_empresa_empresa;
                        SnapDocCompra.num_cantidad = NuevaCantidad;
                        SnapDocCompra.num_valor = double.Parse(docx.num_neto_documentocomprageneral.ToString());
                        db.snap_DocCompraControlIngreso.Add(SnapDocCompra);
                        db.SaveChanges();
                    }
                    else//no es a tienda
                    {

                        if (EsProductoTerminado)
                        {
                            //Si es producto terminado
                            //Resta el origen
                            var xDetalleInsumoProductoTerminado = db.ControlIngresoInsumoProductoTerminado.FirstOrDefault(s => s.num_lote == NumLote
                                && s.num_posicion_x == OriginalPosX
                                && s.num_posicion_y == OriginalPosY
                                && s.num_posicion_z == OriginalPosZ);
                            xDetalleInsumoProductoTerminado.num_cantidad -= NuevaCantidad;
                            db.SaveChanges();

                            //Se crea un lote con el mismo numero, nueva posicion, nueva cantidad
                            ControlIngresoInsumoProductoTerminado Control = new ControlIngresoInsumoProductoTerminado();
                            Control.num_lote = NumLote;
                            Control.cod_bodega_bodega = xDetalleInsumoProductoTerminado.cod_bodega_bodega;
                            Control.cod_seccionbodega_seccionbodega = xDetalleInsumoProductoTerminado.cod_seccionbodega_seccionbodega;
                            Control.num_posicion_x = int.Parse(NuevaPosX.ToString());
                            Control.num_posicion_y = int.Parse(NuevaPosY.ToString());
                            Control.num_posicion_z = int.Parse(NuevaPosZ.ToString());
                            Control.cod_snapcinsumo_snapcimsumo = xDetalleInsumoProductoTerminado.cod_snapcinsumo_snapcimsumo;
                            Control.cod_productoterminado_productoterminado = xDetalleInsumoProductoTerminado.cod_productoterminado_productoterminado;
                            Control.cod_temporada_temporada = xDetalleInsumoProductoTerminado.cod_temporada_temporada;
                            Control.cod_condicionproducto_condicionproducto = xDetalleInsumoProductoTerminado.cod_condicionproducto_condicionproducto;
                            Control.cod_condicionbodega_condicionbodega = xDetalleInsumoProductoTerminado.cod_condicionbodega_condicionbodega;
                            Control.cod_ordenproduccion_ordenproduccion = xDetalleInsumoProductoTerminado.cod_ordenproduccion_ordenproduccion;
                            Control.cod_recetaproductoterminado_recetaproductoterminado = xDetalleInsumoProductoTerminado.cod_recetaproductoterminado_recetaproductoterminado;
                            Control.dat_fechavencimiento = xDetalleInsumoProductoTerminado.dat_fechavencimiento;
                            Control.num_cantidad = NuevaCantidad;
                            Control.num_valor = xDetalleInsumoProductoTerminado.num_valor;
                            Control.num_horas = xDetalleInsumoProductoTerminado.num_horas;
                            Control.num_cantidadbultos = NuevaCantidad;
                            db.ControlIngresoInsumoProductoTerminado.Add(Control);
                            db.SaveChanges();
                        }
                        else
                        {
                            //Resta el origen
                            var xDetalleInsumo = db.ControlIngresoInsumo.FirstOrDefault(s => s.num_lote_controlingresoinsumo == NumLote
                                && s.num_posicion_x == OriginalPosX
                                && s.num_posicion_y == OriginalPosY
                                && s.num_posicion_z == OriginalPosZ);
                            xDetalleInsumo.num_cantidad_ControlIngresoInsumo -= NuevaCantidad;
                            db.SaveChanges();

                            //Se crea un lote con el mismo numero, nueva posicion, nueva cantidad
                            ControlIngresoInsumo Control = new ControlIngresoInsumo();
                            Control.num_lote_controlingresoinsumo = NumLote;
                            Control.cod_bodega_bodega = xDetalleInsumo.cod_bodega_bodega;
                            Control.cod_seccionbodega_seccionbodega = xDetalleInsumo.cod_seccionbodega_seccionbodega;
                            Control.num_posicion_x = int.Parse(NuevaPosX.ToString());
                            Control.num_posicion_y = int.Parse(NuevaPosY.ToString());
                            Control.num_posicion_z = int.Parse(NuevaPosZ.ToString());
                            Control.cod_snapcinsumo_snapcimsumo = xDetalleInsumo.cod_snapcinsumo_snapcimsumo;
                            Control.cod_producto_materiaprima = xDetalleInsumo.cod_producto_materiaprima;
                            Control.cod_temporada_temporada = xDetalleInsumo.cod_temporada_temporada;
                            Control.cod_condicionproducto_condicionproducto = xDetalleInsumo.cod_condicionproducto_condicionproducto;
                            Control.cod_condicionbodega_condicionbodega = xDetalleInsumo.cod_condicionbodega_condicionbodega;
                            Control.dat_fechavencimiento_ControlIngresoInsumo = xDetalleInsumo.dat_fechavencimiento_ControlIngresoInsumo;
                            Control.num_cantidad_ControlIngresoInsumo = NuevaCantidad;
                            Control.num_valor_controlingresoinsumo = xDetalleInsumo.num_valor_controlingresoinsumo;
                            Control.num_horas_controlingresoinsumo = xDetalleInsumo.num_horas_controlingresoinsumo;
                            Control.num_cantidadbultos_controlingresoinsumo = NuevaCantidad;
                            Control.bit_analisisharina_controlingresoinsumo = xDetalleInsumo.bit_analisisharina_controlingresoinsumo;
                            Control.cod_productogenerico_materiaprima = xDetalleInsumo.cod_productogenerico_materiaprima;
                            db.ControlIngresoInsumo.Add(Control);
                            db.SaveChanges();

                        }
                    }
                    dbTran.Commit();
                    res.ok = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.ok = false;
                }
            }
            return res;
        }


        public ResultadoDetalleLoteUbicacion GetDetalleLoteUbicacion(GetDetalleLoteUbicacionRequest getdetalleloteubicacionrequest)
        {
            string Xlote = getdetalleloteubicacionrequest.num_lote;
            bool EsProductoTerminado = false;
            var res = new ResultadoDetalleLoteUbicacion();

            using (var db = new GestionComercialEntities())
            {

                if (getdetalleloteubicacionrequest.TipoLote == "PT")
                {
                    EsProductoTerminado = true;
                }

                if (EsProductoTerminado)
                {
                    var fol = db.ControlIngresoInsumoProductoTerminado.Include("Productos_Terminados").Where(s => s.num_lote == Xlote).ToList();

                    if (fol.Count() > 0)
                    {
                        foreach (var item in fol)
                        {
                            var lotes = new Lote();
                            lotes.cantidad = item.num_cantidad.Value;
                            lotes.fechaVencimiento = (DateTime)item.dat_fechavencimiento;
                            lotes.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                            lotes.cod_condicionbodega_condicionbodega = item.cod_condicionbodega_condicionbodega;
                            lotes.x = item.num_posicion_x;
                            lotes.y = item.num_posicion_y;
                            lotes.z = item.num_posicion_z;
                            lotes.cod_bodega = item.cod_bodega_bodega.ToString();
                            lotes.bodega = item.Bodegas.des_nombre_bodega;
                            lotes.cod_seccion = item.cod_seccionbodega_seccionbodega.ToString();
                            lotes.seccion = (string.IsNullOrEmpty(lotes.cod_seccion)) ? null : item.SeccionBodega.des_nombre_seccionbodega;
                            lotes.cod_producto = item.cod_productoterminado_productoterminado;
                            lotes.producto = item.Productos_Terminados.des_descripcion_productoterminado;
                            lotes.num_lote = Xlote;
                            lotes.valor = item.num_valor.Value;
                            res.listadodetallelote.Add(lotes);
                        }
                        res.valido = true;
                    }
                }
                else
                {
                    var fol = db.ControlIngresoInsumo.Include("Materias_Primas").Where(s => s.num_lote_controlingresoinsumo == Xlote).ToList();

                    if (fol.Count() > 0)
                    {
                        foreach (var item in fol)
                        {
                            var lotes = new Lote();
                            lotes.cantidad = item.num_cantidad_ControlIngresoInsumo;
                            lotes.fechaVencimiento = (DateTime)item.dat_fechavencimiento_ControlIngresoInsumo;
                            lotes.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                            lotes.cod_condicionbodega_condicionbodega = item.cod_condicionbodega_condicionbodega;
                            lotes.x = item.num_posicion_x;
                            lotes.y = item.num_posicion_y;
                            lotes.z = item.num_posicion_z;
                            lotes.cod_bodega = item.cod_bodega_bodega.ToString();
                            lotes.bodega = item.Bodegas.des_nombre_bodega;
                            lotes.cod_seccion = item.cod_seccionbodega_seccionbodega.ToString();
                            lotes.seccion = (string.IsNullOrEmpty(lotes.cod_seccion)) ? null : item.SeccionBodega.des_nombre_seccionbodega;
                            lotes.cod_producto = item.cod_producto_materiaprima;
                            lotes.producto = item.Materias_Primas.des_nombre_materiaprima;
                            lotes.num_lote = Xlote;
                            lotes.valor = item.num_valor_controlingresoinsumo;
                            res.listadodetallelote.Add(lotes);
                        }
                        res.valido = true;
                    }
                }
            }
            return res;
        }
        public ResultadoLoteporProducto GetLoteporProducto(GetLoteporProductoRequest getloteporproductorequest)
        {
            string Tipo = "";
            var res = new ResultadoLoteporProducto();
            using (var db = new GestionComercialEntities())
            {
                bool Prod = db.Materias_Primas.Any(MP => MP.cod_producto_materiaprima == getloteporproductorequest.CodProducto && MP.bit_materiaprima_productoterminado == true);

                if (Prod)
                {
                    Tipo = "PT";
                }
                else
                {
                    Tipo = "MP";
                }
                var fol = db.V_ResumenProductoLoteadoSinLote.Where(s => s.cod_producto_materiaprima == getloteporproductorequest.CodProducto).ToList();
                if (fol.Count() > 0)
                {
                    foreach (var item in fol)
                    {
                        var lote = new Lote();
                        lote.num_loteSinLote = item.Num_Lote.ToString();
                        lote.cantidad = item.Cantidad;
                        if (item.dat_fecha_controlingresoinsumo != null)
                        {
                            lote.fechaCreacion = (DateTime)item.dat_fecha_controlingresoinsumo;
                        }
                        lote.tipo = Tipo;
                        res.listadoloteproducto.Add(lote);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoLotesImprimir LotesImprimir(LotesImprimirRequest lotesimprimirrequest)
        {
            var Fecha = Convert.ToDateTime(lotesimprimirrequest.fecha.ToString());
            var res = new ResultadoLotesImprimir();
            int CodEtiqueta = int.Parse(lotesimprimirrequest.CodEtiqueta.ToString());
            using (var db = new GestionComercialEntities())
            {
                var detfol = new List<V_LotesaImprimir>();
                switch (lotesimprimirrequest.TipoImpresion.ToString())
                {
                    case "Todos":

                        detfol = db.V_LotesaImprimir.Where(s => s.cod_documentocomprageneral_documentocomprageneral
                                          == lotesimprimirrequest.cod_documentocomprageneral_documentocomprageneral && s.cod_proveedor_proveedor
                                          == lotesimprimirrequest.cod_proveedor && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Day
                                          == Fecha.Day && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Month
                                          == Fecha.Month && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Year
                                          == Fecha.Year).ToList();
                        break;

                    case "Producto":

                        detfol = db.V_LotesaImprimir.Where(s => s.cod_documentocomprageneral_documentocomprageneral
                                           == lotesimprimirrequest.cod_documentocomprageneral_documentocomprageneral && s.cod_proveedor_proveedor
                                           == lotesimprimirrequest.cod_proveedor && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Day
                                           == Fecha.Day && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Month
                                           == Fecha.Month && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Year
                                           == Fecha.Year && s.cod_producto_materiaprima == lotesimprimirrequest.cod_producto).ToList();
                        break;

                    case "Lote":

                        string Xlote = lotesimprimirrequest.num_lote;
                        detfol = db.V_LotesaImprimir.Where(s => s.cod_documentocomprageneral_documentocomprageneral
                                           == lotesimprimirrequest.cod_documentocomprageneral_documentocomprageneral && s.cod_proveedor_proveedor
                                           == lotesimprimirrequest.cod_proveedor && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Day
                                           == Fecha.Day && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Month
                                           == Fecha.Month && ((DateTime)(s.dat_fechafactura_documentocomprageneral)).Year
                                           == Fecha.Year && s.num_lote == Xlote).ToList();
                        break;
                }
                if (detfol != null)
                {
                    TcpClient Zebraclient = new TcpClient();
                    Zebraclient.SendTimeout = 2500;
                    Zebraclient.ReceiveTimeout = 2500;
                    //Obtener Ip y Puerto por el nombre de la impresora
                    var impresora = db.Impresoras.FirstOrDefault(s => s.NombreImpresora == lotesimprimirrequest.Impresora);
                    Zebraclient.Connect(impresora.Ip, impresora.Puerto);
                    //Obtener ZPL del código de la etiqueta
                    var zpl = db.Etiquetas_ZPL.FirstOrDefault(s => s.id_etiquetaZPL == CodEtiqueta);
                    foreach (var item in detfol)
                    {
                        try
                        {
                            if (Zebraclient.Connected == true)
                            {
                                var numLote = item.num_lote.ToString();
                                var cantidad = item.num_cantidad;
                                var prod = item.des_nombre_materiaprima;
                                string Lote = numLote;
                                string Zpl = zpl.des_zpl;

                                Zpl = Zpl.Replace("#lote#", Lote);
                                Zpl = Zpl.Replace("#cantidad#", cantidad.ToString());
                                Zpl = Zpl.Replace("#producto#", prod);

                                NetworkStream mynetworkstream;
                                StreamReader mystreamreader;
                                StreamWriter mystreamwriter;
                                mynetworkstream = Zebraclient.GetStream();
                                mystreamreader = new StreamReader(mynetworkstream);
                                mystreamwriter = new StreamWriter(mynetworkstream);
                                mystreamwriter.WriteLine(Zpl);
                                mystreamwriter.Flush();
                            }
                        }
                        catch
                        {
                            res.valido = false;
                        }

                    }
                    Zebraclient.Close();
                    res.valido = true;

                }

            }
            return res;
        }

        public ResultadoEmpresa GetEmpresa()
        {
            var res = new ResultadoEmpresa();
            using (var db = new GestionComercialEntities())
            {
                var det = db.Empresas_Holding.ToList();
                if (det.Count() > 0)
                {
                    foreach (var item in det)
                    {
                        var Emp = new Model.BodegaMovil.Empresas_Holding();
                        Emp.cod_empresa_empresa = item.cod_empresa_empresa;
                        Emp.des_nombre_empresa = item.des_nombre_empresa;
                        res.lista.Add(Emp);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        public ResultadoSucursalEmpresa GetSucursalEmpresa(GetSucursalPorEmpresaRequest GetSucursalPorEmpresarequest)
        {
            var res = new ResultadoSucursalEmpresa();
            using (var db = new GestionComercialEntities())
            {
                var det = db.Sucursal_empresa.Where(s => s.cod_empresa_empresa == GetSucursalPorEmpresarequest.cod_empresa_empresa).ToList();
                if (det.Count() > 0)
                {
                    foreach (var item in det)
                    {
                        var Suc = new Model.BodegaMovil.Sucursal_empresa();
                        Suc.id_empresa_sucursal = item.id_empresa_sucursal;
                        Suc.des_sucursal = item.des_sucursal;
                        res.lista.Add(Suc);
                    }
                    res.valido = true;
                }
            }
            return res;
        }


        public ResultadoBodega GetBodegaPorSucursalString(GetBodegaPorSucursalStringRequest getbodegaporsucursalrequest)
        {
            var res = new ResultadoBodega();

            long Cod = long.Parse(getbodegaporsucursalrequest.sucursal.ToString());

            var bodegas = (from bod in gcEntities.Bodegas
                           where bod.id_empresa_sucursal == Cod
                           select new Model.BodegaMovil.Bodegas()
                           {
                               cod_bodega = bod.cod_bodega_bodega,
                               nombre = bod.des_nombre_bodega
                           }
                          );


            res.listadobodegas = bodegas.ToList();
            return res;
        }


        public ResultadoCrearInventario CrearInventario(CrearInventarioRequest CrearInventariorequest)
        {
            ResultadoCrearInventario res = new ResultadoCrearInventario();
            long NumInventario = 0;
            long NumDetalleInventario = 0;
            long CodBodega = long.Parse(CrearInventariorequest.cod_bodega_bodega.ToString());
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    //Crear Encabezado            
                    Enc_TomaInventario _Enc_TomaInventario = new Enc_TomaInventario();
                    _Enc_TomaInventario.Num_doc_tomainventario = 0;
                    var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                    _Enc_TomaInventario.Dat_fecha_tomainventario = Convert.ToDateTime(Fecha);
                    _Enc_TomaInventario.cod_fichapersonal_fichapersonal = long.Parse(CrearInventariorequest.cod_fichapersonal_fichapersonal);
                    _Enc_TomaInventario.cod_empresa_empresa = CrearInventariorequest.cod_empresa_empresa;
                    _Enc_TomaInventario.id_empresa_sucursal = long.Parse(CrearInventariorequest.id_empresa_sucursal);
                    _Enc_TomaInventario.cod_bodega_bodega = long.Parse(CrearInventariorequest.cod_bodega_bodega);
                    _Enc_TomaInventario.Ajusta_soloCapturado = bool.Parse(CrearInventariorequest.Ajusta_soloCapturado);
                    _Enc_TomaInventario.Dat_fecha_fininventario = null;
                    db.Enc_TomaInventario.Add(_Enc_TomaInventario);
                    db.SaveChanges();
                    NumInventario = _Enc_TomaInventario.Cod_tomainventario;

                    //Obtener productos con stock de la bodega 
                    var Stock = db.V_LotesControlIngresoInsumo
                        .Where(d => d.cod_bodega_bodega == CodBodega && d.num_cantidad > 0).ToList().OrderBy(s => s.cod_producto_materiaprima);

                    foreach (var item in Stock)
                    {
                        var LineaDetalle = db.Det_TomaInventario.FirstOrDefault(oc => oc.Cod_tomainventario == NumInventario
                        && oc.cod_producto_materiaprima == item.cod_producto_materiaprima);
                        if (LineaDetalle != null) // si ya existe entonces sumar cantidad
                        {
                            LineaDetalle.num_cantidadstockinicial_total += item.num_cantidad;
                            db.SaveChanges();
                        }
                        else
                        {
                            //Inserto en Det_toma si la materia prima                                   
                            Det_TomaInventario _Det_TomaInventario = new Det_TomaInventario();
                            _Det_TomaInventario.Cod_tomainventario = NumInventario;
                            _Det_TomaInventario.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            var FechaHora = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                            _Det_TomaInventario.Dat_fechahora_tomainventario = Convert.ToDateTime(FechaHora);
                            _Det_TomaInventario.num_cantidadstockinicial_total = item.num_cantidad;
                            _Det_TomaInventario.num_cantidadstockcapturadoinventario = 0;
                            _Det_TomaInventario.num_cantidadcompras = 0;
                            _Det_TomaInventario.num_cantidadventas = 0;
                            _Det_TomaInventario.num_cantidadstockcalculadoinventario = 0;
                            _Det_TomaInventario.Ajustado = false;
                            _Det_TomaInventario.Diferencia = 0;
                            db.Det_TomaInventario.Add(_Det_TomaInventario);
                            db.SaveChanges();
                            NumDetalleInventario = _Det_TomaInventario.Cod_det_tomainventario;
                        }

                        //Obtener ultimo precio materia prima
                        var MateriaP = db.Materias_Primas.FirstOrDefault(m => m.cod_producto_materiaprima == item.cod_producto_materiaprima);
                        //En ambos casos creamos snap con el id de linea detalle
                        Snap_TomaInventario_Lote _Snap_TomaInventario_Lote = new Snap_TomaInventario_Lote();
                        _Snap_TomaInventario_Lote.Cod_tomainventario = NumInventario;
                        _Snap_TomaInventario_Lote.Cod_det_tomainventario = NumDetalleInventario;
                        _Snap_TomaInventario_Lote.cod_producto_materiaprima = item.cod_producto_materiaprima;
                        _Snap_TomaInventario_Lote.num_lote_controlingresoinsumo = item.num_lote;
                        _Snap_TomaInventario_Lote.num_cantidadstockinicial_xlote = item.num_cantidad;
                        _Snap_TomaInventario_Lote.num_ultimoprecio_materiaprima = MateriaP.num_ultimoprecio_materiaprima;
                        _Snap_TomaInventario_Lote.num_posicion_x = item.num_posicion_x;
                        _Snap_TomaInventario_Lote.num_posicion_y = item.num_posicion_y;
                        _Snap_TomaInventario_Lote.num_posicion_z = item.num_posicion_z;
                        _Snap_TomaInventario_Lote.cod_bodega_bodega = item.cod_bodega_bodega;
                        _Snap_TomaInventario_Lote.FechaLote = item.dat_fecha_controlingresoinsumo;
                        _Snap_TomaInventario_Lote.Diferencia = 0;
                        db.Snap_TomaInventario_Lote.Add(_Snap_TomaInventario_Lote);
                        db.SaveChanges();

                    }
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                    res.Cod_tomainventario = NumInventario;

                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }

        public ResultadoExisteInventarioActivo ExisteInventarioActivo(ExisteInventarioActivoRequest ExisteInventarioActivorequest)
        {
            ResultadoExisteInventarioActivo res = new ResultadoExisteInventarioActivo();

            long CodBodega = long.Parse(ExisteInventarioActivorequest.cod_bodega_bodega.ToString());
            long IDempresasucursal = long.Parse(ExisteInventarioActivorequest.id_empresa_sucursal.ToString());
            using (var db = new GestionComercialEntities())

                try
                {
                    var ExisteInventario = db.Enc_TomaInventario.FirstOrDefault(oc => oc.id_empresa_sucursal == IDempresasucursal
                        && oc.cod_empresa_empresa == ExisteInventarioActivorequest.cod_empresa_empresa && oc.cod_bodega_bodega == CodBodega
                        && oc.Dat_fecha_fininventario == null);
                    if (ExisteInventario != null) // si ya existe entonces retorna cod_inventario
                    {
                        res.Cod_tomainventario = ExisteInventario.Cod_tomainventario;
                    }
                }
                catch (Exception)
                {
                    res.Cod_tomainventario = 0;
                }
            return res;

        }


        public ResultadoSumaCantidadInventarioActivo SumaCantidadInventarioActivo(SumaCantidadInventarioActivoRequest SumaCantidadInventarioActivorequest)
        {
            ResultadoSumaCantidadInventarioActivo res = new ResultadoSumaCantidadInventarioActivo();

            long NumInventario = long.Parse(SumaCantidadInventarioActivorequest.Cod_tomainventario);

            using (var db = new GestionComercialEntities())

            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    var Materia = db.Materias_Primas.FirstOrDefault(oc => oc.cod_producto_materiaprima == SumaCantidadInventarioActivorequest.cod_producto_materiaprima);
                    if (Materia != null) // si existe retorna el nombre
                    {
                        res.NombreProductoLeido = Materia.des_nombre_materiaprima;
                    }
                    else //Sin no debe enviar mensaje 
                    {
                        res.Mensaje = "El Producto leído no existe en el maestro de materias primas.";
                        res.NombreProductoLeido = "";
                        throw new System.Exception();
                    }

                    var LineaDetalle = db.Det_TomaInventario.FirstOrDefault(oc => oc.Cod_tomainventario == NumInventario
                    && oc.cod_producto_materiaprima == SumaCantidadInventarioActivorequest.cod_producto_materiaprima);
                    if (LineaDetalle != null) // si ya existe entonces sumar cantidad
                    {

                        LineaDetalle.num_cantidadstockcapturadoinventario += 1;
                        //Actualiza la fecha-hora con la primera lectura del producto
                        if (LineaDetalle.num_cantidadstockcapturadoinventario == 1)
                        {
                            var FechaHora = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                            LineaDetalle.Dat_fechahora_tomainventario = Convert.ToDateTime(FechaHora);
                        }

                        db.SaveChanges();
                        res.Cantidad = long.Parse(LineaDetalle.num_cantidadstockcapturadoinventario.Value.ToString());
                    }
                    else
                    {
                        //Inserto en Det_toma si la materia prima                                   
                        Det_TomaInventario _Det_TomaInventario = new Det_TomaInventario();
                        _Det_TomaInventario.Cod_tomainventario = NumInventario;
                        _Det_TomaInventario.cod_producto_materiaprima = SumaCantidadInventarioActivorequest.cod_producto_materiaprima;
                        var FechaHora = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                        _Det_TomaInventario.Dat_fechahora_tomainventario = Convert.ToDateTime(FechaHora);
                        _Det_TomaInventario.num_cantidadstockinicial_total = 0;
                        _Det_TomaInventario.num_cantidadstockcapturadoinventario = 1;
                        _Det_TomaInventario.num_cantidadcompras = 0;
                        _Det_TomaInventario.num_cantidadventas = 0;
                        _Det_TomaInventario.num_cantidadstockcalculadoinventario = 0;
                        _Det_TomaInventario.Ajustado = false;
                        _Det_TomaInventario.Diferencia = 0;
                        db.Det_TomaInventario.Add(_Det_TomaInventario);
                        db.SaveChanges();
                        res.Cantidad = 1;
                    }

                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }


        public ResultadoRestaCantidadInventarioActivo RestaCantidadInventarioActivo(RestaCantidadInventarioActivoRequest RestaCantidadInventarioActivorequest)
        {
            ResultadoRestaCantidadInventarioActivo res = new ResultadoRestaCantidadInventarioActivo();

            long NumInventario = long.Parse(RestaCantidadInventarioActivorequest.Cod_tomainventario);
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    var Materia = db.Materias_Primas.FirstOrDefault(oc => oc.cod_producto_materiaprima == RestaCantidadInventarioActivorequest.cod_producto_materiaprima);
                    if (Materia != null) // si existe retorna el nombre
                    {
                        res.NombreProductoLeido = Materia.des_nombre_materiaprima;
                    }
                    else //Si no debe enviar mensaje 
                    {
                        res.Mensaje = "El Producto leído no existe en el maestro de materias primas.";
                        res.NombreProductoLeido = "";
                        throw new System.Exception();
                    }

                    var LineaDetalle = db.Det_TomaInventario.FirstOrDefault(oc => oc.Cod_tomainventario == NumInventario
                    && oc.cod_producto_materiaprima == RestaCantidadInventarioActivorequest.cod_producto_materiaprima);
                    if (LineaDetalle != null) // si ya existe entonces restar cantidad
                    {
                        LineaDetalle.num_cantidadstockcapturadoinventario -= 1;
                        db.SaveChanges();
                        res.Cantidad = long.Parse(LineaDetalle.num_cantidadstockcapturadoinventario.Value.ToString());
                    }
                    else
                    {
                        res.Mensaje = "El Producto leído no existe en el detalle de este inventario. No se puede eliminar lectura";
                        res.NombreProductoLeido = "";
                        throw new System.Exception();
                    }
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }


        public ResultadoTerminarInventarioActivo TerminarInventarioActivo(TerminarInventarioActivoRequest TerminarInventarioActivorequest)
        {
            ResultadoTerminarInventarioActivo res = new ResultadoTerminarInventarioActivo();

            long NumInventario = long.Parse(TerminarInventarioActivorequest.Cod_tomainventario);
            double ventas = 0;
            double controlResta = 0;
            double controlSuma = 0;
            bool AjustaSoloLeido = false;


            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {

                    //Eliminar Det_TomaInventario, Snap_TomaInventario_Lote que no se leido, de lo congelado al iniciar el inventario
                    var LineaEncDetalleEmimina = db.Det_TomaInventario.Include("Enc_TomaInventario").Where(oc => oc.Cod_tomainventario == NumInventario
                        && oc.num_cantidadstockcapturadoinventario == 0).ToList();

                    foreach (var itemEl in LineaEncDetalleEmimina)
                    {
                        AjustaSoloLeido = itemEl.Enc_TomaInventario.Ajusta_soloCapturado.Value;
                        if (AjustaSoloLeido)
                        {
                            db.Snap_TomaInventario_Lote.RemoveRange(db.Snap_TomaInventario_Lote.Where(x => x.Cod_tomainventario == itemEl.Cod_tomainventario
                            && x.Cod_det_tomainventario == itemEl.Cod_det_tomainventario));
                            db.SaveChanges();

                            Det_TomaInventario DetalleE = db.Det_TomaInventario.First(pg => pg.Cod_tomainventario == itemEl.Cod_tomainventario
                            && pg.Cod_det_tomainventario == itemEl.Cod_det_tomainventario);
                            db.Det_TomaInventario.Remove(DetalleE);
                            db.SaveChanges();
                        }
                    }

                    //Congela la fecha y hora de termino para obtener datos que afectan el saldo
                    var FechaHora = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");

                    // var FechaHora = "2018-02-09" + ' ' + "10:26";

                    //2018-02-09 10:26

                    DateTime FechaFinInv = Convert.ToDateTime(FechaHora);

                    var LineaEncDetalle = db.Det_TomaInventario.Include("Enc_TomaInventario").Where(oc => oc.Cod_tomainventario == NumInventario).ToList();
                    //Recorre productos del detalle de inventario
                    foreach (var item in LineaEncDetalle)
                    {
                        DateTime FechaInicioInv = Convert.ToDateTime(item.Enc_TomaInventario.Dat_fecha_tomainventario);
                        DateTime FechaInicioLectura = Convert.ToDateTime(item.Dat_fechahora_tomainventario);

                        AjustaSoloLeido = item.Enc_TomaInventario.Ajusta_soloCapturado.Value;

                        //1. Busca ventas de cada producto entre la fecha inicio y fin y actualizar
                        var Ventas = db.Detalle_DocumentosVentas.Include("Documentos_Ventas").Where(oc => oc.Documentos_Ventas.dat_fecha_documentoventa >= FechaInicioInv
                            && oc.Documentos_Ventas.dat_fecha_documentoventa <= FechaFinInv).ToList();

                        foreach (var itemV in Ventas)
                        {
                            ventas += itemV.num_cantidad_detdocumentoventa.Value;
                        }

                        //2. Busca controlingreso entre la fecha inicio y primera lectura. Si el producto tiene el campo "num_cantidadstockinicial_total"
                        // mayor a cero, entonces descontar la cantidad de este control                        
                        var ControlIngresoResta = db.V_CantidadLoteadaMateriaPrimaLote.Where(oc => oc.dat_fecha_controlingresoinsumo >= FechaInicioInv
                            && oc.dat_fecha_controlingresoinsumo <= FechaInicioLectura && oc.cod_bodega_bodega == item.Enc_TomaInventario.cod_bodega_bodega
                            && oc.cod_producto_materiaprima == item.cod_producto_materiaprima).ToList();

                        foreach (var itemCI in ControlIngresoResta)
                        {
                            if (item.num_cantidadstockinicial_total > 0)
                            {
                                controlResta += itemCI.Cantidad_Loteada.Value;
                            }
                            else
                            {
                                controlSuma += itemCI.Cantidad_Loteada.Value;
                            }

                        }
                        //3. Busca controlingreso. Si está entre primera lectura y fin de inventario, se debe sumar al stock o saldo final
                        var ControlIngresoSuma = db.V_CantidadLoteadaMateriaPrimaLote.Where(oc => oc.dat_fecha_controlingresoinsumo >= FechaInicioLectura
                            && oc.dat_fecha_controlingresoinsumo <= FechaFinInv && oc.cod_bodega_bodega == item.Enc_TomaInventario.cod_bodega_bodega
                            && oc.cod_producto_materiaprima == item.cod_producto_materiaprima).ToList();

                        foreach (var itemCIs in ControlIngresoSuma)
                        {
                            controlSuma += itemCIs.Cantidad_Loteada.Value;
                        }

                        //Calcular saldo     
                        double Saldo = item.num_cantidadstockinicial_total.Value - ventas;
                        if (controlResta > 0)
                        {
                            Saldo -= controlResta;
                        }
                        if (controlSuma > 0)
                        {
                            Saldo += controlSuma;
                        }

                        //Actualizar Compras
                        item.num_cantidadcompras = controlSuma;
                        //Ventas
                        item.num_cantidadventas = ventas;
                        //Actualizar fecha fin y saldo 
                        item.num_cantidadstockcalculadoinventario = Saldo;
                        item.Diferencia = item.num_cantidadstockcapturadoinventario - item.num_cantidadstockcalculadoinventario;
                        item.Enc_TomaInventario.Dat_fecha_fininventario = FechaFinInv;
                        db.SaveChanges();

                        //Obtener la linea de lotes asociados en Snap_TomaInventario_Lote y calcular diferencias
                        double Total_Capturado = item.num_cantidadstockcapturadoinventario.Value;
                        var SnapInvlote = db.Snap_TomaInventario_Lote
                           .Where(oc => oc.Cod_tomainventario == item.Cod_tomainventario && oc.Cod_det_tomainventario == item.Cod_det_tomainventario).ToList()
                           .OrderBy(d => d.FechaLote);

                        if (item.Diferencia != 0)
                        {
                            int count = SnapInvlote.Count();
                            int i = 1;

                            foreach (var itemSnap in SnapInvlote)
                            {
                                if (Total_Capturado == 0)
                                {
                                    itemSnap.Diferencia = itemSnap.num_cantidadstockinicial_xlote.Value * -1;
                                }
                                else if (Total_Capturado >= itemSnap.num_cantidadstockinicial_xlote)
                                {
                                    itemSnap.Diferencia = 0;
                                    Total_Capturado = Total_Capturado - itemSnap.num_cantidadstockinicial_xlote.Value;
                                    //si está en el ultimo registro, se debe ver si aún tiene saldo
                                    if (count == i)
                                    {
                                        itemSnap.Diferencia = Total_Capturado;
                                    }
                                }
                                else
                                {
                                    itemSnap.Diferencia = Total_Capturado - itemSnap.num_cantidadstockinicial_xlote;
                                    Total_Capturado = 0;
                                }
                                db.SaveChanges();
                                i += 1;
                            }
                        }
                    }

                    if (LineaEncDetalle.Count() == 0)
                    {
                        var Encx = db.Enc_TomaInventario.FirstOrDefault(oc => oc.Cod_tomainventario == NumInventario);
                        Encx.Dat_fecha_fininventario = FechaFinInv;
                        db.SaveChanges();
                    }

                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }


        public ResultadoListaSolicitudDespachoAprobada ListaSolicitudDespachoAprobada(GetListaSolicitudDespachoAprobadaRequest GetListaSolicitudDespachoAprobadarequest)
        {
            var res = new ResultadoListaSolicitudDespachoAprobada();

            long Num = long.Parse(GetListaSolicitudDespachoAprobadarequest.id_empresa_sucursal.ToString());

            System.DateTime FechaInicio = Convert.ToDateTime(GetListaSolicitudDespachoAprobadarequest.FechaDesde.ToString());
            System.DateTime FechaFin = Convert.ToDateTime(GetListaSolicitudDespachoAprobadarequest.FechaHasta.ToString());

            using (var db = new GestionComercialEntities())
            {
                long estadoAp = 0;
                long estadoFi = 0;
                var estadoop = db.Opciones.FirstOrDefault(MP => MP.cod_bodegaprincipal_opciones == MP.cod_bodegaprincipal_opciones);
                {
                    if (estadoop != null)
                    {
                        estadoAp = estadoop.cod_estadosolicitudAprobada.Value;
                        estadoFi = estadoop.cod_estadosolicitudFinalizada.Value;
                    }
                }
                var det = new List<V_ListadoSolicitudDespachoAprobada>();
                switch (GetListaSolicitudDespachoAprobadarequest.Estado)
                {
                    case "Aprobado":
                        det = db.V_ListadoSolicitudDespachoAprobada.Where(s => s.id_empresa_sucursalAbastecimiento == Num
                            && s.cod_estadosolicitud == estadoAp).ToList();
                        break;
                    case "Finalizado":
                        det = db.V_ListadoSolicitudDespachoAprobada.Where(s => s.id_empresa_sucursalAbastecimiento == Num
                        && s.cod_estadosolicitud == estadoFi
                        && (s.dat_solicituddespacho_fecha >= FechaInicio
                        && s.dat_solicituddespacho_fecha <= FechaFin)).ToList();
                        break;
                }
                if (det.Count() > 0)
                {
                    foreach (var item in det)
                    {
                        var Sol = new Model.BodegaMovil.ListaSolicitudDespachoAprobada();
                        Sol.id_empresa_sucursal = long.Parse(item.id_empresa_sucursal.ToString());
                        Sol.des_sucursal = item.des_sucursal;
                        Sol.num_solicituddespacho_id = item.num_solicituddespacho_id;
                        Sol.prioridad = int.Parse(item.num_solicituddespacho_prioridad.Value.ToString());
                        Sol.dias_espera = item.diasEspera.Value;
                        Sol.cod_empresa_empresa = item.cod_empresa_empresa;
                        res.lista.Add(Sol);
                    }
                    res.valido = true;
                }
            }
            return res;
        }


        public ResultadoListaSolicitudDespachoAprobadaDespachar ListaSolicitudDespachoAprobadaDespachar(GetListaSolicitudDespachoAprobadaDespacharRequest GetListaSolicitudDespachoAprobadaDespacharrequest)
        {
            var res = new ResultadoListaSolicitudDespachoAprobadaDespachar();

            long Num = long.Parse(GetListaSolicitudDespachoAprobadaDespacharrequest.num_solicituddespacho_id.ToString());
            long sucursalSolicita = long.Parse(GetListaSolicitudDespachoAprobadaDespacharrequest.id_empresa_sucursalSolicita.ToString());
            long sucursalAbastece = long.Parse(GetListaSolicitudDespachoAprobadaDespacharrequest.id_empresa_sucursalAbastecimiento.ToString());                       

            using (var db = new GestionComercialEntities())
            {
                long estadoAp = 0;
                long estadoFi = 0;
                var estadoop = db.Opciones.FirstOrDefault(MP => MP.cod_bodegaprincipal_opciones == MP.cod_bodegaprincipal_opciones);
                {
                    if (estadoop != null)
                    {
                        estadoAp = estadoop.cod_estadosolicitudAprobada.Value;
                        estadoFi = estadoop.cod_estadosolicitudFinalizada.Value;
                    }
                }


                var det = new List<V_ListadoSolicitudDespachoAprobadaDespachar>();
                switch (GetListaSolicitudDespachoAprobadaDespacharrequest.Estado)
                {
                    case "Aprobado":                

                         det = db.V_ListadoSolicitudDespachoAprobadaDespachar.Where(s => s.num_solicituddespacho_id == Num
                         && s.id_empresa_sucursal == sucursalSolicita
                         && s.id_empresa_sucursalAbastecimiento == sucursalAbastece
                         && s.cod_estadosolicitud == estadoAp).ToList();

                        break;
                    case "Finalizado":
                        det = db.V_ListadoSolicitudDespachoAprobadaDespachar.Where(s => s.num_solicituddespacho_id == Num
                        && s.id_empresa_sucursal == sucursalSolicita
                        && s.id_empresa_sucursalAbastecimiento == sucursalAbastece
                        && s.cod_estadosolicitud == estadoFi).ToList();
                        break;
                }

                

                if (det.Count() > 0)
                {
                    foreach (var item in det)
                    {
                        var Sol = new Model.BodegaMovil.ListaSolicitudDespachoAprobadaDespachar();
                        Sol.id_empresa_sucursalaDespachar = long.Parse(item.id_empresa_sucursal_adespachar.ToString());
                        Sol.des_sucursalaDespachar = item.des_sucursalDespachar;
                        Sol.cod_producto_materiaprima = item.cod_producto_materiaprima;
                        Sol.des_producto_materiaprima = item.des_nombre_materiaprima;
                        Sol.num_solicituddespacho_cantaprobada = long.Parse(item.num_solicituddespacho_cantaprobada.Value.ToString());
                        Sol.num_valor = double.Parse(item.num_solicituddespacho_costo.Value.ToString());
                        //Aprobada - Despachada
                        Sol.num_cantidadordendespachar = CantDespachar(long.Parse(item.num_solicituddespacho_id.ToString()), long.Parse(item.id_empresa_sucursal.ToString()), item.cod_producto_materiaprima, item.cod_empresa_empresa, long.Parse(item.id_empresa_sucursal_adespachar.ToString()));
                        res.lista.Add(Sol);
                    }
                    res.valido = true;
                }
            }
            return res;
        }

        private long CantDespachar(long num, long emp, string materia, string empresa, long empadesp)
        {
            long CantRet = 0;
            using (var db = new GestionComercialEntities())
            {
                var Despachada = db.V_CantidadOrdenDespachoSolicitud.FirstOrDefault(f => f.num_solicituddespacho_id == num
                                && f.id_empresa_sucursalsolicituddespacho == emp
                                && f.cod_producto_materiaprima == materia
                                && f.cod_empresa_empresasolicituddespacho == empresa
                                && f.id_empresa_sucursaladespachar == empadesp);
                if (Despachada != null)
                {
                    CantRet = long.Parse(Despachada.num_cantidad_ordendespacho.ToString());
                }
                else
                {
                    CantRet = 0;
                }
            }
            return CantRet;

        }

        public ResultadoLoteReservadoSolicitud LoteReservadoSolicitud(GetLoteReservadoSolicitudRequest GetLoteReservadoSolicitudrequest)
        {
            var res = new ResultadoLoteReservadoSolicitud();
            using (var db = new GestionComercialEntities())
            {
                long Num = long.Parse(GetLoteReservadoSolicitudrequest.num_solicituddespacho_id.ToString());
                long id_empresa_sucursalx = long.Parse(GetLoteReservadoSolicitudrequest.id_empresa_sucursal.ToString());
                long id_empresa_sucursalAbasx = long.Parse(GetLoteReservadoSolicitudrequest.id_empresa_sucursalAbastecimiento.ToString());

                //Lista de lotes ya reservados (esto es para mostrar los lotes en la vista que muestra el tipo)
                List<string> listaReservados = db.V_ListadoSolicitudDespachoAprobadaReservada
                        .Where(l => l.num_solicituddespacho_id == Num
                            && l.id_empresa_sucursal == id_empresa_sucursalx
                            && l.id_empresa_sucursalAbastecimiento == id_empresa_sucursalAbasx
                            && l.cod_producto_materiaprima == GetLoteReservadoSolicitudrequest.cod_producto_materiaprima).Select(l => l.num_lote_controlingresoinsumo).ToList();

                var lot = db.V_LotesControlIngresoInsumo.Where(s => listaReservados.Contains(s.num_lote)).ToList().OrderBy(c => c.dat_fechavencimiento).ToList().ToList();

                if (lot.Count() > 0)
                {

                    var lotessel = new Lote();
                    lotessel.num_lote = "Seleccione...";
                    lotessel.tipo = "SE";
                    res.listaLote.Add(lotessel);
                    res.valido = true;

                    foreach (var item in lot)
                    {
                        var lotes = new Lote();
                        lotes.num_lote = item.num_lote;
                        lotes.tipo = item.tipo;
                        lotes.fechaVencimiento = Convert.ToDateTime(item.dat_fechavencimiento);
                        lotes.cantidad = item.num_cantidad;
                        lotes.valor = item.num_valor_controlingresoinsumo.Value;
                        res.listaLote.Add(lotes);
                    }
                }
            }
            return res;
        }


        public ResultadoMotivoODPorCodigo MotivoODPorCodigo(GetMotivoODPorCodigoRequest GetMotivoODPorCodigorequest)
        {
            var res = new ResultadoMotivoODPorCodigo();

            using (var db = new GestionComercialEntities())
            {
                var det = new List<DataModel.Motivo_OD>();
                if (GetMotivoODPorCodigorequest.cod_motivoOD != "")
                {
                    long Num = long.Parse(GetMotivoODPorCodigorequest.cod_motivoOD);
                    det = db.Motivo_OD.Where(s => s.cod_motivoOD == Num).ToList();
                }
                else
                {
                    //Todos
                    det = db.Motivo_OD.Where(s => s.cod_motivoOD == s.cod_motivoOD).ToList();
                }
                if (det.Count() > 0)
                {

                    var MotSel = new Model.BodegaMovil.Motivo_OD();
                    MotSel.des_motivoOD = "Seleccione...";
                    MotSel.cod_motivoOD = -1;
                    res.Lista.Add(MotSel);
                    res.valido = true;

                    foreach (var item in det)
                    {
                        var Mot = new Model.BodegaMovil.Motivo_OD();
                        Mot.cod_motivoOD = item.cod_motivoOD;
                        Mot.des_motivoOD = item.des_motivoOD;
                        res.Lista.Add(Mot);
                    }
                    res.valido = true;
                }
            }
            return res;
        }


        public ResultadoLotePorProductoSucursal LotePorProductoSucursal(GetLotePorProductoSucursalRequest GetLotePorProductoSucursalrequest)
        {
            var res = new ResultadoLotePorProductoSucursal();
            using (var db = new GestionComercialEntities())
            {

                long id_empresa_sucursalAbasx = long.Parse(GetLotePorProductoSucursalrequest.id_empresa_sucursalAbastecimiento.ToString());
                var lot = db.V_LotesControlIngresoInsumoSucursal.Where(s => s.cod_producto_materiaprima == GetLotePorProductoSucursalrequest.cod_producto_materiaprima
                    && s.id_empresa_sucursal == id_empresa_sucursalAbasx
                    && s.num_cantidad > 0).ToList().OrderBy(c => c.dat_fechavencimiento).ToList().ToList();

                if (lot.Count() > 0)
                {
                    var lotessel = new Lote();
                    lotessel.num_lote = "Seleccione...";
                    lotessel.tipo = "SE";
                    res.listaLote.Add(lotessel);
                    res.valido = true;

                    foreach (var item in lot)
                    {
                        var lotes = new Lote();
                        lotes.num_lote = item.num_lote;
                        lotes.tipo = item.tipo;
                        lotes.fechaVencimiento = Convert.ToDateTime(item.dat_fechavencimiento);
                        lotes.cantidad = item.num_cantidad;
                        lotes.valor = item.num_valor_controlingresoinsumo.Value;
                        res.listaLote.Add(lotes);
                    }


                }
            }
            return res;
        }

        public ResultadoCantidadLoteReservadoProductoSolicitud CantidadLoteReservadoProductoSolicitud(GetCantidadLoteReservadoProductoSolicitudRequest GetCantidadLoteReservadoProductoSolicitudrequest)
        {
            var res = new ResultadoCantidadLoteReservadoProductoSolicitud();
            using (var db = new GestionComercialEntities())
            {
                long Num = long.Parse(GetCantidadLoteReservadoProductoSolicitudrequest.num_solicituddespacho_id.ToString());
                long id_empresa_sucursalx = long.Parse(GetCantidadLoteReservadoProductoSolicitudrequest.id_empresa_sucursal.ToString());
                long id_empresa_sucursalAbasx = long.Parse(GetCantidadLoteReservadoProductoSolicitudrequest.id_empresa_sucursalAbastecimiento.ToString());

                var Cant = db.V_ListadoSolicitudDespachoAprobadaReservada
                            .FirstOrDefault(l => l.num_solicituddespacho_id == Num
                            && l.id_empresa_sucursal == id_empresa_sucursalx
                            && l.id_empresa_sucursalAbastecimiento == id_empresa_sucursalAbasx
                            && l.cod_producto_materiaprima == GetCantidadLoteReservadoProductoSolicitudrequest.cod_producto_materiaprima
                            && l.num_lote_controlingresoinsumo == GetCantidadLoteReservadoProductoSolicitudrequest.num_lote);

                if (Cant != null)
                {
                    res.CantidadReservada = Cant.num_cantidad_ControlIngresoInsumoReservada.Value;
                }
            }
            return res;
        }

        bool EsProductoTerminado = false;
        long codMot = 0;
        public ResultadoCrearOrdenDespacho CrearOrdenDespacho(CrearOrdenDespachoRequest CrearOrdenDespachorequest)
        {
            var res = new ResultadoCrearOrdenDespacho();
            long NumOrden = 0;
            long SucSolDespacho = long.Parse(CrearOrdenDespachorequest.id_empresa_sucursalsolicituddespacho.ToString());
            long SucOrdenDespacho = long.Parse(CrearOrdenDespachorequest.id_empresa_sucursalordendespacho.ToString());
            long SucaDespachar = long.Parse(CrearOrdenDespachorequest.id_empresa_sucursalaDespachar.ToString());
            double CantidadSol = double.Parse(CrearOrdenDespachorequest.num_solicituddespacho_cantsolicitada.ToString());
            long NumSolDespacho = long.Parse(CrearOrdenDespachorequest.num_solicituddespacho_id.ToString());
            string EmpOrdenDespacho = "";

            double CantidadRequerida = double.Parse(CrearOrdenDespachorequest.num_solicituddespacho_cantsolicitada);
            double StockActual = 0;
            double StockUtilizado = 0;
            bool EsProductoTerminado = false;
            string Xlote = "";
            long XBodega = 0;


            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {                  
                    var EmDesp = db.Sucursal_empresa.FirstOrDefault(f => f.id_empresa_sucursal == SucSolDespacho);
                    if (EmDesp != null)
                    {
                        EmpOrdenDespacho = EmDesp.cod_empresa_empresa;
                    }
                    //Crear Encabezado            
                    Enc_OrdenDespacho _Enc_OrdenDespacho = new Enc_OrdenDespacho();
                    _Enc_OrdenDespacho.cod_empresa_empresaordendespacho = EmpOrdenDespacho;
                    _Enc_OrdenDespacho.cod_empresa_empresasolicituddespacho = CrearOrdenDespachorequest.cod_empresa_empresasolicituddespacho;
                    _Enc_OrdenDespacho.id_empresa_sucursalsolicituddespacho = SucSolDespacho;
                    _Enc_OrdenDespacho.id_empresa_sucursalordendespacho = SucOrdenDespacho;
                    var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                    _Enc_OrdenDespacho.dat_ordendespacho = Convert.ToDateTime(Fecha);
                    _Enc_OrdenDespacho.num_solicituddespacho_id = NumSolDespacho;
                    _Enc_OrdenDespacho.id_empresa_sucursaladespachar = SucaDespachar;
                    _Enc_OrdenDespacho.num_solicituddespacho_cantsolicitada = CantidadSol;
                    db.Enc_OrdenDespacho.Add(_Enc_OrdenDespacho);
                    db.SaveChanges();
                    NumOrden = _Enc_OrdenDespacho.num_ordendespacho;
                    //Crea Detalles
                    foreach (var item in CrearOrdenDespachorequest.ListaDetalle)
                    {
                        bool EsPick = bool.Parse(item.Es_PickingAsignado.ToString());
                        DataModel.Det_OrdenDespacho _Det_OrdenDespacho = new DataModel.Det_OrdenDespacho();
                        _Det_OrdenDespacho.num_ordendespacho = NumOrden;
                        _Det_OrdenDespacho.cod_bodega_bodega = long.Parse(item.cod_bodega_bodega.ToString());
                        _Det_OrdenDespacho.cod_producto_materiaprima = item.cod_producto_materiaprima;
                        _Det_OrdenDespacho.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                        _Det_OrdenDespacho.num_cantidad_ordendespacho = double.Parse(item.num_cantidad_ordendespacho.ToString());
                        _Det_OrdenDespacho.num_posicion_x = int.Parse(item.num_posicion_x.ToString());
                        _Det_OrdenDespacho.num_posicion_y = int.Parse(item.num_posicion_y.ToString());
                        _Det_OrdenDespacho.num_posicion_z = int.Parse(item.num_posicion_z.ToString());
                        _Det_OrdenDespacho.Es_PickingAsignado = EsPick;
                        _Det_OrdenDespacho.num_costo = double.Parse(item.num_costo.ToString());
                        if (!EsPick)
                        {
                            codMot = long.Parse(item.cod_motivoOD.ToString());
                            _Det_OrdenDespacho.cod_motivoOD = codMot;
                        }
                        db.Det_OrdenDespacho.Add(_Det_OrdenDespacho);
                        db.SaveChanges();

                        long OriginalPosX = long.Parse(item.num_posicion_x);
                        long OriginalPosY = long.Parse(item.num_posicion_y);
                        long OriginalPosZ = long.Parse(item.num_posicion_z);


                        if (!EsPick)
                        {
                          
                            //Descontar del lote
                            bool Prod = db.Materias_Primas.Any(MP => MP.cod_producto_materiaprima == item.cod_producto_materiaprima && MP.bit_materiaprima_productoterminado == true);
                            if (Prod)
                            {
                                EsProductoTerminado = true;
                            }
                            if (EsProductoTerminado)
                            {
                                //Si es producto terminado
                                //Resta el origen
                                var xDetalleInsumoProductoTerminado = db.ControlIngresoInsumoProductoTerminado.FirstOrDefault(s => s.num_lote == item.num_lote_controlingresoinsumo
                                    && s.num_posicion_x == OriginalPosX
                                    && s.num_posicion_y == OriginalPosY
                                    && s.num_posicion_z == OriginalPosZ);
                                xDetalleInsumoProductoTerminado.num_cantidad -= double.Parse(item.num_cantidad_ordendespacho);
                                db.SaveChanges();
                            }
                            else
                            {
                                //Resta el origen
                                var xDetalleInsumo = db.ControlIngresoInsumo.FirstOrDefault(s => s.num_lote_controlingresoinsumo == item.num_lote_controlingresoinsumo
                                    && s.num_posicion_x == OriginalPosX
                                    && s.num_posicion_y == OriginalPosY
                                    && s.num_posicion_z == OriginalPosZ);
                                xDetalleInsumo.num_cantidad_ControlIngresoInsumo -= double.Parse(item.num_cantidad_ordendespacho);
                                db.SaveChanges();
                            }

                            xDetSolicitudDespachoStockReservado = new Det_SolicitudDespachoStockReservado();
                            xDetSolicitudDespachoStockReservado.cod_bodega_bodegaAbastecimiento = long.Parse(item.cod_bodega_bodega);
                            xDetSolicitudDespachoStockReservado.cod_empresa_empresa = EmpOrdenDespacho;
                            xDetSolicitudDespachoStockReservado.id_empresa_sucursal = decimal.Parse(CrearOrdenDespachorequest.id_empresa_sucursalsolicituddespacho);
                            xDetSolicitudDespachoStockReservado.num_solicituddespacho_id = long.Parse(CrearOrdenDespachorequest.num_solicituddespacho_id);
                            xDetSolicitudDespachoStockReservado.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            xDetSolicitudDespachoStockReservado.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            xDetSolicitudDespachoStockReservado.num_cantidad_ControlIngresoInsumoReservada = double.Parse(item.num_cantidad_ordendespacho);
                            xDetSolicitudDespachoStockReservado.num_posicion_x = int.Parse(OriginalPosX.ToString());
                            xDetSolicitudDespachoStockReservado.num_posicion_y = int.Parse(OriginalPosY.ToString());
                            xDetSolicitudDespachoStockReservado.num_posicion_z = int.Parse(OriginalPosZ.ToString());
                            xDetSolicitudDespachoStockReservado.id_empresa_sucursal_adespachar = decimal.Parse(CrearOrdenDespachorequest.id_empresa_sucursalaDespachar);
                            xDetSolicitudDespachoStockReservado.TipoReserva = "Desde Picking Recorrido";
                            db.Det_SolicitudDespachoStockReservado.Add(xDetSolicitudDespachoStockReservado);
                            db.SaveChanges();


                        }
                        else // Si es Picking recorrido, reserva las cantidades

                       // if (EsPick)
                        {                            
                                                             
                           // Insertar las catidades reservadas y que son descontadas del stock en tabla Det_SolicitudDespachoStockReservado                                       
                            xDetSolicitudDespachoStockReservado = new Det_SolicitudDespachoStockReservado();
                            xDetSolicitudDespachoStockReservado.cod_bodega_bodegaAbastecimiento = long.Parse(item.cod_bodega_bodega);
                            xDetSolicitudDespachoStockReservado.cod_empresa_empresa = EmpOrdenDespacho;
                            xDetSolicitudDespachoStockReservado.id_empresa_sucursal = decimal.Parse(CrearOrdenDespachorequest.id_empresa_sucursalsolicituddespacho);
                            xDetSolicitudDespachoStockReservado.num_solicituddespacho_id = long.Parse(CrearOrdenDespachorequest.num_solicituddespacho_id);
                            xDetSolicitudDespachoStockReservado.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            xDetSolicitudDespachoStockReservado.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            xDetSolicitudDespachoStockReservado.num_cantidad_ControlIngresoInsumoReservada = double.Parse(item.num_cantidad_ordendespacho);
                            xDetSolicitudDespachoStockReservado.num_posicion_x = int.Parse(OriginalPosX.ToString());
                            xDetSolicitudDespachoStockReservado.num_posicion_y = int.Parse(OriginalPosY.ToString());
                            xDetSolicitudDespachoStockReservado.num_posicion_z = int.Parse(OriginalPosZ.ToString());
                            xDetSolicitudDespachoStockReservado.id_empresa_sucursal_adespachar = decimal.Parse(CrearOrdenDespachorequest.id_empresa_sucursalaDespachar);
                            xDetSolicitudDespachoStockReservado.TipoReserva = "Desde Picking Asignado";
                            db.Det_SolicitudDespachoStockReservado.Add(xDetSolicitudDespachoStockReservado);
                            db.SaveChanges();
                         }

                            //Fin
                     //   }
                    }
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                    res.num_ordendespacho = NumOrden;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }

                return res;
            }
        }

        public ResultadoCrearOrdenDespachoFinal CrearOrdenDespachoFinal(CrearOrdenDespachoFinalRequest CrearOrdenDespachofinalrequest)
        {
            var res = new ResultadoCrearOrdenDespachoFinal();
            long NumOrden = 0;
            long NumSolDespacho = long.Parse(CrearOrdenDespachofinalrequest.num_solicituddespacho_id.ToString());
            long SucSolDespacho = long.Parse(CrearOrdenDespachofinalrequest.id_empresa_sucursalsolicituddespacho.ToString());
            string EmpSolDespacho = CrearOrdenDespachofinalrequest.cod_empresa_empresasolicituddespacho;
            long SucVar = 0;

            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {                    
                    //Falta sumar y agrupar (Analizar si es necesario)
                    var Solicitud = db.Det_OrdenDespacho.Include("Enc_OrdenDespacho").Where(d => d.Enc_OrdenDespacho.num_solicituddespacho_id == NumSolDespacho
                       && d.Enc_OrdenDespacho.id_empresa_sucursalsolicituddespacho == SucSolDespacho
                       && d.Enc_OrdenDespacho.cod_empresa_empresasolicituddespacho == EmpSolDespacho
                       && d.num_cantidad_ordendespacho > 0).OrderBy(d => d.Enc_OrdenDespacho.id_empresa_sucursaladespachar).ToList();

                    foreach (var item in Solicitud)
                    {
                        if (SucVar != long.Parse(item.Enc_OrdenDespacho.id_empresa_sucursaladespachar.ToString()))
                        {
                            //Crear Encabezado            
                            Enc_OrdenDespachoFinal _Enc_OrdenDespacho = new Enc_OrdenDespachoFinal();
                            _Enc_OrdenDespacho.cod_empresa_empresasolicituddespacho = EmpSolDespacho;
                            _Enc_OrdenDespacho.id_empresa_sucursaladespachar = long.Parse(item.Enc_OrdenDespacho.id_empresa_sucursaladespachar.ToString());
                            _Enc_OrdenDespacho.id_empresa_sucursalsolicituddespacho = SucSolDespacho;
                            _Enc_OrdenDespacho.num_solicituddespacho_id = NumSolDespacho;
                            var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("HH:mm");
                            _Enc_OrdenDespacho.dat_ordendespacho = Convert.ToDateTime(Fecha);
                            db.Enc_OrdenDespachoFinal.Add(_Enc_OrdenDespacho);
                            db.SaveChanges();
                            NumOrden = _Enc_OrdenDespacho.num_ordendespacho;
                        }

                        //Si existe el mismo detalle suma la cantidad
                        var ExisteDetalle = db.Det_OrdenDespachoFinal.FirstOrDefault(d => d.num_ordendespacho == NumOrden
                        && d.id_empresa_sucursaladespachar == item.Enc_OrdenDespacho.id_empresa_sucursaladespachar
                        && d.cod_producto_materiaprima == item.cod_producto_materiaprima);

                        if (ExisteDetalle!= null)
                        {
                            ExisteDetalle.num_cantidad_ordendespacho += double.Parse(item.num_cantidad_ordendespacho.ToString());                          
                            db.SaveChanges();
                        }
                        else
                        {
                            SucVar = long.Parse(item.Enc_OrdenDespacho.id_empresa_sucursaladespachar.ToString());
                            DataModel.Det_OrdenDespachoFinal _Det_OrdenDespacho = new DataModel.Det_OrdenDespachoFinal();
                            _Det_OrdenDespacho.num_ordendespacho = NumOrden;
                            _Det_OrdenDespacho.id_empresa_sucursaladespachar = long.Parse(item.Enc_OrdenDespacho.id_empresa_sucursaladespachar.ToString());
                            _Det_OrdenDespacho.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            _Det_OrdenDespacho.num_solicituddespacho_cantsolicitada = item.Enc_OrdenDespacho.num_solicituddespacho_cantsolicitada;
                            _Det_OrdenDespacho.num_cantidad_ordendespacho = double.Parse(item.num_cantidad_ordendespacho.ToString());
                            _Det_OrdenDespacho.num_costo = item.num_costo;
                            db.Det_OrdenDespachoFinal.Add(_Det_OrdenDespacho);
                            db.SaveChanges();
                        }                       
                    }

                    long estadoFi = 0;
                    var estadoop = db.Opciones.FirstOrDefault(MP => MP.cod_bodegaprincipal_opciones == MP.cod_bodegaprincipal_opciones);
                    {
                        if (estadoop != null)
                        {                           
                            estadoFi = estadoop.cod_estadosolicitudFinalizada.Value;
                        }
                    }

                    var SolicitudCerrar = db.Enc_SolicitudDespacho.FirstOrDefault(d => d.num_solicituddespacho_id == NumSolDespacho
                    && d.id_empresa_sucursal == SucSolDespacho
                    && d.cod_empresa_empresa == EmpSolDespacho);

                    SolicitudCerrar.cod_estadosolicitud = estadoFi;
                    db.SaveChanges();

                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                    res.num_ordendespacho = NumOrden;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }

                return res;
            }
        }


        public ResultadoExisteCantidadSinCumplir ExisteCantidadSinCumplir(ExisteExisteCantidadSinCumplirRequest ExisteExisteCantidadSinCumplirrequest)
        {
            ResultadoExisteCantidadSinCumplir res = new ResultadoExisteCantidadSinCumplir();
            using (var db = new GestionComercialEntities())
            {
                long NumSolDespacho = long.Parse(ExisteExisteCantidadSinCumplirrequest.num_solicituddespacho_id.ToString());
                long SucSolDespacho = long.Parse(ExisteExisteCantidadSinCumplirrequest.id_empresa_sucursalsolicituddespacho.ToString());
                string EmpSolDespacho = ExisteExisteCantidadSinCumplirrequest.cod_empresa_empresasolicituddespacho;

                var _Cantidad = db.V_SolicitudDespachoComparaTotal.FirstOrDefault(d => d.num_solicituddespacho_id == NumSolDespacho
                    && d.id_empresa_sucursalsolicituddespacho == SucSolDespacho                    
                    && d.num_cantidad_ordendespacho < d.num_solicituddespacho_cantaprobada);

                if (_Cantidad != null)
                {
                    res.Existe = true;
                }
                return res;
            }
        }


        public ResultadoExisteNumeroOrden ExisteNumeroOrden(ExisteNumeroOrdenRequest ExisteNumeroOrdenrequest)
        {
            ResultadoExisteNumeroOrden res = new ResultadoExisteNumeroOrden();
            using (var db = new GestionComercialEntities())
            {
                long Num = long.Parse(ExisteNumeroOrdenrequest.num_ordendespacho.ToString());   
                var _Existe = db.Enc_OrdenDespachoFinal.FirstOrDefault(d => d.num_ordendespacho == Num
                    && (d.Confirmada ==true));
                if (_Existe != null)
                {
                    res.Existe = true;
                }
                return res;
            }
        }


        public ResultadoListaDetalleNumeroOrden ListaDetalleNumeroOrden(GetListaDetalleNumeroOrdenRequest GetListaDetalleNumeroOrdenrequest)
        {
            var res = new ResultadoListaDetalleNumeroOrden();
            long Num = long.Parse(GetListaDetalleNumeroOrdenrequest.num_ordendespacho.ToString());
            using (var db = new GestionComercialEntities())
            {
                var det = new List<Det_OrdenDespachoFinal>();             
                det = db.Det_OrdenDespachoFinal.Include("Materias_Primas").Where(s => s.num_ordendespacho == Num).ToList();                       
                if (det.Count() > 0)
                {
                    foreach (var item in det)
                    {
                        var Sol = new Model.BodegaMovil.ListaDetalleOrden();
                        Sol.cod_producto_materiaprima = item.cod_producto_materiaprima.ToString();
                        Sol.des_producto_materiaprima = item.Materias_Primas.des_nombre_materiaprima;
                        Sol.num_cantidad_ordendespacho = long.Parse(item.num_cantidad_ordendespacho.ToString());
                        if (item.num_cantidad_ordendespachoConfirmada==null)
                        {
                            Sol.num_cantidad_ordendespachoConfirmada=0;
                        }
                        else
	                    {
                           Sol.num_cantidad_ordendespachoConfirmada = long.Parse(item.num_cantidad_ordendespachoConfirmada.Value.ToString());   
	                    }
                                            
                        res.lista.Add(Sol);
                    }
                    res.valido = true;
                }
            }
            return res;
        }


        public ResultadoExisteProductoenOrden ExisteProductoenOrden(ExisteProductoenOrdenRequest ExisteProductoenOrdenrequest)
        {
            ResultadoExisteProductoenOrden res = new ResultadoExisteProductoenOrden();
            long Num = long.Parse(ExisteProductoenOrdenrequest.num_ordendespacho.ToString()); 
            using (var db = new GestionComercialEntities())
            {              
                var _Existe = db.Det_OrdenDespachoFinal.FirstOrDefault(d => d.num_ordendespacho == Num && d.cod_producto_materiaprima == ExisteProductoenOrdenrequest.cod_producto_materiaprima);
                if (_Existe != null)
                {
                    res.Existe = true;
                }
                return res;
            }
        }


        public ResultadoSumaCantidadOrden SumaCantidadOrden(SumaCantidadOrdenRequest SumaCantidadOrdenrequest)
        {
            ResultadoSumaCantidadOrden res = new ResultadoSumaCantidadOrden();
            long Num = long.Parse(SumaCantidadOrdenrequest.num_ordendespacho);
            double Cantidad = double.Parse(SumaCantidadOrdenrequest.Cantidad);

            using (var db = new GestionComercialEntities())

            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {                   
                    var LineaDetalle = db.Det_OrdenDespachoFinal.FirstOrDefault(oc => oc.num_ordendespacho == Num
                    && oc.cod_producto_materiaprima == SumaCantidadOrdenrequest.cod_producto_materiaprima);
                    if (LineaDetalle != null) 
                    {
                        LineaDetalle.num_cantidad_ordendespachoConfirmada = Cantidad;                       
                        db.SaveChanges();                       
                    }                   
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }

        public ResultadoConfirmarOrden ConfirmarOrden(ConfirmarOrdenRequest ConfirmarOrdenrequest)
        {
            ResultadoConfirmarOrden res = new ResultadoConfirmarOrden();
            long Num = long.Parse(ConfirmarOrdenrequest.num_ordendespacho);      
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    var Enc = db.Enc_OrdenDespachoFinal.FirstOrDefault(oc => oc.num_ordendespacho == Num);
                    if (Enc != null)
                    {
                        Enc.Confirmada = true;
                        db.SaveChanges();
                    }
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }

        long cargo = 0;
        public ValidaUsuario ValidarUsuario(ValidarUsuarioBRequest validarusuariobrequest)
        {
            var usr = validarusuariobrequest.usuario.Trim();
            var passwrd = validarusuariobrequest.pass.Trim();
            var modulo = validarusuariobrequest.modulo.Trim();

            var vu = new ValidaUsuario();

            if (usr != "")
            {
                //Invocamos entity framew
                using (var db = new GestionComercialEntities())
                {

                    //Cargo Despachador por defecto desde opciones de configuracion
                    var queryOpciones = db.Opciones.FirstOrDefault(o => o.cod_cargo_despachador_opciones == o.cod_cargo_despachador_opciones);

                    //Si es distinto de Null
                    if (queryOpciones != null)
                    {
                        if (modulo == "Despacho")
                        {
                            cargo = Convert.ToInt16(queryOpciones.cod_cargo_despachador_opciones);
                        }
                        else if (modulo == "Bodega")
                        {
                            cargo = Convert.ToInt16(queryOpciones.cod_bodeguero_opciones);
                        }
                    }

                    //firstordefault retorna la primera coincidencia 
                    var queryFicha = db.FichasPersonales.FirstOrDefault(f => f.des_usuario == usr && f.des_paswor_usuario == passwrd);

                    //Si es distinto de Null
                    if (queryFicha != null)
                    {
                        //Validar el cargo del objeto ficha v/ código de cargo sea el definido en la tabla opciones para Despacho
                        if (queryFicha.cod_cargo_cargo != cargo)
                        {
                            vu.valido = false;
                            vu.mensaje = "Cargo del usuario no autorizado para operar en este módulo";

                        }
                        else
                        {
                            vu.valido = true;
                            vu.mensaje = "Ok";
                            vu.Email = queryFicha.des_mail_fichapersonal;
                            vu.NombreCompleto = queryFicha.des_nombre_fichapersonal + " " + queryFicha.des_appaterno_fichapersonal + " " + queryFicha.des_apmaterno_fichapersonal;
                        }
                    }
                    else
                    {
                        vu.valido = false;
                        vu.mensaje = "Usuario o clave incorrectos";
                    }
                }
            }
            else
            {
                vu.valido = false;
                vu.mensaje = "Debe indicar usuario";
            }
            return vu;
        }


        public ResultadoGetDescuentaStockFifoMateriaPrima GetDescuentaStockFifoMateriaPrima(GetDescuentaStockFifoMateriaPrimaRequest GetDescuentaStockFifoMateriaprimaRequest)
        {
            double CantidadRequerida = GetDescuentaStockFifoMateriaprimaRequest.CantidadRequerida;
            double StockActual = 0;
            double StockUtilizado = 0;
            bool EsProductoTerminado = false;
            string Xlote = "";
            long XBodega = GetDescuentaStockFifoMateriaprimaRequest.cod_bodega_bodega;

            ResultadoGetDescuentaStockFifoMateriaPrima res = new ResultadoGetDescuentaStockFifoMateriaPrima();
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (db.Materias_Primas.Any(MP => MP.cod_producto_materiaprima == GetDescuentaStockFifoMateriaprimaRequest.cod_producto_materiaprima && MP.bit_esproductoterminado == true))
                    {
                        EsProductoTerminado = true;
                    }

                    if (!EsProductoTerminado) //Materia Prima
                    {
                        //1. Busca lotes que tengan stock del producto
                        var Stock = db.ControlIngresoInsumo.Where(oc => oc.cod_producto_materiaprima == GetDescuentaStockFifoMateriaprimaRequest.cod_producto_materiaprima
                            && oc.cod_bodega_bodega == GetDescuentaStockFifoMateriaprimaRequest.cod_bodega_bodega && oc.num_cantidad_ControlIngresoInsumo > 0).OrderBy(s => s.dat_fechavencimiento_ControlIngresoInsumo).ToList();

                        if (Stock == null)
                        {
                            res.Mensaje = "No existe stock para el producto indicado";
                            res.realizado = false;
                        }
                        else
                        {
                            foreach (var itemV in Stock)
                            {
                                if (CantidadRequerida == 0)
                                {
                                    break;
                                }

                                Xlote = itemV.num_lote_controlingresoinsumo;
                                StockActual = double.Parse(itemV.num_cantidad_ControlIngresoInsumo.ToString());
                                if (CantidadRequerida > StockActual)
                                {
                                    itemV.num_cantidad_ControlIngresoInsumo = 0;
                                    CantidadRequerida -= StockActual;
                                    StockUtilizado = StockActual;
                                }
                                else
                                {
                                    itemV.num_cantidad_ControlIngresoInsumo -= CantidadRequerida;
                                    StockUtilizado = CantidadRequerida;
                                    CantidadRequerida -= CantidadRequerida;
                                }
                                db.SaveChanges();

                                switch (GetDescuentaStockFifoMateriaprimaRequest.Tipo)
                                {

                                    //Insertar las catidades reservadas y que son descontadas del stock en tabla Det_SolicitudDespachoStockReservado
                                    case "Picking_Recorrido":
                                        xDetSolicitudDespachoStockReservado = new Det_SolicitudDespachoStockReservado();
                                        xDetSolicitudDespachoStockReservado.cod_bodega_bodegaAbastecimiento = itemV.cod_bodega_bodega;
                                        xDetSolicitudDespachoStockReservado.cod_empresa_empresa = GetDescuentaStockFifoMateriaprimaRequest.cod_empresa_empresa;
                                        xDetSolicitudDespachoStockReservado.id_empresa_sucursal = GetDescuentaStockFifoMateriaprimaRequest.id_empresa_sucursal;
                                        xDetSolicitudDespachoStockReservado.num_solicituddespacho_id = GetDescuentaStockFifoMateriaprimaRequest.Numero;
                                        xDetSolicitudDespachoStockReservado.cod_producto_materiaprima = GetDescuentaStockFifoMateriaprimaRequest.cod_producto_materiaprima;
                                        xDetSolicitudDespachoStockReservado.num_lote_controlingresoinsumo = itemV.num_lote_controlingresoinsumo;
                                        xDetSolicitudDespachoStockReservado.num_cantidad_ControlIngresoInsumoReservada = StockUtilizado;
                                        xDetSolicitudDespachoStockReservado.num_posicion_x = itemV.num_posicion_x;
                                        xDetSolicitudDespachoStockReservado.num_posicion_y = itemV.num_posicion_y;
                                        xDetSolicitudDespachoStockReservado.num_posicion_z = itemV.num_posicion_z;
                                        xDetSolicitudDespachoStockReservado.id_empresa_sucursal_adespachar = GetDescuentaStockFifoMateriaprimaRequest.id_empresa_sucursal_adespachar;
                                        xDetSolicitudDespachoStockReservado.TipoReserva = "Desde Picking Recorrido";
                                        db.Det_SolicitudDespachoStockReservado.Add(xDetSolicitudDespachoStockReservado);
                                        db.SaveChanges();
                                        break;
                                }
                            }

                        }



                    }
                    else //Producto Terminado
                    {

                        //1. Busca lotes que tengan stock del producto
                        var Stock = db.ControlIngresoInsumoProductoTerminado.Where(oc => oc.cod_productoterminado_productoterminado == GetDescuentaStockFifoMateriaprimaRequest.cod_producto_materiaprima
                            && oc.cod_bodega_bodega == GetDescuentaStockFifoMateriaprimaRequest.cod_bodega_bodega && oc.num_cantidad > 0).OrderBy(s => s.dat_fechavencimiento).ToList();

                        if (Stock == null)
                        {
                            res.Mensaje = "No existe stock para el producto indicado";
                            res.realizado = false;
                        }
                        else
                        {
                            foreach (var itemV in Stock)
                            {
                                if (CantidadRequerida == 0)
                                {
                                    break;
                                }


                                Xlote = itemV.num_lote;
                                StockActual = double.Parse(itemV.num_cantidad.ToString());
                                if (CantidadRequerida > StockActual)
                                {
                                    itemV.num_cantidad = 0;
                                    CantidadRequerida -= StockActual;
                                    StockUtilizado = StockActual;
                                }
                                else
                                {
                                    itemV.num_cantidad -= CantidadRequerida;
                                    StockUtilizado = CantidadRequerida;
                                    CantidadRequerida -= CantidadRequerida;
                                }
                                db.SaveChanges();

                                switch (GetDescuentaStockFifoMateriaprimaRequest.Tipo)
                                {

                                    //Insertar las catidades reservadas y que son descontadas del stock en tabla Det_SolicitudDespachoStockReservado
                                    case "Picking_Recorrido":
                                        xDetSolicitudDespachoStockReservado = new Det_SolicitudDespachoStockReservado();
                                        xDetSolicitudDespachoStockReservado.cod_bodega_bodegaAbastecimiento = itemV.cod_bodega_bodega;
                                        xDetSolicitudDespachoStockReservado.cod_empresa_empresa = GetDescuentaStockFifoMateriaprimaRequest.cod_empresa_empresa;
                                        xDetSolicitudDespachoStockReservado.id_empresa_sucursal = GetDescuentaStockFifoMateriaprimaRequest.id_empresa_sucursal;
                                        xDetSolicitudDespachoStockReservado.num_solicituddespacho_id = GetDescuentaStockFifoMateriaprimaRequest.Numero;
                                        xDetSolicitudDespachoStockReservado.cod_producto_materiaprima = GetDescuentaStockFifoMateriaprimaRequest.cod_producto_materiaprima;
                                        xDetSolicitudDespachoStockReservado.num_lote_controlingresoinsumo = itemV.num_lote;
                                        xDetSolicitudDespachoStockReservado.num_cantidad_ControlIngresoInsumoReservada = StockUtilizado;
                                        xDetSolicitudDespachoStockReservado.num_posicion_x = itemV.num_posicion_x;
                                        xDetSolicitudDespachoStockReservado.num_posicion_y = itemV.num_posicion_y;
                                        xDetSolicitudDespachoStockReservado.num_posicion_z = itemV.num_posicion_z;
                                        xDetSolicitudDespachoStockReservado.id_empresa_sucursal_adespachar = GetDescuentaStockFifoMateriaprimaRequest.id_empresa_sucursal_adespachar;
                                        xDetSolicitudDespachoStockReservado.TipoReserva = "Desde Picking Recorrido";
                                        db.Det_SolicitudDespachoStockReservado.Add(xDetSolicitudDespachoStockReservado);
                                        db.SaveChanges();
                                        break;


                                }
                            }

                        }

                    }


                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.realizado = true;

                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.realizado = false;
                }
                return res;
            }

        }
    }
}