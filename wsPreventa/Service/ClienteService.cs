﻿using Contract;
using DataModel;
using Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Empresa;

namespace Service
{
    public class ClienteService : IClienteService
    {
        private DataModel.GestionComercialEntities gcEntities;

        public ClienteService()
        {
            gcEntities = new DataModel.GestionComercialEntities();
        }

        public ResultadoCliente GetClientePorRut(GetClientePorRutRequest getclienteporrutrequest)
        {
            var cliente = gcEntities.Clientes.FirstOrDefault(c => c.cod_cliente_cliente == getclienteporrutrequest.rut);
            var res = new ResultadoCliente();
            var objCliente = new Cliente();

            if (cliente != null)
            {
                objCliente.rut = cliente.cod_cliente_cliente;
                objCliente.nombre = cliente.des_nombre_cliente;
                objCliente.telefono = cliente.des_telefono_cliente;
                objCliente.Email = cliente.des_mail_cliente;
                var giros = gcEntities.Giros.FirstOrDefault(g => g.cod_giro_giro == cliente.cod_giro_giro);
                if (giros != null) 
                { 
                    var gir = new Giro();
                    gir.cod_giro = giros.cod_giro_giro;
                    gir.nombre = giros.des_giro_giro;
                    objCliente.giro = gir;
                }
                var ciudad = gcEntities.Ciudades.FirstOrDefault(c => c.cod_ciudad_ciudad == cliente.cod_ciudad_ciudad);
                if (ciudad != null) 
                {
                    var ciu = new Ciudad();
                    ciu.cod_ciudad = ciudad.cod_ciudad_ciudad;
                    ciu.nombre = ciudad.des_nombre_ciudad;
                    objCliente.ciudad = ciu;
                }
                objCliente.Direccion = cliente.des_direccion_cliente;
                var comuna = gcEntities.Comunas.FirstOrDefault(co => co.cod_comuna_comuna == cliente.cod_comuna_comuna);
                if (comuna != null)
                {
                    var com = new Comuna();
                    com.Cod_comuna = comuna.cod_comuna_comuna;
                    com.nombre = comuna.des_nombre_comuna;
                    objCliente.comuna = com;
                }
                var sucursal = gcEntities.Clientes_Sucursales.First();
                
                    var suc = new Sucursal();
                    suc.Id = sucursal.cod_sucursal_clientesucursal;
                
                res.valido = true;
                res.cliente = objCliente;
                res.mensaje = "Cliente encontrado";
            }
            else 
            {
                res.valido = false;
                res.cliente = null;
                res.mensaje = "El Cliente no se encuentra en la base de datos";
            }

            return res ;
        }

        public ResultadoCliente AddCliente(AddClienteRequest addclienterequest)
        {
            ResultadoCliente res = new ResultadoCliente();

            var cliente = new Clientes();

            cliente.cod_cliente_cliente = addclienterequest.cliente.rut;
            cliente.des_nombre_cliente = addclienterequest.cliente.nombre;
            cliente.des_telefono_cliente = addclienterequest.cliente.telefono;
            cliente.des_mail_cliente = addclienterequest.cliente.Email;
            cliente.cod_giro_giro = (long)addclienterequest.cliente.giro.cod_giro;
            cliente.des_direccion_cliente = addclienterequest.cliente.Direccion;
            cliente.cod_comuna_comuna = addclienterequest.cliente.comuna.Cod_comuna;
            cliente.cod_vendedor_fichapersonal = (long)addclienterequest.requestHeader.IdPersona;

            gcEntities.Clientes.Add(cliente);
            gcEntities.SaveChanges();

            var sucursal = new Clientes_Sucursales();

            sucursal.cod_cliente_cliente = cliente.cod_cliente_cliente;
            sucursal.cod_comuna_comuna = cliente.cod_comuna_comuna;
            sucursal.des_direccion_clientesucursal = cliente.des_direccion_cliente;
            sucursal.bit_esmatriz_clientesucursal = false;
          

            gcEntities.Clientes_Sucursales.Add(sucursal);
            gcEntities.SaveChanges();

            res.valido = true;
            res.mensaje = "Cliente Agregado";
            res.cliente = null;
            return res;
        }

        public ResultadoCiudad GetCiudades(GetCiudadesRequest gertciudadesrequest)
        {
            var res = new ResultadoCiudad();
            var ciudades = gcEntities.Ciudades.Where(c => c.cod_region_region == gertciudadesrequest.cod_region).OrderBy(ci => ci.des_nombre_ciudad);
            if (ciudades.Count() == 0)
            {
                res.valido = false;
                res.listaCiudades = null;
            }
            else
            {
                foreach (var item in ciudades)
                {
                    var ciudad = new Ciudad();
                    ciudad.cod_ciudad = item.cod_ciudad_ciudad;
                    ciudad.nombre = item.des_nombre_ciudad;
                    res.listaCiudades.Add(ciudad);
                }
                res.valido = true;
            }
            return res;
        }

        public ResultadoRegion GetRegiones(GetRegionesRequest getregionesrequest)
        {
            var res = new ResultadoRegion();
            var regiones = gcEntities.Regiones.OrderBy(r => r.des_nombre_region).ToList();
            if (regiones.Count() == 0)
            {
                res.valido = false;
                res.listaRegiones = null;
            }
            else
            {
                foreach (var item in regiones)
                {
                    var region = new Region();
                    region.cod_region = item.cod_region_region;
                    region.nombre = item.des_nombre_region;
                    res.listaRegiones.Add(region);
                }
                res.valido = true;
            }
            return res;
        }
    }
}
