﻿using Contract;
using DataModel;
using Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ComunaService :IComunaService
    {
        public ResultadoComuna GetComunas(GetComunasRequest getComunasRequest)
        {
            ResultadoComuna res = new ResultadoComuna();
            using (var db = new GestionComercialEntities())
            {
                var comunas = db.Comunas.ToList();

                foreach (var item in comunas)
                {
                    var comuna = new Comuna();
                    comuna.Cod_comuna = item.cod_comuna_comuna;
                    comuna.nombre = item.des_nombre_comuna;
                    res.ListadoComunas.Add(comuna);
                }
            }
            return res;
        }
    }
}
