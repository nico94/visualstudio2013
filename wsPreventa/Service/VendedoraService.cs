﻿using Contract;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Vendedora;

namespace Service
{
    public class VendedoraService : IVendedoraService
    {
        public ResultadoVendedora GetVendedoras(GetVendedorasRequest getvendedorasrequest)
        {
            ResultadoVendedora res = new ResultadoVendedora();

            using (var db = new GestionComercialEntities())
            {
                var empresa = db.Sucursal_empresa.FirstOrDefault(s => s.id_empresa_sucursal == getvendedorasrequest.requestHeader.Establecimiento);
                if (empresa == null)
                {
                    res.valido = false;
                    res.listaVendedoras = null;
                    res.mensaje = "la sucursal no existe";
                }
                else
                {
                    var Vendedora = db.FichasPersonales.ToList().Where(v => v.cod_empresa_empresa == empresa.cod_empresa_empresa && v.cod_cargo_cargo == 1);

                    if (Vendedora.Count() == 0)
                    {
                        res.valido = false;
                        res.listaVendedoras = null;
                        res.mensaje = "La empresa no tiene vendedoras asociadas";
                    }
                    else
                    {
                        foreach (var item in Vendedora)
                        {
                            var ven = new Vendedora();
                            ven.cod_vendedora = item.cod_fichapersonal_fichapersonal;
                            ven.Nombre = item.des_nombre_fichapersonal;
                            ven.Apellido = item.des_appaterno_fichapersonal;
                            ven.foto = item.img_foto_fichapersonal;
                            res.listaVendedoras.Add(ven);
                        }

                        res.valido = true;
                        res.mensaje = "Vendedoras encontradas";
                    }
                }
            }

            return res;
        }
    }
}
