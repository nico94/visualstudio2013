﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using DataModel;
using Model.Envio_Correo;
using System.Diagnostics;
using System.Reflection;
//using EASendMail;


namespace Service
{
public class EnvioCorreoService : IEnvioCorreo
   {
    
        
    public ResultadoEnvioCorreo GetEnvioCorreo(GetEnvioCorreoRequest getenviocorreorequest)
    {
        string response = "OK";
        ResultadoEnvioCorreo res = new ResultadoEnvioCorreo();
        string smtpServer = ConfigurationManager.AppSettings["smtp"];
        SmtpClient client = new SmtpClient(smtpServer);
        if (getenviocorreorequest.correofrom == "")
        {
            getenviocorreorequest.correofrom = ConfigurationManager.AppSettings["correoFrom"];
        }

        MailAddress from = new MailAddress(getenviocorreorequest.correofrom);

        string concat = "";

        foreach (var item in listadoCorreo().listaCorreo)
        {

            if (item.correo != null)
            {
                concat += item.correo + ";";
            }

        }

        getenviocorreorequest.correoto = concat.Substring(0, concat.Length - 1);

        MailAddress to = new MailAddress(getenviocorreorequest.correoto);
        MailMessage mail = new MailMessage(from, to);
        mail.IsBodyHtml = true;
        mail.Subject = getenviocorreorequest.asunto;
        mail.Body = "<font>";
        mail.Body += "<font face=Courier New>Estimado(a),";
        mail.Body += "<br/>"; 
        mail.Body += "Junto con saludar, informo listado productos necesarios para realizar orden de compra:";
        mail.Body += "<br/>"; 
        mail.Body += "<br/>"; 
        mail.Body += "<table> <TABLE BORDER=1 WIDTH=600 HEIGTh=50>";
        mail.Body += "<td>Nombre</td> <td>Stock</td> ";
      
        foreach (var item in getenviocorreorequest.listaDetalleCorreo)
        {

            mail.Body += "<tr>";
            string Texto = item.producto.ToString();
            mail.Body += "<td>" + Texto + "</td>" + "<td>" + item.cantidad.ToString().Trim() +"</td>" + "<br/>";
            mail.Body += "</tr>";

        }
        mail.Body += "</table>";

       // mail.Body += "<hr/>"; //linea
        mail.Body += "<br/>"; //salto de linea
        mail.Body += "<br/>"; 
        mail.Body += "Atte:.";
        mail.Body += "<br/>"; 
        mail.Body += getenviocorreorequest.usuarioenvio;
        mail.Body += "<br/>";
        mail.Body += "Bodega";
        mail.Body += "</font>";
        
        try
        {
            client.Send(mail);
            res.enviado = true;
        }
        catch (SmtpException smtpe)
        {

            response = smtpe.Message;
            res.enviado = false;
        }

        mail.Dispose();
        return res;
    }
         public ResultadoCorreoComprador listadoCorreo()
         {
             //Lista de personas cuyo cargo tiene la opcion de comprador
             var res = new ResultadoCorreoComprador();
             using (var db = new GestionComercialEntities())
             {

                 var x = (from Fichas in db.FichasPersonales
                          join cargo in db.Cargos on Fichas.cod_cargo_cargo equals cargo.cod_cargo_cargo
                          where cargo.esComprador == true
                          select new correoComprador()
                          {
                              correo = Fichas.des_mail_fichapersonal
                          }
                      );

                 res.listaCorreo = x.ToList();
                 return res;
             }

         }
   }
}
