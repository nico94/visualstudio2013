﻿using Contract;
using DataModel;
using Model.Despacho;
using Model.Usuario;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class DespachoService: IDespachoService
    {

        long cargo = 0;

        public ValidaUsuario ValidarUsuario(ValidarUsuarioRequest validarUsuarioRequest)
        {
            var usr = validarUsuarioRequest.usuario.Trim();
            var passwrd = validarUsuarioRequest.pass.Trim();
            var modulo = validarUsuarioRequest.modulo.Trim();
           
            var vu = new ValidaUsuario();

            if (usr != "")
            {
                //Invocamos entity framew
                using (var db = new GestionComercialEntities())
                {
                  
                    //Cargo Despachador por defecto desde opciones de configuracion
                    var queryOpciones = db.Opciones.FirstOrDefault(o => o.cod_cargo_despachador_opciones == o.cod_cargo_despachador_opciones);

                    //Si es distinto de Null
                    if (queryOpciones != null)
                    {
                        if (modulo == "Despacho")
                        {
                            cargo = Convert.ToInt16(queryOpciones.cod_cargo_despachador_opciones);
                        }
                        else if (modulo == "Bodega")
                        {
                            cargo = Convert.ToInt16(queryOpciones.cod_bodeguero_opciones);
                        }
                    }
                    
                    //firstordefault retorna la primera coincidencia 
                    var queryFicha = db.FichasPersonales.FirstOrDefault(f => f.des_usuario == usr && f.des_paswor_usuario == passwrd);

                    //Si es distinto de Null
                    if (queryFicha != null)
                    {
                        //Validar el cargo del objeto ficha v/ código de cargo sea el definido en la tabla opciones para Despacho
                        if (queryFicha.cod_cargo_cargo != cargo)
                        {
                            vu.valido = false;
                            vu.mensaje = "Cargo del usuario no autorizado para operar en este módulo";
                            
                        }
                        else
                        {
                            vu.valido = true;
                            vu.mensaje = "Ok";
                            vu.Email = queryFicha.des_mail_fichapersonal;
                            vu.NombreCompleto = queryFicha.des_nombre_fichapersonal + " " + queryFicha.des_appaterno_fichapersonal + " " + queryFicha.des_apmaterno_fichapersonal;
                        }
                    }
                    else
                    {
                        vu.valido = false;
                        vu.mensaje = "Usuario o clave incorrectos";
                    }
                }
            }
            else 
            {
                vu.valido = false;
                vu.mensaje = "Debe indicar usuario";
            }
            return vu;
        }

        public ResultadoTiket listadoTicket(FiltraTickettRequest filtraticketrequest)
        {

            bool DespaNoDespa = filtraticketrequest.bit_despachada_documentoventa;


            var res = new ResultadoTiket();
            using (var db = new GestionComercialEntities())
            {
                var Despacho = db.V_TicketPorDespachar.Where(o => o.bit_despachada_documentoventa == DespaNoDespa);

                res.valido = Despacho.Any();

                foreach (var item in Despacho)
                {
                    var ticket = new Ticket();
                    ticket.cod_ordencompracliente_ordencompracliente = item.cod_ordencompracliente_ordencompracliente;
                    ticket.dat_fecha_occliente = item.dat_fecha_occliente;
                    ticket.Vendedor = item.Vendedor;
                    res.listaTiket.Add(ticket);
                }
            }
            return res;

        }

        public ResultadoDetalleTiket listadoDetalleTicket(FiltraDetalleTicketRequest filtradetalleticketrequest)
        {
              {
                        long Ticket = filtradetalleticketrequest.cod_ordencompracliente_ordencompracliente;
                         
                        var dt = new ResultadoDetalleTiket();

                        if (Ticket > 0)
                        {

                            using (var db = new GestionComercialEntities())
                            {
                                var DetalleDespacho = db.V_DetalleTicketPorDespachar.Where(o => o.cod_ordencompracliente_ordencompracliente == Ticket);

                                if (DetalleDespacho.Count() == 0)

                                {
                                    dt.valido = false;
                                }
                                else
                                {
                                    foreach (var item in DetalleDespacho)
                                    {
                                        var detalleticket = new DetalleTicket();
                                        detalleticket.cod_ordencompracliente_ordencompracliente = item.cod_ordencompracliente_ordencompracliente;
                                        detalleticket.cod_productoterminado_productoterminado = item.cod_productoterminado_productoterminado;
                                        detalleticket.cod_productoterminado_productoterminado_2 = item.cod_productoterminado_productoterminado_2;
                                        detalleticket.cod_productoterminado_productoterminado_3 = item.cod_productoterminado_productoterminado_3;
                                        detalleticket.des_descripcion_productoterminado = item.des_descripcion_productoterminado;
                                        detalleticket.num_cantidad_detdocumentoventa = item.num_cantidad_detdocumentoventa;
                                        detalleticket.num_cantidad_detdocumentoventadespachada = 0;
                                        dt.listaDetalleTiket.Add(detalleticket);
                                    }

                                    dt.valido = true;
                                }
                            }
                        }
                        return dt;
                    }
        }

        public ResultadoUpdateTicket UpdateTicket(ResultadoUpdateTicketRequest resultadoUpdateticketrequest)
        {
            {
                long Ticket = resultadoUpdateticketrequest.cod_ordencompracliente_ordencompracliente;
                
                var dt = new ResultadoUpdateTicket();

                if (Ticket > 0)
                {
                    using (var db = new GestionComercialEntities())
                    {
                        //Actualiza la(s) ventas asociadas al Ticket (Orden Compra)
                        var DetalleDespacho = db.Documentos_Ventas.Where(o => o.cod_ordencompracliente_ordencompracliente == Ticket);
                        
                        if (DetalleDespacho.Count() == 0)
                        {
                            dt.bit_despachada_documentoventa = false;
                            dt.dat_fecha_Despacho = DateTime.Now;
                        }
                        else
                        {
                            foreach (var item in DetalleDespacho)
                            {
                                dt.bit_despachada_documentoventa = true;
                                item.bit_despachada_documentoventa = true;
                                dt.dat_fecha_Despacho = DateTime.Now;                                                                                           
                            }
                            db.SaveChanges();
                            dt.bit_despachada_documentoventa = true;
                        }

                     }
                }
                return dt;
            }
        }

    }
}
