﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.DashboardWeb;
using DevExpress.DataAccess.ConnectionParameters;

namespace VisorDashboard{
    public partial class Visor : System.Web.UI.Page
    {
        string id = null;
        string usuario = null;
        string index = null;
        string dash = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            id =  Request.QueryString["id"];
            usuario = Request.QueryString["usuario"];
            index = Request.QueryString["index"];

            string costos = Server.MapPath("~/App_Data/Dashboards/Costos.xml");
            string ingresos = Server.MapPath("~/App_Data/Dashboards/Ingresos.xml");
            string balance = Server.MapPath("~/App_Data/Dashboards/Balance.xml");

            if (index != null) {
                switch (index){
                    case "0":
                        dash = costos;
                        break;
                    case "1":
                        dash = ingresos;
                        break;
                    case "2":
                        dash = balance;
                        break;
                    default:
                        dash = null;
                        break;
                }          
                if (dash != null) { 
                    ASPxDashboardViewer1.DashboardSource = dash;
                }
            }
        }

        protected void ASPxDashboardViewer1_CustomParameters(object sender, CustomParametersWebEventArgs e)
        {
            var cuartelID = e.Parameters.FirstOrDefault(p => p.Name == "ID");
            var usuarioID = e.Parameters.FirstOrDefault(p => p.Name == "IdUsuario");
            if (cuartelID != null && id != null)
            {
                cuartelID.Value = id;
            }
            if (usuarioID != null && usuario != null)
            {
                usuarioID.Value = usuario;
            }
        }
    }
}