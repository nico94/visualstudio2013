﻿<%@ Page Title="Visor" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="Visor.aspx.cs" Inherits="VisorDashboard.Visor" %>

<%@ Register Assembly="DevExpress.Dashboard.v16.1.Web, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.DashboardWeb" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        #Content2 {
            min-height: 100%;
            height: 900px;
        }
    </style>
    <dx:ASPxDashboardViewer 
        ID="ASPxDashboardViewer1" 
        runat="server" 
        Width="100%" 
        height="800px"
        oncustomparameters="ASPxDashboardViewer1_CustomParameters">

    </dx:ASPxDashboardViewer>
</asp:Content>