//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSCosecha.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Enc_palletizaje_Cosecha
    {
        public long Folio_Pallet_Cosecha { get; set; }
        public Nullable<System.DateTime> dat_fecha_palletcosecha { get; set; }
        public Nullable<bool> bit_despachado { get; set; }
        public long cod_temporada_temporada { get; set; }
        public Nullable<long> cod_envcosecha_envcosecha { get; set; }
        public Nullable<double> num_kilos_bruto { get; set; }
        public Nullable<double> num_kilos_tara { get; set; }
        public Nullable<double> num_kilos_Neto { get; set; }
    
        public virtual Envase_cosecha Envase_cosecha { get; set; }
    }
}
