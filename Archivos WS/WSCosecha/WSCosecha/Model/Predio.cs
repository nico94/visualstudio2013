//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSCosecha.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Predio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Predio()
        {
            this.Cuarteles = new HashSet<Cuarteles>();
        }
    
        public long cod_predio_predio { get; set; }
        public string des_direccion_predio { get; set; }
        public string des_nombrepredio_predio { get; set; }
        public double num_hectareas_predio { get; set; }
        public string cod_empresa_empresa { get; set; }
        public long cod_centrocosto_centrocosto { get; set; }
        public long cod_comuna_comuna { get; set; }
        public string rut_razonSocialProd { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cuarteles> Cuarteles { get; set; }
    }
}
