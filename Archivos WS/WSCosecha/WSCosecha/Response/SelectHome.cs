﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WSCosecha.Model;

namespace WSCosecha.Response
{
    public class SelectHome
    {
        public List<listaTemporada> listaTemporada = new List<listaTemporada>();
        public List<listaPredio> listaPredio = new List<listaPredio>();
        public List<listaCuartel> listaCuartel = new List<listaCuartel>();
        public List<listaEnvaseCosecha> listaEnvaseCosecha = new List<listaEnvaseCosecha>();

        public bool estado { get; set; }
        public string mensaje { get; set; }
    }
}