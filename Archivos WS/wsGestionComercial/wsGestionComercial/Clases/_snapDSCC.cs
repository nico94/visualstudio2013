﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsGestionComercial.Clases
{
    public class _snapDSCC
    {
        public int cod_subcentrocosto_subcentrocosto { get; set; }
        public float num_porcentaje_snapsubcentrocostodefgasto { get; set; }
        public int cod_centrocosto_centrocosto { get; set; }
        public int cod_definiciongasto_definiciongasto { get; set; }
        public int cod_temporada_temporada { get; set; }
    }
}