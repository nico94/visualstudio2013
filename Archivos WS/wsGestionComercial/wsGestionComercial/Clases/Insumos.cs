﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsGestionComercial.Clases
{


    public class ListaInsumos
    {
        public string cod_producto_materiaprima { get; set; } 
        public double cod_fichapersonal_fichapersonal { get; set; }
        public string des_nombre_materiaprima { get; set; }
        public double Cantidad { get; set; }
        public DateTime Fecha_Entrega { get; set; }
        public long cod_condicionproducto_condicionproducto { get; set; }
        public string des_descripcion_condicionproducto { get; set; }
    }

    public class ListaCondcionesProductos
    {
        public double cod_condicionproducto_condicionproducto { get; set; }
        public string des_descripcion_condicionproducto { get; set; }
        public bool bit_estadoproducto_CondicionesProductos { get; set; }
    }

    public class ListaDetalleInsumosFichaLote
    {
        public string cod_producto_materiaprima { get; set; }
        public string des_nombre_materiaprima { get; set; }
        public long cod_fichapersonal_fichapersonal { get; set; }
        public long cod_bodega_bodega { get; set; }
        public string des_nombre_bodega { get; set; }
        public string des_sucursal { get; set; }
        public double Cantidad_Utilizada { get; set; }
        public double Valor_Insumo { get; set; }
        public int num_posicion_x { get; set; }
        public int num_posicion_y { get; set; }
        public int num_posicion_z { get; set; }
        public string num_lote_controlingresoinsumo { get; set; }
        public DateTime Fecha_Entrega { get; set; }
        public DateTime Fecha_Consumo { get; set; }
        public string num_posicion_xyz { get; set; }
        public string key { get; set; }
    }


    public class ListaDetalleInsumoFichaLoteDevolucion
    {
        public string cod_producto_materiaprima { get; set; }
        public double cod_fichapersonal_fichapersonal { get; set; }
        public double cod_bodega_bodega { get; set; }
        public string num_lote_controlingresoinsumo { get; set; }
        public DateTime fecha_movimiento { get; set; }
        public double cod_fichapersonal_responsable_devolucion { get; set; }
        public double Cantidad { get; set; }
        public string tipo_movimiento { get; set; }
        public DateTime Fecha_Consumo { get; set; }
        public int num_posicion_x { get; set; }
        public int num_posicion_y { get; set; }
        public int num_posicion_z { get; set; }
        public DateTime Fecha_Entrega { get; set; }
        public string key { get; set; }
    }

    public class DetalleInsumoFichaLoteCustom
    {
        public string cod_producto_materiaprima { get; set; }
        public long cod_fichapersonal_fichapersonal { get; set; }
        public long cod_bodega_bodega { get; set; }
        public double Cantidad_Utilizada { get; set; }
        public double Valor_Insumo { get; set; }
        public int num_posicion_x { get; set; }
        public int num_posicion_y { get; set; }
        public int num_posicion_z { get; set; }
        public string num_lote_controlingresoinsumo { get; set; }
        public System.DateTime Fecha_Entrega { get; set; }
        public System.DateTime Fecha_Consumo { get; set; }
        public double cod_fichapersonal_responsable_devolucion { get; set; }
    }

    public class ListaMateriasPrimas
    {
        public string cod_producto_materiaprima { get; set; }
        public string des_nombre_materiaprima { get; set; }
    }

    public class ControlIngresoInsumoCustom
    {
        public string num_lote_controlingresoinsumo { get; set; }
        public Nullable<System.DateTime> dat_fechavencimiento_ControlIngresoInsumo { get; set; }
        public Nullable<double> num_cantidad_ControlIngresoInsumo { get; set; }
        public double num_valor_controlingresoinsumo { get; set; }
        public Nullable<double> num_horas_controlingresoinsumo { get; set; }
        public Nullable<double> num_cantidadbultos_controlingresoinsumo { get; set; }
        public Nullable<bool> bit_analisisharina_controlingresoinsumo { get; set; }
        public Nullable<decimal> cod_seccionbodega_seccionbodega { get; set; }
        public Nullable<int> num_posicion_x { get; set; }
        public Nullable<int> num_posicion_y { get; set; }
        public Nullable<int> num_posicion_z { get; set; }
        public string num_posicion_xyz { get; set; }
        public long cod_bodega_bodega { get; set; }
        public string des_nombre_bodega { get; set; }
        public string key { get; set; }
    }

    public class GastosProducto
    {
        public string cod_producto { get; set; }
        public long cod_snapcinsumo_snapcimsumo { get; set; }
      
    }
    
}