﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsGestionComercial.Clases
{
    public class ListaDetalleProductos
    {
        public string v1 { get; set; }
        public string v2 { get; set; }
        public string v3 { get; set; }
        public string v4 { get; set; }
        public string v5 { get; set; }
    }

    public class DocumentosVenta
    {
        public long cod_documentoventa_documentoventa { get; set; }
        public long num_Documentoventa_NroDoc { get; set; }
        public DateTime Fecha { get; set; }
        public string Cliente { get; set; }
        public double num_neto_documentoventa { get; set; }
    
    }

    public class ListadoSnap_DetalleNeumaticos_controlIngreso
    {
        public long cod_documentoventa_documentoventa { get; set; }
        public long Id_neumatico { get; set; }
        public string num_lote_controlingresoinsumo { get; set; }
        public decimal KMS_neumatico { get; set; }
        public bool EnUso { get; set; }
        public int num_posicion_x { get; set; }
        public int num_posicion_y { get; set; }
        public int num_posicion_z { get; set; }
        public string num_posicion_xyz { get; set; }
        public long cod_bodega_bodega { get; set; }
        public long cod_centrogasto_centrogasto { get; set; }
        public long cod_subcentrogasto_subcentrogasto { get; set; }
        public string des_bodega_bodega { get; set; }
        public string des_centrogasto_centrogasto { get; set; }
        public string des_subcentrogasto_subcentrogasto { get; set; }
        public string materiaPrima { get; set; }
    }
}