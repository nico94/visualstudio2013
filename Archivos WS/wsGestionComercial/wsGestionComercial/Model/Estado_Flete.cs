//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Estado_Flete
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estado_Flete()
        {
            this.Enc_RegistroFlete = new HashSet<Enc_RegistroFlete>();
            this.Opciones_Flete = new HashSet<Opciones_Flete>();
            this.Opciones_Flete1 = new HashSet<Opciones_Flete>();
            this.Opciones_Flete2 = new HashSet<Opciones_Flete>();
            this.Opciones_Flete3 = new HashSet<Opciones_Flete>();
            this.Opciones_Flete4 = new HashSet<Opciones_Flete>();
        }
    
        public long cod_estadoflete { get; set; }
        public string des_estadoflete { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Enc_RegistroFlete> Enc_RegistroFlete { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones_Flete> Opciones_Flete { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones_Flete> Opciones_Flete1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones_Flete> Opciones_Flete2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones_Flete> Opciones_Flete3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones_Flete> Opciones_Flete4 { get; set; }
    }
}
