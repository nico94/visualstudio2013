//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Unidades
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Unidades()
        {
            this.Materias_Primas = new HashSet<Materias_Primas>();
        }
    
        public long cod_unidad_unidad { get; set; }
        public string des_nombre_unidad { get; set; }
        public long cod_estadomateria_estadomateria { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Materias_Primas> Materias_Primas { get; set; }
    }
}
