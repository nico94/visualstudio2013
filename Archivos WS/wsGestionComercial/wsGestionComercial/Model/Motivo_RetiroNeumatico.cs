//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Motivo_RetiroNeumatico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Motivo_RetiroNeumatico()
        {
            this.AsignacionNeumatico_Maquina = new HashSet<AsignacionNeumatico_Maquina>();
        }
    
        public long cod_motivo_retiro { get; set; }
        public string des_motivo_retiro { get; set; }
        public bool Devuelve_stock { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AsignacionNeumatico_Maquina> AsignacionNeumatico_Maquina { get; set; }
    }
}
