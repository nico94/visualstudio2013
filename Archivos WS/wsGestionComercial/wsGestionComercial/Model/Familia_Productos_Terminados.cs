//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Familia_Productos_Terminados
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Familia_Productos_Terminados()
        {
            this.Sub_Familia_Productos_Terminados = new HashSet<Sub_Familia_Productos_Terminados>();
            this.Productos_Terminados = new HashSet<Productos_Terminados>();
            this.Opciones = new HashSet<Opciones>();
            this.Opciones1 = new HashSet<Opciones>();
            this.Opciones2 = new HashSet<Opciones>();
            this.Opciones3 = new HashSet<Opciones>();
        }
    
        public long cod_familiaproductosterminados_familiaproductosterminados { get; set; }
        public string des_descripcion_familiaproductosterminados { get; set; }
        public Nullable<bool> bit_essubproducto_familiaproductosterminados { get; set; }
        public Nullable<short> orden { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sub_Familia_Productos_Terminados> Sub_Familia_Productos_Terminados { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos_Terminados> Productos_Terminados { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones3 { get; set; }
    }
}
