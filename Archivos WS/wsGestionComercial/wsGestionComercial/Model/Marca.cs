//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Marca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Marca()
        {
            this.Maquinarias = new HashSet<Maquinarias>();
            this.Productos_Terminados = new HashSet<Productos_Terminados>();
        }
    
        public long cod_marca_marca { get; set; }
        public string des_marca_marca { get; set; }
        public Nullable<bool> bit_espropia_marca { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Maquinarias> Maquinarias { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos_Terminados> Productos_Terminados { get; set; }
    }
}
