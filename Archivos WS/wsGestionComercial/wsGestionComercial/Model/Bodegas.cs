//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bodegas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bodegas()
        {
            this.Documentos_Ventas = new HashSet<Documentos_Ventas>();
            this.ComprasPOSPyme = new HashSet<ComprasPOSPyme>();
            this.Documentos_CompraGenerales = new HashSet<Documentos_CompraGenerales>();
            this.ControlIngresoInsumoProductoTerminado = new HashSet<ControlIngresoInsumoProductoTerminado>();
            this.ControlIngresoInsumo = new HashSet<ControlIngresoInsumo>();
            this.Detalle_InsumoFichaLote = new HashSet<Detalle_InsumoFichaLote>();
            this.Opciones = new HashSet<Opciones>();
            this.Opciones1 = new HashSet<Opciones>();
            this.Opciones2 = new HashSet<Opciones>();
        }
    
        public long cod_bodega_bodega { get; set; }
        public string des_nombre_bodega { get; set; }
        public bool bit_essiloharina_bodega { get; set; }
        public decimal id_empresa_sucursal { get; set; }
        public Nullable<bool> bit_estienda_bodega { get; set; }
        public Nullable<long> cod_centrocosto_centrocosto { get; set; }
        public Nullable<long> cod_subcentrocosto_subcentrocosto { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Documentos_Ventas> Documentos_Ventas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ComprasPOSPyme> ComprasPOSPyme { get; set; }
        public virtual Centro_Costo Centro_Costo { get; set; }
        public virtual Sucursal_empresa Sucursal_empresa { get; set; }
        public virtual Sub_CentroCostos Sub_CentroCostos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Documentos_CompraGenerales> Documentos_CompraGenerales { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ControlIngresoInsumoProductoTerminado> ControlIngresoInsumoProductoTerminado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ControlIngresoInsumo> ControlIngresoInsumo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_InsumoFichaLote> Detalle_InsumoFichaLote { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opciones> Opciones2 { get; set; }
    }
}
