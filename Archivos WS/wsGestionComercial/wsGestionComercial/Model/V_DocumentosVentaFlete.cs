//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsGestionComercial.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_DocumentosVentaFlete
    {
        public string des_nombre_cliente { get; set; }
        public long cod_documentoventa_documentoventa { get; set; }
        public Nullable<long> num_Documentoventa_NroDoc { get; set; }
        public Nullable<System.DateTime> dat_fecha_documentoventa { get; set; }
        public Nullable<double> num_neto_documentoventa { get; set; }
        public Nullable<long> cod_tipodocumento_tipodocumentoventa { get; set; }
    }
}
