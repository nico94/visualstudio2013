﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial
{
    
    public class PosPyme : IPosPyme
    {
        public ResultadoPos Agregar_Compra2(PosRequest posrequest)
        {
            ResultadoPos res = new ResultadoPos();

            using (var db = new GestionComercialEntities())
            {
                ComprasPOSPyme compraspospyme = new ComprasPOSPyme();                
                Detalle_ComprasPOSPyme detallepospyme = new Detalle_ComprasPOSPyme();

                compraspospyme.cod_tipodocumento_tipodocumentoventa = posrequest.cod_tipodocumento_tipodocumentoventa;
                compraspospyme.cod_doccompra_POSPyme = posrequest.cod_doccompra_POSPyme;
                compraspospyme.dat_fechafactura_documentocomprageneral = posrequest.dat_fechafactura_documentocomprageneral;
                compraspospyme.cod_proveedor_proveedor = posrequest.cod_proveedor_proveedor;
                compraspospyme.cod_sucursal_sucursalproveedor = posrequest.cod_sucursal_sucursalproveedor;
                compraspospyme.cod_ordencompra_ordencompra = posrequest.cod_ordencompra_ordencompra;
                compraspospyme.cod_empresa_empresa = posrequest.cod_empresa_empresa;
                compraspospyme.id_empresa_sucursal = posrequest.id_empresa_sucursal;
                compraspospyme.cod_bodega_bodega = posrequest.cod_bodega_bodega;
                compraspospyme.cod_condicionpago_condicionpago = posrequest.cod_condicionpago_condicionpago;
                compraspospyme.dat_fechacontable_documentocomprageneral = posrequest.dat_fechacontable_documentocomprageneral;
                compraspospyme.des_glosa_documentocomprageneral = posrequest.des_glosa_documentocomprageneral;

                //Cargar valores no null que no se ingresan en el formulario
                var temporada = db.Temporadas.FirstOrDefault(t => t.bit_activa_temporada == true);
                compraspospyme.cod_temporada_temporada = temporada.cod_temporada_temporada;

                //LIST
                //listadetallepospyme = new List<ListDetallePosPymeRequest>();
                //List<ListaDetalleProductos> listadetalleproductos = new List<ListaDetalleProductos>();
                //var detallelist = db.Temporadas.ToList();

                //foreach (var item in listadetallepospyme)
                //{
                //    //listadetalleproductos.Add(new ListaDetalleProductos()
                //    //{
                //    //    v1 = item.v1,
                //    //    v2 = item.v2,
                //    //    v3 = item.v3
                //    //});
                //    //Rescatar valores
                //    detallepospyme.cod_doccompra_POSPyme = Convert.ToDecimal(item.v1);

                //    db.Detalle_ComprasPOSPyme.Add(detallepospyme);
                //}

                db.ComprasPOSPyme.Add(compraspospyme);

                try
                {
                    db.SaveChanges();
                    res.estado = true;
                }
                catch (Exception e)
                {
                    res.estado = false;
                }
            }
            return res;
        }



        public ResultadoPos Agregar_DetalleProducto(ListDetallePosPymeRequest listdetallepospymerequest)
        {
            ResultadoPos res = new ResultadoPos();

            using (var db = new GestionComercialEntities())
            {
                Detalle_ComprasPOSPyme detallepospyme = new Detalle_ComprasPOSPyme();

                detallepospyme.num_valor_detalledocumentocomprageneral = listdetallepospymerequest.num_valor_detalledocumentocomprageneral;
                detallepospyme.id_linea = listdetallepospymerequest.id_linea;
                detallepospyme.num_cantidad_detalledocumentocomprageneral = listdetallepospymerequest.num_cantidad_detalledocumentocomprageneral;
                detallepospyme.num_descuento_detalledocumentocomprageneral = listdetallepospymerequest.num_descuento_detalledocumentocomprageneral;
                detallepospyme.cod_doccompra_POSPyme = listdetallepospymerequest.cod_doccompra_POSPyme;
                detallepospyme.cod_proveedor_proveedor = listdetallepospymerequest.cod_proveedor_proveedor;
                detallepospyme.cod_temporada_temporada = listdetallepospymerequest.cod_temporada_temporada;
                detallepospyme.cod_tipodocumento_tipodocumentoventa = listdetallepospymerequest.cod_tipodocumento_tipodocumentoventa;
                detallepospyme.cod_empresa_empresa = listdetallepospymerequest.cod_empresa_empresa;
                detallepospyme.cod_productoterminado_productoterminado = listdetallepospymerequest.cod_productoterminado_productoterminado;

                db.Detalle_ComprasPOSPyme.Add(detallepospyme);

                try
                {
                    db.SaveChanges();
                    res.estado = true;
                }
                catch (Exception e)
                {
                    res.estado = false;
                }
            }
            return res;
        }



        public string Editar_Compra(int value)
        {
            throw new NotImplementedException();
        }

        public string Eliminar_Compra(int value)
        {
            throw new NotImplementedException();
        }

        public string Listar_Compra(int value)
        {
            throw new NotImplementedException();
        }
    }
}
