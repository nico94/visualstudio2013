﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Model;


namespace wsGestionComercial.Servicios
{ 
    public class Venta : IVenta
    {
        public ResultadoUpdateComisionProductoTerminado UpdateComisionProductoTerminado(UpdateComisionProductoTerminadoRequest UpdateComisionProductoterminadorequest)
        {
            ResultadoUpdateComisionProductoTerminado res = new ResultadoUpdateComisionProductoTerminado();                      
            using (var db = new GestionComercialEntities())
            {             
                using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados != -1 
                            && UpdateComisionProductoterminadorequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado == -1 
                            && UpdateComisionProductoterminadorequest.cod_productoterminado_productoterminado == "-1")
                        {
                            ////Actualiza toda la familia
                            var ProductoTerminado = db.Productos_Terminados.Where(s => s.cod_familiaproductosterminados_familiaproductosterminados == UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados).ToList();
                            ProductoTerminado.ForEach(s => s.num_comision_productoterminado = UpdateComisionProductoterminadorequest.num_comision);                                                                             
                            db.SaveChanges();
                            res.actualizado = true;
                        }
                        else if (UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados != -1 && UpdateComisionProductoterminadorequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado != -1 
                            && UpdateComisionProductoterminadorequest.cod_productoterminado_productoterminado == "-1")
                        {
                            ////Actualiza toda la Familia y Sub familia
                            var ProductoTerminado = db.Productos_Terminados.Where(s => s.cod_familiaproductosterminados_familiaproductosterminados == UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados
                                && s.cod_subfamiliaprodterminado_subfamiliaproductoterminado == UpdateComisionProductoterminadorequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado).ToList();
                            ProductoTerminado.ForEach(s => s.num_comision_productoterminado = UpdateComisionProductoterminadorequest.num_comision);
                            db.SaveChanges();
                            res.actualizado = true;
                        }
                        else if (UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados != -1
                            && UpdateComisionProductoterminadorequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado != -1
                            && UpdateComisionProductoterminadorequest.cod_productoterminado_productoterminado != "-1")
                        {
                            ////Actualiza el producto
                            var ProductoTerminado = db.Productos_Terminados.Where(s => s.cod_familiaproductosterminados_familiaproductosterminados == UpdateComisionProductoterminadorequest.cod_familiaproductosterminados_familiaproductosterminados
                                && s.cod_subfamiliaprodterminado_subfamiliaproductoterminado == UpdateComisionProductoterminadorequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado
                                && s.cod_productoterminado_productoterminado == UpdateComisionProductoterminadorequest.cod_productoterminado_productoterminado).ToList();
                            ProductoTerminado.ForEach(s => s.num_comision_productoterminado = UpdateComisionProductoterminadorequest.num_comision);
                            db.SaveChanges();
                            res.actualizado = true;
                        }
                        dbTran.Commit();
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.actualizado = false;

                    }
                }

            }
            return res;
        }
    }
}
