﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Insumo" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Insumo.svc or Insumo.svc.cs at the Solution Explorer and start debugging.
    public class Insumo : IInsumo
    {
        public ResultadoListaInsumoFichas ListadoInsumoFichas(ListaInsumoFichasRequest ListaInsumoFichasrequest)
        {
            ResultadoListaInsumoFichas res = new ResultadoListaInsumoFichas();


            using (var db = new GestionComercialEntities())
            {

                try
                {
                    var insumos = db.Insumo_Ficha_Personal.Include("Materias_Primas").Include("CondicionesProductos")
                        .Where(con => con.cod_fichapersonal_fichapersonal == ListaInsumoFichasrequest.codFicha).ToList();

                    if (insumos.Count() == 0)
                    {
                        //res.listaInsumos.Add(new ListaInsumos());
                    }
                    else
                    {
                        foreach (var item in insumos)
                        {
                            var fichaPersonal = new ListaInsumos();
                            fichaPersonal.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            fichaPersonal.des_nombre_materiaprima = item.Materias_Primas.des_nombre_materiaprima;
                            fichaPersonal.Cantidad = item.Cantidad;
                            fichaPersonal.Fecha_Entrega = item.Fecha_Entrega;
                            fichaPersonal.cod_condicionproducto_condicionproducto = (long)item.cod_condicionproducto_condicionproducto;
                            fichaPersonal.des_descripcion_condicionproducto = item.CondicionesProductos.des_descripcion_condicionproducto;
                            fichaPersonal.cod_fichapersonal_fichapersonal = item.cod_fichapersonal_fichapersonal;
                            res.listaInsumos.Add(fichaPersonal);

                            //Doc.Fecha = Convert.ToDateTime(item.dat_fecha_documentoventa);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaInsumos = null;
                }
            }
            return res;
        }





        public ResultadoListaCondicionesProductos ListadoCondicionesProductos()
        {
            ResultadoListaCondicionesProductos res = new ResultadoListaCondicionesProductos();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var estados = db.CondicionesProductos.ToList();

                    if (estados.Count() == 0)
                    {
                        //res.listaCondicioneProductos.Add(new ListaCondcionesProductos());
                    }
                    else
                    {
                        foreach (var item in estados)
                        {
                            var condicionesProductos = new ListaCondcionesProductos();
                            condicionesProductos.cod_condicionproducto_condicionproducto = item.cod_condicionproducto_condicionproducto;
                            condicionesProductos.des_descripcion_condicionproducto = item.des_descripcion_condicionproducto;
                            condicionesProductos.bit_estadoproducto_CondicionesProductos = (bool)item.bit_estadoproducto_CondicionesProductos;
                            res.listaCondicioneProductos.Add(condicionesProductos);

                            //Doc.Fecha = Convert.ToDateTime(item.dat_fecha_documentoventa);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaCondicioneProductos = null;
                }
                return res;
            }
        }


        public GuardarGastoInsumo guardarGastoInsumo(GuardarGastoInsumoRequest GuardarGastoInsumorequest)
        {
            GuardarGastoInsumo guardar = new GuardarGastoInsumo();

            using (var db = new GestionComercialEntities())
            {
                var FichaPersonal = db.FichasPersonales.FirstOrDefault(c => c.cod_fichapersonal_fichapersonal == GuardarGastoInsumorequest.insumoFichaPersona.cod_fichapersonal_fichapersonal);
                var MateriasPrimas = db.Materias_Primas.FirstOrDefault(c => c.cod_producto_materiaprima == GuardarGastoInsumorequest.insumoFichaPersona.cod_producto_materiaprima);
                var DetalleInsumoFichaLote = db.Detalle_InsumoFichaLote.Where(c => c.cod_producto_materiaprima == GuardarGastoInsumorequest.insumoFichaPersona.cod_producto_materiaprima
                && c.cod_fichapersonal_fichapersonal == GuardarGastoInsumorequest.insumoFichaPersona.cod_fichapersonal_fichapersonal
                && c.Fecha_Entrega == GuardarGastoInsumorequest.insumoFichaPersona.Fecha_Entrega).ToList();
                var CondicionesProducto = db.CondicionesProductos.FirstOrDefault(c => c.cod_condicionproducto_condicionproducto == GuardarGastoInsumorequest.insumoFichaPersona.cod_condicionproducto_condicionproducto);

                GuardarGastoInsumorequest.insumoFichaPersona.FichasPersonales = FichaPersonal;
                GuardarGastoInsumorequest.insumoFichaPersona.Materias_Primas = MateriasPrimas;
                GuardarGastoInsumorequest.insumoFichaPersona.Detalle_InsumoFichaLote = DetalleInsumoFichaLote;
                GuardarGastoInsumorequest.insumoFichaPersona.CondicionesProductos = CondicionesProducto;



                db.Insumo_Ficha_Personal.Add(GuardarGastoInsumorequest.insumoFichaPersona);


                // Submit the change to the database.
                try
                {
                    db.SaveChanges();
                    guardar.resultado = true;
                }
                catch (Exception)
                {
                    // Make something.
                    guardar.resultado = false;
                }
                return guardar;
            }

        }


        public ListarDetalleInsumoFichaLote listarDetalleInsumoFichaLote(ListarDetalleInsumoFichaLoteRequest listarDetalleInsumoFichaLoteRequest)
        {
            ListarDetalleInsumoFichaLote res = new ListarDetalleInsumoFichaLote();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var detalles = db.Detalle_InsumoFichaLote.Include("Bodegas").Include("Bodegas.Sucursal_empresa").Include("Insumo_Ficha_Personal.Materias_Primas").Where(c => c.cod_fichapersonal_fichapersonal == listarDetalleInsumoFichaLoteRequest.cod_fichapersonal_fichapersonal
                        && c.cod_producto_materiaprima == listarDetalleInsumoFichaLoteRequest.cod_producto_materiaprima
                        && c.Cantidad_Utilizada > 0).ToList();

                    if (detalles.Count() == 0)
                    {
                        //res.listaDetalleInsumosFichaLote.Add(new ListaDetalleInsumosFichaLote());
                    }
                    else
                    {
                        foreach (var item in detalles)
                        {
                            var detalleInsumosFicha = new ListaDetalleInsumosFichaLote();
                            detalleInsumosFicha.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            detalleInsumosFicha.cod_fichapersonal_fichapersonal = item.cod_fichapersonal_fichapersonal;
                            detalleInsumosFicha.cod_bodega_bodega = item.cod_bodega_bodega;
                            detalleInsumosFicha.des_nombre_bodega = item.Bodegas.des_nombre_bodega;
                            detalleInsumosFicha.des_sucursal = item.Bodegas.Sucursal_empresa.des_sucursal;
                            detalleInsumosFicha.Cantidad_Utilizada = item.Cantidad_Utilizada;
                            detalleInsumosFicha.Valor_Insumo = item.Valor_Insumo;
                            detalleInsumosFicha.num_posicion_x = item.num_posicion_x;
                            detalleInsumosFicha.num_posicion_y = item.num_posicion_y;
                            detalleInsumosFicha.num_posicion_z = item.num_posicion_z;
                            detalleInsumosFicha.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            detalleInsumosFicha.Fecha_Entrega = item.Fecha_Entrega;
                            detalleInsumosFicha.Fecha_Consumo = item.Fecha_Consumo;
                            detalleInsumosFicha.des_nombre_materiaprima = item.Insumo_Ficha_Personal.Materias_Primas.des_nombre_materiaprima;
                            detalleInsumosFicha.num_posicion_xyz = item.num_posicion_x + ";" + item.num_posicion_y + ";" + item.num_posicion_x;

                            res.listaDetalleInsumosFichaLote.Add(detalleInsumosFicha);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaDetalleInsumosFichaLote = null;
                }
                return res;
            }
        }

        public ListarDetalleInsumoFichaLote2 listarDetalleInsumoFichaLote2(ListarDetalleInsumoFichaLoteRequest listarDetalleInsumoFichaLoteRequest)
        {
            ListarDetalleInsumoFichaLote2 res = new ListarDetalleInsumoFichaLote2();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var detalles = db.Detalle_InsumoFichaLote.Include("Bodegas").Include("Bodegas.Sucursal_empresa").Include("Insumo_Ficha_Personal.Materias_Primas").Where(c => c.cod_fichapersonal_fichapersonal == listarDetalleInsumoFichaLoteRequest.cod_fichapersonal_fichapersonal
                        && c.Cantidad_Utilizada > 0).ToList();

                    if (detalles.Count() == 0)
                    {
                        //res.listaDetalleInsumosFichaLote.Add(new ListaDetalleInsumosFichaLote());
                    }
                    else
                    {
                        foreach (var item in detalles)
                        {
                            var detalleInsumosFicha = new ListaDetalleInsumosFichaLote();
                            detalleInsumosFicha.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            detalleInsumosFicha.cod_fichapersonal_fichapersonal = item.cod_fichapersonal_fichapersonal;
                            detalleInsumosFicha.cod_bodega_bodega = item.cod_bodega_bodega;
                            detalleInsumosFicha.des_nombre_bodega = item.Bodegas.des_nombre_bodega;
                            detalleInsumosFicha.des_sucursal = item.Bodegas.Sucursal_empresa.des_sucursal;
                            detalleInsumosFicha.Cantidad_Utilizada = item.Cantidad_Utilizada;
                            detalleInsumosFicha.Valor_Insumo = item.Valor_Insumo;
                            detalleInsumosFicha.num_posicion_x = item.num_posicion_x;
                            detalleInsumosFicha.num_posicion_y = item.num_posicion_y;
                            detalleInsumosFicha.num_posicion_z = item.num_posicion_z;
                            detalleInsumosFicha.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            detalleInsumosFicha.Fecha_Entrega = item.Fecha_Entrega;
                            detalleInsumosFicha.Fecha_Consumo = item.Fecha_Consumo;
                            detalleInsumosFicha.des_nombre_materiaprima = item.Insumo_Ficha_Personal.Materias_Primas.des_nombre_materiaprima;
                            detalleInsumosFicha.num_posicion_xyz = item.num_posicion_x + ";" + item.num_posicion_y + ";" + item.num_posicion_x;
                            detalleInsumosFicha.key = item.cod_producto_materiaprima + "|" + item.num_lote_controlingresoinsumo + "|" + item.Fecha_Consumo + "|" + item.Fecha_Consumo;

                            res.listaDetalleInsumosFichaLote.Add(detalleInsumosFicha);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaDetalleInsumosFichaLote = null;
                }
                return res;
            }
        }

        


        //Guarda el detalleFichaInsumo y ademas hace el descuento de la bodega y agrega a la misma bodega el producto que se presto 
        //(asignandolo a una nueva bodega que se obtiene de la tabla opciones)
        public GuardarDetalleInsumoFichaLote guardarDetalleInsumoFichaLote(GuardarDetalleInsumoFichaLoteRequest guardarDetalleInsumoFichaLoteRequest)
        {
            GuardarDetalleInsumoFichaLote res = new GuardarDetalleInsumoFichaLote();
            
            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Se guarda  Detalle_Insumo_Ficha_Personal
                        var bodega = db.Bodegas.FirstOrDefault(c => c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega);
                        //var ficha = db.FichasPersonales.FirstOrDefault(c1 => c1.cod_fichapersonal_fichapersonal == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_fichapersonal_fichapersonal);
                        var insumoFichaPersonal = db.Insumo_Ficha_Personal.FirstOrDefault(c => c.cod_producto_materiaprima == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_producto_materiaprima
                            && c.cod_fichapersonal_fichapersonal == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_fichapersonal_fichapersonal
                            && c.Fecha_Entrega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Fecha_Entrega);

                        //Se obtiene el valor de insumo, ya que no viene en el request
                        var valorInsumo = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega
                            && c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega
                            && c.num_posicion_x == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_x
                            && c.num_posicion_y == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_y
                            && c.num_posicion_z == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_z).num_valor_controlingresoinsumo;

                        var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                       

                        //Se guarda el DetalleInsumoFichaLote
                        Detalle_InsumoFichaLote _Detalle_InsumoFichaLote = new Detalle_InsumoFichaLote();

                        _Detalle_InsumoFichaLote.cod_producto_materiaprima = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_producto_materiaprima;
                        _Detalle_InsumoFichaLote.Insumo_Ficha_Personal = insumoFichaPersonal; 
                        _Detalle_InsumoFichaLote.Bodegas = bodega;
                        _Detalle_InsumoFichaLote.Cantidad_Utilizada = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Cantidad_Utilizada;
                        _Detalle_InsumoFichaLote.Valor_Insumo = valorInsumo;
                        _Detalle_InsumoFichaLote.num_posicion_x = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_x;
                        _Detalle_InsumoFichaLote.num_posicion_y = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_y;
                        _Detalle_InsumoFichaLote.num_posicion_z = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_z;
                        _Detalle_InsumoFichaLote.Fecha_Entrega = insumoFichaPersonal.Fecha_Entrega;
                        _Detalle_InsumoFichaLote.num_lote_controlingresoinsumo = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_lote_controlingresoinsumo;
                        _Detalle_InsumoFichaLote.Fecha_Consumo = Convert.ToDateTime(Fecha);

                        db.Detalle_InsumoFichaLote.Add(_Detalle_InsumoFichaLote);
                        //Se guarda
                        db.SaveChanges();


                        //Se guarda el DetalleInsumoFichaLoteHistoria
                        //Código Ficha responsable
                        var codFichaResponsable = db.FichasPersonales.FirstOrDefault(f => f.cod_fichapersonal_fichapersonal == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_fichapersonal_responsable_devolucion).cod_fichapersonal_fichapersonal;
                        Detalle_InsumoFichaLote_Historia _Detalle_InsumoFichaLote_Historia = new Detalle_InsumoFichaLote_Historia();

                        _Detalle_InsumoFichaLote_Historia.Detalle_InsumoFichaLote = _Detalle_InsumoFichaLote;
                        _Detalle_InsumoFichaLote_Historia.cod_producto_materiaprima = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_producto_materiaprima;
                        _Detalle_InsumoFichaLote_Historia.cod_fichapersonal_fichapersonal = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_fichapersonal_fichapersonal;
                        _Detalle_InsumoFichaLote_Historia.cod_bodega_bodega = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega;
                        _Detalle_InsumoFichaLote_Historia.fecha_movimiento = Convert.ToDateTime(Fecha);
                        _Detalle_InsumoFichaLote_Historia.Cantidad = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Cantidad_Utilizada;
                        _Detalle_InsumoFichaLote_Historia.tipo_movimiento = "asignacion";
                        _Detalle_InsumoFichaLote_Historia.num_posicion_x = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_x;
                        _Detalle_InsumoFichaLote_Historia.num_posicion_y = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_y;
                        _Detalle_InsumoFichaLote_Historia.num_posicion_z = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_z;
                        _Detalle_InsumoFichaLote_Historia.Fecha_Entrega = insumoFichaPersonal.Fecha_Entrega;
                        _Detalle_InsumoFichaLote_Historia.num_lote_controlingresoinsumo = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_lote_controlingresoinsumo;
                        _Detalle_InsumoFichaLote_Historia.Fecha_Consumo = Convert.ToDateTime(Fecha);
                        _Detalle_InsumoFichaLote_Historia.cod_fichapersonal_responsable_devolucion = codFichaResponsable;

                        //Se guarda
                        db.Detalle_InsumoFichaLote_Historia.Add(_Detalle_InsumoFichaLote_Historia);


                        //Obtengo el codigo de la bodega "Temporal"
                        var codigoBodegaTemporal = db.Opciones.FirstOrDefault().cod_bodegaasignacionInsumos;
                        if (codigoBodegaTemporal != null)
                        {

                            var bodega1 = db.Bodegas.FirstOrDefault(c => c.cod_bodega_bodega == codigoBodegaTemporal);

                            //Actualizar bodega actual (Descuenta el insumo de la tabla ControlIngresoInsumo)
                            var controlIngresoInsumo = db.ControlIngresoInsumo.FirstOrDefault(c => c.num_lote_controlingresoinsumo == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_lote_controlingresoinsumo
                                && c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega);
                            controlIngresoInsumo.num_cantidad_ControlIngresoInsumo = controlIngresoInsumo.num_cantidad_ControlIngresoInsumo - guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Cantidad_Utilizada;
                            //Se guarda
                            db.SaveChanges();

                            var bodegaTemporal = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == codigoBodegaTemporal
                                && c.cod_producto_materiaprima == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_producto_materiaprima
                                && c.num_posicion_x == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_x
                                && c.num_posicion_y == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_y
                                && c.num_posicion_z == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_posicion_z);

                            //Ya existe un registro con el codigo, por lo que hay que actualizar la cantidad de insumos
                            if(bodegaTemporal != null)
                            {
                                bodegaTemporal.num_cantidad_ControlIngresoInsumo = bodegaTemporal.num_cantidad_ControlIngresoInsumo + guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Cantidad_Utilizada;
                                //Se guarda
                                db.SaveChanges();
                            }
                            //No eiste el registro por lo que hay que insertar uno nuevo
                            else
                            {
                                var registroBodegaCopia = db.ControlIngresoInsumo.FirstOrDefault(c => c.num_lote_controlingresoinsumo == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.num_lote_controlingresoinsumo
                                && c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.cod_bodega_bodega);

                                ControlIngresoInsumo nuevoRegistroBodega = new ControlIngresoInsumo();

                                nuevoRegistroBodega.num_lote_controlingresoinsumo = registroBodegaCopia.num_lote_controlingresoinsumo;
                                nuevoRegistroBodega.cod_bodega_bodega = long.Parse(codigoBodegaTemporal.ToString());//Cambia
                                nuevoRegistroBodega.cod_snapcinsumo_snapcimsumo = registroBodegaCopia.cod_snapcinsumo_snapcimsumo;
                                nuevoRegistroBodega.cod_producto_materiaprima = registroBodegaCopia.cod_producto_materiaprima;
                                nuevoRegistroBodega.cod_temporada_temporada = registroBodegaCopia.cod_temporada_temporada;
                                nuevoRegistroBodega.cod_condicionproducto_condicionproducto = registroBodegaCopia.cod_condicionproducto_condicionproducto;
                                nuevoRegistroBodega.cod_condicionbodega_condicionbodega = registroBodegaCopia.cod_condicionbodega_condicionbodega;
                                nuevoRegistroBodega.dat_fechavencimiento_ControlIngresoInsumo = registroBodegaCopia.dat_fechavencimiento_ControlIngresoInsumo;
                                nuevoRegistroBodega.num_cantidad_ControlIngresoInsumo = guardarDetalleInsumoFichaLoteRequest.detalleInsumoFichaLote.Cantidad_Utilizada;//Cambia
                                nuevoRegistroBodega.num_valor_controlingresoinsumo = registroBodegaCopia.num_valor_controlingresoinsumo;
                                nuevoRegistroBodega.num_horas_controlingresoinsumo = registroBodegaCopia.num_horas_controlingresoinsumo;
                                nuevoRegistroBodega.num_cantidadbultos_controlingresoinsumo = registroBodegaCopia.num_cantidadbultos_controlingresoinsumo;
                                nuevoRegistroBodega.cod_productogenerico_materiaprima = registroBodegaCopia.cod_productogenerico_materiaprima;
                                nuevoRegistroBodega.bit_analisisharina_controlingresoinsumo = registroBodegaCopia.bit_analisisharina_controlingresoinsumo;
                                nuevoRegistroBodega.cod_seccionbodega_seccionbodega = registroBodegaCopia.cod_seccionbodega_seccionbodega;
                                nuevoRegistroBodega.num_posicion_x = registroBodegaCopia.num_posicion_x;
                                nuevoRegistroBodega.num_posicion_y = registroBodegaCopia.num_posicion_y;
                                nuevoRegistroBodega.num_posicion_z = registroBodegaCopia.num_posicion_z;
                                nuevoRegistroBodega.CondicionesProductos = registroBodegaCopia.CondicionesProductos;
                                nuevoRegistroBodega.Materias_Primas = registroBodegaCopia.Materias_Primas;
                                nuevoRegistroBodega.Materias_Primas1 = registroBodegaCopia.Materias_Primas1;
                                nuevoRegistroBodega.snap_ControlIngresoInsumo = registroBodegaCopia.snap_ControlIngresoInsumo;
                                nuevoRegistroBodega.Temporadas = registroBodegaCopia.Temporadas;
                                nuevoRegistroBodega.Bodegas = bodega1;
                                db.ControlIngresoInsumo.Add(nuevoRegistroBodega);

                            }

                            db.SaveChanges();
                            dbTran.Commit();
                            res.mensaje = "Exito; código de bodega temporal: " + codigoBodegaTemporal;
                            res.resultado = true;
                        }
                        else
                        {
                            res.mensaje = "No existe un código de bodega temporal";
                            dbTran.Rollback();
                            res.resultado = false;
                        }
                    }
                    catch(Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }

        public ObtenerCodigoBodegaTemporal obtenerCodigoBodegaTemporal()
        {
            ObtenerCodigoBodegaTemporal res = new ObtenerCodigoBodegaTemporal();

            using (var db = new GestionComercialEntities())
            {
                var codigoBodegaTemporal = db.Opciones.FirstOrDefault().cod_bodegaasignacionInsumos;
                res.codigoBodegaTemporal = long.Parse(codigoBodegaTemporal.ToString());
            }
            return res;
        }


        public ListarDetalleInsumoFichaLoteDevolucion listarDetalleInsumoFichaLoteDevolucion(ListarDetalleInsumoFichaLoteDevolucionRequest listaDetalleInsumoFichaLoteDevolucionRequest)
        {
            ListarDetalleInsumoFichaLoteDevolucion res = new ListarDetalleInsumoFichaLoteDevolucion();

            using (var db = new GestionComercialEntities())
            {
                try
                {                 

                    var detalles = db.Detalle_InsumoFichaLote_Historia.Where(c => c.cod_fichapersonal_fichapersonal == listaDetalleInsumoFichaLoteDevolucionRequest.cod_fichapersonal_fichapersonal
                        && c.tipo_movimiento == "devolucion").ToList();


                    if (detalles.Count() == 0)
                    {
                        //res.listaDetalleInsumoFichaLoteDevolucion.Add(new ListaDetalleInsumoFichaLoteDevolucion());
                    }
                    else
                    {
                        foreach (var item in detalles)
                        {
                            var detalleInsumosFicha = new ListaDetalleInsumoFichaLoteDevolucion();
                            detalleInsumosFicha.cod_producto_materiaprima = item.cod_producto_materiaprima;
                            detalleInsumosFicha.cod_fichapersonal_fichapersonal = item.cod_fichapersonal_fichapersonal;
                            detalleInsumosFicha.cod_bodega_bodega = item.cod_bodega_bodega;
                            detalleInsumosFicha.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            detalleInsumosFicha.fecha_movimiento = item.fecha_movimiento;
                            detalleInsumosFicha.cod_fichapersonal_responsable_devolucion = item.cod_fichapersonal_responsable_devolucion;
                            detalleInsumosFicha.Cantidad = item.Cantidad;
                            detalleInsumosFicha.tipo_movimiento = item.tipo_movimiento;
                            detalleInsumosFicha.Fecha_Consumo = item.Fecha_Consumo;
                            detalleInsumosFicha.num_posicion_x = item.num_posicion_x;
                            detalleInsumosFicha.num_posicion_y = item.num_posicion_y;
                            detalleInsumosFicha.num_posicion_z = item.num_posicion_z;
                            detalleInsumosFicha.Fecha_Entrega = item.Fecha_Entrega;
                            detalleInsumosFicha.key = item.cod_producto_materiaprima + "|" + item.num_lote_controlingresoinsumo + "|" + item.Fecha_Consumo + "|" + item.Fecha_Consumo;

                            res.listaDetalleInsumoFichaLoteDevolucion.Add(detalleInsumosFicha);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaDetalleInsumoFichaLoteDevolucion = null;
                }
            }
            return res;
        }

        public GuardarDetalleInsumoFichaLoteDevolucion guardarDetalleInsumoFichaLoteDevolucion(GuardarDetalleInsumoFichaLoteDevolucionRequest guardarDetalleInsumoFichaLoteDevolucionRequest)
        {
            GuardarDetalleInsumoFichaLoteDevolucion res = new GuardarDetalleInsumoFichaLoteDevolucion();

            
            using(var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var cod = guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_fichapersonal_responsable_devolucion;
                        guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_fichapersonal_responsable_devolucion = db.FichasPersonales.FirstOrDefault(f => f.cod_fichapersonal_fichapersonal == cod).cod_fichapersonal_fichapersonal;
                        var fechaAhora = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                        guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.fecha_movimiento = DateTime.Parse(fechaAhora);

                        //Se descuenta la cantidad utilizada de la tabla Detalle_InsumoFichaLote
                        var detalleInsumoFichaLote = db.Detalle_InsumoFichaLote.FirstOrDefault(c => c.cod_producto_materiaprima == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_producto_materiaprima
                            && c.cod_fichapersonal_fichapersonal == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_fichapersonal_fichapersonal
                            && c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_bodega_bodega
                            && c.num_lote_controlingresoinsumo == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_lote_controlingresoinsumo
                            && c.Fecha_Entrega == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.Fecha_Entrega
                            && c.Fecha_Consumo == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.Fecha_Consumo
                            && c.num_posicion_x == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_x
                            && c.num_posicion_y == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_y
                            && c.num_posicion_z == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_z);
                        detalleInsumoFichaLote.Cantidad_Utilizada = detalleInsumoFichaLote.Cantidad_Utilizada - guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.Cantidad;
                        
                        //Se guarda la decolución en la tabla Detalle_InsumoFichaLote_Devolucion
                        db.Detalle_InsumoFichaLote_Historia.Add(guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion);


                        //Se devuelve el producto utilizado a la tabla ControlIngresoInsumo
                        var bodegaControlIngresoInsumo = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_bodega_bodega
                                && c.num_lote_controlingresoinsumo == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_lote_controlingresoinsumo
                                && c.num_posicion_x == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_x
                                && c.num_posicion_y == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_y
                                && c.num_posicion_z == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_z);
                        bodegaControlIngresoInsumo.num_cantidad_ControlIngresoInsumo = bodegaControlIngresoInsumo.num_cantidad_ControlIngresoInsumo + guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.Cantidad;

                        //Obtengo el codigo de la bodega "Temporal"
                        var codigoBodegaTemporal = db.Opciones.FirstOrDefault().cod_bodegaasignacionInsumos;
                        //Se hace el descuento de la bodega temporal en la tabla ControlIngresoInsumo
                        var bodegaControlIngresoInsumoTemporal = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == codigoBodegaTemporal
                                && c.cod_producto_materiaprima == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.cod_producto_materiaprima
                                && c.num_posicion_x == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_x
                                && c.num_posicion_y == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_y
                                && c.num_posicion_z == guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.num_posicion_z);
                        bodegaControlIngresoInsumoTemporal.num_cantidad_ControlIngresoInsumo = bodegaControlIngresoInsumoTemporal.num_cantidad_ControlIngresoInsumo - guardarDetalleInsumoFichaLoteDevolucionRequest.devolucion.Cantidad;



                        db.SaveChanges();
                        dbTran.Commit();
                        res.mensaje = "Exito";
                        res.resultado = true;

                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                    
                }


            }
            return res;
        }

        public ListarMateriasPrimas listarMateriasPrimas()
        {
            ListarMateriasPrimas res = new ListarMateriasPrimas();

            using (var db = new GestionComercialEntities())
            {
                //Código bodega "temporal"
                var codigoBodegaTemporal = db.Opciones.FirstOrDefault().cod_bodegaasignacionInsumos;

                var controlIngresoInsumo = (from control in db.ControlIngresoInsumo
                                            where control.num_cantidad_ControlIngresoInsumo > 0 && control.cod_bodega_bodega != codigoBodegaTemporal
                                            group control by new { control.cod_producto_materiaprima } into g
                                            select new
                                            {
                                                //num_cantidad_ControlIngresoInsumo = g.Sum(p => p.num_cantidad_ControlIngresoInsumo),
                                                cod_producto_materiaprima = g.Select(s => s.cod_producto_materiaprima).FirstOrDefault()
                                            }).ToList();

                foreach (var item in controlIngresoInsumo)
                {
                    ListaMateriasPrimas materiaPrima = new ListaMateriasPrimas();
                    var tmp = db.Materias_Primas.FirstOrDefault(c => c.cod_producto_materiaprima == item.cod_producto_materiaprima);
                    
                    materiaPrima.cod_producto_materiaprima = tmp.cod_producto_materiaprima;
                    materiaPrima.des_nombre_materiaprima = tmp.des_nombre_materiaprima;
                    res.materiasPrimas.Add(materiaPrima);
                }
            }

            return res;
        }


        public ListarControlIngresoInsumo listarControlIngresoInsumo(ListarControlIngresoInsumoRequest listarControlIngresoInsumoRequest)
        {
            ListarControlIngresoInsumo res = new ListarControlIngresoInsumo();

            using (var db = new GestionComercialEntities())
            {

                //Código bodega "temporal"
                var codigoBodegaTemporal = db.Opciones.FirstOrDefault().cod_bodegaasignacionInsumos;

                var listaLotes = db.ControlIngresoInsumo.Include("Bodegas").Where(e => e.num_cantidad_ControlIngresoInsumo > 0
                        && e.cod_producto_materiaprima == listarControlIngresoInsumoRequest.cod_producto_materiaprima
                        && e.cod_bodega_bodega != codigoBodegaTemporal).ToList();

                foreach (var item in listaLotes)
                {
                    ControlIngresoInsumoCustom tmp = new ControlIngresoInsumoCustom();
                    tmp.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                    tmp.dat_fechavencimiento_ControlIngresoInsumo = item.dat_fechavencimiento_ControlIngresoInsumo;
                    tmp.num_cantidad_ControlIngresoInsumo = item.num_cantidad_ControlIngresoInsumo;
                    tmp.num_valor_controlingresoinsumo = item.num_valor_controlingresoinsumo;
                    tmp.num_horas_controlingresoinsumo = item.num_horas_controlingresoinsumo;
                    tmp.num_cantidadbultos_controlingresoinsumo = item.num_cantidadbultos_controlingresoinsumo;
                    tmp.bit_analisisharina_controlingresoinsumo = item.bit_analisisharina_controlingresoinsumo;
                    tmp.cod_seccionbodega_seccionbodega = item.cod_seccionbodega_seccionbodega;
                    tmp.num_posicion_x = item.num_posicion_x;
                    tmp.num_posicion_y = item.num_posicion_y;
                    tmp.num_posicion_z = item.num_posicion_z;
                    tmp.num_posicion_xyz = item.num_posicion_x + ";" + item.num_posicion_y + ";" + item.num_posicion_z;
                    tmp.cod_bodega_bodega = item.Bodegas.cod_bodega_bodega;
                    tmp.des_nombre_bodega = item.Bodegas.des_nombre_bodega;
                    tmp.key = item.num_cantidad_ControlIngresoInsumo + "|" + item.cod_bodega_bodega + "|" + item.num_posicion_x + "|" + item.num_posicion_y + "|" + item.num_posicion_z;
                    res.listaControlIngresoInsumo.Add(tmp);
                }
                
            }

            return res;
        }


        public ResultadoProductoTerminado ListaProductoTerminado()
        {
            ResultadoProductoTerminado res = new ResultadoProductoTerminado();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.Productos_Terminados.ToList();
                    foreach (var item in lista)
                    {
                        var productos = new Productos_Terminados();
                        productos.cod_productoterminado_productoterminado = item.cod_productoterminado_productoterminado;
                        productos.des_descripcion_productoterminado = item.des_descripcion_productoterminado;
                        res.lista.Add(productos);
                    }
                }
                catch (Exception)
                {
                    res.lista = null;
                }
            }
            return res;
        }


        public ResultadoFamiliaProductoTerminado ListaFamiliaProductoTerminado()
        {
            ResultadoFamiliaProductoTerminado res = new ResultadoFamiliaProductoTerminado();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.Familia_Productos_Terminados.ToList();
                    foreach (var item in lista)
                    {
                        var familiaproductos = new Familia_Productos_Terminados();
                        familiaproductos.cod_familiaproductosterminados_familiaproductosterminados = item.cod_familiaproductosterminados_familiaproductosterminados;
                        familiaproductos.des_descripcion_familiaproductosterminados = item.des_descripcion_familiaproductosterminados;
                        res.lista.Add(familiaproductos);
                    }
                }
                catch (Exception)
                {
                    res.lista = null;
                }
            }
            return res;
        }


        public ResultadoSubFamiliaProductoTerminadoPorFamilia ListaSubFamiliaProductoTerminadoPorFamilia(SubFamiliaProductoTerminadoPorFamiliaRequest SubFamiliaProductoTerminadoPorfamiliarequest)
        {
            ResultadoSubFamiliaProductoTerminadoPorFamilia res = new ResultadoSubFamiliaProductoTerminadoPorFamilia();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.Sub_Familia_Productos_Terminados.Where(d => d.cod_familiaproductosterminados_familiaproductosterminados == SubFamiliaProductoTerminadoPorfamiliarequest.cod_familiaproductosterminados_familiaproductosterminados).ToList();
                    foreach (var item in lista)
                    {
                        var subfamiliaproductos = new Sub_Familia_Productos_Terminados();
                        subfamiliaproductos.cod_subfamiliaprodterminado_subfamiliaproductoterminado = item.cod_subfamiliaprodterminado_subfamiliaproductoterminado;
                        subfamiliaproductos.des_descripcion_subfamiliaproductosterminados = item.des_descripcion_subfamiliaproductosterminados;
                        res.lista.Add(subfamiliaproductos);
                    }
                }
                catch (Exception)
                {
                    res.lista = null;
                }
            }
            return res;
        }


        public ResultadoProductoTerminadoPorSubFamilia ListaProductoTerminadoPorSubFamilia(ProductoTerminadoPorSubFamiliaRequest ProductoTerminadoPorSubfamiliarequest)
        {
            ResultadoProductoTerminadoPorSubFamilia res = new ResultadoProductoTerminadoPorSubFamilia();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.Productos_Terminados.Where(d => d.cod_familiaproductosterminados_familiaproductosterminados == ProductoTerminadoPorSubfamiliarequest.cod_familiaproductosterminados_familiaproductosterminados
                        && d.cod_subfamiliaprodterminado_subfamiliaproductoterminado == ProductoTerminadoPorSubfamiliarequest.cod_subfamiliaprodterminado_subfamiliaproductoterminado).ToList();
                    foreach (var item in lista)
                    {
                        var productos = new Productos_Terminados();
                        productos.cod_productoterminado_productoterminado = item.cod_productoterminado_productoterminado;
                        productos.des_descripcion_productoterminado = item.des_descripcion_productoterminado;
                        res.lista.Add(productos);
                    }
                }
                catch (Exception)
                {
                    res.lista = null;
                }
            }
            return res;
        }
    }
}
