﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;
using System.Web.Script.Serialization;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace wsGestionComercial.ContractReqResponse
{   
    public class Indicadores : IIndicadores
    {
        public ResultadoIndicador Indicador(IndicadorRequest Indicadorrequest)
        {
            ResultadoIndicador res = new ResultadoIndicador();
            try
            {                
                string apiUrl = "http://www.mindicador.cl/api/" + Indicadorrequest.Tipo + "/" + Indicadorrequest.Fecha.ToString("dd-MM-yyyy") + "";
                string jsonString = "{}";
                WebClient http = new WebClient();
                http.Headers.Add(HttpRequestHeader.Accept, "application/json");
                jsonString = http.DownloadString(apiUrl);
                var jo = JObject.Parse(jsonString);
                var id = jo["serie"].ToString();
                id = id.Replace("\r\n", "").Replace("[", "").Replace("]", "");

                if (id != string.Empty)
                {
                    var jo2 = JObject.Parse(id);
                    var iid2 = jo2["valor"].ToString();
                    res.Valor = double.Parse(iid2);  
                }
                else
                {
                    res.Valor = 0; 
                }
                              
            }
            catch (Exception)
            {
                res.Valor = 0;
            }
            return res;
        }
    }
}
