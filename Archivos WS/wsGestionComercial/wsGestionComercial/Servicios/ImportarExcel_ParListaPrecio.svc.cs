﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Model;


namespace wsGestionComercial.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ImportarExcel_ParListaPrecio" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ImportarExcel_ParListaPrecio.svc or ImportarExcel_ParListaPrecio.svc.cs at the Solution Explorer and start debugging.
    public class ImportarExcel_ParListaPrecio : IImportarExcel_ParListaPrecio
    {
        public ResultadoInsertarListaPrecios InsertarListaPrecios(string idSession)
        {
            ResultadoInsertarListaPrecios res = new ResultadoInsertarListaPrecios();

            using (var db = new GestionComercialEntities())
            {
                DataTable dt = new DataTable();
                var ListaPrecio = db.PasoListaPrecioCliente.Where(lp => lp.pk == idSession).ToList();

                using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in ListaPrecio)
                        {
                            snap_ListaPrecioCliente precioCliente;
                            precioCliente = db.snap_ListaPrecioCliente.FirstOrDefault(obj => obj.cod_productoterminado_productoterminado == item.cod_producto_terminado && obj.cod_sucursal_clientesucursal == item.cod_sucursal_cliente);
                            if (precioCliente == null)
                            {
                                precioCliente = new snap_ListaPrecioCliente();
                                precioCliente.cod_productoterminado_productoterminado = item.cod_producto_terminado;
                                precioCliente.num_precio_listapreciocliente = item.num_precio_lista_cliente;
                                precioCliente.num_descuento_listapreciocliente = item.num_descuento_precio_lista_cliente;
                                precioCliente.num_comisionventa_listapreciocliente = item.num_comision_venta;
                                precioCliente.cod_sucursal_clientesucursal = item.cod_sucursal_cliente;

                                var cs_temp = db.Clientes_Sucursales.FirstOrDefault(cs => cs.cod_sucursal_clientesucursal == item.cod_sucursal_cliente);

                                precioCliente.cod_cliente_cliente = cs_temp.cod_cliente_cliente;

                                db.snap_ListaPrecioCliente.Add(precioCliente);
                                db.SaveChanges();
                            }
                            else
                            {
                                
                                precioCliente.cod_productoterminado_productoterminado = item.cod_producto_terminado;
                                precioCliente.num_precio_listapreciocliente = item.num_precio_lista_cliente;
                                precioCliente.num_descuento_listapreciocliente = item.num_descuento_precio_lista_cliente;
                                precioCliente.num_comisionventa_listapreciocliente = item.num_comision_venta;
                                precioCliente.cod_sucursal_clientesucursal = item.cod_sucursal_cliente;

                                var cs_temp = db.Clientes_Sucursales.FirstOrDefault(cs => cs.cod_sucursal_clientesucursal == item.cod_sucursal_cliente);

                                precioCliente.cod_cliente_cliente = cs_temp.cod_cliente_cliente;
                                db.SaveChanges();
                            }                           
                            
                        }
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception)
                    {

                        dbTran.Rollback();
                        res.resultado = false;
                        res.mensaje = "Los datos no se han podido cargar en el sistema";
                    }
                }
            }

            return res;
        }
    }
}
