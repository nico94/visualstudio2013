﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{
    public class ImportarExcel_ParGastoMO : IImportarExcel_ParGastoMO
    {
        public ResultadoInsertarGastoMO InsertarGastoMO(string idSession)
        {
            ResultadoInsertarGastoMO res = new ResultadoInsertarGastoMO();

            long idDefinicion_Gasto = 0;
            long idGasto_ManoObra = 0;
            long idsnap_SubCentroCosto_DefinicionGasto = 0;
            long idGasto_HorasExtras = 0;

            using (var db = new GestionComercialEntities())
            {
                var MO = db.PasoGastoMO.Where(mo => mo.pk == idSession).ToList();
                    var temporada = db.Temporadas.FirstOrDefault(t => t.bit_activa_temporada == true);

                    try
                    {
                        int cc = 0;
                        bool flag = false;
                        string errorValidacion = "";

                        foreach (var validar in MO)
                        {
                            //--------------  VALIDAR
                            var ficha = db.FichasPersonales.FirstOrDefault(fp => fp.des_rut_fichapersonal == validar.rut_ficha_personal.ToString());
                            var cuartel = db.Cuarteles.FirstOrDefault(c => c.cod_cuartel_cuartel == validar.cod_cuartel);
                            var labor = db.Labor_ManoObra.FirstOrDefault(lmo => lmo.cod_labormanoobra_labormanoobra == validar.cod_labormo_labormo);
                            
                            flag = ficha == null ? flag : true ;
                            flag = cuartel == null ? false : flag ;
                            flag = labor == null ? false : flag;
                            //-------------- FIN: VALIDAR

                            if (!flag)
                            {
                                errorValidacion = "ERROR: Rut: " + validar.rut_ficha_personal + " ó Cuartel: " + validar.cod_cuartel + " ó Labor: " + validar.cod_labormo_labormo + " No Existe";
                                break;
                            }
                        }//-------------- fin: foreach

                        if (flag)
	                    {
                            foreach (var item in MO)
                            {
                                var ficha = db.FichasPersonales.FirstOrDefault(fp => fp.des_rut_fichapersonal == item.rut_ficha_personal.ToString());
                                var contrato = db.Contrato.FirstOrDefault(c => c.cod_fichapersonal_fichapersonal == ficha.cod_fichapersonal_fichapersonal);

                                var saberCuarte = (from c in db.Cuarteles
                                                   join b in db.Sub_CentroCostos on c.cod_subcentrocosto_subcentrocosto equals b.cod_subcentrocosto_subcentrocosto
                                                   join a in db.Centro_Costo on b.cod_centrocosto_centrocosto equals a.cod_centrocosto_centrocosto
                                                   join s in db.Sucursal_empresa on a.id_empresa_sucursal equals s.id_empresa_sucursal
                                                   where c.cod_cuartel_cuartel == item.cod_cuartel
                                                   orderby b.cod_centrocosto_centrocosto, c.cod_subcentrocosto_subcentrocosto
                                                   select new
                                                   {
                                                       c.cod_empresa_empresa,
                                                       c.cod_subcentrocosto_subcentrocosto,
                                                       c.cod_centrocosto_centrocosto
                                                   }).FirstOrDefault();

                                var _cantidadTemp = (from cv in db.ControlValorMO
                                                     where item.num_cantidad_gmo >= cv.num_min_controlValorMO && item.num_cantidad_gmo <= cv.num_max_controlValorMO && cv.bit_esaldia_controlvalorMO == item.bit_esaldia_gmo
                                                     select new { cv.num_valor_controlvalorMO, cv.num_bono_controlvalorMO }
                                                ).FirstOrDefault();

                                var diasemana = (from fp in db.FichasPersonales
                                                 join c in db.Contrato on fp.cod_fichapersonal_fichapersonal equals c.cod_fichapersonal_fichapersonal
                                                 join t in db.Turnos on c.cod_turno_turno equals t.cod_turno_turno
                                                 join a in db.Anexo_Contrato on c.cod_numcontrato_contrato equals a.cod_numcontrato_contrato into pp
                                                 from p in pp.DefaultIfEmpty()
                                                 where fp.des_rut_fichapersonal == ficha.des_rut_fichapersonal
                                                 select new
                                                 {
                                                     fp.cod_fichapersonal_fichapersonal,
                                                     fp.num_sueldobase_fichapersonal,
                                                     fp.num_valorhorasextra_fichapersonal,
                                                     c.bit_activo_contrato,
                                                     c.cod_numcontrato_contrato,
                                                     t.num_horalv_turno,
                                                     t.num_horasb_turno
                                                 }).FirstOrDefault();



                                /*
                                 * VERIFICAR CONSULTAS
                                 * 
                                 *  NULL
                                 */
                                bool validacion = false;

                                validacion = saberCuarte != null ? true : validacion;
                                validacion = _cantidadTemp != null ? validacion : false;
                                validacion = diasemana != null ? validacion : false;

                                if (!validacion)
                                {
                                    //ERROR
                                    res.mensaje = "Error al Guardar: Verifique que las tablas relacionadas contengan datos.";
                                    res.mensaje = res.mensaje + "<br> Cuarteles <br> Sub_CentroCostos <br> Centro_Costo <br> Sucursal_empresa <br>Verifique: bit_esaldia_controlvalorMO en: ControlValorMO";
                                    res.resultado = false;
                                    break;
                                }

                                /*
                                 * FIN : VERIFICAR CONSULTAS
                                 * 
                                 *  NULL
                                 */



                                if (cc != item.cod_cuartel)
                                {
                                    Definicion_Gasto DG = new Definicion_Gasto();

                                    cc = Convert.ToInt32(item.cod_cuartel);

                                    //####### ENCABEZADO #############
                                    DG.dat_fecha_definiciongasto = item.dat_fecha_dg;
                                    DG.des_glosa_definiciongasto = item.des_glosa;
                                    DG.cod_fichapersonal_fichapersonal = ficha.cod_fichapersonal_fichapersonal;
                                    DG.cod_temporada_temporada = temporada.cod_temporada_temporada;
                                    DG.cod_empresa_empresa = saberCuarte.cod_empresa_empresa;
                                    DG.bit_aprobada_definiciongasto = false;

                                    db.Definicion_Gasto.Add(DG);
                                    db.SaveChanges();

                                    //Obtener ultimo id creado en definicion gasto
                                    idDefinicion_Gasto = DG.cod_definiciongasto_definiciongasto;

                                    snap_SubCentroCosto_DefinicionGasto _snap = new snap_SubCentroCosto_DefinicionGasto();

                                    _snap.cod_subcentrocosto_subcentrocosto = Convert.ToInt64(saberCuarte.cod_subcentrocosto_subcentrocosto);
                                    _snap.num_porcentaje_snapsubcentrocostodefgasto = 100;
                                    _snap.cod_centrocosto_centrocosto = saberCuarte.cod_centrocosto_centrocosto;
                                    _snap.cod_definiciongasto_definiciongasto = idDefinicion_Gasto;
                                    _snap.cod_temporada_temporada = temporada.cod_temporada_temporada;

                                    db.snap_SubCentroCosto_DefinicionGasto.Add(_snap);

                                    db.SaveChanges();

                                    idsnap_SubCentroCosto_DefinicionGasto = _snap.cod_definiciongasto_definiciongasto;
                                    //####### FIN:ENCABEZADO #############

                                }


                                if (cc == item.cod_cuartel)
                                {
                                    Gasto_ManoObra GMO = new Gasto_ManoObra();

                                    //------------ DETALLE ---------------
                                    //GMO.cod_gastomanoobra_gastomanoobra = 0;
                                    GMO.bit_esaldia_gastomanoobra = item.bit_esaldia_gmo;
                                    GMO.num_horas_gastomanoobra = item.num_horas_gmo;
                                    GMO.num_cantidad_gastomanoobra = item.num_cantidad_gmo;
                                    GMO.num_bonolabor_gastomanoobra = item.num_bonolabor_gmo;
                                    GMO.num_valorlabor_gastomanoobra = _cantidadTemp.num_valor_controlvalorMO;
                                    GMO.cod_labormanoobra_labormanoobra = Convert.ToInt32(item.cod_labormo_labormo);
                                    GMO.cod_definiciongasto_definiciongasto = idDefinicion_Gasto;
                                    GMO.cod_fichapersonal_fichapersonal = ficha.cod_fichapersonal_fichapersonal;
                                    GMO.cod_temporada_temporada = temporada.cod_temporada_temporada;
                                    GMO.num_valorlabore_gastomanoobra = 0;
                                    GMO.cod_numcontrato_contrato = Convert.ToInt64(contrato.cod_numcontrato_contrato);
                                    GMO.cod_anexo_anexo = null;
                                    //DateTime dateValue = DateTime.Today;
                                    DateTime dateValue = Convert.ToDateTime(item.dat_fecha_dg);
                                    var _dia = dateValue.ToString("dddd", new CultureInfo("es-ES"));
                                    if (_dia.Equals("lunes") || _dia.Equals("martes") || _dia.Equals("miércoles") || _dia.Equals("jueves") || _dia.Equals("viernes"))
                                    {
                                        GMO.num_horas_contrato = diasemana.num_horalv_turno;
                                    }
                                    else
                                    {
                                        GMO.num_horas_contrato = diasemana.num_horasb_turno;
                                    }
                                    GMO.bit_imponible_gastomanoobra = true;

                                    db.Gasto_ManoObra.Add(GMO);
                                    db.SaveChanges();

                                    idGasto_ManoObra = GMO.cod_gastomanoobra_gastomanoobra;

                                    if (item.num_horas_gastohextras > 0)
                                    {
                                        Gasto_HorasExtras GHE = new Gasto_HorasExtras();

                                        //GHE.cod_gastohoraextra_gastohoraextra = 0;
                                        GHE.num_horas_gastohorasextras = item.num_horas_gastohextras;
                                        GHE.num_valor_gastohoraextra = diasemana.num_valorhorasextra_fichapersonal;
                                        GHE.cod_labormanoobra_labormanoobra = Convert.ToInt32(item.cod_labormo_labormo);
                                        GHE.cod_fichapersonal_fichapersonal = ficha.cod_fichapersonal_fichapersonal;
                                        GHE.cod_definiciongasto_definiciongasto = idDefinicion_Gasto;
                                        GHE.cod_temporada_temporada = temporada.cod_temporada_temporada;
                                        GHE.num_cantidad_gastohorasextras = item.num_cantidad_gastohextras;
                                        //GHE.cod_gastomanoobra_gastomanoobra = id2;


                                        db.Gasto_HorasExtras.Add(GHE);

                                        db.SaveChanges();

                                        idGasto_HorasExtras = GHE.cod_gastohoraextra_gastohoraextra;
                                    }
                                    //Guardar Detalle

                                    //---------- FIN: DETALLE ------------


                                }//------------ fin: if(cc == item.cod_cuartel)

                                res.mensaje = "Datos Guardados correctamente.";
                                res.resultado = true;
                            }//-------------- FIN: FOREACH


                            

	                    }//-------------- FIN: if FLAG
                        else
                        {
                            res.mensaje = errorValidacion;
                            res.resultado = false;
                        }
                    }//------------- fin : try
                    catch (Exception e)
                    {
                        idDefinicion_Gasto = idDefinicion_Gasto != 0 ? idDefinicion_Gasto : 0;
                        idsnap_SubCentroCosto_DefinicionGasto = idsnap_SubCentroCosto_DefinicionGasto != 0 ? idsnap_SubCentroCosto_DefinicionGasto : 0;
                        idGasto_ManoObra = idGasto_ManoObra != 0 ? idGasto_ManoObra : 0;
                        idGasto_HorasExtras = idGasto_HorasExtras != 0 ? idGasto_HorasExtras : 0;

                        var _tempDG = db.Definicion_Gasto.Where(dg => dg.cod_definiciongasto_definiciongasto == idDefinicion_Gasto).ToList();
                        var _tempSnap = db.snap_SubCentroCosto_DefinicionGasto.Where(sp => sp.cod_definiciongasto_definiciongasto == idsnap_SubCentroCosto_DefinicionGasto).ToList();
                        var _tempMO = db.Gasto_ManoObra.Where(mo => mo.cod_gastomanoobra_gastomanoobra == idGasto_ManoObra).ToList();
                        var _tempHE = db.Gasto_HorasExtras.Where(he => he.cod_gastohoraextra_gastohoraextra == idGasto_HorasExtras).ToList();


                        foreach (var item4 in _tempHE)
                        {
                            Gasto_HorasExtras DG = new Gasto_HorasExtras();
                            DG = item4;
                            db.Gasto_HorasExtras.Add(DG);
                            db.SaveChanges();
                        }
                        foreach (var item3 in _tempMO)
                        {
                            Gasto_ManoObra DG = new Gasto_ManoObra();
                            DG = item3;
                            db.Gasto_ManoObra.Remove(DG);
                            db.SaveChanges();
                        }
                        foreach (var item2 in _tempSnap)
                        {
                            snap_SubCentroCosto_DefinicionGasto DG = new snap_SubCentroCosto_DefinicionGasto();
                            DG = item2;
                            db.snap_SubCentroCosto_DefinicionGasto.Remove(DG);
                            db.SaveChanges();
                        }
                        foreach (var item1 in _tempDG)
                        {
                            Definicion_Gasto DG = new Definicion_Gasto();
                            DG = item1;
                            db.Definicion_Gasto.Remove(DG);
                            db.SaveChanges();
                        }

                        res.mensaje = "Error: Error no controlado.";
                        res.resultado = false;
                    }//--------------- fin: catch

                }

           

            return res;

        }// --------- FIN: ResultadoInsertarGastoMO
    }
}
