﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{

    public class ImportarExcel_ParInsumo : IImportarExcel_ParInsumo
    {
        //-- Inicio:InsertarMateriaPrima --
        public ResultadoIsertarMateriaPrima InsertarMateriaPrima(string idSession)
        {
            ResultadoIsertarMateriaPrima res = new ResultadoIsertarMateriaPrima();

            using (var db = new GestionComercialEntities())
            {
                DataTable dt = new DataTable();
                var MP = db.PasoIngresarMP.Where(mp => mp.pk == idSession).ToList();
                
                
                using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in MP)
                        {
                            var MPrima = db.Materias_Primas.FirstOrDefault(mp => mp.cod_producto_materiaprima == item.cod_materiaprima);
                            var CCG = db.Centro_Gasto.FirstOrDefault(mp => mp.cod_centrogasto_centrogasto == item.cod_centrogasto);
                            var CSCG = db.SubCentro_Gasto.FirstOrDefault(mp => mp.cod_subcentrogasto_subcentrogasto == item.cod_subcentrogasto);
                            var CPP = db.Pesos_Productos.FirstOrDefault(mp => mp.cod_pesoproducto_pesoproducto == item.cod_pesoproducto);
                            var CU = db.Unidades.FirstOrDefault(mp => mp.cod_unidad_unidad == item.cod_unidad);
                            var CFPT = db.Familia_Productos_Terminados.FirstOrDefault(mp => mp.cod_familiaproductosterminados_familiaproductosterminados == item.cod_familiapt);
                            var CSFPT = db.Sub_Familia_Productos_Terminados.FirstOrDefault(mp => mp.cod_subfamiliaprodterminado_subfamiliaproductoterminado == item.cod_subfamiliapt);
                            var CP = db.Cortes_Productos.FirstOrDefault(mp => mp.cod_corteproducto_corteproducto == item.cod_corteproducto);
                            var CM = db.Marca.FirstOrDefault(mp => mp.cod_marca_marca == item.cod_marca);
                            var CModelo = db.Modelo.FirstOrDefault(mp => mp.cod_modelo_modelo == item.cod_modelo);
                            var CC = db.Color_Producto.FirstOrDefault(mp => mp.cod_color_color == item.cod_color);
                            var CT = db.Talla.FirstOrDefault(mp => mp.cod_talla_talla == item.cod_talla);

                            if (MPrima != null) { res.resultado = false; res.mensaje = "Cod. #" + item.cod_materiaprima + " Materia Prima duplicado."; throw new System.Exception(); }
                            if (CCG == null) { res.resultado = false; res.mensaje = "Cod. #" + item.cod_centrogasto + " Centro Gasto no existe."; throw new System.Exception(); }
                            if (CSCG == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_subcentrogasto+" SubCentro Gasto no existe."; throw new System.Exception(); }
                            if (CPP == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_pesoproducto+" Peso Producto no existe."; throw new System.Exception(); }
                            if (CU == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_unidad+" Unidad no existe."; throw new System.Exception(); }
                            if (CFPT == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_familiapt+" Familia Producto Terminado no existe."; throw new System.Exception(); }
                            if (CSFPT == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_subfamiliapt+" SubFamilia Producto Terminado no existe."; throw new System.Exception(); }
                            if (CP == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_corteproducto+" Corte Producto no existe."; throw new System.Exception(); }
                            if (CM == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_marca+" Marca no existe."; throw new System.Exception(); }
                            if (CModelo == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_modelo+" Modelo no existe."; throw new System.Exception(); }
                            if (CC == null) { res.resultado = false; res.mensaje = "Cod. #"+item.cod_color+" Color no existe."; throw new System.Exception(); }
                            //if (CT == null) { res.resultado = false; res.mensaje = "Cod. #" + item.cod_talla + " Talla no existe."; throw new System.Exception(); }
                            //if (!MP.cod_producto_materiaprima.Equals("")) { res.resultado = false; return res; }

                            Productos_Terminados prod = new Productos_Terminados();
                            Materias_Primas materiaprima = new Materias_Primas();

                            materiaprima.cod_producto_materiaprima = item.cod_materiaprima;
                            materiaprima.num_precioponderado_materiaprima = 0;
                            materiaprima.bit_controlstock_materiasprimas = item.bit_mp_controlastock;
                            materiaprima.num_costototalcompra_materiaprima = 0;
                            materiaprima.des_cuentacontable_materiasprimas = item.des_cuentacontable;
                            materiaprima.des_centrogastos_materiasprimas = item.des_centrogasto;
                            materiaprima.des_centrocosto_materiasprimas = item.des_centrocosto;
                            materiaprima.des_nombre_materiaprima = item.des_materiaprima;
                            materiaprima.bit_estrigo_materiaprima = false;
                            materiaprima.num_ultimoprecio_materiaprima = 0;
                            materiaprima.num_mayorprecio_materiaprima = 0;
                            materiaprima.num_stock_materiaprima = 0;
                            materiaprima.bit_esharina_materiaprima = false;
                            materiaprima.num_minimoprecio_materiaprima = 0;
                            materiaprima.num_stockcritico_materiaprima = 0;
                            materiaprima.dat_fecha_materiaprima = item.dat_materiaprima;
                            materiaprima.num_stockminimo_materiaprima = 0;
                            materiaprima.num_stockmaximo_materiaprima = 0;
                            materiaprima.bit_esmateriaprimaelav_materiaprima = false;
                            materiaprima.cod_subcentrogasto_subcentrogasto = Convert.ToInt64(item.cod_subcentrogasto);
                            materiaprima.cod_centrogasto_centrogasto = Convert.ToInt64(item.cod_centrogasto);
                            materiaprima.cod_unidad_unidad = item.cod_unidad;
                            //materiaprima.cod_productogenerico_materiaprima = item.cod_productogen_mp;
                            materiaprima.cod_productogenerico_materiaprima = null;
                            materiaprima.cod_pesoproducto_pesoproducto = item.cod_pesoproducto;
                            materiaprima.bit_essubproductoharina_materiaprima = false;
                            materiaprima.num_rojovenci_materiaprima = item.num_rojovencimiento_mp;
                            materiaprima.num_amarillovenci_materiaprima = item.num_amarillovencimiento_mp;
                            materiaprima.bit_materiaprima_productoterminado = item.bit_mp_sevendera;
                            materiaprima.bit_esproductoterminado = item.bit_mp_esprodterminado;
                            materiaprima.bit_stock_loteauto_materiaprima = item.bit_mp_loteautomatico;
                            materiaprima.cod_ingactivo_ingactivo = null;
                            materiaprima.bit_fechaven = item.bit_mp_fechavencimiento;
                            materiaprima.bit_stock_esconlote = item.bit_stockconlote;

                            if (item.bit_mp_sevendera == true || item.bit_mp_esprodterminado == true)
                            {
                                prod.cod_productoterminado_productoterminado = item.cod_materiaprima;
                                prod.des_descripcion_productoterminado = item.des_materiaprima;
                                prod.cod_subfamiliaprodterminado_subfamiliaproductoterminado = Convert.ToInt64(item.cod_subfamiliapt);
                                prod.cod_prodasociado_productoterminado = null;
                                prod.cod_familiaproductosterminados_familiaproductosterminados = Convert.ToInt64(item.cod_familiapt);
                                prod.cod_pesoproducto_pesoproducto = Convert.ToInt64(item.cod_pesoproducto);
                                prod.cod_corteproducto_corteproducto = Convert.ToInt64(item.cod_corteproducto);
                                prod.num_stockminimo_productoterminado = item.num_stockminpt;
                                prod.num_stockmaximo_productoterminado = item.num_stockmaxpt;
                                prod.num_stockcritico_productoterminado = item.num_stockcriticopt;
                                prod.num_stock_productoterminado = 0;
                                prod.num_valorcosto_productoterminado = 0;
                                prod.num_valorventa_productoterminado = Convert.ToDouble(item.num_valorventapt);
                                prod.cod_marca_marca = item.cod_marca;
                                prod.bit_activo_productoterminado = true;
                                prod.dat_fechareceta_productoterminado = item.dat_materiaprima; // OJO
                                prod.bit_materiaprima_productoterminado = item.bit_mp_esprodterminado;
                                prod.bit_favorito_productoterminado = false;
                                prod.bit_muestra_productoterminado = true;
                                prod.orden = 0;
                                prod.bit_ticket = false;
                                prod.cod_productoterminado_productoterminado_2 = item.cod_pt_pt_2;
                                prod.cod_productoterminado_productoterminado_3 = item.cod_pt_pt_3;
                                prod.cod_modelo_modelo = item.cod_modelo;
                                prod.cod_talla_talla = item.cod_talla;
                                prod.cod_color_color = item.cod_color;
                                prod.bit_fechaven = item.bit_mp_fechavencimiento;

                                db.Materias_Primas.Add(materiaprima);

                                db.Productos_Terminados.Add(prod);

                                db.SaveChanges();

                                //materiaprima.cod_productogenerico_materiaprima = item.cod_productogen_mp;

                                //Materias_Primas MPexcel = new Materias_Primas();
                                //var materiaPrimaExcel = db.Materias_Primas.FirstOrDefault(mp => mp.cod_producto_materiaprima == item.cod_materiaprima);
                                //materiaPrimaExcel.cod_productogenerico_materiaprima = item.cod_productogen_mp;

                                //MPexcel.cod_productogenerico_materiaprima = materiaPrimaExcel.cod_productogenerico_materiaprima;
                                //db.SaveChanges();
                            }
                            else
                            {
                                db.Materias_Primas.Add(materiaprima);

                                db.SaveChanges();

                                //Materias_Primas MPexcel = new Materias_Primas();
                                //var materiaPrimaExcel = db.Materias_Primas.FirstOrDefault(mp => mp.cod_producto_materiaprima == item.cod_materiaprima);
                                //materiaPrimaExcel.cod_productogenerico_materiaprima = item.cod_productogen_mp;

                                //MPexcel.cod_productogenerico_materiaprima = materiaPrimaExcel.cod_productogenerico_materiaprima;
                                //db.SaveChanges();
                            }

                            //try { db.SaveChanges(); res.resultado = true; }
                            ////try { res.resultado = true; }
                            //catch (Exception e) { res.resultado = false; }
                            

                        }//end for 


                        

                        dbTran.Commit();
                        res.resultado = true;

                    }
                    catch (Exception e)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                        //res.mensaje = e.InnerException.InnerException.Message;
                        
                    }



                }// end BeginTransaction()

                
                

                return res;

            }// end GestionComercialEntities()
        }
        //-- Fin:InsertarMateriaPrima --
    }
}
