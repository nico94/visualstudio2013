﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{   
    public class Compra : ICompra
    {
        public ResultadoProrratearInvoice ProrratearInvoice(ProrratearInvoiceRequest ProrratearInvoicerequest)
        {
            ResultadoProrratearInvoice res = new ResultadoProrratearInvoice();
            long num_gasto = 0; 
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    var queryDocumentos = db.Detalle_DocumentoCompraGeneral.Where(f => f.cod_documentocomprageneral_documentocomprageneral == ProrratearInvoicerequest.cod_documentocomprageneral_documentocomprageneral
                        && f.cod_proveedor_proveedor == ProrratearInvoicerequest.cod_proveedor_proveedor
                        && f.cod_tipodocumento_tipodocumentoventa == ProrratearInvoicerequest.cod_tipodocumento_tipodocumentoventa
                        && f.cod_empresa_empresa == ProrratearInvoicerequest.cod_empresa_empresa).ToList();
                    double TotalCompra = queryDocumentos.Sum(d => d.num_valor_detalledocumentocomprageneral* d.num_cantidad_detalledocumentocomprageneral);

                    //Recorriendo Cada Productos de la Compra
                    foreach (var item in queryDocumentos)
                    {                        
                        //Obtener porcentaje por cada producto de compra
                        decimal Porc = Convert.ToDecimal( ((item.num_valor_detalledocumentocomprageneral * item.num_cantidad_detalledocumentocomprageneral)  / TotalCompra) * 100 );              
                        Porc = Porc / 100;
                        //Obtener Total de Cada Costo y sacar porcentaje
                        var queryCostos = db.V_GastosDocCompra.Where(f => f.cod_documentocomprageneral_documentocomprageneral == ProrratearInvoicerequest.cod_documentocomprageneral_documentocomprageneral
                        && f.cod_proveedor_proveedor == ProrratearInvoicerequest.cod_proveedor_proveedor
                        && f.cod_tipodocumento_tipodocumentoventa == ProrratearInvoicerequest.cod_tipodocumento_tipodocumentoventa
                        && f.cod_empresa_empresa == ProrratearInvoicerequest.cod_empresa_empresa).ToList();
                        decimal Cos = 0;
                        double SubTotalCos = 0;
                        foreach (var itemCosto in queryCostos)
                        {
                            //Sumar porcentajes  
                            Cos = Convert.ToDecimal(itemCosto.num_valor_otrosgastos) * Porc;               
                            SubTotalCos += Convert.ToDouble( Cos);
                            num_gasto = itemCosto.cod_definiciongasto_definiciongasto;
                        }   

                        //Obtener Costo Add
                       double CostoAdd= SubTotalCos / item.num_cantidad_detalledocumentocomprageneral;

                       CostoAdd = Math.Round(CostoAdd, 1);
                       
                       //Nuevo Costo
                       int NuevoCosto;
                       NuevoCosto = (int)(item.num_valor_detalledocumentocomprageneral + CostoAdd);

                       var listagastosproducto = new List<GastosProducto>();

                       

                      
                       if ((bool)db.Materias_Primas.FirstOrDefault(MP => MP.cod_producto_materiaprima == item.cod_producto_materiaprima).bit_materiaprima_productoterminado)
                       {
                           //Si es producto terminado, se obtienen todos los lotes de la factura
                           var x = (from snap in db.snap_ControlIngresoInsumo
                                    join control in db.ControlIngresoInsumoProductoTerminado on snap.cod_snapcinsumo_snapcimsumo equals control.cod_snapcinsumo_snapcimsumo
                                    where snap.cod_documentocomprageneral_documentocomprageneral == ProrratearInvoicerequest.cod_documentocomprageneral_documentocomprageneral
                                    && snap.cod_proveedor_proveedor == ProrratearInvoicerequest.cod_proveedor_proveedor
                                    && snap.cod_tipodocumento_tipodocumentoventa == ProrratearInvoicerequest.cod_tipodocumento_tipodocumentoventa
                                    && snap.cod_empresa_empresa == ProrratearInvoicerequest.cod_empresa_empresa
                                    && control.cod_productoterminado_productoterminado == item.cod_producto_materiaprima
                                    select new GastosProducto()
                                    {
                                        cod_producto = control.cod_productoterminado_productoterminado,
                                        cod_snapcinsumo_snapcimsumo = snap.cod_snapcinsumo_snapcimsumo
                                    }
                           );
                           listagastosproducto = x.ToList();
                           foreach (var itemLotes in listagastosproducto)
                           {                               
                               var xDetalleInsumoProductoTerminado = db.ControlIngresoInsumoProductoTerminado.FirstOrDefault(s => s.cod_snapcinsumo_snapcimsumo == itemLotes.cod_snapcinsumo_snapcimsumo
                                   && s.cod_productoterminado_productoterminado == itemLotes.cod_producto);
                               xDetalleInsumoProductoTerminado.num_valor = NuevoCosto;
                               db.SaveChanges();                              
                           }                         
                       }
                       else
                       {
                           //Si es materia prima
                           var x = (from snap in db.snap_ControlIngresoInsumo
                                    join control in db.ControlIngresoInsumo on snap.cod_snapcinsumo_snapcimsumo equals control.cod_snapcinsumo_snapcimsumo
                                    where snap.cod_documentocomprageneral_documentocomprageneral == ProrratearInvoicerequest.cod_documentocomprageneral_documentocomprageneral
                                    && snap.cod_proveedor_proveedor == ProrratearInvoicerequest.cod_proveedor_proveedor
                                    && snap.cod_tipodocumento_tipodocumentoventa == ProrratearInvoicerequest.cod_tipodocumento_tipodocumentoventa
                                    && snap.cod_empresa_empresa == ProrratearInvoicerequest.cod_empresa_empresa
                                    && control.cod_producto_materiaprima == item.cod_producto_materiaprima
                                    select new GastosProducto()
                                    {
                                        cod_producto = control.cod_producto_materiaprima,
                                        cod_snapcinsumo_snapcimsumo = snap.cod_snapcinsumo_snapcimsumo
                                    }
                          );
                           listagastosproducto = x.ToList();
                           foreach (var itemLotes in listagastosproducto)
                           {
                               var xDetalleInsumo = db.ControlIngresoInsumo.FirstOrDefault(s => s.cod_snapcinsumo_snapcimsumo == itemLotes.cod_snapcinsumo_snapcimsumo
                                   && s.cod_producto_materiaprima == itemLotes.cod_producto);
                               xDetalleInsumo.num_valor_controlingresoinsumo = NuevoCosto;
                               db.SaveChanges();                               
                           }                        
                       }
                    }
                    //Dejar Gasto asociado a Compra como prorrateado
                    var xSnapCompraCosto = db.Snap_DocCompragral_Gastos.FirstOrDefault(s => s.cod_definiciongasto_definiciongasto == num_gasto);
                    xSnapCompraCosto.Prorrateado = true;
                    db.SaveChanges();   
                    dbTran.Commit();
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }



        public ResultadoCrearOrdenCompraWeb CrearOrdenCompraWeb(CrearOrdenCompraWebRequest CrearOrdenCompraWebrequest)
        {
            ResultadoCrearOrdenCompraWeb res = new ResultadoCrearOrdenCompraWeb();
            double precio = 0;
            long orden = 0;
            long det_orden = 0;
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    //Obtener medio de pago desde parametros
                    var mediopago = db.Opciones.First(d => d.cod_anticipo_opciones == d.cod_anticipo_opciones);
                    //Obtener Temporada activa
                    var temporada = db.Temporadas.FirstOrDefault(ta => ta.bit_activa_temporada == true);
                    //Obtener el precio del producto terminado
                    var Producto = db.Productos_Terminados.FirstOrDefault(p => p.cod_productoterminado_productoterminado == CrearOrdenCompraWebrequest.cod_productoterminado_productoterminado);
                    if (Producto != null)
                    {
                        precio = Producto.num_valorventa_productoterminado.Value;
                        if (precio == 0)
                        {
                            res.Mensaje = "Producto sin Precio";
                            throw new System.Exception();
                        }
                    }
                    else
                    {
                        res.Mensaje = "Producto sin Precio";
                        throw new System.Exception();
                    }

                    Ordenes_Compras_Clientes _Ordenes_Compras_Clientes = new Ordenes_Compras_Clientes();
                    Detalle_OrdenCompraClientes _Detalle_OrdenCompraClientes = new Detalle_OrdenCompraClientes();

                    var Orden = db.Ordenes_Compras_Clientes.FirstOrDefault(oc => oc.cod_cliente_cliente == CrearOrdenCompraWebrequest.cod_cliente_cliente
                        && oc.bit_procesada_occliente == null
                        && oc.id_empresa_sucursal == CrearOrdenCompraWebrequest.id_empresa_sucursal
                        && oc.cod_empresa_empresa == CrearOrdenCompraWebrequest.cod_empresa_empresa);
                    if (Orden != null) // si trae una orden no procesada, entonces seguimos agregando productos al detalle de la orden
                    {
                        orden = Orden.cod_ordencompracliente_ordencompracliente;

                        var detoc_temp = db.Detalle_OrdenCompraClientes.FirstOrDefault(occ => occ.cod_ordencompracliente_ordencompracliente == orden);

                        det_orden = detoc_temp.cod_detalleocclientes_detalleocclientes;
                    }
                    else
                    {
                        //crear encabezado
                        _Ordenes_Compras_Clientes.id_empresa_sucursal = CrearOrdenCompraWebrequest.id_empresa_sucursal;
                        _Ordenes_Compras_Clientes.cod_empresa_empresa = CrearOrdenCompraWebrequest.cod_empresa_empresa;
                        _Ordenes_Compras_Clientes.id_empresa_sucursal = CrearOrdenCompraWebrequest.id_empresa_sucursal;
                        _Ordenes_Compras_Clientes.cod_mediopago_mediopago = mediopago.cod_mediopagoefectivo_opciones.Value;
                        _Ordenes_Compras_Clientes.cod_cliente_cliente = CrearOrdenCompraWebrequest.cod_cliente_cliente;
                        _Ordenes_Compras_Clientes.bit_procesada_occliente = false;
                        db.Ordenes_Compras_Clientes.Add(_Ordenes_Compras_Clientes);
                        db.SaveChanges();
                        orden = _Ordenes_Compras_Clientes.cod_ordencompracliente_ordencompracliente;

                        //En ambos casos creamos detalle
                        _Detalle_OrdenCompraClientes.cod_ordencompracliente_ordencompracliente = orden;
                        _Detalle_OrdenCompraClientes.cod_productoterminado_productoterminado = CrearOrdenCompraWebrequest.cod_productoterminado_productoterminado;
                        _Detalle_OrdenCompraClientes.num_cantidad_detalleocclientes = 1;
                        _Detalle_OrdenCompraClientes.num_precio_detalleocclientes = precio;
                        _Detalle_OrdenCompraClientes.num_descuento_detalleocclientes = 0;
                        _Detalle_OrdenCompraClientes.cod_temporada_temporada = temporada.cod_temporada_temporada;
                        _Detalle_OrdenCompraClientes.num_pordescuento_detalleocclientes = 0;
                        _Detalle_OrdenCompraClientes.num_preciosindesc_detalleocclientes = 0;
                        _Detalle_OrdenCompraClientes.id_empresa_sucursal = CrearOrdenCompraWebrequest.id_empresa_sucursal;
                        db.Detalle_OrdenCompraClientes.Add(_Detalle_OrdenCompraClientes);
                        db.SaveChanges();
                        det_orden = _Detalle_OrdenCompraClientes.cod_detalleocclientes_detalleocclientes;
                    }

                    //Llenamos snap
                    Snap_OCCliente_Foto _Snap_OCCliente_Foto = new Snap_OCCliente_Foto();

                    _Snap_OCCliente_Foto.cod_detalleocclientes_detalleocclientes = det_orden;
                    _Snap_OCCliente_Foto.des_foto_occliente = CrearOrdenCompraWebrequest.des_foto_occliente;
                    _Snap_OCCliente_Foto.foto_occliente_occliente = CrearOrdenCompraWebrequest.Foto;
                    _Snap_OCCliente_Foto.dat_fecha_foto = DateTime.Now;
                    _Snap_OCCliente_Foto.qty_subida_foto = 1;
                    _Snap_OCCliente_Foto.qty_procesada_foto = 1;
                    _Snap_OCCliente_Foto.bit_eliminada_foto = false;
                    _Snap_OCCliente_Foto.cod_ordencompracliente_ordencompracliente = orden;
                    _Snap_OCCliente_Foto.cod_productoterminado_productoterminado = CrearOrdenCompraWebrequest.cod_productoterminado_productoterminado;
                    _Snap_OCCliente_Foto.id_empresa_sucursal = CrearOrdenCompraWebrequest.id_empresa_sucursal;

                    db.Snap_OCCliente_Foto.Add(_Snap_OCCliente_Foto);


                    db.SaveChanges();
                    dbTran.Commit();
                    res.Mensaje = "Ok";
                    res.COCC = orden;
                    res.CPT = CrearOrdenCompraWebrequest.cod_productoterminado_productoterminado;
                    res.IES = CrearOrdenCompraWebrequest.id_empresa_sucursal;
                    res.CDOCC = det_orden;
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    dbTran.Rollback();
                    res.Realizado = false;
                }
                return res;
            }
        }





        public ResultadoListaOrdenCompraWeb ListaOrdenCompraWeb(ListarOrdenCompraWebRequest ListarOrdenCompraWebrequest)
        {
            ResultadoListaOrdenCompraWeb res = new ResultadoListaOrdenCompraWeb();

            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista_CDOCC = db.Snap_OCCliente_Foto.Where(oc => oc.cod_detalleocclientes_detalleocclientes == ListarOrdenCompraWebrequest.CDOCC).ToList();
                    //var lista_DOCC = db.Detalle_OrdenCompraClientes.Where(doc => doc.cod_ordencompracliente_ordencompracliente == ListarOrdenCompraWebrequest.COCC).ToList();

                    res.listaSnap = lista_CDOCC;
                    //res.listaDet = lista_DOCC;

                    res.Mensaje = "Ok";
                    res.Realizado = true;
                }
                catch (Exception)
                {
                    res.Mensaje = "Error";
                    res.Realizado = false;
                }
                return res;
            }
        }


        [WebInvoke(Method = "POST", 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "compraweb/{foto}")]
        public CompraWeb CrearOrdenCompraWebAPP(string Foto)
        {
            ResultadoCrearOrdenCompraWebAPP res = new ResultadoCrearOrdenCompraWebAPP();


            using (var db = new GestionComercialEntities())
            {

                Bitmap bmp = null;
                string directorio = "C:/PruebaImagen/Imagen.jpg";
                string nombre = "ImagenWs.jpg";

                string x = Foto.Replace("data:image/png;base64,", "");
                var bytes = Convert.FromBase64String(x);
                using (var imagefile = new FileStream(directorio, FileMode.Create))
                {
                    imagefile.Write(bytes, 0, bytes.Length);
                    imagefile.Flush();
                }

                //string x = Foto.Replace("data:image/png;base64,", "");
                //byte[] imgBytes = Convert.FromBase64String(x);
                //MemoryStream ms = new MemoryStream(imgBytes,0,imgBytes.Length);

                //ms.Write(imgBytes, 0, imgBytes.Length);
                //System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                //image.Save(directorio, System.Drawing.Imaging.ImageFormat.Jpeg);


                //var imagenBytes = Convert.FromBase64String(Foto);
                //var img = System.Text.Encoding.UTF8.GetString(imagenBytes);

  
                //MemoryStream ms = new MemoryStream(imagenBytes);

                //ms.Position = 0;

                //bmp = (Bitmap)Bitmap.FromStream(ms);
                //ms.Close();
                //ms = null;
                //imagenBytes = null;

                //bmp.Save(directorio);
                
                
                //File.WriteAllBytes(directorio,imagenBytes);


                //Image image;
                
                //using (MemoryStream ms = new MemoryStream(imagenBytes)) {
                //    image = Image.FromStream(ms);
                //}
                //image.Save(directorio);

                
                //Bitmap originalBMP = new Bitmap(Foto);

                //int thumbnailSize = 897;
                //int newWidth, newHeight;
                //if (originalBMP.Width > originalBMP.Height)
                //{
                //    newWidth = thumbnailSize;
                //    newHeight = originalBMP.Height * thumbnailSize / originalBMP.Width;
                //}
                //else
                //{
                //    newWidth = originalBMP.Width * thumbnailSize / originalBMP.Height;
                //    newHeight = thumbnailSize;
                //}
                //Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
                //Graphics oGraphics = Graphics.FromImage(newBMP);

                //oGraphics.SmoothingMode = SmoothingMode.AntiAlias; oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

                
                //originalBMP.Dispose();
                //newBMP.Dispose();
                //oGraphics.Dispose();

                //ComprimirImagen(fileNameTN, fileNameCO, 50);

                //res.Realizado = true;
            }

            return new CompraWeb() { foto = Foto};
        }

        //**********************************************
        static void ComprimirImagen(string inputFile, string ouputfile, long compression)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(inputFile);
            EncoderParameters eps = new EncoderParameters(1);

            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compression);
            string mimetype = GetMimeType(new System.IO.FileInfo(inputFile).Extension);
            ImageCodecInfo ici = GetEncoderInfo(mimetype);

            image.Save(ouputfile, ici, eps);
        }
        static string GetMimeType(string ext)
        {
            switch (ext.ToLower())
            {
                case ".bmp":
                case ".dib":
                case ".rle":
                    return "image/bmp";

                case ".jpg":
                case ".jpeg":
                case ".jpe":
                case ".fif":
                    return "image/jpeg";

                case "gif":
                    return "image/gif";
                case ".tif":
                case ".tiff":
                    return "image/tiff";
                case "png":
                    return "image/png";
                default:
                    return "image/jpeg";
            }
        }
        static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();

            ImageCodecInfo encoder = (from enc in encoders
                                      where enc.MimeType == mimeType
                                      select enc).First();
            return encoder;
        }
        //**********************************************

    }
}
