﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial
{
    public class Flete : IFlete
    {
        string Producto;
        double? TotalFlete = 0;
        long Facturado;
        long TipoFactura;
        long PagoCredito;
        string Cliente;
        long temporadaactiva;
        long Persona;
        string cliente;

        static Documentos_Ventas xCabecera = new Documentos_Ventas();
        static Detalle_DocumentosVentas xDetalle = new Detalle_DocumentosVentas();
        static Cobranzas_Ventas xCabeceraCobranza = new Cobranzas_Ventas();
        static snap_CobranzaVentas xDetalleCobranza = new snap_CobranzaVentas();
        static snap_DetalleAnticipoClientes xDetalleAnticipo = new snap_DetalleAnticipoClientes();

        //Recibe lista de fletes a facturar
        public ResultadoFacturarFlete FletesFacturar(FacturarFleteRequest facturarFleterequest)
        {
            ResultadoFacturarFlete res = new ResultadoFacturarFlete();
            Int64 cod_documentoventa_documentoventa = 0;
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    var query = db.Opciones_Flete.FirstOrDefault();
                    if (query != null)
                    {
                        Producto = query.cod_productoterminado_productoterminadoFlete;
                        Facturado = query.Estado_Flete4.cod_estadoflete;
                        PagoCredito = query.cod_condicionpago_condicionpagocredito.Value;
                    }

                    var queryop = db.Opciones.FirstOrDefault();
                    if (queryop != null)
                    {
                        TipoFactura = queryop.cod_factura_opciones.Value;
                    }

                    var querypersona = db.FichasPersonales.FirstOrDefault(f => f.cod_fichapersonal_fichapersonal == facturarFleterequest.usuario);
                    if (querypersona != null)
                    {
                        Persona = querypersona.cod_fichapersonal_fichapersonal;
                    }

                    var temp = db.Temporadas.FirstOrDefault(ta => ta.bit_activa_temporada == true);
                    if (temp != null)
                    {
                        temporadaactiva = temp.cod_temporada_temporada;
                    }

                    //Re-Ordenar por Cliente para ir generando sus Facturas
                    facturarFleterequest.listaFlete = facturarFleterequest.listaFlete.ToList().OrderBy(c => c.cod_cliente_cliente).ToList();

                    foreach (Enc_RegistroFlete item in facturarFleterequest.listaFlete)
                    {
                        long NumFlete = item.cod_registroflete;
                        //Obtener datos del flete para actualizar estado
                        Enc_RegistroFlete queryFlete = db.Enc_RegistroFlete.FirstOrDefault(a => a.cod_registroflete == NumFlete);
                        if (queryFlete != null)
                        {
                            queryFlete.cod_estadoflete = Facturado;
                            queryFlete.Facturado = true;
                            db.SaveChanges();
                        }

                        //Obtener Nombre del flete 
                        Clientes queryClie = db.Clientes.FirstOrDefault(a => a.cod_cliente_cliente == item.cod_cliente_cliente);
                        if (queryClie != null)
                        {
                            cliente = queryClie.des_nombre_cliente;
                        }

                        //nueva factura para nuevo cliente
                        if (Cliente != item.cod_cliente_cliente)
                        {
                            xCabecera = new Documentos_Ventas();
                            xDetalle = new Detalle_DocumentosVentas();

                            TotalFlete = 0;
                            //Crea Encabezado
                            xCabecera.cod_cliente_cliente = queryFlete.cod_cliente_cliente;
                            xCabecera.cod_sucursal_clientesucursal = queryFlete.cod_sucursal_clientesucursal;
                            xCabecera.cod_condicionpago_condicionpago = PagoCredito;
                            xCabecera.cod_fichapersonal_fichapersonal = Persona;
                            xCabecera.cod_empresa_empresa = facturarFleterequest.empresa;

                            var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                            xCabecera.dat_fecha_documentoventa = Convert.ToDateTime(Fecha);

                            xCabecera.id_empresa_sucursal = facturarFleterequest.sucursal;

                            long MaxDoc = 0;

                            try
                            {
                                MaxDoc = db.Documentos_Ventas.Where(u => u.cod_tipodocumento_tipodocumentoventa == TipoFactura).Max(u => u.cod_documentoventa_documentoventa);
                            }
                            catch (Exception)
                            {

                                MaxDoc = 0;
                            }
                            
                            xCabecera.num_Documentoventa_NroDoc = MaxDoc + 1;
                            xCabecera.cod_tipodocumento_tipodocumentoventa = TipoFactura; //Factura, obtener desde opciones 
                            db.Documentos_Ventas.Add(xCabecera);
                            db.SaveChanges();
                            cod_documentoventa_documentoventa = xCabecera.cod_documentoventa_documentoventa;

                            //Crea Detalle_DocumentosVentas
                            xDetalle.bit_tiene_ticketcambio = false;
                            xDetalle.cod_documentoventa_documentoventa = cod_documentoventa_documentoventa;
                            xDetalle.cod_productoterminado_productoterminado = Producto;
                            xDetalle.cod_temporada_temporada = temporadaactiva;
                            xDetalle.id_empresa_sucursal = facturarFleterequest.sucursal; ;
                            xDetalle.num_cantidad_detdocumentoventa = 1;
                            xDetalle.num_comision_detdocumentoventa = 0;
                            xDetalle.num_descuento_detdocumentoventa = 0;
                            xDetalle.num_iva_detdocumentoventa = 0;
                            xDetalle.num_pordescuento_detdocumentoventa = 0;
                            xDetalle.num_precio_detdocumentoventa = 0; // Dejar en cero y actualizar al final la suma de todos los fletes
                            xDetalle.num_preciosindesc_detdocumentoventa = 0;
                            db.Detalle_DocumentosVentas.Add(xDetalle);
                            db.SaveChanges();
                            //Lista de salida de Facturas creadas
                            var Doc = new DocumentosVenta();
                            Doc.Cliente = cliente;
                            Doc.cod_documentoventa_documentoventa = cod_documentoventa_documentoventa;
                            Doc.num_Documentoventa_NroDoc = xCabecera.num_Documentoventa_NroDoc.Value;
                            Doc.Fecha = Convert.ToDateTime(Fecha);
                            Doc.num_neto_documentoventa = (double)((xCabecera.num_neto_documentoventa != null) ? xCabecera.num_neto_documentoventa : 0);
                            res.listaDocumentos.Add(Doc);
                        }

                        //Esto es para insertar el snap_Facturación_Fletes que no aparece en el modelo por ser una relación many to many. Entity lo agrega en Enc_RegistroFlete como ICollection<Detalle_DocumentosVentas> Detalle_DocumentosVentas
                        db.Entry(xDetalle).Collection(x => x.Enc_RegistroFlete).Load();
                        xDetalle.Enc_RegistroFlete.Add(queryFlete);
                        db.SaveChanges();

                        Cliente = item.cod_cliente_cliente;
                        TotalFlete += queryFlete.Valor_flete.Value;
                        //Actualizar el total valor flete de los fletes en el detalle
                        xDetalle.num_precio_detdocumentoventa = TotalFlete;
                        db.SaveChanges();
                        //Actualizar el total valor neto de los fletes en el encabezado
                        xCabecera.num_neto_documentoventa = TotalFlete;
                        db.SaveChanges();


                    } //Fin For de Fletes                                      
                    dbTran.Commit();
                    res.realizado = true;

                    //creamos las cobranzas, se hace de esta forma para poder utilizar el siguiente método de manera independiente, debe ser al final para poder obtener el monto del detalle
                    foreach (var item in res.listaDocumentos)
                    {
                        ResultadoCrearCobranza resc = new ResultadoCrearCobranza();
                        var req1 = new CrearCobranzaRequest();
                        req1.documentoFactura = item.cod_documentoventa_documentoventa;
                        var response1 = CrearCobranza(req1);
                    }

                }
                catch (Exception)
                {
                    dbTran.Rollback();
                }
                return res;
            }
        }
        //Recibe un numero de factura y genera una cobranza
        public ResultadoCrearCobranza CrearCobranza(CrearCobranzaRequest CrearCobranzarequest)
        {
            ResultadoCrearCobranza res = new ResultadoCrearCobranza();
            Int64 cod_numerocobranzaventa_numerocobranzaventa = 0;
            double? MontoFactura = 0;
            double? MontoUtilizado = 0;
            using (var db = new GestionComercialEntities())
            using (System.Data.Entity.DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    //Obtener datos del documento de venta 
                    Documentos_Ventas queryDoc = db.Documentos_Ventas.FirstOrDefault(a => a.cod_documentoventa_documentoventa == CrearCobranzarequest.documentoFactura);
                    if (queryDoc != null)
                    {
                        xCabeceraCobranza = new Cobranzas_Ventas();
                        xDetalleCobranza = new snap_CobranzaVentas();
                        //Crea Encabezado
                        xCabeceraCobranza.id_empresa_sucursal = queryDoc.id_empresa_sucursal;
                        xCabeceraCobranza.num_cuotas_cobranzaventa = 1;
                        xCabeceraCobranza.num_numerodelacuota_cobranzaventa = 1;
                        xCabeceraCobranza.num_saldo_cobranzaventa = queryDoc.num_neto_documentoventa;
                        MontoFactura = queryDoc.num_neto_documentoventa.Value;
                        var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                        xCabeceraCobranza.dat_fechapagocuota_cobranzaventa = Convert.ToDateTime(Fecha);
                        xCabeceraCobranza.num_valorcuota_cobranzaventa = queryDoc.num_neto_documentoventa;
                        xCabeceraCobranza.cod_mediopago_mediopago = queryDoc.cod_mediopago_documentoventa;
                        xCabeceraCobranza.cod_temporada_temporada = queryDoc.cod_temporada_temporada;
                        xCabeceraCobranza.cod_cliente_cliente = queryDoc.cod_cliente_cliente;
                        db.Cobranzas_Ventas.Add(xCabeceraCobranza);
                        db.SaveChanges();
                        cod_numerocobranzaventa_numerocobranzaventa = xCabeceraCobranza.cod_numerocobranzaventa_numerocobranzaventa;
                    }
                    //Crea snap_CobranzaVentas
                    xDetalleCobranza.cod_numerocobranzaventa_numerocobranzaventa = cod_numerocobranzaventa_numerocobranzaventa;
                    xDetalleCobranza.cod_documentoventa_documentoventa = CrearCobranzarequest.documentoFactura;
                    xDetalleCobranza.id_empresa_sucursal = xCabeceraCobranza.id_empresa_sucursal;
                    db.snap_CobranzaVentas.Add(xDetalleCobranza);
                    db.SaveChanges();

                    do
                    {
                        //Obtener Saldos de anticipo
                        List<Anticipos_Clientes> queryAnti = db.Anticipos_Clientes.Where(b => b.num_saldo_anticipocliente > 0
                            && b.cod_cliente_cliente == queryDoc.cod_cliente_cliente
                            && b.id_empresa_sucursal == queryDoc.id_empresa_sucursal).ToList().OrderBy(s => s.cod_anticipocliente_anticipocliente).ToList();
                        foreach (var item in queryAnti)
                        {

                            if (item.num_saldo_anticipocliente >= MontoFactura)
                            {
                                MontoUtilizado = MontoFactura;
                                MontoFactura = 0;
                            }
                            else
                            {
                                MontoFactura -= item.num_saldo_anticipocliente;
                                MontoUtilizado = item.num_saldo_anticipocliente;
                            }

                            //Descuenta saldo del anticipo utilizado
                            item.num_saldo_anticipocliente -= MontoUtilizado;
                            db.SaveChanges();

                            //Insertar Snap
                            xDetalleAnticipo = new snap_DetalleAnticipoClientes();
                            xDetalleAnticipo.cod_anticipocliente_anticipocliente = item.cod_anticipocliente_anticipocliente;
                            xDetalleAnticipo.cod_numerocobranzaventa_numerocobranzaventa = cod_numerocobranzaventa_numerocobranzaventa;
                            xDetalleAnticipo.id_empresa_sucursal = xCabeceraCobranza.id_empresa_sucursal;
                            xDetalleAnticipo.num_montoabono_detalleanticipocliente = MontoUtilizado;
                            db.snap_DetalleAnticipoClientes.Add(xDetalleAnticipo);
                            db.SaveChanges();

                            //actualizar Cobranzas_Ventas
                            var Fecha = DateTime.Now.ToString("dd-MM-yyyy") + ' ' + DateTime.Now.ToString("hh:mm");
                            xCabeceraCobranza.dat_fechaultimobono_cobranzaventa = Convert.ToDateTime(Fecha);
                            xCabeceraCobranza.num_montoabonado_cobranzaventa += MontoUtilizado;
                            xCabeceraCobranza.num_saldo_cobranzaventa -= MontoUtilizado;
                            db.SaveChanges();

                        }
                        MontoFactura = 0;

                    } while (MontoFactura != 0);

                    dbTran.Commit();
                    res.realizado = true;

                }
                catch (Exception)
                {
                    dbTran.Rollback();
                }
                return res;
            }
        }

        public ResultadoFacturasCreadas FacturasCreadas()
        {
            ResultadoFacturasCreadas res = new ResultadoFacturasCreadas();
            using (var db = new GestionComercialEntities())
                try
                {
                    var queryop = db.Opciones.FirstOrDefault();
                    if (queryop != null)
                    {
                        TipoFactura = queryop.cod_factura_opciones.Value;
                    }

                    var Documentos = db.V_DocumentosVentaFlete.Where(o => o.cod_tipodocumento_tipodocumentoventa == TipoFactura).ToList().OrderByDescending(c => c.cod_documentoventa_documentoventa).ToList();
                    if (Documentos.Count() == 0)
                    {
                        res.listaDocumentos = null;
                    }
                    else
                    {
                        foreach (var item in Documentos)
                        {
                            var Doc = new DocumentosVenta();
                            Doc.Cliente = item.des_nombre_cliente;
                            Doc.cod_documentoventa_documentoventa = item.cod_documentoventa_documentoventa;
                            Doc.num_Documentoventa_NroDoc = (long)((item.num_Documentoventa_NroDoc != null) ? item.num_Documentoventa_NroDoc : 0);
                            Doc.Fecha = Convert.ToDateTime(item.dat_fecha_documentoventa);
                            Doc.num_neto_documentoventa = (double)((item.num_neto_documentoventa != null) ? item.num_neto_documentoventa : 0);
                            res.listaDocumentos.Add(Doc);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaDocumentos = null;
                }
            return res;
        }

        public ResultadoAsigacionNeumatico ListadoAsignacion(AsigacionNeumaticoRequest Asigacionneumaticorequest)
        {
            ResultadoAsigacionNeumatico res = new ResultadoAsigacionNeumatico();

            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.V_ListadoAsignacionNeumatico
                        .Where(c => c.cod_maquina_maquinarias == Asigacionneumaticorequest.cod_maquina_maquinarias && c.TipoMovimiento == Asigacionneumaticorequest.TipoMovimiento).OrderBy(s => s.Fecha_Asignacion).ToList();

                    if (lista.Count() == 0)
                    {
                        res.listaAsignacion.Add(new V_ListadoAsignacionNeumatico());
                        res.realizado = false;
                    }
                    else
                    {
                        foreach (var item in lista)
                        {
                            var asignacion = new V_ListadoAsignacionNeumatico();
                            asignacion.des_descripcion_centrogasto = item.des_descripcion_centrogasto;
                            asignacion.des_descripcion_subcentrogasto = item.des_descripcion_subcentrogasto;
                            asignacion.Id_neumatico = item.Id_neumatico;
                            asignacion.cod_maquina_maquinarias = item.cod_maquina_maquinarias;
                            asignacion.Fecha_Asignacion = item.Fecha_Asignacion;
                            asignacion.X = item.X;
                            asignacion.Y = item.Y;
                            asignacion.Lado = item.Lado;
                            asignacion.Lado_Destino = item.Lado_Destino;
                            asignacion.Des_EstadoProducto = item.Des_EstadoProducto;
                            asignacion.Mostrar = item.Mostrar;
                            asignacion.Mostrar_Destino = item.Mostrar_Destino;
                            asignacion.cod_motivo_retiro = item.cod_motivo_retiro;
                            asignacion.des_motivo_retiro = item.des_motivo_retiro;
                            res.listaAsignacion.Add(asignacion);
                        }
                        res.realizado = true;
                    }
                }
                catch (Exception)
                {
                    res.realizado = false;
                    res.listaAsignacion = null;
                }
            }
            return res;
        }


        public ResultadoPosicionTipoMaquina PosicionTipoMaquina(PosicionTipoMaquinaRequest PosiciontipoMaquinarequest)
        {
            ResultadoPosicionTipoMaquina res = new ResultadoPosicionTipoMaquina();


            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = new List<Detalle_posicionesNeumaticoTipoMaquina>();

                    //Lista de Posiciones ya asignadas a la maquinaria (esto es para mostrar solo las posiciones no asignadas de la maquina)
                    List<Int32> listaAsignados = db.AsignacionNeumatico_Maquina
                        .Where(l => l.Activo == true
                            && l.cod_maquina_maquinarias == PosiciontipoMaquinarequest.Maqunaria).Select(l => l.Mostrar).ToList();

                    if (PosiciontipoMaquinarequest.TipoMovimiento == "Asignacion")
                    {
                        lista = db.Detalle_posicionesNeumaticoTipoMaquina
                       .Where(c => c.cod_centrogasto_centrogasto == PosiciontipoMaquinarequest.cod_centrogasto_centrogasto
                           && c.cod_subcentrogasto_subcentrogasto == PosiciontipoMaquinarequest.cod_subcentrogasto_subcentrogasto
                           && c.cod_tipomaquinaria_tipomaquinaria == PosiciontipoMaquinarequest.cod_tipomaquinaria_tipomaquinaria
                           && c.Lado == PosiciontipoMaquinarequest.Lado
                           && !listaAsignados.Contains(c.Mostrar)).ToList().OrderBy(c => c.Mostrar).ToList();
                    }
                    else if (PosiciontipoMaquinarequest.TipoMovimiento == "Reemplazo")
                    {
                        lista = db.Detalle_posicionesNeumaticoTipoMaquina
                       .Where(c => c.cod_centrogasto_centrogasto == PosiciontipoMaquinarequest.cod_centrogasto_centrogasto
                           && c.cod_subcentrogasto_subcentrogasto == PosiciontipoMaquinarequest.cod_subcentrogasto_subcentrogasto
                           && c.cod_tipomaquinaria_tipomaquinaria == PosiciontipoMaquinarequest.cod_tipomaquinaria_tipomaquinaria
                           && c.Lado == PosiciontipoMaquinarequest.Lado
                           && listaAsignados.Contains(c.Mostrar)).ToList().OrderBy(c => c.Mostrar).ToList();
                    }

                    if (lista.Count() > 0)
                    {
                        foreach (var item in lista)
                        {
                            var posiciones = new Detalle_posicionesNeumaticoTipoMaquina();
                            posiciones.X = item.X;
                            posiciones.Y = item.Y;
                            posiciones.Lado = item.Lado;
                            posiciones.Mostrar = item.Mostrar;
                            res.listaPosicion.Add(posiciones);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaPosicion = null;
                }
            }
            return res;
        }


        public ResultadoNeumaticoLote NeumaticosLote(NeumaticoLoteRequest Neumaticoloterequest)
        {
            ResultadoNeumaticoLote res = new ResultadoNeumaticoLote();

            using (var db = new GestionComercialEntities())
            {
                try
                {

                    var bbodega = db.Bodegas.FirstOrDefault(c => c.id_empresa_sucursal == Neumaticoloterequest.id_empresa_sucursal);

                    var lista = db.V_AsignacionNeumaticos
                        .Where(v => v.Mcod_centrogasto_centrogasto == Neumaticoloterequest.cod_centrogasto_centrogasto
                            && v.Mcod_subcentrogasto_subcentrogasto == Neumaticoloterequest.cod_subcentrogasto_subcentrogasto 
                            && v.EnUso == false
                            && v.Ccod_bodega_bodega == bbodega.cod_bodega_bodega).ToList();
                  

                    if (lista.Count() == 0)
                    {
                        res.listaNeumatico = null;
                    }
                    else
                    {

                        foreach (var item in lista)
                        {
                            var neumaticos = new ListadoSnap_DetalleNeumaticos_controlIngreso();
                            neumaticos.Id_neumatico = item.Id_neumatico;
                            neumaticos.num_lote_controlingresoinsumo = item.num_lote_controlingresoinsumo;
                            neumaticos.num_posicion_x = item.num_posicion_x;
                            neumaticos.num_posicion_y = item.num_posicion_y;
                            neumaticos.num_posicion_z = item.num_posicion_z;
                            neumaticos.num_posicion_xyz = item.num_posicion_x + ";" + item.num_posicion_y + ";" + item.num_posicion_x;
                            //neumaticos.num_posicion_xyz = item.Ubicacion.Trim();
                            neumaticos.cod_bodega_bodega = item.cod_bodega_bodega;
                            var bodega = db.Bodegas.FirstOrDefault(c => c.cod_bodega_bodega == item.cod_bodega_bodega);
                            neumaticos.des_bodega_bodega = bodega.des_nombre_bodega;
                            neumaticos.materiaPrima = item.des_nombre_materiaprima;
                            res.listaNeumatico.Add(neumaticos);
                        }

                    }
                }
                catch (Exception)
                {
                    res.listaNeumatico = null;
                }
            }
            return res;
        }

        public ResultadoGuardarAsignacion GuardarAsigancionNeumatico(GuardarAsignacionRequest GuardarasignacionRequest)
        {
            ResultadoGuardarAsignacion res = new ResultadoGuardarAsignacion();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        //crear nueva maquina asigancion de neumatico
                        var maquinaria = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == GuardarasignacionRequest.AsignacionNeumatico.cod_maquina_maquinarias);
                        var Fecha = GuardarasignacionRequest.AsignacionNeumatico.Fecha_Asignacion.ToString("dd-MM-yyyy") + ' ' + GuardarasignacionRequest.AsignacionNeumatico.Fecha_Asignacion.ToString("hh:mm");
                        AsignacionNeumatico_Maquina _AsignacionNeumatico_Maquina = new AsignacionNeumatico_Maquina();
                        _AsignacionNeumatico_Maquina.Activo = GuardarasignacionRequest.AsignacionNeumatico.Activo;
                        _AsignacionNeumatico_Maquina.Id_neumatico = GuardarasignacionRequest.AsignacionNeumatico.Id_neumatico;
                        _AsignacionNeumatico_Maquina.Maquinarias = maquinaria;
                        _AsignacionNeumatico_Maquina.Fecha_Asignacion = Convert.ToDateTime(Fecha);
                        _AsignacionNeumatico_Maquina.Mostrar = GuardarasignacionRequest.AsignacionNeumatico.Mostrar;
                        _AsignacionNeumatico_Maquina.Mostrar_Destino = GuardarasignacionRequest.AsignacionNeumatico.Mostrar;
                        _AsignacionNeumatico_Maquina.Lado = GuardarasignacionRequest.AsignacionNeumatico.Lado;
                        _AsignacionNeumatico_Maquina.X = GuardarasignacionRequest.AsignacionNeumatico.X;
                        _AsignacionNeumatico_Maquina.Y = GuardarasignacionRequest.AsignacionNeumatico.Y;
                        _AsignacionNeumatico_Maquina.Id_EstadoProducto = GuardarasignacionRequest.AsignacionNeumatico.Id_EstadoProducto;
                        _AsignacionNeumatico_Maquina.TipoMovimiento = "Asignacion";
                        _AsignacionNeumatico_Maquina.Lado_Destino = GuardarasignacionRequest.AsignacionNeumatico.Lado;                        
                        db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina);
                        db.SaveChanges();

                        //actualizar neumatico utilizado del detalle de los neumaticos
                        Snap_DetalleNeumaticos_controlIngreso NeuTemp = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(pg => pg.Id_neumatico == GuardarasignacionRequest.AsignacionNeumatico.Id_neumatico);
                        NeuTemp.EnUso = true;
                        db.SaveChanges();

                        // Se descuenta el neumatico utilizado de la tabla ControlIngresoInsumo
                        var _ControlIngresoInsumo = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == GuardarasignacionRequest.codbodega
                        && c.num_lote_controlingresoinsumo == GuardarasignacionRequest.num_lote.Trim()
                        && c.num_posicion_x == GuardarasignacionRequest.X
                        && c.num_posicion_y == GuardarasignacionRequest.Y
                        && c.num_posicion_z == GuardarasignacionRequest.Z);
                        _ControlIngresoInsumo.num_cantidad_ControlIngresoInsumo = _ControlIngresoInsumo.num_cantidad_ControlIngresoInsumo - 1;
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;

                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;

        }

        public ResultadoOpcionesFlete Opciones_FleteEmpresa(OpcionesFleteRequest Opcionesfleterequest)
        {
            ResultadoOpcionesFlete res = new ResultadoOpcionesFlete();

            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var OpcionesF = db.Opciones_Flete
                        .Include("Tipo_Maquinaria")
                        .Include("Tipo_Maquinaria1")
                        .Include("Tipo_Maquinaria2")
                        .FirstOrDefault(c => c.cod_empresa_empresa == Opcionesfleterequest.cod_empresa);
                    if (OpcionesF != null)
                    {
                        var op = new Opciones_Flete();
                        op.Tipo_Maquinaria = OpcionesF.Tipo_Maquinaria;
                        op.Tipo_Maquinaria = OpcionesF.Tipo_Maquinaria1;
                        op.Tipo_Maquinaria = OpcionesF.Tipo_Maquinaria2;
                        res.OpcionesFlete = op;

                    }
                }
                catch (Exception)
                {
                    res.OpcionesFlete = null;
                }
            }
            return res;
        }


        public ResultadoEliminarAsignacion EliminarAsignacionNeumatico(EliminarAsignacionRequest Eliminarasignacionrequest)
        {
            ResultadoEliminarAsignacion res = new ResultadoEliminarAsignacion();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        ////////////////////Recupero datos a eliminar
                        Snap_DetalleNeumaticos_controlIngreso SnapTemp = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(dg => dg.Id_neumatico == Eliminarasignacionrequest.Id_neumatico);
                        SnapTemp.EnUso = false;
                        db.SaveChanges();

                        ////////////////////ControlIngresoInsumo update para devolver cantidad eliminada
                        ControlIngresoInsumo ControlTemp = db.ControlIngresoInsumo.FirstOrDefault(pg => pg.num_lote_controlingresoinsumo
                            == SnapTemp.num_lote_controlingresoinsumo
                            && pg.cod_bodega_bodega == SnapTemp.cod_bodega_bodega
                            && pg.num_posicion_x == SnapTemp.num_posicion_x
                            && pg.num_posicion_y == SnapTemp.num_posicion_y
                            && pg.num_posicion_z == SnapTemp.num_posicion_z);
                        ControlTemp.num_cantidad_ControlIngresoInsumo = ControlTemp.num_cantidad_ControlIngresoInsumo + 1;
                        db.SaveChanges();

                        AsignacionNeumatico_Maquina AsigIN = db.AsignacionNeumatico_Maquina.First(pg => pg.Id_neumatico == Eliminarasignacionrequest.Id_neumatico
                        && pg.cod_maquina_maquinarias == Eliminarasignacionrequest.cod_maquina_maquinarias);
                        db.AsignacionNeumatico_Maquina.Remove(AsigIN);
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception ex)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                    return res;
                }
            }
        }


        public ResultadoReemplazarNeumatico ReemplazarNeumatico(ReemplazarNeumaticoRequest Reemplazarneumaticorequest)
        {
            ResultadoReemplazarNeumatico res = new ResultadoReemplazarNeumatico();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {

                        //Obtener el id_neumatico con la posicion y la maquina que viene como argumento y que esté activo
                        var asignacion_origen = db.AsignacionNeumatico_Maquina.FirstOrDefault(c => c.Mostrar == Reemplazarneumaticorequest.ReemplazarNeumatico.Mostrar
                            && c.cod_maquina_maquinarias == Reemplazarneumaticorequest.ReemplazarNeumatico.cod_maquina_maquinarias && c.Activo == true);

                        //Obtener el lote origen desde Snap_DetalleNeumaticos_controlIngreso con el id_neumatico
                        var lote_origen = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(c => c.Id_neumatico == asignacion_origen.Id_neumatico);

                        //Obtener informacion del lote origen desde 
                        var infolote_origen = db.ControlIngresoInsumo.FirstOrDefault(c => c.num_lote_controlingresoinsumo == lote_origen.num_lote_controlingresoinsumo);

                        //sumar 1 numatico al lote origen
                        var _ControlIngresoInsumo_origen = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == infolote_origen.cod_bodega_bodega
                         && c.num_lote_controlingresoinsumo == infolote_origen.num_lote_controlingresoinsumo
                         && c.num_posicion_x == infolote_origen.num_posicion_x
                         && c.num_posicion_y == infolote_origen.num_posicion_y
                         && c.num_posicion_z == infolote_origen.num_posicion_z);
                        _ControlIngresoInsumo_origen.num_cantidad_ControlIngresoInsumo = _ControlIngresoInsumo_origen.num_cantidad_ControlIngresoInsumo + 1;
                        db.SaveChanges();

                        //dejar inactivo el neumatico origen en tabla AsignacionNeumatico_Maquina
                        asignacion_origen.Activo = false;
                        db.SaveChanges();

                        //actualizar neumatico utilizado del detalle de los neumaticos
                        Snap_DetalleNeumaticos_controlIngreso NeuTemp_origen = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(pg => pg.Id_neumatico == asignacion_origen.Id_neumatico);
                        NeuTemp_origen.EnUso = false;
                        db.SaveChanges();
                        //crear nueva maquina asignacion de neumatico como Reemplazo 
                        var maquinaria = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == Reemplazarneumaticorequest.ReemplazarNeumatico.cod_maquina_maquinarias);
                        var Fecha = Reemplazarneumaticorequest.ReemplazarNeumatico.Fecha_Asignacion.ToString("dd-MM-yyyy") + ' ' + Reemplazarneumaticorequest.ReemplazarNeumatico.Fecha_Asignacion.ToString("hh:mm");
                        AsignacionNeumatico_Maquina _AsignacionNeumatico_Maquina = new AsignacionNeumatico_Maquina();
                        _AsignacionNeumatico_Maquina.Activo = Reemplazarneumaticorequest.ReemplazarNeumatico.Activo;
                        _AsignacionNeumatico_Maquina.Id_neumatico = Reemplazarneumaticorequest.ReemplazarNeumatico.Id_neumatico;
                        _AsignacionNeumatico_Maquina.Maquinarias = maquinaria;
                        _AsignacionNeumatico_Maquina.Fecha_Asignacion = Convert.ToDateTime(Fecha);
                        //db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina);
                        _AsignacionNeumatico_Maquina.Mostrar = Reemplazarneumaticorequest.ReemplazarNeumatico.Mostrar;
                        _AsignacionNeumatico_Maquina.Mostrar_Destino = Reemplazarneumaticorequest.ReemplazarNeumatico.Mostrar;
                        _AsignacionNeumatico_Maquina.Lado = Reemplazarneumaticorequest.ReemplazarNeumatico.Lado;
                        _AsignacionNeumatico_Maquina.Lado_Destino = Reemplazarneumaticorequest.ReemplazarNeumatico.Lado;
                        _AsignacionNeumatico_Maquina.X = Reemplazarneumaticorequest.ReemplazarNeumatico.X;
                        _AsignacionNeumatico_Maquina.Y = Reemplazarneumaticorequest.ReemplazarNeumatico.Y;
                        _AsignacionNeumatico_Maquina.Id_EstadoProducto = Reemplazarneumaticorequest.ReemplazarNeumatico.Id_EstadoProducto;
                        _AsignacionNeumatico_Maquina.TipoMovimiento = "Reemplazo";
                        db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina);
                        db.SaveChanges();

                        //actualizar neumatico utilizado del detalle de los neumaticos
                        Snap_DetalleNeumaticos_controlIngreso NeuTemp = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(pg => pg.Id_neumatico == Reemplazarneumaticorequest.ReemplazarNeumatico.Id_neumatico);
                        NeuTemp.EnUso = true;
                        db.SaveChanges();

                        // Se descuenta el neumatico utilizado de la tabla ControlIngresoInsumo
                        var _ControlIngresoInsumo = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == Reemplazarneumaticorequest.codbodega
                        && c.num_lote_controlingresoinsumo == Reemplazarneumaticorequest.num_lote
                        && c.num_posicion_x == Reemplazarneumaticorequest.X
                        && c.num_posicion_y == Reemplazarneumaticorequest.Y
                        && c.num_posicion_z == Reemplazarneumaticorequest.Z);
                        _ControlIngresoInsumo.num_cantidad_ControlIngresoInsumo = _ControlIngresoInsumo.num_cantidad_ControlIngresoInsumo - 1;
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }





        public ResultadoRotarNeumatico RotarNeumatico(RotarNeumaticoRequest Rotarneumaticorequest)
        {
            ResultadoRotarNeumatico res = new ResultadoRotarNeumatico();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {

                        //Obtener el id_neumatico con la posicion y la maquina que viene como argumento y que esté activo
                        var asignacion_origen = db.AsignacionNeumatico_Maquina.FirstOrDefault(c => c.Mostrar_Destino == Rotarneumaticorequest.posicion_origen
                            && c.cod_maquina_maquinarias == Rotarneumaticorequest.cod_maquinaria && c.Activo == true);

                        //Obtener el id_neumatico con la posicion y la maquina que viene como argumento y que esté activo
                        var asignacion_destino = db.AsignacionNeumatico_Maquina.FirstOrDefault(c => c.Mostrar_Destino == Rotarneumaticorequest.posicion_destino
                            && c.cod_maquina_maquinarias == Rotarneumaticorequest.cod_maquinaria && c.Activo == true);

                        //dejar inactiva la asignacion del neumatico origen en tabla AsignacionNeumatico_Maquina
                        asignacion_origen.Activo = false;
                        db.SaveChanges();

                        //dejar inactivo la asignacion del neumatico destino en tabla AsignacionNeumatico_Maquina
                        asignacion_destino.Activo = false;
                        db.SaveChanges();

                        //crear nueva maquina asignacion de neumatico origen como Rotacion  
                        var maquinaria = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == asignacion_origen.cod_maquina_maquinarias);
                        var Fecha = Rotarneumaticorequest.RotarNeumatico.Fecha_Asignacion.ToString("dd-MM-yyyy") + ' ' + Rotarneumaticorequest.RotarNeumatico.Fecha_Asignacion.ToString("hh:mm");
                        AsignacionNeumatico_Maquina _AsignacionNeumatico_Maquina = new AsignacionNeumatico_Maquina();
                        _AsignacionNeumatico_Maquina.Activo = true;
                        _AsignacionNeumatico_Maquina.Id_neumatico = asignacion_origen.Id_neumatico;
                        _AsignacionNeumatico_Maquina.Maquinarias = maquinaria;
                        _AsignacionNeumatico_Maquina.Fecha_Asignacion = Convert.ToDateTime(Fecha);
                        _AsignacionNeumatico_Maquina.Mostrar = Rotarneumaticorequest.posicion_origen;
                        _AsignacionNeumatico_Maquina.Mostrar_Destino = Rotarneumaticorequest.posicion_destino;
                        _AsignacionNeumatico_Maquina.Lado = Rotarneumaticorequest.lado_origen;
                        _AsignacionNeumatico_Maquina.Lado_Destino = Rotarneumaticorequest.lado_destino;
                        _AsignacionNeumatico_Maquina.X = asignacion_destino.X;
                        _AsignacionNeumatico_Maquina.Y = asignacion_destino.Y;
                        _AsignacionNeumatico_Maquina.Id_EstadoProducto = Rotarneumaticorequest.RotarNeumatico.Id_EstadoProducto;
                        _AsignacionNeumatico_Maquina.TipoMovimiento = "Rotacion";
                        db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina);
                        db.SaveChanges();

                        //crear nueva maquina asignacion de neumatico destino como Rotacion  
                        var maquinaria1 = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == asignacion_destino.cod_maquina_maquinarias);

                        AsignacionNeumatico_Maquina _AsignacionNeumatico_Maquina1 = new AsignacionNeumatico_Maquina();
                        _AsignacionNeumatico_Maquina1.Activo = true;
                        _AsignacionNeumatico_Maquina1.Id_neumatico = asignacion_destino.Id_neumatico;
                        _AsignacionNeumatico_Maquina1.Maquinarias = maquinaria1;
                        _AsignacionNeumatico_Maquina1.Fecha_Asignacion = Convert.ToDateTime(Fecha);
                        _AsignacionNeumatico_Maquina1.Mostrar = Rotarneumaticorequest.posicion_destino;
                        _AsignacionNeumatico_Maquina1.Mostrar_Destino = Rotarneumaticorequest.posicion_origen;
                        _AsignacionNeumatico_Maquina1.Lado = Rotarneumaticorequest.lado_destino;
                        _AsignacionNeumatico_Maquina1.Lado_Destino = Rotarneumaticorequest.lado_origen;
                        _AsignacionNeumatico_Maquina1.X = asignacion_origen.X;
                        _AsignacionNeumatico_Maquina1.Y = asignacion_origen.Y;
                        _AsignacionNeumatico_Maquina1.Id_EstadoProducto = Rotarneumaticorequest.RotarNeumatico.Id_EstadoProducto;
                        _AsignacionNeumatico_Maquina1.TipoMovimiento = "Rotacion";
                        db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina1);
                        db.SaveChanges();

                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }


        public ResultadoAsigacionNeumaticoActivo ListadoAsignacionActivo(AsigacionNeumaticoActivoRequest AsigacionNeumaticoactivorequest)
        {
            ResultadoAsigacionNeumaticoActivo res = new ResultadoAsigacionNeumaticoActivo();

            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = new List<V_ListadoAsignacionNeumatico>();

                    switch (AsigacionNeumaticoactivorequest.Opcion)
                    {
                        case "Activo":
                            {
                                lista = db.V_ListadoAsignacionNeumatico
                                                .Where(c => c.cod_maquina_maquinarias == AsigacionNeumaticoactivorequest.cod_maquina_maquinarias && c.Activo == true).ToList();
                            }
                            break;
                        case "Inactivo":
                            {
                                lista = db.V_ListadoAsignacionNeumatico
                                                .Where(c => c.cod_maquina_maquinarias == AsigacionNeumaticoactivorequest.cod_maquina_maquinarias && c.Activo == false).ToList();
                            }
                            break;
                        case "Historia":
                            {
                                lista = db.V_ListadoAsignacionNeumatico
                                                .Where(c => c.cod_maquina_maquinarias == AsigacionNeumaticoactivorequest.cod_maquina_maquinarias).OrderBy(s => s.Fecha_Asignacion).ToList();
                            }
                            break;
                    }

                    if (lista.Count() == 0)
                    {
                        res.listaAsignacion.Add(new V_ListadoAsignacionNeumatico());
                    }
                    else
                    {
                        foreach (var item in lista)
                        {
                            var asigancion = new V_ListadoAsignacionNeumatico();
                            asigancion.des_descripcion_centrogasto = item.des_descripcion_centrogasto;
                            asigancion.des_descripcion_subcentrogasto = item.des_descripcion_subcentrogasto;
                            asigancion.Id_neumatico = item.Id_neumatico;
                            asigancion.cod_maquina_maquinarias = item.cod_maquina_maquinarias;
                            asigancion.Fecha_Asignacion = item.Fecha_Asignacion;
                            asigancion.X = item.X;
                            asigancion.Y = item.Y;
                            asigancion.Lado = item.Lado;
                            asigancion.Lado_Destino = item.Lado_Destino;
                            asigancion.Lado_Destino = item.Lado_Destino;
                            asigancion.Des_EstadoProducto = item.Des_EstadoProducto;
                            asigancion.Mostrar = item.Mostrar;
                            asigancion.Mostrar_Destino = item.Mostrar_Destino;
                            asigancion.Mostrar_Destino = item.Mostrar_Destino;
                            asigancion.TipoMovimiento = item.TipoMovimiento;
                            asigancion.Activo = item.Activo;
                            res.listaAsignacion.Add(asigancion);
                        }
                    }
                }
                catch (Exception)
                {
                    res.listaAsignacion = null;
                }
            }
            return res;
        }


        public ResultadoRetirarNeumatico RetirarNeumatico(RetirarNeumaticoRequest Retirarneumaticorequest)
        {
            ResultadoRetirarNeumatico res = new ResultadoRetirarNeumatico();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {

                        //Obtener el id_neumatico con la posicion y la maquina que viene como argumento y que esté activo
                        var asignacion = db.AsignacionNeumatico_Maquina.FirstOrDefault(c => c.Mostrar_Destino == Retirarneumaticorequest.RetirarNeumatico.Mostrar_Destino
                            && c.cod_maquina_maquinarias == Retirarneumaticorequest.cod_maquinaria && c.Activo == true);

                        //dejar inactiva la asignacion del neumatico origen en tabla AsignacionNeumatico_Maquina
                        asignacion.Activo = false;
                        db.SaveChanges();

                        //Si el motivo devuelve a stock
                        var devuelve = db.Motivo_RetiroNeumatico.FirstOrDefault(c => c.cod_motivo_retiro == Retirarneumaticorequest.RetirarNeumatico.cod_motivo_retiro
                            && c.Devuelve_stock == true);

                        //crear nueva maquina asignacion activa o inactiva de neumatico origen como Retiro  
                        var maquinaria = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == asignacion.cod_maquina_maquinarias);
                        var Fecha = Retirarneumaticorequest.RetirarNeumatico.Fecha_Asignacion.ToString("dd-MM-yyyy") + ' ' + Retirarneumaticorequest.RetirarNeumatico.Fecha_Asignacion.ToString("hh:mm");
                        AsignacionNeumatico_Maquina _AsignacionNeumatico_Maquina = new AsignacionNeumatico_Maquina();

                        if (devuelve != null)
                        {
                            //Obtener el lote origen desde Snap_DetalleNeumaticos_controlIngreso con el id_neumatico
                            var lote_origen = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(c => c.Id_neumatico == asignacion.Id_neumatico);

                            //Obtener informacion del lote origen desde 
                            var infolote_origen = db.ControlIngresoInsumo.FirstOrDefault(c => c.num_lote_controlingresoinsumo == lote_origen.num_lote_controlingresoinsumo);

                            //sumar 1 numatico al lote origen
                            var _ControlIngresoInsumo_origen = db.ControlIngresoInsumo.FirstOrDefault(c => c.cod_bodega_bodega == infolote_origen.cod_bodega_bodega
                             && c.num_lote_controlingresoinsumo == infolote_origen.num_lote_controlingresoinsumo
                             && c.num_posicion_x == infolote_origen.num_posicion_x
                             && c.num_posicion_y == infolote_origen.num_posicion_y
                             && c.num_posicion_z == infolote_origen.num_posicion_z);
                            _ControlIngresoInsumo_origen.num_cantidad_ControlIngresoInsumo = _ControlIngresoInsumo_origen.num_cantidad_ControlIngresoInsumo + 1;
                            db.SaveChanges();

                            //actualizar neumatico utilizado del detalle de los neumaticos
                            Snap_DetalleNeumaticos_controlIngreso NeuTemp_origen = db.Snap_DetalleNeumaticos_controlIngreso.FirstOrDefault(pg => pg.Id_neumatico == asignacion.Id_neumatico);
                            NeuTemp_origen.EnUso = false;
                            db.SaveChanges();
                            _AsignacionNeumatico_Maquina.Activo = true;
                        }
                        else
                        {
                            _AsignacionNeumatico_Maquina.Activo = false;
                        }
                        _AsignacionNeumatico_Maquina.Id_neumatico = asignacion.Id_neumatico;
                        _AsignacionNeumatico_Maquina.Maquinarias = maquinaria;
                        _AsignacionNeumatico_Maquina.Fecha_Asignacion = Convert.ToDateTime(Fecha);
                        _AsignacionNeumatico_Maquina.Mostrar = asignacion.Mostrar;
                        _AsignacionNeumatico_Maquina.Mostrar_Destino = asignacion.Mostrar_Destino;
                        _AsignacionNeumatico_Maquina.Lado = asignacion.Lado;
                        _AsignacionNeumatico_Maquina.Lado_Destino = asignacion.Lado_Destino;
                        _AsignacionNeumatico_Maquina.X = asignacion.X;
                        _AsignacionNeumatico_Maquina.Y = asignacion.Y;
                        _AsignacionNeumatico_Maquina.Id_EstadoProducto = Retirarneumaticorequest.RetirarNeumatico.Id_EstadoProducto;
                        _AsignacionNeumatico_Maquina.TipoMovimiento = "Retiro";
                        _AsignacionNeumatico_Maquina.cod_motivo_retiro = Retirarneumaticorequest.RetirarNeumatico.cod_motivo_retiro;
                        db.AsignacionNeumatico_Maquina.Add(_AsignacionNeumatico_Maquina);
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }

        public ResultadoGuardarControlMaquinaria GuardarControlMaquinaria(GuardarControlMaquinariaRequest GuardarControlmaquinariarequest)
        {
            ResultadoGuardarControlMaquinaria res = new ResultadoGuardarControlMaquinaria();

            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        //crear nueva maquina asigancion de neumatico
                        var maquinaria = db.Maquinarias.FirstOrDefault(c => c.cod_maquina_maquinarias == GuardarControlmaquinariarequest.DetalleControlMaquinaria.cod_maquina_maquinarias);
                        var FechaIngreso = GuardarControlmaquinariarequest.DetalleControlMaquinaria.Fecha_IngresoControl.ToString("dd-MM-yyyy") + ' ' + GuardarControlmaquinariarequest.DetalleControlMaquinaria.Fecha_IngresoControl.ToString("hh:mm");
                        var FechaVencimineto = GuardarControlmaquinariarequest.DetalleControlMaquinaria.Fecha_Vencimiento.ToString("dd-MM-yyyy") + ' ' + GuardarControlmaquinariarequest.DetalleControlMaquinaria.Fecha_Vencimiento.ToString("hh:mm");
                        var certificacion = db.Certificacion.FirstOrDefault(c => c.cod_certificacion == GuardarControlmaquinariarequest.DetalleControlMaquinaria.cod_certificacion);

                        Detalle_ControlMaquinaria _Detalle_ControlMaquinaria = new Detalle_ControlMaquinaria();
                        _Detalle_ControlMaquinaria.Certificacion = certificacion;
                        _Detalle_ControlMaquinaria.Maquinarias = maquinaria;
                        _Detalle_ControlMaquinaria.Fecha_IngresoControl = Convert.ToDateTime(FechaIngreso);
                        _Detalle_ControlMaquinaria.Fecha_Vencimiento = Convert.ToDateTime(FechaVencimineto);
                        _Detalle_ControlMaquinaria.Valor = GuardarControlmaquinariarequest.DetalleControlMaquinaria.Valor;
                        db.Detalle_ControlMaquinaria.Add(_Detalle_ControlMaquinaria);
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                        res.Id_DetalleCertificacion = _Detalle_ControlMaquinaria.Id_DetalleCertificacion;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;

                    }
                }
            }
            return res;
        }


        public ResultadoEliminarControlMaquinaria EliminarControlMaquinaria(EliminarControlMaquinariaRequest EliminarControlmaquinariarequest)
        {
            ResultadoEliminarControlMaquinaria res = new ResultadoEliminarControlMaquinaria();
            using (var db = new GestionComercialEntities())
            {
                long og = 0;
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var Detalle = db.Detalle_ControlMaquinaria.FirstOrDefault(dg => dg.Id_DetalleCertificacion == EliminarControlmaquinariarequest.Id_DetalleCertificacion);
                        var lista = Detalle.Definicion_Gasto.ToList();
                        foreach (var item in lista)
                        {
                            og = item.cod_definiciongasto_definiciongasto;

                            var ogastos = db.Gastos_Otros.First(pg => pg.cod_definiciongasto_definiciongasto == og);
                            db.Gastos_Otros.Remove(ogastos);
                            db.SaveChanges();
                            Detalle.Definicion_Gasto.Remove(item);                          
                            db.SaveChanges();
                            var snapdef = db.snap_SubCentroCosto_DefinicionGasto.First(pg => pg.cod_definiciongasto_definiciongasto == og);
                            db.snap_SubCentroCosto_DefinicionGasto.Remove(snapdef);
                            db.SaveChanges();
                            var def = db.Definicion_Gasto.First(pg => pg.cod_definiciongasto_definiciongasto == og);
                            db.Definicion_Gasto.Remove(def);
                            db.SaveChanges();
                            db.Detalle_ControlMaquinaria.Remove(Detalle);
                            db.SaveChanges();
                        };                                             

                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception ex)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                    return res;
                }
            }
        }

        public ResultadoEstadoProductos ListaEstadoProductos()
        {
            ResultadoEstadoProductos res = new ResultadoEstadoProductos();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var lista = db.Estado_Productos.ToList();                  
                        foreach (var item in lista)
                        {
                            var estados = new Estado_Productos();
                            estados.Id_EstadoProducto = item.Id_EstadoProducto;
                            estados.Des_EstadoProducto = item.Des_EstadoProducto;
                            res.lista.Add(estados);
                        }                    
                }
                catch (Exception)
                {                    
                    res.lista = null;
                }
            }
            return res;
        }


        public ResultadoEstadoProductoPorCodigo EstadoProductoPorCodigo(EstadoProductoPorCodigoRequest EstadoProductoPorcodigorequest)
        {
            ResultadoEstadoProductoPorCodigo res = new ResultadoEstadoProductoPorCodigo();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var estado = db.Estado_Productos.FirstOrDefault(x=> x.Id_EstadoProducto==EstadoProductoPorcodigorequest.Id_EstadoProducto);
                    if (estado != null)
                    {
                        res.Estado.Id_EstadoProducto = estado.Id_EstadoProducto;
                        res.Estado.Des_EstadoProducto = estado.Des_EstadoProducto;
                    }
                }
                catch (Exception)
                {
                    res.Estado = null;
                }
            }
            return res;
        }

        public ResultadoSubCentroGastoPorCodigo SubCentroGastoPorCodigo(SubCentroGastoPorCodigoRequest SubCentroGastoPorcodigorequest)
        {
            ResultadoSubCentroGastoPorCodigo res = new ResultadoSubCentroGastoPorCodigo();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var subcentro = db.SubCentro_Gasto.FirstOrDefault(x => x.cod_subcentrogasto_subcentrogasto == SubCentroGastoPorcodigorequest.cod_subcentrogasto_subcentrogasto);
                    if (subcentro != null)
                    {                  
                        res.SubCentro.cod_subcentrogasto_subcentrogasto = subcentro.cod_subcentrogasto_subcentrogasto;
                        res.SubCentro.des_descripcion_subcentrogasto = subcentro.des_descripcion_subcentrogasto;                
                    }
                }
                catch (Exception)
                {
                    res.SubCentro = null;
                }
            }
            return res;
        }

        public ResultadoCentroGastoPorCodigo CentroGastoPorCodigo(CentroGastoPorCodigoRequest CentroGastoPorodigorequest)
        {
            ResultadoCentroGastoPorCodigo res = new ResultadoCentroGastoPorCodigo();
            using (var db = new GestionComercialEntities())
            {
                try
                {
                    var centro = db.Centro_Gasto.FirstOrDefault(x => x.cod_centrogasto_centrogasto == CentroGastoPorodigorequest.cod_centrogasto_centrogasto);
                    if (centro != null)
                    {
                        res.Centro.cod_centrogasto_centrogasto = centro.cod_centrogasto_centrogasto;
                        res.Centro.des_descripcion_centrogasto = centro.des_descripcion_centrogasto;
                    }
                }
                catch (Exception)
                {
                    res.Centro = null;
                }
            }
            return res;
        }

        public ResultadoGuardarAlarmaNeumatico GuardarAlarmaNeumatico(GuardarAlarmaNeumaticoRequest GuardarAlarmaneumaticorequest)
        {
            ResultadoGuardarAlarmaNeumatico res = new ResultadoGuardarAlarmaNeumatico();
            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {      
                        var estado = db.Estado_Productos.FirstOrDefault(c => c.Id_EstadoProducto == GuardarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Estado_Productos.Id_EstadoProducto);                   
                        var subcentro = db.SubCentro_Gasto.FirstOrDefault(c => c.cod_subcentrogasto_subcentrogasto == GuardarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.SubCentro_Gasto.cod_subcentrogasto_subcentrogasto);                 
                        DB_SemaforoEstadoNeumatico _DB_SemaforoEstadoNeumatico = new DB_SemaforoEstadoNeumatico();
                        _DB_SemaforoEstadoNeumatico.Estado_Productos = estado;
                        _DB_SemaforoEstadoNeumatico.SubCentro_Gasto = subcentro;
                        _DB_SemaforoEstadoNeumatico.Km_Recorrido_Cambiar = GuardarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Km_Recorrido_Cambiar;
                        _DB_SemaforoEstadoNeumatico.KM_Recorrido_Max = GuardarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.KM_Recorrido_Max;
                        _DB_SemaforoEstadoNeumatico.Km_Recorrido_ProximoCambio = GuardarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Km_Recorrido_ProximoCambio;                  
                        db.DB_SemaforoEstadoNeumatico.Add(_DB_SemaforoEstadoNeumatico);
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;                       
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }       

        public ResultadoEditarAlarmaNeumatico EditarAlarmaNeumatico(EditarAlarmaNeumaticoRequest EditarAlarmaneumaticorequest)
        {
            ResultadoEditarAlarmaNeumatico res = new ResultadoEditarAlarmaNeumatico();
            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        DB_SemaforoEstadoNeumatico DB_SemaforoEstadoNeumaticoTemp = db.DB_SemaforoEstadoNeumatico.FirstOrDefault(pg => pg.Id == EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Id);
                        var estado = db.Estado_Productos.FirstOrDefault(c => c.Id_EstadoProducto == EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Estado_Productos.Id_EstadoProducto);
                        var subcentro = db.SubCentro_Gasto.FirstOrDefault(c => c.cod_subcentrogasto_subcentrogasto == EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.SubCentro_Gasto.cod_subcentrogasto_subcentrogasto);
                        DB_SemaforoEstadoNeumaticoTemp.Estado_Productos = estado;
                        DB_SemaforoEstadoNeumaticoTemp.SubCentro_Gasto = subcentro;
                        DB_SemaforoEstadoNeumaticoTemp.Km_Recorrido_Cambiar = EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Km_Recorrido_Cambiar;
                        DB_SemaforoEstadoNeumaticoTemp.KM_Recorrido_Max = EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.KM_Recorrido_Max;
                        DB_SemaforoEstadoNeumaticoTemp.Km_Recorrido_ProximoCambio = EditarAlarmaneumaticorequest.DetalleDB_SemaforoEstadoNeumatico.Km_Recorrido_ProximoCambio;               
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                }
            }
            return res;
        }


        public ResultadoObtieneRegistroAlarmaNeumatico ObtieneRegistroAlarmaNeumatico(ObtieneRegistroAlarmaNeumaticoRequest ObtieneRegistroAlarmaneumaticorequest)
        {
            ResultadoObtieneRegistroAlarmaNeumatico res = new ResultadoObtieneRegistroAlarmaNeumatico();
            using (var db = new GestionComercialEntities())
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var _RegistroAlarmaNeumatico = db.DB_SemaforoEstadoNeumatico
                            .FirstOrDefault(a => a.Id == ObtieneRegistroAlarmaneumaticorequest.Id);

                        var estado = db.Estado_Productos.FirstOrDefault(c => c.Id_EstadoProducto == _RegistroAlarmaNeumatico.Estado_Productos.Id_EstadoProducto);
                        var subcentro = db.SubCentro_Gasto.FirstOrDefault(c => c.cod_subcentrogasto_subcentrogasto == _RegistroAlarmaNeumatico.SubCentro_Gasto.cod_subcentrogasto_subcentrogasto);
                        var centro = db.Centro_Gasto.FirstOrDefault(c => c.cod_centrogasto_centrogasto == _RegistroAlarmaNeumatico.SubCentro_Gasto.Centro_Gasto.cod_centrogasto_centrogasto); 

                        Estado_Productos estadon = new Estado_Productos();
                        estadon.Id_EstadoProducto = estado.Id_EstadoProducto;
                        estadon.Des_EstadoProducto = estado.Des_EstadoProducto;

                        SubCentro_Gasto subcentron = new SubCentro_Gasto();
                        subcentron.cod_centrogasto_centrogasto = subcentro.cod_centrogasto_centrogasto;
                        subcentron.cod_subcentrogasto_subcentrogasto = subcentro.cod_subcentrogasto_subcentrogasto;
                        subcentron.des_descripcion_subcentrogasto = subcentro.des_descripcion_subcentrogasto;

                        Centro_Gasto centron = new Centro_Gasto();
                        centron.cod_centrogasto_centrogasto = centro.cod_centrogasto_centrogasto;
                        centron.des_descripcion_centrogasto = centro.des_descripcion_centrogasto; 

                        res.RegistroAlarmaNeumatico.Estado_Productos = estadon;
                        res.RegistroAlarmaNeumatico.SubCentro_Gasto = subcentron;
                        res.RegistroAlarmaNeumatico.SubCentro_Gasto.Centro_Gasto = centron;

                        res.RegistroAlarmaNeumatico.Km_Recorrido_Cambiar = _RegistroAlarmaNeumatico.Km_Recorrido_Cambiar;
                        res.RegistroAlarmaNeumatico.KM_Recorrido_Max = _RegistroAlarmaNeumatico.KM_Recorrido_Max;
                        res.RegistroAlarmaNeumatico.Km_Recorrido_ProximoCambio = _RegistroAlarmaNeumatico.Km_Recorrido_ProximoCambio;
                        res.RegistroAlarmaNeumatico.Id = _RegistroAlarmaNeumatico.Id;
                    }
                    catch (Exception)
                    {
                        dbTran.Rollback();                        
                    }
                }
            }
            return res;
        }


        public ResultadoEliminarRegistroAlarmaNeumatico EliminarRegistroAlarmaNeumatico(EliminarRegistroAlarmaNeumaticoRequest EliminarRegistroAlarmaneumaticorequest)
        {
            ResultadoEliminarRegistroAlarmaNeumatico res = new ResultadoEliminarRegistroAlarmaNeumatico();
            using (var db = new GestionComercialEntities())
            {
           
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {                 
                        var RegistroSemaforo = db.DB_SemaforoEstadoNeumatico.FirstOrDefault(dg => dg.Id == EliminarRegistroAlarmaneumaticorequest.Id);
                        db.DB_SemaforoEstadoNeumatico.Remove(RegistroSemaforo);                      
                        db.SaveChanges();
                        dbTran.Commit();
                        res.resultado = true;
                    }
                    catch (Exception ex)
                    {
                        dbTran.Rollback();
                        res.resultado = false;
                    }
                    return res;
                }
            }
        }
        
        public ResultadoExisteRegistroAlarmaNeumatico ExisteRegistroAlarmaNeumatico(ExisteRegistroAlarmaNeumaticoRequest ExisteRegistroAlarmaneumaticorequest)
        {
            ResultadoExisteRegistroAlarmaNeumatico res = new ResultadoExisteRegistroAlarmaNeumatico();
            using (var db = new GestionComercialEntities())
            {               
                    try
                    {
                 
                        bool RegistroSemaforo = db.DB_SemaforoEstadoNeumatico.Any(dg => dg.Id_EstadoProducto == ExisteRegistroAlarmaneumaticorequest.Id_EstadoProducto
                            && dg.cod_centrogasto_centrogasto == ExisteRegistroAlarmaneumaticorequest.cod_centrogasto_centrogasto
                            && dg.cod_subcentrogasto_subcentrogasto == ExisteRegistroAlarmaneumaticorequest.cod_subcentrogasto_subcentrogasto);

                        if (RegistroSemaforo)
                        {
                            res.resultado = true;
                        }
                    }
                    catch (Exception ex)
                    {                        
                        res.resultado = false;
                    }
                    return res;                
            }
        }
    }
}