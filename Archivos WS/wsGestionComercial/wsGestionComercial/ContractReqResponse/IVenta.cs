﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{ 
    [ServiceContract]
    public interface IVenta
    {
        [OperationContract]
        ResultadoUpdateComisionProductoTerminado UpdateComisionProductoTerminado(UpdateComisionProductoTerminadoRequest UpdateComisionProductoterminadorequest);
    }

    public class UpdateComisionProductoTerminadoRequest
    {
        public long cod_familiaproductosterminados_familiaproductosterminados;
        public long cod_subfamiliaprodterminado_subfamiliaproductoterminado;
        public string cod_productoterminado_productoterminado;
        public double num_comision; 
    }
    public class ResultadoUpdateComisionProductoTerminado
    {
        public string mensaje;
        public bool actualizado;
    }

}
