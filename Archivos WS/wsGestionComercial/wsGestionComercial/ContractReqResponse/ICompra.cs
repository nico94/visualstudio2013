﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICompra" in both code and config file together.
    [ServiceContract]
    public interface ICompra
    {
        [OperationContract]
        ResultadoProrratearInvoice ProrratearInvoice(ProrratearInvoiceRequest ProrratearInvoicerequest);

        [OperationContract]
        ResultadoCrearOrdenCompraWeb CrearOrdenCompraWeb(CrearOrdenCompraWebRequest CrearOrdenCompraWebrequest);

        [OperationContract]
        ResultadoListaOrdenCompraWeb ListaOrdenCompraWeb(ListarOrdenCompraWebRequest ListarOrdenCompraWebrequest);

        [OperationContract]
        CompraWeb CrearOrdenCompraWebAPP(string Foto);

    }
    public class ResultadoCrearOrdenCompraWebAPP
    {
        //public bool Realizado;
        //public string Mensaje;
        //public long COCC;
        //public string CPT;
        //public long IES;
        //public long CDOCC;
        public string Foto;
    }



    public class ProrratearInvoiceRequest
    {
        public string cod_documentocomprageneral_documentocomprageneral;
        public string cod_proveedor_proveedor;
        public long cod_tipodocumento_tipodocumentoventa;       
        public string cod_empresa_empresa;
    }

    public class ResultadoProrratearInvoice
    {
        public bool Realizado;
    }

    public class CrearOrdenCompraWebRequest
    {     
        public long id_empresa_sucursal;
        public string cod_empresa_empresa;       
        public string cod_cliente_cliente;
        public string cod_productoterminado_productoterminado;
        public string des_foto_occliente;
        public byte[] Foto;         
    }

    public class ResultadoCrearOrdenCompraWeb
    {
        public bool Realizado;
        public string Mensaje;
        public long COCC;
        public string CPT;
        public long IES;
        public long CDOCC;
    }

    public class ResultadoListaSubFamiliaFotos
    {
        public List<Sub_Familia_Productos_Terminados> lista;
        public ResultadoListaSubFamiliaFotos()
        {
            lista = new List<Sub_Familia_Productos_Terminados>();
        }
    }
    public class ResultadoListaOrdenCompraWeb
    {
        public List<Snap_OCCliente_Foto> listaSnap = new List<Snap_OCCliente_Foto>();
        //public List<Detalle_OrdenCompraClientes> listaDet = new List<Detalle_OrdenCompraClientes>();
        public bool Realizado;
        public string Mensaje;
    }
    public class ListarOrdenCompraWebRequest
    {
        public long COCC;
        public string CPT;
        public long IES;
        public long CDOCC;
    }

}
