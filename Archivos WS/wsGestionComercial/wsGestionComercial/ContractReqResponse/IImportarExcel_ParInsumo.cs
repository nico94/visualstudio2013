﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Model;

namespace wsGestionComercial.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IImportarExcel_ParInsumo" in both code and config file together.
    [ServiceContract]
    public interface IImportarExcel_ParInsumo
    {
        [OperationContract]
        ResultadoIsertarMateriaPrima InsertarMateriaPrima(string idSession);
    }

    //Clase Importar Materia Prima
    public class InsertaMateriaPrimaRequest
    {
        public List<Materias_Primas> listaMP;
    }
    public class ResultadoIsertarMateriaPrima
    {
        public bool resultado;
        public string mensaje;
    }
}
