﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace wsGestionComercial.ContractReqResponse
{
   
    [ServiceContract]
    public interface IIndicadores
    {
        [OperationContract]
        ResultadoIndicador Indicador(IndicadorRequest Indicadorrequest);
    }

    public class IndicadorRequest
    {
        public string Tipo;
        public DateTime Fecha; 
    }

    public class ResultadoIndicador
    {
        public double Valor;
    }
}
