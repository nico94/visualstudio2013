﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace wsGestionComercial.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IImportarExcel_ParGastoMO" in both code and config file together.
    [ServiceContract]
    public interface IImportarExcel_ParGastoMO
    {
        [OperationContract]
        ResultadoInsertarGastoMO InsertarGastoMO(string idSession);
    }

    public class ResultadoInsertarGastoMO
    {
        public bool resultado;
        public string mensaje;
    }
}
