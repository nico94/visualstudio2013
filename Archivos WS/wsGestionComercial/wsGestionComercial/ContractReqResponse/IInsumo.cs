﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial
{
 
    [ServiceContract]
    public interface IInsumo
    {
        [OperationContract]
        ResultadoListaInsumoFichas ListadoInsumoFichas(ListaInsumoFichasRequest ListaInsumoFichasrequest);
        [OperationContract]
        ResultadoListaCondicionesProductos ListadoCondicionesProductos();
        [OperationContract]
        GuardarGastoInsumo guardarGastoInsumo(GuardarGastoInsumoRequest guardarGastoInsumoRequest);
        [OperationContract]
        ListarDetalleInsumoFichaLote listarDetalleInsumoFichaLote(ListarDetalleInsumoFichaLoteRequest listarDetalleInsumoFichaLoteRequest);
        [OperationContract]
        ListarDetalleInsumoFichaLote2 listarDetalleInsumoFichaLote2(ListarDetalleInsumoFichaLoteRequest listarDetalleInsumoFichaLoteRequest);
        [OperationContract]
        GuardarDetalleInsumoFichaLote guardarDetalleInsumoFichaLote(GuardarDetalleInsumoFichaLoteRequest guardarDetalleInsumoFichaLoteRequest);
        [OperationContract]
        ObtenerCodigoBodegaTemporal obtenerCodigoBodegaTemporal();
        [OperationContract]
        ListarDetalleInsumoFichaLoteDevolucion listarDetalleInsumoFichaLoteDevolucion(ListarDetalleInsumoFichaLoteDevolucionRequest listaDetalleInsumoFichaLoteDevolucionRequest);
        [OperationContract]
        GuardarDetalleInsumoFichaLoteDevolucion guardarDetalleInsumoFichaLoteDevolucion(GuardarDetalleInsumoFichaLoteDevolucionRequest guardarDetalleInsumoFichaLoteDevolucionRequest);
        [OperationContract]
        ListarMateriasPrimas listarMateriasPrimas();
        [OperationContract]
        ListarControlIngresoInsumo listarControlIngresoInsumo(ListarControlIngresoInsumoRequest listarControlIngresoInsumoRequest);
        [OperationContract]
        ResultadoProductoTerminado ListaProductoTerminado();
        [OperationContract]
        ResultadoFamiliaProductoTerminado ListaFamiliaProductoTerminado();
        [OperationContract]
        ResultadoSubFamiliaProductoTerminadoPorFamilia ListaSubFamiliaProductoTerminadoPorFamilia(SubFamiliaProductoTerminadoPorFamiliaRequest SubFamiliaProductoTerminadoPorfamiliarequest);
        [OperationContract]
        ResultadoProductoTerminadoPorSubFamilia ListaProductoTerminadoPorSubFamilia(ProductoTerminadoPorSubFamiliaRequest ProductoTerminadoPorSubfamiliarequest);
    }

    public class ProductoTerminadoPorSubFamiliaRequest
    {
        public long cod_familiaproductosterminados_familiaproductosterminados;
        public long cod_subfamiliaprodterminado_subfamiliaproductoterminado;
    }
    public class ResultadoProductoTerminadoPorSubFamilia
    {
        public List<Productos_Terminados> lista;
        public ResultadoProductoTerminadoPorSubFamilia()
        {
            lista = new List<Productos_Terminados>();
        }
    }
    public class SubFamiliaProductoTerminadoPorFamiliaRequest
    {
        public long cod_familiaproductosterminados_familiaproductosterminados;
    }
    public class ResultadoSubFamiliaProductoTerminadoPorFamilia
    {
        public List<Sub_Familia_Productos_Terminados> lista;
        public ResultadoSubFamiliaProductoTerminadoPorFamilia()
        {
            lista = new List<Sub_Familia_Productos_Terminados>();
        }
    }

    public class ResultadoFamiliaProductoTerminado
    {
        public List<Familia_Productos_Terminados> lista;
        public ResultadoFamiliaProductoTerminado()
        {
            lista = new List<Familia_Productos_Terminados>();
        }
    }
    public class ResultadoProductoTerminado
    {
        public List<Productos_Terminados> lista;
        public ResultadoProductoTerminado()
        {
            lista = new List<Productos_Terminados>();
        }
    }

    public class ListarControlIngresoInsumo
    {
        public List<ControlIngresoInsumoCustom> listaControlIngresoInsumo;

        public ListarControlIngresoInsumo()
        {
            listaControlIngresoInsumo = new List<ControlIngresoInsumoCustom>();
        }
    }

    public class ListarControlIngresoInsumoRequest
    {
        public string cod_producto_materiaprima;
    }

    public class ListarMateriasPrimas
    {
        public List<ListaMateriasPrimas> materiasPrimas;

        public ListarMateriasPrimas()
        {
            materiasPrimas = new List<ListaMateriasPrimas>();
        }
    }

    public class ListaInsumoFichasRequest
    {
        public long codFicha;
    }

    public class ResultadoListaInsumoFichas
    {
        public string Mensaje;
        public List<ListaInsumos> listaInsumos;

        public ResultadoListaInsumoFichas()
        {
            listaInsumos = new List<ListaInsumos>();
        }

    }

    public class ResultadoListaCondicionesProductos
    {
        public List<ListaCondcionesProductos> listaCondicioneProductos;

        public ResultadoListaCondicionesProductos()
        {
            listaCondicioneProductos = new List<ListaCondcionesProductos>();
        }
    }

    public class GuardarGastoInsumo
    {
        public bool resultado;

    }

    public class GuardarGastoInsumoRequest
    {
        public Insumo_Ficha_Personal insumoFichaPersona;

        public GuardarGastoInsumoRequest()
        {
            insumoFichaPersona = new Insumo_Ficha_Personal();
        }
    }

    public class ListarDetalleInsumoFichaLote
    {
        public List<ListaDetalleInsumosFichaLote> listaDetalleInsumosFichaLote;

        public ListarDetalleInsumoFichaLote()
        {
            listaDetalleInsumosFichaLote = new List<ListaDetalleInsumosFichaLote>();
        }
    }

    public class ListarDetalleInsumoFichaLote2
    {
        public List<ListaDetalleInsumosFichaLote> listaDetalleInsumosFichaLote;

        public ListarDetalleInsumoFichaLote2()
        {
            listaDetalleInsumosFichaLote = new List<ListaDetalleInsumosFichaLote>();
        }
    }

    public class ListarDetalleInsumoFichaLoteRequest
    {
        public string cod_producto_materiaprima;
        public long cod_fichapersonal_fichapersonal;
        public long cod_bodega_bodega;
        public DateTime Fecha_Entrega;
        public DateTime Fecha_Consumo;
        public decimal num_lote_controlingresoinsumo;
        public int num_posicion_x;
        public int num_posicion_y;
        public int num_posicion_z;
    }

    public class GuardarDetalleInsumoFichaLote
    {
        public string mensaje;
        public bool resultado;
    }

    public class GuardarDetalleInsumoFichaLoteRequest
    {
        public DetalleInsumoFichaLoteCustom detalleInsumoFichaLote;

        public GuardarDetalleInsumoFichaLoteRequest()
        {
            detalleInsumoFichaLote = new DetalleInsumoFichaLoteCustom();
        }
    }

    public class ObtenerCodigoBodegaTemporal
    {
        public long codigoBodegaTemporal;
    }

    public class ListarDetalleInsumoFichaLoteDevolucion
    {
        public List<ListaDetalleInsumoFichaLoteDevolucion> listaDetalleInsumoFichaLoteDevolucion;

        public ListarDetalleInsumoFichaLoteDevolucion()
        {
            listaDetalleInsumoFichaLoteDevolucion = new List<ListaDetalleInsumoFichaLoteDevolucion>();
        }
    }

    public class ListarDetalleInsumoFichaLoteDevolucionRequest
    {
        public double cod_fichapersonal_fichapersonal;
    }

    public class GuardarDetalleInsumoFichaLoteDevolucion
    {
        public string mensaje;
        public bool resultado;
    }

    public class GuardarDetalleInsumoFichaLoteDevolucionRequest
    {

        public Detalle_InsumoFichaLote_Historia devolucion;

        public GuardarDetalleInsumoFichaLoteDevolucionRequest()
        {
            devolucion = new Detalle_InsumoFichaLote_Historia();
        }
    }

}
