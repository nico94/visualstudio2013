﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using wsGestionComercial.Clases;
using wsGestionComercial.Model;

namespace wsGestionComercial
{
   
    [ServiceContract]
    public interface IFlete
    {

        [OperationContract]
        ResultadoFacturarFlete FletesFacturar(FacturarFleteRequest facturarFleterequest);
        [OperationContract]
        ResultadoFacturasCreadas FacturasCreadas();
        [OperationContract]
        ResultadoCrearCobranza CrearCobranza(CrearCobranzaRequest CrearCobranzarequest);
        [OperationContract]
        ResultadoAsigacionNeumatico ListadoAsignacion(AsigacionNeumaticoRequest Asigacionneumaticorequest);
        [OperationContract]
        ResultadoPosicionTipoMaquina PosicionTipoMaquina(PosicionTipoMaquinaRequest PosiciontipoMaquinarequest);
        [OperationContract]
        ResultadoNeumaticoLote NeumaticosLote(NeumaticoLoteRequest Neumaticoloterequest);
        [OperationContract]
        ResultadoGuardarAsignacion GuardarAsigancionNeumatico(GuardarAsignacionRequest GuardarasignacionRequest);
        [OperationContract]
        ResultadoOpcionesFlete Opciones_FleteEmpresa(OpcionesFleteRequest Opcionesfleterequest);
        [OperationContract]
        ResultadoEliminarAsignacion EliminarAsignacionNeumatico(EliminarAsignacionRequest Eliminarasignacionrequest);
        [OperationContract]
        ResultadoReemplazarNeumatico ReemplazarNeumatico(ReemplazarNeumaticoRequest Reemplazarneumaticorequest);
        [OperationContract]
        ResultadoRotarNeumatico RotarNeumatico(RotarNeumaticoRequest Rotarneumaticorequest);
        [OperationContract]
        ResultadoAsigacionNeumaticoActivo ListadoAsignacionActivo(AsigacionNeumaticoActivoRequest AsigacionNeumaticoactivorequest);
        [OperationContract]
        ResultadoRetirarNeumatico RetirarNeumatico(RetirarNeumaticoRequest Retirarneumaticorequest);
        [OperationContract]
        ResultadoGuardarControlMaquinaria GuardarControlMaquinaria(GuardarControlMaquinariaRequest GuardarControlmaquinariarequest);
        [OperationContract]
        ResultadoEliminarControlMaquinaria EliminarControlMaquinaria(EliminarControlMaquinariaRequest EliminarControlmaquinariarequest);
        [OperationContract]
        ResultadoEstadoProductos ListaEstadoProductos();
        [OperationContract]
        ResultadoEstadoProductoPorCodigo EstadoProductoPorCodigo(EstadoProductoPorCodigoRequest EstadoProductoPorcodigorequest);
        [OperationContract]
        ResultadoSubCentroGastoPorCodigo SubCentroGastoPorCodigo(SubCentroGastoPorCodigoRequest SubCentroGastoPorcodigorequest);
        [OperationContract]
        ResultadoGuardarAlarmaNeumatico GuardarAlarmaNeumatico(GuardarAlarmaNeumaticoRequest GuardarAlarmaneumaticorequest);
        [OperationContract]
        ResultadoCentroGastoPorCodigo CentroGastoPorCodigo(CentroGastoPorCodigoRequest CentroGastoPorodigorequest);
        [OperationContract]
        ResultadoEditarAlarmaNeumatico EditarAlarmaNeumatico(EditarAlarmaNeumaticoRequest EditarAlarmaneumaticorequest);
        [OperationContract]
        ResultadoObtieneRegistroAlarmaNeumatico ObtieneRegistroAlarmaNeumatico(ObtieneRegistroAlarmaNeumaticoRequest ObtieneRegistroAlarmaneumaticorequest);
        [OperationContract]
        ResultadoEliminarRegistroAlarmaNeumatico EliminarRegistroAlarmaNeumatico(EliminarRegistroAlarmaNeumaticoRequest EliminarRegistroAlarmaneumaticorequest);
        [OperationContract]
        ResultadoExisteRegistroAlarmaNeumatico ExisteRegistroAlarmaNeumatico(ExisteRegistroAlarmaNeumaticoRequest ExisteRegistroAlarmaneumaticorequest);
    }

    public class ExisteRegistroAlarmaNeumaticoRequest
    {
        public long Id_EstadoProducto;
        public long cod_centrogasto_centrogasto;
        public long cod_subcentrogasto_subcentrogasto;
    }

    public class ResultadoExisteRegistroAlarmaNeumatico
    {
        public bool resultado;
    }

    public class EliminarRegistroAlarmaNeumaticoRequest
    {
        public long Id;
    }

    public class ResultadoEliminarRegistroAlarmaNeumatico
    {
        public bool resultado;

    }
    
    public class ObtieneRegistroAlarmaNeumaticoRequest
    {
        public long Id;
    }
    public class ResultadoObtieneRegistroAlarmaNeumatico
    {
        public DB_SemaforoEstadoNeumatico RegistroAlarmaNeumatico;
        public ResultadoObtieneRegistroAlarmaNeumatico()
        {
            RegistroAlarmaNeumatico = new DB_SemaforoEstadoNeumatico();
        }
    }
    
    public class EditarAlarmaNeumaticoRequest
    {
        public DB_SemaforoEstadoNeumatico DetalleDB_SemaforoEstadoNeumatico;

        public EditarAlarmaNeumaticoRequest()
        {
            DetalleDB_SemaforoEstadoNeumatico = new DB_SemaforoEstadoNeumatico();
        }
    }

    public class ResultadoEditarAlarmaNeumatico
    {
        public bool resultado;
    }

    public class GuardarAlarmaNeumaticoRequest
    {
        public DB_SemaforoEstadoNeumatico DetalleDB_SemaforoEstadoNeumatico;

        public GuardarAlarmaNeumaticoRequest()
        {
            DetalleDB_SemaforoEstadoNeumatico = new DB_SemaforoEstadoNeumatico();
        }
    }

    public class ResultadoGuardarAlarmaNeumatico
    {
        public bool resultado;      
    }

    public class CentroGastoPorCodigoRequest
    {
        public long cod_centrogasto_centrogasto;
    }

    public class ResultadoCentroGastoPorCodigo
    {
        public Centro_Gasto Centro;
        public ResultadoCentroGastoPorCodigo()
        {
            Centro = new Centro_Gasto();
        }
    }

    public class SubCentroGastoPorCodigoRequest
    {
        public long cod_subcentrogasto_subcentrogasto;
    }

    public class ResultadoSubCentroGastoPorCodigo
    {
        public SubCentro_Gasto SubCentro;
        public ResultadoSubCentroGastoPorCodigo()
        {
            SubCentro = new SubCentro_Gasto();
        }
    }

    public class EstadoProductoPorCodigoRequest
    {
        public long Id_EstadoProducto;
    }

    public class ResultadoEstadoProductoPorCodigo
    {
        public Estado_Productos Estado;
        public ResultadoEstadoProductoPorCodigo()
        {
            Estado = new Estado_Productos();
        }
    }

    public class ResultadoEstadoProductos
    {
        public List<Estado_Productos> lista;
        public ResultadoEstadoProductos()
        {
            lista = new List<Estado_Productos>();
        }
    }

    public class EliminarControlMaquinariaRequest
    {
        public long Id_DetalleCertificacion;
    }

    public class ResultadoEliminarControlMaquinaria
    {
        public bool resultado;

    }
    public class GuardarControlMaquinariaRequest
    {
        public Detalle_ControlMaquinaria DetalleControlMaquinaria;

        public GuardarControlMaquinariaRequest()
        {
            DetalleControlMaquinaria = new Detalle_ControlMaquinaria();
        }
    }

    public class ResultadoGuardarControlMaquinaria
    {
        public bool resultado;
        public long Id_DetalleCertificacion;
    }

    public class RetirarNeumaticoRequest
    {
        public long cod_maquinaria;
        public AsignacionNeumatico_Maquina RetirarNeumatico;
        public RetirarNeumaticoRequest()
        {
            RetirarNeumatico = new AsignacionNeumatico_Maquina();
        }
    }
    public class ResultadoRetirarNeumatico
    {
        public bool resultado;
    }

    public class AsigacionNeumaticoActivoRequest
    {
        public long cod_maquina_maquinarias;
        public string Opcion;       
    }
    public class ResultadoAsigacionNeumaticoActivo
    {
        public bool realizado;
        public string Mensaje;

        public List<V_ListadoAsignacionNeumatico> listaAsignacion;
        public ResultadoAsigacionNeumaticoActivo()
        {
            listaAsignacion = new List<V_ListadoAsignacionNeumatico>();
        }
    }

    public class RotarNeumaticoRequest
    {
        public long cod_maquinaria;
        public int posicion_origen;
        public int posicion_destino;
        public string lado_origen;
        public string lado_destino;

        public AsignacionNeumatico_Maquina RotarNeumatico;

         public RotarNeumaticoRequest()
        {
            RotarNeumatico = new AsignacionNeumatico_Maquina();
        }       
    }

    public class ResultadoRotarNeumatico
    {
        public bool resultado;

    }

    public class ReemplazarNeumaticoRequest
    {
        public string num_lote;
        public int codbodega;
        public Int32 X;
        public Int32 Y;
        public Int32 Z;

        public AsignacionNeumatico_Maquina ReemplazarNeumatico;

        public ReemplazarNeumaticoRequest()
        {
            ReemplazarNeumatico = new AsignacionNeumatico_Maquina();
        }
    }

    public class ResultadoReemplazarNeumatico
    {
        public bool resultado;

    }

    public class OpcionesFleteRequest
    {
        public string cod_empresa;
    }

    public class ResultadoOpcionesFlete
    {
        public bool realizado;
        public string Mensaje;

        public Opciones_Flete OpcionesFlete;
        public ResultadoOpcionesFlete()
        {
            OpcionesFlete = new Opciones_Flete();
        }
    }

    public class NeumaticoLoteRequest
    {
        public long cod_subcentrogasto_subcentrogasto;
        public long cod_centrogasto_centrogasto;
        public long id_empresa_sucursal;
    }

    public class ResultadoNeumaticoLote
    {
        public bool realizado;
        public string Mensaje;

        public List<ListadoSnap_DetalleNeumaticos_controlIngreso> listaNeumatico;
        public ResultadoNeumaticoLote()
        {
            listaNeumatico = new List<ListadoSnap_DetalleNeumaticos_controlIngreso>();
        }
    }

    public class PosicionTipoMaquinaRequest
    {
        public long cod_tipomaquinaria_tipomaquinaria;
        public long cod_subcentrogasto_subcentrogasto;
        public long cod_centrogasto_centrogasto;
        public string Lado;
        public long Maqunaria;
        public string TipoMovimiento;
    }

    public class ResultadoPosicionTipoMaquina
    {
        public bool realizado;
        public string Mensaje;

        public List<Detalle_posicionesNeumaticoTipoMaquina> listaPosicion;
        public ResultadoPosicionTipoMaquina()
        {
            listaPosicion = new List<Detalle_posicionesNeumaticoTipoMaquina>();
        }
    }
     public class AsigacionNeumaticoRequest
    {
        public long cod_maquina_maquinarias;
        public string TipoMovimiento;
    }
    public class ResultadoAsigacionNeumatico
    {
        public bool realizado;
        public string Mensaje;

        public List<V_ListadoAsignacionNeumatico> listaAsignacion;
        public ResultadoAsigacionNeumatico()
        {
            listaAsignacion = new List<V_ListadoAsignacionNeumatico>();
        }
    }
    public class FacturarFleteRequest
    {
        public long usuario;
        public string empresa;
        public long sucursal;

        public List<Enc_RegistroFlete> listaFlete;
        public FacturarFleteRequest()
        {
            listaFlete = new List<Enc_RegistroFlete>();
        }
    }
    public class ResultadoFacturarFlete
    {
        public bool realizado;
        public string Mensaje;

        public List<DocumentosVenta> listaDocumentos;
        public ResultadoFacturarFlete()
        {
            listaDocumentos = new List<DocumentosVenta>();
        }
    }
    public class ResultadoFacturasCreadas
    {
        public List<DocumentosVenta> listaDocumentos;
        public ResultadoFacturasCreadas()
        {
            listaDocumentos = new List<DocumentosVenta>();
        }
    }
    public class CrearCobranzaRequest
    {
        public long documentoFactura;             
    }
    public class ResultadoCrearCobranza
    {
        public bool realizado;
        public string Mensaje;               
    }    

    public class GuardarAsignacionRequest
    {
        public string num_lote;
        public int codbodega;
        public Int32 X;
        public Int32 Y;
        public Int32 Z;

        public AsignacionNeumatico_Maquina AsignacionNeumatico;

        public GuardarAsignacionRequest()
        {
            AsignacionNeumatico = new AsignacionNeumatico_Maquina();
        }
    }

    public class ResultadoGuardarAsignacion
    {
        public bool resultado;

    }

    public class EliminarAsignacionRequest
    {
        public long Id_neumatico;
        public long cod_maquina_maquinarias;        
    }

    public class ResultadoEliminarAsignacion
    {
        public bool resultado;

    }

}
