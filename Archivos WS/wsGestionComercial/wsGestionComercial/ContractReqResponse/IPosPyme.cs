﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace wsGestionComercial
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPosPyme" in both code and config file together.
    [ServiceContract]
    public interface IPosPyme
    {
        [OperationContract]
        //bool Agregar_Compra(ResultadoPos);
        ResultadoPos Agregar_Compra2(PosRequest posrequest);
        //ResultadoPos Agregar_Compra2(PosRequest posrequest, List<ListDetallePosPymeRequest> listadetallepospyme);
        [OperationContract]
        ResultadoPos Agregar_DetalleProducto(ListDetallePosPymeRequest listdetallepospymerequest);
        [OperationContract]
        string Editar_Compra(int value);
        [OperationContract]
        string Eliminar_Compra(int value);
        [OperationContract]
        string Listar_Compra(int value);
    }


    public class PosRequest
    {
        public int cod_tipodocumento_tipodocumentoventa;
        public int cod_doccompra_POSPyme;
        public DateTime dat_fechafactura_documentocomprageneral;
        public string cod_proveedor_proveedor;
        public int cod_sucursal_sucursalproveedor;
        public int cod_ordencompra_ordencompra;
        public string cod_empresa_empresa;
        public int id_empresa_sucursal;
        public int cod_bodega_bodega; 
        public int cod_condicionpago_condicionpago;
        public DateTime dat_fechacontable_documentocomprageneral;
        public int bit_exentoiva_documentocomprageneral;
        public int cod_condiciontransporte_condiciontransporte;
        public string des_glosa_documentocomprageneral;
    }

    public class ResultadoPos
    {
        public bool estado;
        public string Mensaje;
    }


    public class ListDetallePosPymeRequest
    {
        public float num_valor_detalledocumentocomprageneral { get; set; }
        public decimal id_linea { get; set; }
        public float num_cantidad_detalledocumentocomprageneral { get; set; }
        public float num_descuento_detalledocumentocomprageneral { get; set; }
        public decimal cod_doccompra_POSPyme { get; set; }
        public string cod_proveedor_proveedor { get; set; }
        public int cod_temporada_temporada { get; set; }
        public int cod_tipodocumento_tipodocumentoventa { get; set; }
        public string cod_empresa_empresa { get; set; }
        public string cod_productoterminado_productoterminado { get; set; }
    }

}
