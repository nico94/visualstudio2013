﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using WSCosecha.Response;

namespace WSCosecha
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        // TODO: Add your service operations here
        //[OperationContract]
        //[WebInvoke(Method="GET",
        //    ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "json/{user},{pass}")]
        //LoginUser Select(string user,string pass);

        [OperationContract]
        LoginUser Select(string user, string pass);
        [OperationContract]
        Foto fotoAPP(string user);

        [OperationContract]
        SelectHome ViewSelectHome();
        [OperationContract]
        allUser allUser();
        
        [OperationContract]
        string DespachoPallet(string cod_despacho, string cod_predio, string cod_cuartel, string cod_responsable, string gps_latitud,
            string gps_longitud, string dat_fecha, string estado);
        
        /*
        [OperationContract]
        [WebGet(UriTemplate="/GetData/{value}")]
        string DespachoPallet(string value);
        */

        [OperationContract]
        string DetailDespachoPallet(string cod_despacho_detalle, string cod_despacho, string num_cantidad_bandeja, string num_peso_bandeja, string estado, 
            string cod_tarjeta_id);

        [OperationContract]
        ResultadoImpresion ImprimirZPL(ImprimirZPLRequest imprimirzplrequest);

    }
    public class ImprimirZPLRequest
    {
        public string zpl;
        public string ip;
        public int puerto;
    }

    public class ResultadoImpresion
    {
        public bool realizado;
        public string mensaje;
    }

}
