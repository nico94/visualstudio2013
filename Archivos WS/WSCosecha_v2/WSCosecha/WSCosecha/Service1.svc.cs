﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using WSCosecha.Model;
using WSCosecha.Response;
using System.Web;
using System.Web.Services;

using WSCosecha.Model;
using System.Net.Sockets;
using System.IO;

namespace WSCosecha
{

    
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    //[WebService(Namespace="http://WSCosecha.com/")]
    public class Service1 : IService1
    {


        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "json/{user},{pass}")]
        public LoginUser Select(string user,string pass)
        {
            using(var db = new GestionComercialEntities())
            {
                var ficha = db.FichasPersonales.FirstOrDefault(f => f.des_usuario == user && f.des_paswor_usuario == pass);
                if( ficha != null)
                {
                    return new LoginUser() { 
                        codigo = Convert.ToInt32(ficha.cod_fichapersonal_fichapersonal),
                        nombre = ficha.des_nombre_fichapersonal, 
                        rut = ficha.des_rut_fichapersonal,
                        estado = true
                    };

                }
                return new LoginUser() { estado = false };
            }
        }


        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "foto/{user}")]
        public Foto fotoAPP(string user)
        {
            using (var db = new GestionComercialEntities())
            {
                return new Foto()
                {
                    estado = true
                };
            }
        }


        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "selecthome/")]
        public SelectHome ViewSelectHome()
        {
            using(var db = new GestionComercialEntities())
            {
                List<listaTemporada> listaTemporada = new List<listaTemporada>();
                List<listaPredio> listaPredio = new List<listaPredio>();
                List<listaCuartel> listaCuartel = new List<listaCuartel>();

                var temporada = db.Temporadas.ToList();
                var predio = db.Predio.ToList();
                var cuartel = db.Cuarteles.ToList();

                foreach (var item in temporada)
                {
                    listaTemporada.Add(new listaTemporada()
                    { 
                        cod_temporada = Convert.ToInt32(item.cod_temporada_temporada), 
                        temporada = item.des_temporada_temporada
                    });
                }
                foreach (var item in predio)
                {
                    listaPredio.Add(new listaPredio()
                    {
                        cod_predio = Convert.ToInt32(item.cod_predio_predio),
                        predio = item.des_nombrepredio_predio
                    });
                }
                foreach (var item in cuartel)
                {
                    listaCuartel.Add(new listaCuartel()
                    {
                        cod_cuartel = Convert.ToInt32(item.cod_cuartel_cuartel),
                        cuartel = item.des_nombre_cuartel
                    });
                }
                return new SelectHome() { listaTemporada = listaTemporada, listaPredio = listaPredio, listaCuartel = listaCuartel, estado = true };
            }
        }
        [WebInvoke(Method="POST",
            ResponseFormat=WebMessageFormat.Json,
            UriTemplate="allUser/")]
        public allUser allUser()
        {
            using(var db = new GestionComercialEntities())
            {
                List<Response.FichasPersonales> listaFicha = new List<Response.FichasPersonales>();

                var fichas = db.FichasPersonales.ToList();
                foreach (var item in fichas)
                {
                    listaFicha.Add(new Response.FichasPersonales()
                    {
                        codUsuario = Convert.ToString(item.cod_fichapersonal_fichapersonal),
                        RutUsuario = item.des_rut_fichapersonal,
                        NombreUsuario = item.des_nombre_fichapersonal,
                        ApePUsuario = item.des_appaterno_fichapersonal,
                        ApeMUsuario = item.des_apmaterno_fichapersonal
                    });
                }
                return new allUser() { listaFicha = listaFicha };
            }
        }


        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "dp/{cod_despacho},{cod_predio},{cod_cuartel},{cod_responsable},{gps_latitud},{gps_longitud},{dat_fecha},{estado}")]
        public string DespachoPallet(string cod_despacho, string cod_predio, string cod_cuartel, string cod_responsable, string gps_latitud,
            string gps_longitud, string dat_fecha, string estado)            
        {
            //return "Dato: " + user;
            
            using (var db = new GestionComercialEntities())
            {
                int cod_temp = Convert.ToInt32(cod_despacho);
                int predio_temp = Convert.ToInt32(cod_predio);
                int cuartel_temp = Convert.ToInt32(cod_cuartel);
                int codresp_temp = Convert.ToInt32(cod_responsable);
                string gpslat_temp = gps_latitud;
                string gpslong_temp = gps_longitud;
                DateTime fecha_temp = Convert.ToDateTime(dat_fecha);
                string estado_temp = estado;

                DespachoPallet dep = new Model.DespachoPallet();
                RecepcionBandeja reb = new Model.RecepcionBandeja();
                
                //cod_recepcion
                //cod_lectura_despacho
                //cod_predio
                //cod_cuartel
                //num_cantidad_bandeja
                //dat_fecha
                //num_pallet
                //num_peso_pallet
                //num_peso_promedio
                //cod_responsable
                //bit_estado

                reb.cod_predio = predio_temp;
                reb.cod_cuartel = cuartel_temp;
                reb.dat_fecha = fecha_temp;
                var ultimopallet = db.RecepcionBandeja.OrderByDescending(df => df.num_pallet).First();
                reb.num_pallet = (Convert.ToInt32(ultimopallet.num_pallet) + 1);
                reb.cod_responsable = codresp_temp;
                reb.bit_estado = false;

                db.RecepcionBandeja.Add(reb);

                var ultimoregistro = db.RecepcionBandeja.OrderByDescending(df => df.cod_recepcion).First();
                var res2 = (Convert.ToInt32(ultimoregistro.cod_recepcion) + 1);
                dep.cod_despacho = (Convert.ToInt32(ultimoregistro.cod_recepcion)+1);
                //dep.cod_despacho = cod_temp;
                dep.cod_predio = predio_temp;

                dep.Cuarteles = db.Cuarteles.FirstOrDefault(c => c.cod_cuartel_cuartel == cuartel_temp);
                dep.FichasPersonales = db.FichasPersonales.FirstOrDefault(f => f.cod_fichapersonal_fichapersonal == codresp_temp);

                dep.gps_latitud = gpslat_temp;
                dep.gps_longitud = gpslong_temp;
                dep.dat_fecha = fecha_temp;
                dep.estado = estado_temp;

                db.DespachoPallet.Add(dep);

                try
                {
                    db.SaveChanges();
                    return Convert.ToString(res2);
                }
                catch (Exception ex)
                {
                    return "false";
                }      
          
            }
        }

        [WebInvoke(Method = "POST",
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ddp/{cod_despacho_detalle},{cod_despacho},{num_cantidad_bandeja},{num_peso_bandeja},{estado},{cod_tarjeta_id}")]
        public string DetailDespachoPallet(string cod_despacho_detalle, string cod_despacho, string num_cantidad_bandeja, string num_peso_bandeja, 
            string estado, string cod_tarjeta_id)
        {
            using(var db = new GestionComercialEntities()){

                int cod_temp = Convert.ToInt32(cod_despacho);

                //var enc = db.DespachoPallet.FirstOrDefault(d => d.cod_despacho == cod_temp);
                //var ultimoregistro = db.RecepcionBandeja.OrderByDescending(df => df.cod_recepcion).First();

                db.Detalle_DespachoPallet.Add(new Detalle_DespachoPallet()
                {
                    //cod_despacho_detalle = Convert.ToInt32(cod_despacho_detalle),
                    cod_despacho = cod_temp,
                    num_cantidad_bandeja = Convert.ToInt32(num_cantidad_bandeja),
                    num_peso_bandeja = Convert.ToInt32(num_peso_bandeja),
                    estado = estado,
                    cod_tarjeta_id = cod_tarjeta_id
                });
                //Obtener qty bandejas pallet
                //var qtyBandeja = db.Detalle_DespachoPallet.Where(dp => dp.);

                try
                {
                    db.SaveChanges();
                    return "true";
                }
                catch (Exception ex)
                {
                    return "false";
                }   
            }
        }



        //****************************//
        public string JSONData(string id,string dat)
        {
            return "Json: " + id + " Dat: " + dat;
        }






        public ResultadoImpresion ImprimirZPL(ImprimirZPLRequest imprimirzplrequest)
        {
            var Ip = imprimirzplrequest.ip;
            var Puerto = imprimirzplrequest.puerto;
            var res = new ResultadoImpresion();
            TcpClient Zebraclient = new TcpClient();
            try
            {

                Zebraclient.SendTimeout = 2500;
                Zebraclient.ReceiveTimeout = 2500;
                Zebraclient.Connect(Ip, Puerto);

                if (Zebraclient.Connected == true)
                {

                    NetworkStream mynetworkstream;
                    StreamReader mystreamreader;
                    StreamWriter mystreamwriter;
                    mynetworkstream = Zebraclient.GetStream();
                    mystreamreader = new StreamReader(mynetworkstream);
                    mystreamwriter = new StreamWriter(mynetworkstream);


                    mystreamwriter.WriteLine(imprimirzplrequest.zpl);
                    mystreamwriter.Flush();


                    Zebraclient.Close();
                    res.realizado = true;
                    res.mensaje = "Impresion realizada con exito";
                }

            }
            catch
            {
                res.realizado = false;
                res.mensaje = "No se pudo realizar la impresion";
            }
            return res;
        }



    }

}
