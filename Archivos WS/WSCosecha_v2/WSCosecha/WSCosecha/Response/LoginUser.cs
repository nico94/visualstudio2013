﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WSCosecha.Response
{
    public class LoginUser
    {
        public int codigo { get; set; }
        public string rut { get; set; }
        public string nombre { get; set; }

        public bool estado { get; set; }
        public string mensaje { get; set; }

    }
}